package com.wodrob.app;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.wodrob.app.adapter.AddItemViewPagerAdapter;
import com.wodrob.app.views.SlidingTabLayout;


public class AddCanvasItem extends AppCompatActivity {

    AddItemViewPagerAdapter adapter;
    ViewPager pager;
    SlidingTabLayout tabs;
    Toolbar toolbar;
    int Numboftabs =3;
    public static boolean isfilterclicked=false;
    private TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_closet_item);


        init();

        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.divider);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
        
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        if(getIntent().getExtras().getString("type").equalsIgnoreCase("item")){

            CharSequence itemTitles[]={"Catalog","My Items","Suggested"};
            adapter =  new AddItemViewPagerAdapter(getSupportFragmentManager(),itemTitles,Numboftabs,"item");


        }else if(getIntent().getExtras().getString("type").equalsIgnoreCase("outfit")){

            CharSequence itemTitles[]={"Outfit","Lookbook","Suggested"};
            adapter =  new AddItemViewPagerAdapter(getSupportFragmentManager(),itemTitles,Numboftabs,"outfit");

        }



        tv_title = (TextView) findViewById(R.id.tv_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_title.setTypeface(face);
        pager = (ViewPager) findViewById(R.id.pager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_canvas_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_filter) {

            isfilterclicked = true;
            Intent intent = new Intent(AddCanvasItem.this,FilterActivity.class);
            startActivityForResult(intent, 5);

            return true;
        }else if (id == android.R.id.home){

            onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("image_path","");
        setResult(3, intent);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 5)
        {
            adapter.notifyDataSetChanged();
        }
    }
}
