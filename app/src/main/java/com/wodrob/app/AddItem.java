package com.wodrob.app;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.adapter.AddItemPagerAdapter;
import com.wodrob.app.views.DatePickerFragment;
import com.wodrob.app.views.NonSwipeableViewPager;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

public class AddItem extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolbar;

    NonSwipeableViewPager vp_add_item;
    AddItemPagerAdapter addItemPagerAdapter;
    String type = "category";
    TextView tv_title,tv_activity_title;
    EditText ed_new_tag;
    String date = "";
    DbHelper db;
    ImageView im_edit_image,im_remove_image,im_add_image;
    TextView[] indicator_array;
    boolean is_image_added = false;


    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_GALLERY = 2;

    public static String selected_category_name = "";
    public static String selected_brand_id="";
    public static ArrayList<String> selected_category_list;
    public static ArrayList<String> selected_sub_category_list;
    public static ArrayList<String> selected_style_list;
    public static ArrayList<String> selected_brand_list;
    public static ArrayList<String> selected_color_list;
    public static ArrayList<String> selected_size_list;
    public static ArrayList<String> selected_store_list;
    public static ArrayList<String> selected_purchase_date_list;
    public static ArrayList<String> selected_price_list;
    public static ArrayList<String> selected_washing_instrcutions;
    public static ArrayList<String> selected_additional_tags;

    public static ArrayList<String> pixel_array_list;

    LinearLayout selected_item_display_layout,indicator_layout;
    private Button btn_setdate;

    int RESULT_LOAD_IMAGE = 4;

    public static ImageView im_item;
    ImageView im_forward,im_backward;
    String out_additional_tag,out_care_tip,out_category,out_color,out_date,out_price,out_size,out_style,out_category_id,out_brand_id,out_brand_name,out_store_id,out_store_name;
    private String search_string;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        init();
        vp_add_item.setPagingEnabled(false);
        AddIndicators();

        btn_setdate.setOnClickListener(this);
        im_edit_image.setOnClickListener(this);
        im_add_image.setOnClickListener(this);
        im_remove_image.setOnClickListener(this);

        im_forward.setOnClickListener(this);
        im_backward.setOnClickListener(this);

          ed_new_tag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if (!ed_new_tag.getText().toString().equals("")) {
                        update_Search(type, ed_new_tag.getText().toString());
                        addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, "", type);
                        vp_add_item.setAdapter(addItemPagerAdapter);
                        ed_new_tag.setText("");
                        if (type.equalsIgnoreCase("price")) {

                            vp_add_item.setCurrentItem(6);

                        }
                    }
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(ed_new_tag.getWindowToken(), 0);
                }
                return handled;
            }
        });


        /*search*/

          ed_new_tag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() != 0) {

                    search_string = s.toString();
                    RefreshView(type,search_string);

                } else {

                    RefreshView(type,"");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        vp_add_item.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0) {

                    type = "Category";
                    tv_title.setText("CATEGORY");
                    DisplaySelectedItem("Category");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(0);

                } else if(position == 1){

                    type = "SubCategory";
                    tv_title.setText("SUBCATEGORY");
                    DisplaySelectedItem("SubCategory");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(1);

                }else if(position == 2){

                    type = "Style";
                    tv_title.setText("STYLE");
                    DisplaySelectedItem("Style");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(2);

                }else if(position == 3){

                    type = "Brand";
                    tv_title.setText("BRAND");
                    DisplaySelectedItem("Brand");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(3);

                }else if(position == 4){

                    type = "Color";
                    tv_title.setText("COLOR");
                    DisplaySelectedItem("Color");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(4);

                }else if(position == 5){

                    type = "Size";
                    tv_title.setText("SIZE");
                    DisplaySelectedItem("Size");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(5);

                }else if(position == 6){

                    type = "Price";
                    tv_title.setText("PRICE");
                    DisplaySelectedItem("Price");
                    if(selected_price_list.size()==0){

                        ed_new_tag.setVisibility(View.VISIBLE);

                    }else {

                        ed_new_tag.setVisibility(View.GONE);

                    }
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_NUMBER);
                    btn_setdate.setVisibility(View.GONE);
                    Setindicator(6);

                }else if(position == 7){

                    type = "Date";
                    tv_title.setText("PURCHASE DATE");
                    DisplaySelectedItem("Date");
                    ed_new_tag.setVisibility(View.GONE);

                    if(selected_purchase_date_list.size()==0){

                        btn_setdate.setVisibility(View.VISIBLE);

                    }else {

                        btn_setdate.setVisibility(View.GONE);

                    }
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    Setindicator(7);

                }else if(position == 8){

                    type = "Store";
                    tv_title.setText("STORE");
                    DisplaySelectedItem("Store");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    btn_setdate.setVisibility(View.GONE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    Setindicator(8);

                }else if(position == 9){

                    type = "Washinginstructions";
                    tv_title.setText("WASHING INSTRUCTIONS");
                    DisplaySelectedItem("Washinginstructions");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    btn_setdate.setVisibility(View.GONE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    Setindicator(9);

                }else if(position == 10){

                    type = "tags";
                    tv_title.setText("ADDITIONAL TAGS");
                    DisplaySelectedItem("tags");
                    ed_new_tag.setVisibility(View.VISIBLE);
                    btn_setdate.setVisibility(View.GONE);
                    ed_new_tag.setInputType(InputType.TYPE_CLASS_TEXT);
                    Setindicator(10);

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_activity_title.setTypeface(face);
        vp_add_item = (NonSwipeableViewPager) findViewById(R.id.vp_add_item);
        im_item = (ImageView) findViewById(R.id.im_item_image);

        im_forward = (ImageView) findViewById(R.id.im_forward);
        im_backward = (ImageView) findViewById(R.id.im_backward);

        im_remove_image = (ImageView) findViewById(R.id.im_remove_image);
        im_edit_image = (ImageView) findViewById(R.id.im_edit_image);
        im_add_image = (ImageView) findViewById(R.id.im_add_image);
        addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this,"",type);
        vp_add_item.setAdapter(addItemPagerAdapter);
        tv_title = (TextView) findViewById(R.id.tv_title);
        ed_new_tag = (EditText) findViewById(R.id.ed_new_tag);
        btn_setdate = (Button) findViewById(R.id.btn_setdate);
        selected_item_display_layout = (LinearLayout) findViewById(R.id.selected_layout);
        db = new DbHelper(getApplicationContext());
        pDialog = new ProgressDialog(this);

        selected_category_list = new ArrayList<>();
        selected_sub_category_list = new ArrayList<>();
        selected_style_list = new ArrayList<>();
        selected_brand_list = new ArrayList<>();
        selected_color_list = new ArrayList<>();
        selected_size_list = new ArrayList<>();
        selected_washing_instrcutions = new ArrayList<>();
        selected_additional_tags = new ArrayList<>();
        selected_store_list = new ArrayList<>();
        selected_purchase_date_list = new ArrayList<>();
        selected_price_list = new ArrayList<>();
        pixel_array_list = new ArrayList<>();

        indicator_layout = (LinearLayout) findViewById(R.id.indicator_layout);
        indicator_array = new TextView[11];

    }


    private void Generatedata() {

        out_additional_tag = GetAddItemOutPut.GetAdditionalTag();
        out_care_tip = GetAddItemOutPut.GetOutCareTip();
        out_category = GetAddItemOutPut.GetCategory();
        out_date = GetAddItemOutPut.GetOutDate();
        out_price = GetAddItemOutPut.GetOutPrice();
        out_size = GetAddItemOutPut.GetOutSize();
        out_style = GetAddItemOutPut.GetOutStyle();
        out_color = GetAddItemOutPut.GetOutColor();

        out_category_id = GetAddItemOutPut.GetOutCategoryID(this);
        out_brand_id = GetAddItemOutPut.GetOutBrandID(this);
        out_brand_name = GetAddItemOutPut.GetOutBrand();
        out_store_id = GetAddItemOutPut.GetOutStoreID(this);
        out_store_name = GetAddItemOutPut.GetOutStore();


       UploadData();

    }

    private void UploadData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new UploadAddItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"item/create");

            }
            else {

                new UploadAddItemData().execute(WodrobConstant.BASE_URL+"item/create");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }



    @SuppressLint("NewApi")
    public void DisplaySelectedItem(String type) {

        selected_item_display_layout.removeAllViews();

        if(type.equalsIgnoreCase("Category")){

            if (selected_category_list.size() != 0) {
                for (int j = 0; j < selected_category_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_category_name);
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_category_list.remove(((ImageView) v).getId());
                            DisplaySelectedItem("Cateogory");
                            vp_add_item.setPagingEnabled(false);
                            vp_add_item.setAdapter(addItemPagerAdapter);

                        }
                    });
                }
            }

        }else if (type.equalsIgnoreCase("SubCategory")) {

            if (selected_sub_category_list.size() != 0) {
                for (int j = 0; j < selected_sub_category_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_sub_category_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_sub_category_list.remove(((ImageView) v).getId());
                            DisplaySelectedItem("SubCategory");
                        }
                    });
                }
            }

        } else if (type.equalsIgnoreCase("style")) {

            if (selected_style_list.size() != 0) {
                for (int j = 0; j < selected_style_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_style_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_style_list.remove(((ImageView) v).getId());
                            //DisplaySelectedItem("Style");
                            RefreshView("Style","");
                        }
                    });
                }
            }

        }else if(type.equalsIgnoreCase("brand")){

            if (selected_brand_list.size() != 0) {
                for (int j = 0; j < selected_brand_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_brand_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_brand_list.remove(((ImageView) v).getId());
                           // DisplaySelectedItem("Brand");
                            RefreshView("Brand","");
                        }
                    });
                }
            }


        }else if(type.equalsIgnoreCase("color")){

            if (selected_color_list.size() != 0) {
                for (int j = 0; j < selected_color_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_color_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_color_list.remove(((ImageView) v).getId());
                            DisplaySelectedItem("Color");
                        }
                    });
                }
            }


        }else if(type.equalsIgnoreCase("size")){

            if (selected_size_list.size() != 0) {
                for (int j = 0; j < selected_size_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_size_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_size_list.remove(((ImageView) v).getId());
                            //DisplaySelectedItem("Size");
                            RefreshView("Size","");
                        }
                    });
                }
            }


        }else if(type.equalsIgnoreCase("washinginstructions")){


            if (selected_washing_instrcutions.size() != 0) {
                for (int j = 0; j < selected_washing_instrcutions.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_washing_instrcutions.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_washing_instrcutions.remove(((ImageView) v).getId());
                          //  DisplaySelectedItem("washinginstructions");

                            RefreshView("washinginstructions","");
                        }
                    });
                }
            }

        }else if(type.equalsIgnoreCase("date")){

            btn_setdate.setVisibility(View.GONE);

            if (selected_purchase_date_list.size() != 0) {
                for (int j = 0; j < selected_purchase_date_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_purchase_date_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_purchase_date_list.remove(((ImageView) v).getId());
                           // DisplaySelectedItem("date");
                            RefreshView("Date","");
                            btn_setdate.setVisibility(View.VISIBLE);

                        }
                    });
                }
            }


        }else if(type.equalsIgnoreCase("store")){

            if (selected_store_list.size() != 0) {
                for (int j = 0; j < selected_store_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_store_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_store_list.remove(((ImageView) v).getId());
                           // DisplaySelectedItem("store");
                            RefreshView("Store","");

                        }
                    });
                }
            }


        }else if(type.equalsIgnoreCase("price")){

            if (selected_price_list.size() != 0) {
                for (int j = 0; j < selected_price_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_price_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_price_list.remove(((ImageView) v).getId());
                         //   DisplaySelectedItem("price");
                            RefreshView("price","");

                        }
                    });
                }
            }else {

                ed_new_tag.setVisibility(View.VISIBLE);

            }

        }else if(type.equalsIgnoreCase("tags")){

            if (selected_additional_tags.size() != 0) {
                for (int j = 0; j < selected_additional_tags.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_additional_tags.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);

                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_additional_tags.remove(((ImageView) v).getId());
                          //  DisplaySelectedItem("tags");
                            RefreshView("tags","");

                        }
                    });
                }
            }

        }
    }

    /*refresh view from adapter*/
    public void RefreshView(String type,String search) {
        if (type.equalsIgnoreCase("Category")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
           // ed_new_tag.setText("");
            vp_add_item.setCurrentItem(0);
            vp_add_item.setPagingEnabled(true);
            DisplaySelectedItem("Category");


        } else if (type.equalsIgnoreCase("SubCategory")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(1);


        } else if (type.equalsIgnoreCase("style")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(2);


        } else if (type.equalsIgnoreCase("brand")) {


            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(3);

        } else if (type.equalsIgnoreCase("color")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
         //   ed_new_tag.setText("");
            vp_add_item.setCurrentItem(4);

        } else if (type.equalsIgnoreCase("size")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
         //   ed_new_tag.setText("");
            vp_add_item.setCurrentItem(5);

        } else if (type.equalsIgnoreCase("price")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
         //   ed_new_tag.setText("");
            vp_add_item.setCurrentItem(6);

        } else if (type.equalsIgnoreCase("date")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(7);

        } else if (type.equalsIgnoreCase("store")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(8);

        } else if (type.equalsIgnoreCase("washinginstructions")) {


            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(9);

        } else if (type.equalsIgnoreCase("tags")) {

            addItemPagerAdapter = new AddItemPagerAdapter(AddItem.this, search, type);
            vp_add_item.setAdapter(addItemPagerAdapter);
          //  ed_new_tag.setText("");
            vp_add_item.setCurrentItem(10);

        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_setdate){

            ShowDatePicker();

        }else if(v.getId() == R.id.im_edit_image){

            Intent intent = new Intent(AddItem.this,EditImageActivity.class);
            startActivityForResult(intent, 2);

        }else if(v.getId() == R.id.im_add_image){

            ChooseItem();
           // Generatedata();

        }else if(v.getId() == R.id.im_remove_image){

            im_item.setImageBitmap(null);
            im_item.setImageDrawable(null);

            pixel_array_list.clear();
            vp_add_item.removeAllViews();
            vp_add_item.setAdapter(addItemPagerAdapter);
            im_remove_image.setVisibility(View.GONE);
            im_edit_image.setVisibility(View.GONE);
            im_add_image.setVisibility(View.VISIBLE);
            is_image_added = false;

        }else if(v.getId() == R.id.im_forward){

            int current_position = vp_add_item.getCurrentItem();
            if(current_position == 0){
                if(selected_category_list.size() == 0){

                    Toast.makeText(getApplicationContext(),"Please select a category",Toast.LENGTH_LONG).show();

                }else{

                    vp_add_item.setCurrentItem(current_position + 1);

                }
            }else{
                vp_add_item.setCurrentItem(current_position + 1);

            }

        }else if(v.getId() == R.id.im_backward){

            int current_position = vp_add_item.getCurrentItem();
            vp_add_item.setCurrentItem(current_position - 1);

        }
    }

    private void ShowDatePicker() {

        DatePickerFragment newFragment = new DatePickerFragment();

        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        newFragment.setArguments(args);
        newFragment.setCallBack(ondate);

        newFragment.show(getFragmentManager(), "datePicker");

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            date = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
            selected_purchase_date_list.clear();
            selected_purchase_date_list.add(date);
            type = "date";
            DisplaySelectedItem("date");

        }
    };

    public void update_Search(String type, String data) {

        if(type.equalsIgnoreCase("Category")){

            Toast.makeText(getApplicationContext(),"Can't add new category,Please Pick one from the list",Toast.LENGTH_LONG).show();
            DisplaySelectedItem("Category");


        }else if(type.equalsIgnoreCase("SubCategory")){

            Toast.makeText(getApplicationContext(),"Can't add new subcategory,Please Pick one from the list",Toast.LENGTH_LONG).show();
            DisplaySelectedItem("SubCategory");


        }else if(type.equalsIgnoreCase("Size")){

            Toast.makeText(getApplicationContext(),"Can't add new size,Please Pick one from the list",Toast.LENGTH_LONG).show();
            DisplaySelectedItem("Size");


        }
        else if (type.equalsIgnoreCase("price")) {

            selected_price_list.add(data);
            DisplaySelectedItem("price");

        }else if(type.equalsIgnoreCase("brand")){

            selected_brand_list.clear();
            selected_brand_list.add(data);
            DisplaySelectedItem("brand");

        }else if(type.equalsIgnoreCase("style")){

            selected_style_list.add(data);
            DisplaySelectedItem("style");

        }else if(type.equalsIgnoreCase("Store")){

            selected_store_list.clear();
            selected_store_list.add(data);
            DisplaySelectedItem("store");

        }else if(type.equalsIgnoreCase("Tags")){

            selected_additional_tags.add(data);
            DisplaySelectedItem("Tags");

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_add_item) {

            if(AddItem.selected_category_list.size()==0){

                Toast.makeText(getApplicationContext(),"Please select one category",Toast.LENGTH_LONG).show();

            }else{

                Generatedata();

            }
            //ChooseItem();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void ChooseItem() {
        final Dialog dialog = new Dialog(AddItem.this,R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_item_dialog_layout);
        // dialog.setTitle("Add Image");

        // set the custom dialog components - text, image and button
        LinearLayout ll_camera = (LinearLayout) dialog.findViewById(R.id.ll_camera);
        LinearLayout ll_camera_roll = (LinearLayout) dialog.findViewById(R.id.ll_camera_roll);
        LinearLayout ll_catalogue = (LinearLayout) dialog.findViewById(R.id.ll_catalogue);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        ll_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                String picturePath = Environment.getExternalStorageDirectory() + File.separator + "wodrob.jpg";
                File file = new File(picturePath);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(file));
                    startActivityForResult(intent, PICK_FROM_CAMERA);

            }
        });
        ll_camera_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, RESULT_LOAD_IMAGE);

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_FROM_CAMERA){
           if( resultCode != RESULT_CANCELED) {
               im_edit_image.setVisibility(View.VISIBLE);
               String picturePath = Environment.getExternalStorageDirectory() + File.separator + "wodrob.jpg";
               File file = new File(picturePath);
               BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
               bmpFactoryOptions.inJustDecodeBounds = true;
               Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 600);

               ByteArrayOutputStream stream = new ByteArrayOutputStream();
               bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
               byte[] byteArray = stream.toByteArray();

               Intent intent = new Intent(AddItem.this, CropActivity.class);
               intent.putExtra("image", byteArray);
               startActivityForResult(intent, 2);

            }

        }
        else if(requestCode == 2){
            byte[] byteArray = data.getByteArrayExtra("edited_image");
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            im_item.setImageBitmap(bmp);
            im_add_image.setVisibility(View.GONE);
            im_remove_image.setVisibility(View.VISIBLE);
            is_image_added = true;


            int pixel1 = Wodrob.getDominantColor1(bmp);
            int pixel2 = bmp.getPixel(0,0);
            int pixel3 = bmp.getPixel(50,0);
            int pixel4 = bmp.getPixel(0,100);
            int pixel5 = bmp.getPixel(100, 0);

            pixel_array_list.add(String.format("#%08X", pixel1));
            pixel_array_list.add(String.format("#%08X", pixel2));
            pixel_array_list.add(String.format("#%08X", pixel3));
            pixel_array_list.add(String.format("#%08X", pixel4));
            pixel_array_list.add(String.format("#%08X", pixel5));

            vp_add_item.removeAllViews();
            vp_add_item.setAdapter(addItemPagerAdapter);

        } else if (requestCode == 3){
            byte[] byteArray = data.getByteArrayExtra("edited_image");
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            im_item.setImageBitmap(bmp);

        }else if(requestCode == RESULT_LOAD_IMAGE){
            im_edit_image.setVisibility(View.VISIBLE);
            if(data != null) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                File file = new File(picturePath);
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 600);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent = new Intent(AddItem.this, CropActivity.class);
                intent.putExtra("image", byteArray);
                startActivityForResult(intent, 2);
            }
        } else if (requestCode == 6) {

            RefreshView("Color","");
        }
    }


     /*background class to upload image with data*/


    private class UploadAddItemData extends AsyncTask<String, Void, Void> {

        String out;
        String category_id;
        private String Content;
        private String Error = null;
        String data = "";
        Bitmap bitmap = null;
        int height = 0;
        int width = 0;

        @Override
        protected void onPreExecute() {

            category_id = db.GetCategory_id(out_category);
            pDialog.setMessage("Loading...");
            pDialog.show();

            if(is_image_added) {

                bitmap = ((BitmapDrawable) im_item.getDrawable()).getBitmap();
                height = bitmap.getHeight();
                width = bitmap.getWidth();

            }
            try {

                data +="&"+ URLEncoder.encode("category_id", "UTF-8")+"="+ category_id+"&"
                        + URLEncoder.encode("category_name", "UTF-8")+"="+out_category+"&"
                        + URLEncoder.encode("size", "UTF-8")+"="+out_size+"&"
                        + URLEncoder.encode("care_tips", "UTF-8")+"="+out_care_tip+"&"
                        + URLEncoder.encode("purchase_date", "UTF-8")+"="+out_date+"&"
                        + URLEncoder.encode("style_name", "UTF-8")+"="+out_style+"&"
                        + URLEncoder.encode("price", "UTF-8")+"="+out_price+"&"
                        + URLEncoder.encode("tags", "UTF-8")+"="+out_additional_tag+"&"
                        + URLEncoder.encode("hexs", "UTF-8")+"="+out_color+"&"
                        + URLEncoder.encode("image", "UTF-8")+"="+Wodrob.image_to_String(bitmap)+"&"
                        + URLEncoder.encode("height", "UTF-8")+"="+height+"&"
                        + URLEncoder.encode("width", "UTF-8")+"="+width+"&"
                        + URLEncoder.encode("brand_id", "UTF-8")+"="+out_brand_id+"&"
                        + URLEncoder.encode("brand_name", "UTF-8")+"="+out_brand_name;

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equals("true")){

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else{

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

                }

        }
    }


    public void clearsearch(){

        ed_new_tag.setText("");
    }

    @SuppressLint("NewApi")
    private void AddIndicators() {


        for(int i = 0; i < 11; i++) {
            indicator_array[i] = new TextView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(10,10);
            params.setMargins(4, 4, 4, 4);
            indicator_array[i].setLayoutParams(params);
            indicator_layout.addView(indicator_array[i]);
        }
        Setindicator(0);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void Setindicator(int position) {

        for(int i = 0; i < 11; i++) {

            if(i == position){

                indicator_array[i].setBackground(getResources().getDrawable(R.drawable.selecteditem_dot));

            }else {

                indicator_array[i].setBackground(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            }
        }
    }


}
