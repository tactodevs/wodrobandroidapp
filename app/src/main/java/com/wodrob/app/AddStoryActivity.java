package com.wodrob.app;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.model.AddStoryModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class AddStoryActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int RESULT_LOAD_IMAGE = 2;
    private static final int PICK_FROM_CAMERA = 1;
    String story_type = "item";
    Toolbar toolbar;
    TextView tv_title;
    Button btn_items,btn_outfits,btn_looks;
    Drawable leftroundeddrawable,rightroundeddrawable;
    private LinearLayout.LayoutParams layoutParams;
    LinearLayout rl_description_layout;
    Button btn_add_more;
    ArrayList<AddStoryModel> image_array_list;
    int count  = 0;
    int clicked_image_position;

   // EditText ed_story_description,ed_title;
    String story_description,story_title,imagejson;
    private ProgressDialog pDialog;
    String global_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story);
        init();
        CreateLayout();
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.closet_main_menu);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Create a Story");
        btn_items = (Button) findViewById(R.id.btn_items);
        btn_add_more = (Button) findViewById(R.id.btn_add_more);
        btn_outfits = (Button) findViewById(R.id.btn_outfits);
        btn_looks = (Button) findViewById(R.id.btn_looks);
        btn_items.setOnClickListener(this);
        btn_outfits.setOnClickListener(this);
        btn_add_more.setOnClickListener(this);
        btn_looks.setOnClickListener(this);
        leftroundeddrawable = getResources().getDrawable(R.drawable.leftroundedbutton);
        rightroundeddrawable = getResources().getDrawable(R.drawable.rightroundedbutton);

        leftroundeddrawable.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
        rightroundeddrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        btn_items.setBackgroundDrawable(leftroundeddrawable);
        btn_looks.setBackgroundDrawable(rightroundeddrawable);
        rl_description_layout = (LinearLayout) findViewById(R.id.rl_description_layout);

        image_array_list = new ArrayList<>();
       // ed_story_description = (EditText) findViewById(R.id.ed_story_description);
       // ed_title = (EditText) findViewById(R.id.ed_title);

        pDialog = new ProgressDialog(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_items){

            leftroundeddrawable.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
            rightroundeddrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            btn_items.setBackgroundDrawable(leftroundeddrawable);
            btn_looks.setBackgroundDrawable(rightroundeddrawable);
            btn_outfits.setBackgroundColor(getResources().getColor(R.color.White));
            story_type = "item";
            rl_description_layout.removeAllViews();
           // ed_title.setText("");
            //ed_story_description.setText("");
            count = 0;
            image_array_list.clear();
            CreateLayout();

        }else if(v.getId() == R.id.btn_outfits){

            leftroundeddrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            rightroundeddrawable.setColorFilter(getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
            btn_items.setBackgroundDrawable(leftroundeddrawable);
            btn_looks.setBackgroundDrawable(rightroundeddrawable);
            btn_outfits.setBackgroundColor(getResources().getColor(R.color.divider));
            story_type = "outfit";
            rl_description_layout.removeAllViews();
            //ed_title.setText("");
            //ed_story_description.setText("");
            count = 0;
            image_array_list.clear();
            CreateLayout();

        }else if(v.getId() == R.id.btn_looks){

            leftroundeddrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            rightroundeddrawable.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
            btn_items.setBackgroundDrawable(leftroundeddrawable);
            btn_looks.setBackgroundDrawable(rightroundeddrawable);
            btn_outfits.setBackgroundColor(getResources().getColor(R.color.White));
            story_type = "looks";
            rl_description_layout.removeAllViews();
            //ed_title.setText("");
            //ed_story_description.setText("");
            count = 0;
            image_array_list.clear();
            CreateLayout();

        }else if(v.getId() == R.id.btn_add_more){

            CreateLayout();

        }
    }

    public void CreateLayout(){

        LinearLayout ll_image = new LinearLayout(this);
        ll_image.setOrientation(LinearLayout.VERTICAL);
        ll_image.setBackgroundColor(Color.parseColor("#FFFFFF"));
        ll_image.setId(count);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.topMargin=20;
        params.gravity= Gravity.CENTER;

        ImageView im_cancel = new ImageView(this);
        im_cancel.setId(count);
        im_cancel.setImageDrawable(getResources().getDrawable(R.drawable.filter_cancel));
        layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.END;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = 5;
        im_cancel.setLayoutParams(layoutParams);

        ImageView im_story = new ImageView(this);
        im_story.setBackgroundColor(Color.GRAY);
               // im_story.setImageDrawable(getResources().getDrawable(R.drawable.shirt));
        layoutParams = new LinearLayout.LayoutParams(400,400);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        layoutParams.topMargin = 20;
        layoutParams.bottomMargin = 30;
        im_story.setId(count);
        im_story.setLayoutParams(layoutParams);

        EditText ed_description = new EditText(this);
        ed_description.setBackgroundColor(Color.WHITE);
        ed_description.setHint("Add description");
        layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,80);
        layoutParams.topMargin = 20;
        layoutParams.leftMargin = 10;
        ed_description.setId(count);
        ed_description.setLayoutParams(layoutParams);

        EditText ed_title = new EditText(this);
        ed_title.setBackgroundColor(Color.WHITE);
        ed_title.setHint("Add title");
        layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,80);
        layoutParams.topMargin = 20;
        layoutParams.leftMargin = 10;
        ed_title.setId(count);
        ed_title.setLayoutParams(layoutParams);

        if(count > 0 ){

            ll_image.addView(im_cancel);

        }
        ll_image.addView(ed_title);
        ll_image.addView(ed_description);
        ll_image.addView(im_story);
        ll_image.setLayoutParams(params);

        rl_description_layout.addView(ll_image);
        image_array_list.add(new AddStoryModel(im_story, ed_description,ed_title));
        count = count + 1;

        //onclick of im_story

        im_story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clicked_image_position = v.getId();

                if(story_type.equalsIgnoreCase("item")){

                    Intent intent = new Intent(AddStoryActivity.this,AddCanvasItem.class);
                    intent.putExtra("type",story_type);
                    startActivityForResult(intent,3);

                }else if(story_type.equalsIgnoreCase("outfit")){

                    Intent intent = new Intent(AddStoryActivity.this,AddCanvasItem.class);
                    intent.putExtra("type",story_type);
                    startActivityForResult(intent,3);

                }else if(story_type.equalsIgnoreCase("looks")){

                    ChooseItem();

                }

            }
        });
        //onclick of cancel

        im_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0; i<image_array_list.size(); i++){

                    if(image_array_list.get(i).getIm_story().getId() == v.getId()){

                        image_array_list.remove(i);
                        rl_description_layout.removeViewAt(i);
                    }
                }
            }
        });
    }

    private void ChooseItem() {
        final Dialog dialog = new Dialog(AddStoryActivity.this,R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_story_menu);
        // dialog.setTitle("Add Image");

        // set the custom dialog components - text, image and button
        LinearLayout ll_camera = (LinearLayout) dialog.findViewById(R.id.ll_camera);
        LinearLayout ll_camera_roll = (LinearLayout) dialog.findViewById(R.id.ll_camera_roll);

        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);


        ll_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                String picturePath = Environment.getExternalStorageDirectory() + File.separator + "wodrobstory.jpg";
                File file = new File(picturePath);
                if(file != null) {
                    file.delete();
                }
                File files = new File(picturePath);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(files));
                startActivityForResult(intent, PICK_FROM_CAMERA);

            }
        });
        ll_camera_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, RESULT_LOAD_IMAGE);

            }
        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){

            Intent intent = new Intent();
            setResult(2,intent);
            finish();
            return true;

        }else if(id  == R.id.mi_add_item){

           if(Validate()){

               GenerateData();

           }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public boolean Validate(){

        for(int i = 0;i<image_array_list.size();i++){

            if(i == 0){

                if(image_array_list.get(i).getEd_title().getText().toString().equalsIgnoreCase("")){

                    Toast.makeText(getApplicationContext(),"Please add title",Toast.LENGTH_LONG).show();

                    return false;

                }else if (image_array_list.get(i).getIm_story().getDrawable() == null){

                    Toast.makeText(getApplicationContext(),"Add Image For Story",Toast.LENGTH_LONG).show();

                    return false;

                }

            }else {

                if(image_array_list.get(i).getIm_story().getDrawable() == null){

                    Toast.makeText(getApplicationContext(),"Image cannot be empty",Toast.LENGTH_LONG).show();
                    return false;

                }
            }

        }

        return true;
    }
    private void GenerateData() {

        JSONArray jsonArray = new JSONArray();
        JSONObject jObjectData;
        JSONObject dataobject;

        for (int i = 0; i < image_array_list.size(); i++) {
             jObjectData = new JSONObject();
            if(i>0){

                try {

                    Bitmap image_bitmap =((BitmapDrawable)image_array_list.get(i).getIm_story().getDrawable()).getBitmap();
                    jObjectData.put("title",image_array_list.get(i).getEd_title().toString());
                    jObjectData.put("image_description", image_array_list.get(i).getEd_description().getText().toString());
                    jObjectData.put("image", Wodrob.image_to_String(image_bitmap));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                jsonArray.put(jObjectData);

            }



        }
        dataobject = new JSONObject();
        try {

            dataobject.put("story_data",jsonArray);
            Log.d("json", dataobject.toString());
            Log.d("json",dataobject.toString());
        } catch (JSONException e) {

            e.printStackTrace();
        }


        story_description = image_array_list.get(0).getEd_description().getText().toString();
        story_title = image_array_list.get(0).getEd_title().getText().toString();
        Bitmap image_bitmap =((BitmapDrawable)image_array_list.get(0).getIm_story().getDrawable()).getBitmap();
        global_image = Wodrob.image_to_String(image_bitmap);
        //imagejson = dataobject.toString();
        imagejson = jsonArray.toString();
        UploadData();
    }

    private void UploadData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new UploadStory().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"story/create");

            }
            else {

                new UploadStory().execute(WodrobConstant.BASE_URL+"story/create");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*upload story*/

    private class UploadStory extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            try {

                data +="&"+ URLEncoder.encode("title", "UTF-8")+"="+ story_title+"&"
                        + URLEncoder.encode("description", "UTF-8")+"="+story_description+"&"
                        + URLEncoder.encode("privacy_level", "UTF-8")+"="+"public"+"&"
                        + URLEncoder.encode("type", "UTF-8")+"="+story_type+"&"
                        + URLEncoder.encode("image", "UTF-8")+"="+global_image+"&"
                        + URLEncoder.encode("patron_id", "UTF-8")+"="+WodrobConstant.patron_id+"&"
                        + URLEncoder.encode("story_data", "UTF-8")+"="+imagejson;

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                Log.d("res",sb.toString());

                String res = sb.toString();

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equals("true")){

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else{

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_FROM_CAMERA){
                if (resultCode != RESULT_CANCELED) {
                    String picturePath = Environment.getExternalStorageDirectory() + File.separator + "wodrobstory.jpg";
                    File file = new File(picturePath);
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = true;
                    Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 400, 400);

                    image_array_list.get(clicked_image_position).getIm_story().setImageBitmap(bmp);

                }

        }else if(requestCode == RESULT_LOAD_IMAGE){
            if(data != null) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                File file = new File(picturePath);
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 400, 400);

                image_array_list.get(clicked_image_position).getIm_story().setImageBitmap(bmp);

            }
        }
        else if(requestCode == 3){
            if(data != null) {

                if(!data.getStringExtra("image_path").equalsIgnoreCase("")){

                    Picasso.with(image_array_list.get(clicked_image_position).getIm_story().getContext()).load(data.getStringExtra("image_path")).into(image_array_list.get(clicked_image_position).getIm_story());

                }

            }
        }

    }
}
