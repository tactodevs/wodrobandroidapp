package com.wodrob.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.adapter.ExploreCatalogAdapter;
import com.wodrob.app.model.ExploreCatalogModel;
import com.wodrob.app.views.EndlessOnScrollListener;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class AddStoryImage extends AppCompatActivity {

    Toolbar toolbar;
    TextView tv_title;
    ProgressDialog pDialog;

    ArrayList<ExploreCatalogModel> exploreCatalogModelArrayList;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    ExploreCatalogAdapter singleadapter;
    ExploreCatalogModel exploreCatalogModel;
    DbHelper db;
    String next_page_url;
    boolean loading = true;
    private GridLayoutManager gridlayoutmanager;

    String filter_category_id = "";
    String filter_style = "";
    String filter_brands = "";
    String filter_size = "";
    String filter_color = "";
    String filter_tags = "";
    boolean isfirstime = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
        if(getIntent().getExtras().getString("type").equalsIgnoreCase("item")) {

            GetFilterString();

        }
        GetData(getIntent().getExtras().getString("type"));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it
               Intent intent = new Intent(AddStoryImage.this, ViewCatalog.class);
               intent.putExtra("image_path", exploreCatalogModelArrayList.get(position).getImage_path());
               setResult(3, intent);
                finish();

            }
        });

        recyclerView.addOnScrollListener(new EndlessOnScrollListener(gridlayoutmanager) {

            @Override
            public void onScrolledToEnd() {
                if (!loading) {
                    loading = true;
                    // add 10 by 10 to tempList then notify changing in data
                    if (!next_page_url.equals("")) {
                        singleadapter.notifyDataSetChanged();
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, next_page_url);

                            } else {

                                new FetchItemData().execute(next_page_url);

                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
                loading = false;
            }
        });

    }

    private void GetFilterString() {

        String[] filterstring = Wodrob.GenerateFiterString(this).split("/");

        try{

            filter_category_id = filterstring[0];
            filter_style = filterstring[1];
            filter_color = filterstring[2];
            filter_size = filterstring[3];
            filter_brands = filterstring[4];
            filter_tags = filterstring[5];

        }catch (ArrayIndexOutOfBoundsException e){

        }

    }
    private void GetData(String type) {

        if(type.equalsIgnoreCase("item")){

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "product");

                } else {

                    new FetchItemData().execute(WodrobConstant.BASE_URL + "product");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }else  if(type.equalsIgnoreCase("outfit")){
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "outfit");

                } else {

                    new FetchItemData().execute(WodrobConstant.BASE_URL + "outfit");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.canvas_cancel);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_title = (TextView) findViewById(R.id.tv_title);
        if(getIntent().getExtras().getString("type").equalsIgnoreCase("item")){

            tv_title.setText("Select Item");

        }else  if(getIntent().getExtras().getString("type").equalsIgnoreCase("outfit")){

            tv_title.setText("Select Outift");


        }
        pDialog = new ProgressDialog(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.addItemDecoration(new SingleSpacingDecoration(2, "parent"));


        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        pDialog = new ProgressDialog(this);
        gridlayoutmanager = new GridLayoutManager(this, 2);
        exploreCatalogModelArrayList = new ArrayList<>();
        recyclerView.setLayoutManager(gridlayoutmanager);

        setlayout();
    }

    private void setlayout() {

        singleadapter = new ExploreCatalogAdapter(exploreCatalogModelArrayList, R.layout.item_grid_layout);
        recyclerView.setAdapter(singleadapter);
    }
      /*get item data*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_story_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

       if(id == android.R.id.home){

            Intent intent = new Intent();
            setResult(2,intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private class FetchItemData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try{

                if(getIntent().getExtras().getString("type").equalsIgnoreCase("item")){

                    data +="&"+ URLEncoder.encode("categories", "UTF-8")+"="+ filter_category_id+"&"
                            + URLEncoder.encode("styles", "UTF-8")+"="+filter_style+"&"
                            + URLEncoder.encode("brand", "UTF-8")+"="+filter_brands+"&"
                            + URLEncoder.encode("sizes", "UTF-8")+"="+filter_size+"&"
                            + URLEncoder.encode("colors", "UTF-8")+"="+filter_color;
                }



                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                            if(getIntent().getExtras().getString("type").equalsIgnoreCase("item")){

                                JSONObject dataobject = jsonObject.getJSONObject("data");
                                JSONArray data_array = dataobject.getJSONArray("data");
                                next_page_url = dataobject.getString("next_page_url");
                                //count = 1;
                                if(next_page_url.equals(null)){

                                    next_page_url = "";
                                    loading = false;
                                    // count = 0;

                                }

                                for(int i = 0;i<data_array.length();i++){

                                    JSONObject data_obj = data_array.getJSONObject(i);
                                    String name = data_obj.getString("name");
                                    String product_id = data_obj.getString("_id");
                                    JSONArray image_array = data_obj.getJSONArray("images");
                                    JSONObject image_obj = image_array.getJSONObject(0);

                                    String image_path = image_obj.getString("filename");

                                    exploreCatalogModel = new ExploreCatalogModel(product_id,name,image_path);
                                    exploreCatalogModelArrayList.add(exploreCatalogModel);
                                    //  cataloglist.add(catalogItem);po

                                }
                                singleadapter.notifyDataSetChanged();

                            }else if(getIntent().getExtras().getString("type").equalsIgnoreCase("outfit")){


                                JSONObject dataobject = jsonObject.getJSONObject("data");
                                JSONArray data_array = dataobject.getJSONArray("data");
                         /*   next_page_url = dataobject.getString("next_page_url");
                            count = 1;
                            if(next_page_url.equals(null)){

                                next_page_url = "";
                                count = 0;

                            }*/

                                for(int i = 0;i<data_array.length();i++){

                                    JSONObject data_obj = data_array.getJSONObject(i);
                                    JSONArray occassio_array = data_obj.getJSONArray("occasion");
                                    String name = data_obj.getString("name");
                                    String image_path = data_obj.getString("image");
                                    String outfit_id = data_obj.getString("_id");


                                    exploreCatalogModel = new ExploreCatalogModel(outfit_id,name,image_path);
                                    exploreCatalogModelArrayList.add(exploreCatalogModel);
                                    //  cataloglist.add(catalogItem);po
                                }
                                singleadapter.notifyDataSetChanged();
                            }


                        }else{

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();

            }
        }
    }

}
