package com.wodrob.app;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wodrob.app.fragment.OutfitCategoryFilter;
import com.wodrob.app.interfaces.AddITemInterface;
import com.wodrob.app.interfaces.EditDialogListener;
import com.wodrob.app.model.CanvasImage;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class CanvasActivity extends AppCompatActivity implements EditDialogListener, AddITemInterface, View.OnClickListener {

    private static final int GETIMAGE = 1;
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;

    boolean isclicked = false;
    RelativeLayout canvas_layout, rl_parent_canvas;
    RelativeLayout.LayoutParams layoutParams;
    ImageView selectedimage;
    String image_path, selected_product_id, selected_image_id, in_wish_list;
    Button btn_add_item;
    Button btn_undo, btn_redo, btn_center;

    FrameLayout items_layout;
    OutfitCategoryFilter addItemFragment;
    TextView tv_title;
    boolean isfragment_active = false;
    String flip_type = "0";
    int noofitems = 0;
    int undo_count = 0;

    public static ArrayList<ImageView> canvasitems;
    public ArrayList<CanvasImage> parent_layout_array_list;
    ArrayList<ArrayList> canvas_layout_list;
    ArrayList<CanvasImage> redo_array_list;


    CanvasImage canvasImage;

    private float depth = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas);
        init();
        btn_add_item.setOnClickListener(this);
        btn_undo.setOnClickListener(this);
        btn_redo.setOnClickListener(this);
        btn_center.setOnClickListener(this);

    }

    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        canvas_layout = (RelativeLayout) findViewById(R.id.canvas_layout);
        rl_parent_canvas = (RelativeLayout) findViewById(R.id.rl_parent_canvas);
        canvas_layout.setDrawingCacheEnabled(true);
        rl_parent_canvas.setDrawingCacheEnabled(true);
        btn_add_item = (Button) findViewById(R.id.btn_add_item);
        btn_undo = (Button) findViewById(R.id.btn_undo);
        btn_redo = (Button) findViewById(R.id.btn_redo);
        btn_center = (Button) findViewById(R.id.btn_center);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("CREATE OUTFIT");
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_title.setTypeface(face);

        items_layout = (FrameLayout) findViewById(R.id.items_layout);
        canvasitems = new ArrayList<ImageView>();
        parent_layout_array_list = new ArrayList<>();
        canvas_layout_list = new ArrayList<>();
        redo_array_list = new ArrayList<>();

    }

    @SuppressLint("NewApi")
    public void CreateImage(Bitmap bitmap, String type) {

        final ImageView imageView = new ImageView(this);

        if (type.equalsIgnoreCase("new")) {

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            imageView.setImageBitmap(bitmap);
            ((BitmapDrawable) imageView.getDrawable()).setAntiAlias(true);
            layoutParams = new RelativeLayout.LayoutParams(width, height);
            imageView.setLayoutParams(layoutParams);
            imageView.setImageBitmap(bitmap);
            imageView.setScaleType(ImageView.ScaleType.MATRIX);

        } else if (type.equalsIgnoreCase("clone")) {

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            imageView.setImageBitmap(bitmap);
            ((BitmapDrawable) imageView.getDrawable()).setAntiAlias(true);
            layoutParams = new RelativeLayout.LayoutParams(selectedimage.getWidth(), selectedimage.getHeight());
            imageView.setLayoutParams(layoutParams);
            imageView.setRotation(selectedimage.getRotation());
            imageView.setImageBitmap(bitmap);
            imageView.setImageMatrix(selectedimage.getImageMatrix());
            imageView.setScaleType(ImageView.ScaleType.MATRIX);

        } else if (type.equalsIgnoreCase("remote")) {

            Picasso.with(getApplicationContext())
                    .load(image_path)
                    .into(imageView);
            Bitmap image_remote = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

            layoutParams = new RelativeLayout.LayoutParams(image_remote.getWidth(), image_remote.getHeight());
            imageView.setLayoutParams(layoutParams);
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
            imageView.setTag(selected_product_id + "/" + selected_image_id + "/" + flip_type + "/" + in_wish_list);
            imageView.setId(canvasitems.size());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Call some material design APIs here
                imageView.setZ(depth);
            } else {

                //imageView.setElevation(depth);
                // Implement this feature without material design
            }

            depth = depth + 1;

        }
        canvasitems.add(imageView);
        canvas_layout.addView(imageView);

        float[] matrixvalues = new float[9];
        imageView.getImageMatrix().getValues(matrixvalues);
        float scaleX = matrixvalues[Matrix.MSCALE_X];
        float scaleY = matrixvalues[Matrix.MSCALE_Y];

        canvasImage = new CanvasImage(imageView, 0, 0, layoutParams.width, layoutParams.height, (RelativeLayout.LayoutParams) imageView.getLayoutParams(), imageView.getImageMatrix(), "Add", imageView.getRotation(),scaleX,scaleY);
        undo_count = undo_count + 1;
        parent_layout_array_list.add(canvasImage);
        redo_array_list.clear();
        noofitems = noofitems + 1;

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isclicked) {

                    String trans = "0";
                    float selected_image_depth = 0;
                    selectedimage = imageView;
                    ImageView view = (ImageView) v;
                    //imageView.bringToFront();
                    Bitmap image_bitmap = ((BitmapDrawable) view.getDrawable()).getBitmap();
                    float angle = view.getRotation();
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    ImageEditDialog imageEditDialog = null;
                    int item_count = canvas_layout.getChildCount();
                    Matrix matrix = view.getImageMatrix();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                        selected_image_depth = view.getZ();
                        if (view.getZ() < depth - 1) {
                            trans = "0";
                        }

                    } else {

                    }

                    if (trans.equals("1")) {

                        imageEditDialog = new ImageEditDialog(CanvasActivity.this, matrix, params, angle, image_bitmap, true, selected_image_depth, noofitems);

                    } else {

                        imageEditDialog = new ImageEditDialog(CanvasActivity.this, matrix, params, angle, image_bitmap, false, selected_image_depth, noofitems);

                    }
                    imageEditDialog.show();
                    isclicked = false;

                }
            }
        });

        imageView.setOnTouchListener(new View.OnTouchListener() {

            RelativeLayout.LayoutParams parms;
            float dx = 0, dy = 0, x = 0, y = 0;
            float angle = 0;

            private long lastTouchDown;
            private int CLICK_ACTION_THRESHHOLD = 100;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final ImageView view = (ImageView) v;
                ((BitmapDrawable) view.getDrawable()).setAntiAlias(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:

                        parms = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        dx = event.getRawX() - parms.leftMargin;
                        dy = event.getRawY() - parms.topMargin;

                        savedMatrix.set(matrix);
                        start.set(event.getX(), event.getY());
                        mode = DRAG;
                        lastEvent = null;
                        lastTouchDown = System.currentTimeMillis();
                        break;

                    case MotionEvent.ACTION_POINTER_DOWN:
                        oldDist = spacing(event);
                        if (oldDist > 10f) {
                            savedMatrix.set(matrix);
                            midPoint(mid, event);
                            mode = ZOOM;
                        }
                        lastEvent = new float[4];
                        lastEvent[0] = event.getX(0);
                        lastEvent[1] = event.getX(1);
                        lastEvent[2] = event.getY(0);
                        lastEvent[3] = event.getY(1);
                        d = rotation(event);
                        break;
                    case MotionEvent.ACTION_UP:

                        if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_THRESHHOLD) {

                            isclicked = true;
                            matrix.set(savedMatrix);
                        } else {

                            parms = (RelativeLayout.LayoutParams) view.getLayoutParams();

                            float[] values = new float[9];
                            matrix.getValues(values);


                            float[] matrixvalues = new float[9];
                            view.getImageMatrix().getValues(matrixvalues);
                            float scaleX = matrixvalues[Matrix.MSCALE_X];
                            float scaleY = matrixvalues[Matrix.MSCALE_Y];

                            canvasImage = new CanvasImage(view, parms.leftMargin, parms.topMargin, parms.width, parms.height, (RelativeLayout.LayoutParams) view.getLayoutParams(), view.getImageMatrix(), "action", view.getRotation(),scaleX,scaleY);
                            parent_layout_array_list.add(canvasImage);
                            redo_array_list.clear();
                            undo_count = undo_count + 1;


                        }
                        break;

                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        lastEvent = null;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG) {

                          //  view.setScaleType(ImageView.ScaleType.MATRIX);

                            x = event.getRawX();
                            y = event.getRawY();

                            parms.leftMargin = (int) (x - dx);
                            parms.topMargin = (int) (y - dy);

                            parms.rightMargin = 0 ;
                            parms.bottomMargin = 0;

                            if(parms.leftMargin > 400){

                                parms.rightMargin = parms.leftMargin+parms.width;

                            }

                            if(parms.topMargin > 500){

                                parms.bottomMargin = parms.topMargin + parms.height;

                            }


                          // matrix.set(savedMatrix);

                            view.setLayoutParams(parms);

                        } else if (mode == ZOOM) {

                            view.setScaleType(ImageView.ScaleType.FIT_XY);
                            float newDist = spacing(event);
                            //if (newDist > 10f) {

                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);


                            if (oldDist < newDist) {

                                parms.width = parms.width + ((int) scale * 5);
                                parms.height = parms.height + ((int) scale * 5);
                                view.setLayoutParams(parms);

                            } else {

                                float tempscale = (oldDist / newDist);

                                parms.width = parms.width - ((int) tempscale * 5);
                                parms.height = parms.height - ((int) tempscale * 5);

                                view.setLayoutParams(parms);

                            }

                            view.setImageMatrix(matrix);
                            if (lastEvent != null && event.getPointerCount() == 2) {
                                // view.setScaleType(ImageView.ScaleType.MATRIX);
                                newRot = rotation(event);
                                float r = newRot - d;
                                float[] values = new float[9];
                                matrix.getValues(values);
                                float tx = values[2];
                                float ty = values[5];
                                float sx = values[0];
                                float xc = (view.getWidth() / 2) * sx;
                                float yc = (view.getHeight() / 2) * sx;

                                float imageWidth = view.getDrawable().getIntrinsicWidth();
                                float imageHeight = view.getDrawable().getIntrinsicHeight();
                                RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
                                RectF viewRect = new RectF(0, 0, view.getWidth(),
                                        view.getHeight());

                                angle = r;
                                // view.setRotation(r);
                                //view.animate().rotation(r);
                                matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
                               // matrix.postRotate(r, tx + xc, ty + yc);
                                view.setLayoutParams(parms);
                                view.setImageMatrix(matrix);
                                view.animate().rotationBy(angle).setDuration(0).setInterpolator(new LinearInterpolator()).start();

                            }
                        }
                        break;
                }

               // view.setLayoutParams(parms);
                //view.setRotation(r);
                return false;

            }
        });
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
        // return FloatMath.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_add_image) {

            /*Intent intent = new Intent(CanvasActivity.this,AddCanvasItem.class);
            startActivityForResult(intent,GETIMAGE);*/
            ChooseAction();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NewApi")
    @Override
    public void onReturnValue(Bitmap image, String action) {

        if (action.equalsIgnoreCase("Remove")) {

            AnimationSet snowMov1 = new AnimationSet(true);
            RotateAnimation rotate1 = new RotateAnimation(0,360, Animation.RELATIVE_TO_SELF,0.5f , Animation.RELATIVE_TO_SELF,0.5f );
            rotate1.setStartOffset(0);
            rotate1.setDuration(100);
            rotate1.setRepeatCount(2);
            snowMov1.addAnimation(rotate1);
            TranslateAnimation trans1 =  new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.1f, Animation.RELATIVE_TO_PARENT, 0.9f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, -0.9f);
            trans1.setDuration(300);
            snowMov1.addAnimation(trans1);

            selectedimage.setAnimation(snowMov1);
            canvas_layout.removeView(selectedimage);
            noofitems = noofitems - 1;

        } else if (action.equalsIgnoreCase("flip")) {

            if (flip_type.equals("0")) {

                flip_type = "1";

            } else {

                flip_type = "0";

            }
            selectedimage.setImageBitmap(image);

        } else if (action.equalsIgnoreCase("clone")) {

            CreateImage(image, "clone");

        } else if (action.equalsIgnoreCase("forward")) {

            int position1 = 0;
            int position2 = 0;
            float next_image_depth = 0, selected_image_depth = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Call some material design APIs here
                if (canvasitems.size() > 1) {

                    for (int i = 0; i < canvasitems.size(); i++) {


                        next_image_depth = selectedimage.getZ();
                        selected_image_depth = selectedimage.getZ() + 1;

                        if (canvasitems.get(i).getZ() == selectedimage.getZ() + 1) {

                            position1 = i;

                        }

                        if (canvasitems.get(i).getZ() == selectedimage.getZ()) {

                            position2 = i;

                        }

                    }

                    canvasitems.get(position1).setZ(next_image_depth);
                    canvasitems.get(position2).setZ(selected_image_depth);

                    ((View) selectedimage.getParent()).requestLayout();

                }

            } else {

                selectedimage.bringToFront();
                ((View) selectedimage.getParent()).requestLayout();

            }


        } else if (action.equalsIgnoreCase("backward")) {

            int position1 = 0;
            int position2 = 0;
            float current_image_depth = 0, selected_image_depth = 0;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                if (canvasitems.size() > 1) {

                    for (int i = 0; i < canvasitems.size(); i++) {


                        current_image_depth = selectedimage.getZ();
                        selected_image_depth = selectedimage.getZ() - 1;

                        if (canvasitems.get(i).getZ() == selectedimage.getZ() - 1) {

                            position1 = i;

                        }

                        if (canvasitems.get(i).getZ() == selectedimage.getZ()) {

                            position2 = i;

                        }

                    }

                    canvasitems.get(position1).setZ(current_image_depth);
                    canvasitems.get(position2).setZ(selected_image_depth);

                    ((View) selectedimage.getParent()).requestLayout();

                }

            } else {

                Wodrob.sendViewToBack(selectedimage);
                ((View) selectedimage.getParent()).requestLayout();

            }


        }

    }

    @Override
    public void OnDrag(Matrix matrix, RelativeLayout.LayoutParams params, Float rotation) {

        selectedimage.setImageMatrix(matrix);
        selectedimage.setLayoutParams(params);
        selectedimage.setRotation(rotation);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GETIMAGE) {

            image_path = data.getStringExtra("image_path");
            selected_product_id = data.getStringExtra("selected_product_id");
            in_wish_list = data.getStringExtra("in_wish_list");
            selected_image_id = data.getStringExtra("selected_image_id");

            if (!image_path.equals("")) {

                CreateImage(null, "remote");

            }

        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_add_item) {

            Intent intent = new Intent(CanvasActivity.this, AddCanvasItem.class);
            intent.putExtra("type","item");
            startActivityForResult(intent, GETIMAGE);

        } else if (v.getId() == R.id.btn_undo) {


            if(parent_layout_array_list.size() != 0) {
                //undo codde
                int position = parent_layout_array_list.size() - 1;
                if (parent_layout_array_list.get(position).getType().equalsIgnoreCase("Add")) {

                    canvas_layout.removeView(parent_layout_array_list.get(position).getView());
                    redo_array_list.add(parent_layout_array_list.get(position));
                    parent_layout_array_list.remove(position);

                } else if (parent_layout_array_list.get(position).getType().equalsIgnoreCase("action")) {

                    redo_array_list.add(parent_layout_array_list.get(position));
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) parent_layout_array_list.get(position).getView().getLayoutParams();
                    params.leftMargin = parent_layout_array_list.get(position-1).getLeft_margin();
                    params.topMargin = parent_layout_array_list.get(position-1).getTop_margin();
                    params.width = parent_layout_array_list.get(position-1).getWidth();
                    params.height = parent_layout_array_list.get(position - 1).getHeight();

                    parent_layout_array_list.get(position).getView().setLayoutParams(params);

                    parent_layout_array_list.get(position).getMatrix().setScale(parent_layout_array_list.get(position-1).getMatrixx(), parent_layout_array_list.get(position-1).getMatrixy());
                    parent_layout_array_list.get(position).getView().setImageMatrix(parent_layout_array_list.get(position - 1).getMatrix());

                    parent_layout_array_list.get(position).getView().setRotation(parent_layout_array_list.get(position - 1).getAngle());
                    parent_layout_array_list.remove(position);

                }
            }

        } else if (v.getId() == R.id.btn_redo) {

            if (redo_array_list.size() != 0) {

                int position = redo_array_list.size() - 1;

                if (redo_array_list.get(position).getType().equalsIgnoreCase("Add")) {

                    canvas_layout.addView(redo_array_list.get(position).getView());
                    parent_layout_array_list.add(redo_array_list.get(position));
                    redo_array_list.remove(position);

                } else if (redo_array_list.get(position).getType().equalsIgnoreCase("action")) {

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) redo_array_list.get(position).getView().getLayoutParams();
                    params.leftMargin = redo_array_list.get(position).getLeft_margin();
                    params.topMargin = redo_array_list.get(position).getTop_margin();
                    params.width = redo_array_list.get(position).getWidth();
                    params.height = redo_array_list.get(position).getHeight();
                    redo_array_list.get(position).getView().setLayoutParams(params);

                    redo_array_list.get(position).getMatrix().setScale(redo_array_list.get(position).getMatrixx(),redo_array_list.get(position).getMatrixy());
                    redo_array_list.get(position).getView().setImageMatrix(redo_array_list.get(position).getMatrix());

                    redo_array_list.get(position).getView().setRotation(redo_array_list.get(position).getAngle());

                    parent_layout_array_list.add(redo_array_list.get(position));
                    redo_array_list.remove(position);

                }

            }
            //rl_parent_canvas.addView(parent_layout_array_list.get(0));
            // rl_parent_canvas.invalidate();

        }else if(v.getId() == R.id.btn_center){



        }

    }

    private void ChooseAction() {

        final Dialog dialog = new Dialog(CanvasActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.canvas_publish_dialog);
        // dialog.setTitle("Add Image");

        // set the custom dialog components - text, image and button
        LinearLayout ll_publish_outfit = (LinearLayout) dialog.findViewById(R.id.ll_publish_outfit);
        LinearLayout ll_save_to_draft = (LinearLayout) dialog.findViewById(R.id.ll_save_to_draft);
        LinearLayout ll_style_chat = (LinearLayout) dialog.findViewById(R.id.ll_style_chat);
        LinearLayout ll_discard = (LinearLayout) dialog.findViewById(R.id.ll_discard);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        dialog.show();

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        ll_publish_outfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                if (noofitems == 0) {

                    Toast.makeText(getApplicationContext(), "Please add some items to canvas", Toast.LENGTH_LONG).show();

                } else {

                    int temp = canvas_layout.getChildCount();

                    Bitmap bitmap = Bitmap.createBitmap(canvas_layout.getDrawingCache());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    canvas_layout.destroyDrawingCache();

                    Intent intent = new Intent(CanvasActivity.this, PublishActivity.class);
                    intent.putExtra("publish_image", byteArray);
                    startActivity(intent);

                }


            }
        });

        ll_save_to_draft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                for (int i = 0; i < canvasitems.size(); i++) {

                    String imag_tag = canvasitems.get(i).getTag().toString();

                    Toast.makeText(getApplicationContext(), imag_tag, Toast.LENGTH_LONG).show();

                }

            }
        });

        ll_style_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        ll_discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                ConfirmDiscard();

            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

    }

    /*popup for discard */
    public void ConfirmDiscard() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                CanvasActivity.this);

// Setting Dialog Title
        alertDialog2.setTitle("Confirm Discard");

// Setting Dialog Message
        alertDialog2.setMessage("Are you sure you want discard the outfit ?");

// Setting Icon to Dialog

// Setting Positive "Yes" Btn
        alertDialog2.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        canvas_layout.removeAllViews();
                        dialog.cancel();
                        finish();
                    }
                });

// Setting Negative "NO" Btn
        alertDialog2.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog

                        dialog.cancel();
                    }
                });

// Showing Alert Dialog
        alertDialog2.show();
    }

    @Override
    public void onBackPressed() {

        if (isfragment_active) {

            getSupportFragmentManager().beginTransaction().remove(addItemFragment).commit();
            btn_add_item.setVisibility(View.VISIBLE);
            canvas_layout.setVisibility(View.VISIBLE);
            isfragment_active = false;

        } else {

            finish();

        }

    }

    @Override
    public void NewItem(String path) {

        getSupportFragmentManager().beginTransaction().remove(addItemFragment).commit();
        btn_add_item.setVisibility(View.VISIBLE);
        canvas_layout.setVisibility(View.VISIBLE);
        isfragment_active = false;

        image_path = path;
        CreateImage(null, "remote");

    }

}
