package com.wodrob.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.wodrob.app.model.ItemBodyTypeModel;
import com.wodrob.app.model.ItemBrandModel;
import com.wodrob.app.model.ItemBustSizeModel;
import com.wodrob.app.model.ItemCareTipsModel;
import com.wodrob.app.model.ItemCategoryModel;
import com.wodrob.app.model.ItemColorModel;
import com.wodrob.app.model.ItemOccasionModel;
import com.wodrob.app.model.ItemSizeModel;
import com.wodrob.app.model.ItemSkinToneModel;
import com.wodrob.app.model.ItemStoreModel;
import com.wodrob.app.model.ItemStyleModel;
import com.wodrob.app.model.ItemTagModel;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 17/12/15.
 */
public class DbHelper extends SQLiteOpenHelper {


    Context context;


    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "wodrob";

    // Table Names
    private static final String TABLE_CATEGORY = "category";
    private static final String TABLE_STYLE = "style";
    private static final String TABLE_CARE_TIP = "care_tip";
    private static final String TABLE_COLOR = "color";
    private static final String TABLE_OCCASION = "occasion";
    private static final String TABLE_SIZE = "size";
    private static final String TABLE_TAG = "tag";
    private static final String TABLE_BRAND = "brand";
    private static final String TABLE_STORE = "store";
    private static final String TABLE_BODY_TYPE = "body_type";
    private static final String TABLE_SKIN_TONE = "skin_tone";
    private static final String TABLE_BUST_SIZE = "bust_size";

    private static final String KEY_CATEGORY_ID = "category_id";
    private static final String KEY_STYLE_ID = "style_id";
    private static final String KEY_CARE_TIP_ID = "care_tip_id";
    private static final String KEY_COLOR_ID = "color_id";
    private static final String KEY_OCCASSION_ID = "occassion_id";
    private static final String KEY_SIZE_ID = "size_id";
    private static final String KEY_TAG_ID = "tag_id";
    private static final String KEY_BRAND_ID = "brand_id";
    private static final String KEY_STORE_ID = "store_id";
    private static final String KEY_BODY_TYPE_ID = "body_type_id";
    private static final String KEY_SKIN_TONE_ID = "skin_tone_id";
    private static final String KEY_BUST_SIZE_ID = "bust_size_id";


    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_UPDATED_AT = "updated_at";
    private static final String KEY_IMAGE_PATH = "image_path";
    private static final String KEY_SELECTED_IMAGE_PATH = "selected_image";
    private static final String KEY_UNSELECTED_IMAGE_PATH = "unselected_image";


    //category colom names
    private static final String KEY_CATEGORY_NAME = "category_name";
    private static final String KEY_CATEGORY_PARENT_ID = "parent_id";

    /*colom for style table*/
    private static final String KEY_STYLE_NAME = "style_name";

    /*caretips colom names*/

    private static final String KEY_CARE_TIP_NAME = "care_tip_name";

    /*color colom names*/

    private static final String KEY_HEX_CODE = "hexcode";
    private static final String KEY_COLOR_NAME = "color_name";
    private static final String KEY_DESCRIPTION = "description";

    /*occassion colom names*/
    private static final String KEY_OCCASSION_NAME = "occasion_name";



    /*size colom names*/

    private static final String KEY_SIZE_VALUE = "value";

    /*tags colom names*/

    private static final String KEY_TAG_NAME = "tag_name";

    /*brand colom names*/
    private static final String KEY_BRAND_NAME = "brand_name";

    /*store colom names*/
    private static final String KEY_STORE_NAME = "store_name";
    private static final String KEY_MODE_REF_TYPE = "mode_ref_type";
    private static final String KEY_MODE_REF = "mode_ref";

    /*body type colom names*/
    private static final String KEY_GENDER = "gender";
    private static final String KEY_BODY_TYPE_NAME = "body_type_name";
    private static final String KEY_BODY_TYPE_DESCRIPTION = "body_type_description";
    private static final String KEY_IMAGE_WIDTH = "width";
    private static final String KEY_IMAGE_HEIGHT = "image_height";

    /*skin tone  colom names*/

    private static final String KEY_SKIN_TONE_NAME = "skin_tone_name";
    private static final String KEY_SKIN_TONE_DESCRIPTION = "skin_tone_description";

    /*bust size colom names*/
    private static final String KEY_BAND = "band";
    private static final String KEY_CUP = "cup";
    private static final String KEY_DISPLAY = "display";

    // Table Create Statements
    // Todo table create statement

    private static final String CREATE_TABLE_CATEGORY = "CREATE TABLE "
            + TABLE_CATEGORY + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_CATEGORY_ID + " TEXT UNIQUE," + KEY_CATEGORY_NAME + " TEXT," + KEY_SELECTED_IMAGE_PATH + " TEXT," + KEY_UNSELECTED_IMAGE_PATH + " TEXT," + KEY_CATEGORY_PARENT_ID + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";

    /*creating item style table*/
    private static final String CREATE_TABLE_STYLE = "CREATE TABLE "
            + TABLE_STYLE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_STYLE_ID + " TEXT UNIQUE," + KEY_STYLE_NAME + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";

    /*creating item caretip table*/
    private static final String CREATE_TABLE_CARE_TIP = "CREATE TABLE "
            + TABLE_CARE_TIP + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_CARE_TIP_ID + " TEXT UNIQUE," + KEY_CARE_TIP_NAME + " TEXT," + KEY_UPDATED_AT + " TEXT," + KEY_IMAGE_PATH + " TEXT" + ")";

    /*creating item color table*/
    private static final String CREATE_TABLE_COLOR = "CREATE TABLE "
            + TABLE_COLOR + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_COLOR_ID + " TEXT UNIQUE," + KEY_COLOR_NAME + " TEXT," + KEY_HEX_CODE + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";

    /*creating item color table*/
    private static final String CREATE_TABLE_OCCASION = "CREATE TABLE "
            + TABLE_OCCASION + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_OCCASSION_ID + " TEXT UNIQUE," + KEY_OCCASSION_NAME + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";
    /*creating item color table*/
    private static final String CREATE_TABLE_SIZE = "CREATE TABLE "
            + TABLE_SIZE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_SIZE_ID + " TEXT UNIQUE," + KEY_SIZE_VALUE + " TEXT," + KEY_CATEGORY_ID + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";

    /*creating item color table*/
    private static final String CREATE_TABLE_TAGS = "CREATE TABLE "
            + TABLE_TAG + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_TAG_ID + " TEXT UNIQUE," + KEY_TAG_NAME + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";

    /*creating item brand table*/
    private static final String CREATE_TABLE_BRAND = "CREATE TABLE "
            + TABLE_BRAND + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_BRAND_ID + " TEXT UNIQUE," + KEY_BRAND_NAME + " TEXT," + KEY_IMAGE_PATH + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";


    /*creating item Store table*/
    private static final String CREATE_TABLE_STORE = "CREATE TABLE "
            + TABLE_STORE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_STORE_ID + " TEXT UNIQUE," + KEY_STORE_NAME + " TEXT," + KEY_MODE_REF_TYPE + " TEXT," + KEY_MODE_REF + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";


    /*creating table body type*/
    private static final String CREATE_TABLE_BODY_TYPE = "CREATE TABLE "
            + TABLE_BODY_TYPE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_BODY_TYPE_ID + " TEXT UNIQUE," + KEY_BODY_TYPE_NAME + " TEXT," + KEY_BODY_TYPE_DESCRIPTION + " TEXT," + KEY_GENDER + " TEXT," + KEY_IMAGE_PATH + " TEXT," + KEY_IMAGE_WIDTH + " TEXT," + KEY_IMAGE_HEIGHT + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";


    /*creating table body type*/
    private static final String CREATE_TABLE_SKIN_TONE = "CREATE TABLE "
            + TABLE_SKIN_TONE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_SKIN_TONE_ID + " TEXT UNIQUE," + KEY_SKIN_TONE_NAME + " TEXT," + KEY_SKIN_TONE_DESCRIPTION + " TEXT," + KEY_IMAGE_PATH + " TEXT," + KEY_IMAGE_WIDTH + " TEXT," + KEY_IMAGE_HEIGHT + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";


    /*creating table Bust size*/
    private static final String CREATE_TABLE_BUST_SIZE = "CREATE TABLE "
            + TABLE_BUST_SIZE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_BUST_SIZE_ID + " TEXT UNIQUE," + KEY_BAND + " TEXT," + KEY_CUP + " TEXT," + KEY_DISPLAY + " TEXT," + KEY_UPDATED_AT + " TEXT" + ")";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_CATEGORY);
        db.execSQL(CREATE_TABLE_STYLE);
        db.execSQL(CREATE_TABLE_CARE_TIP);
        db.execSQL(CREATE_TABLE_COLOR);
        db.execSQL(CREATE_TABLE_OCCASION);
        db.execSQL(CREATE_TABLE_SIZE);
        db.execSQL(CREATE_TABLE_TAGS);
        db.execSQL(CREATE_TABLE_BRAND);
        db.execSQL(CREATE_TABLE_BODY_TYPE);
        db.execSQL(CREATE_TABLE_STORE);
        db.execSQL(CREATE_TABLE_SKIN_TONE);
        db.execSQL(CREATE_TABLE_BUST_SIZE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STYLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARE_TIP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COLOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OCCASION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SIZE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COLOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRAND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BODY_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SKIN_TONE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUST_SIZE);

        onCreate(db);
    }

    /*
        method to insert into catgory table
 */
    public long update_category(ItemCategoryModel categoryModel, String id) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_ID, categoryModel.getCategory_id().toString());
        values.put(KEY_CATEGORY_NAME, categoryModel.getCategory_name().toString());
        values.put(KEY_SELECTED_IMAGE_PATH, categoryModel.getSelected_path());
        values.put(KEY_UNSELECTED_IMAGE_PATH, categoryModel.getUnselected_path());
        values.put(KEY_UPDATED_AT, categoryModel.getUpdated_at().toString());
        values.put(KEY_CATEGORY_PARENT_ID, categoryModel.getParent_id().toString());
        // insert row
        long category_id = db.update(TABLE_CATEGORY, values, KEY_CATEGORY_ID + "=" + id, null);
        return category_id;
    }

    public long insert_category(ItemCategoryModel categoryModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        long category_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_ID, categoryModel.getCategory_id().toString());
        values.put(KEY_CATEGORY_NAME, categoryModel.getCategory_name().toString());
        values.put(KEY_SELECTED_IMAGE_PATH, categoryModel.getSelected_path());
        values.put(KEY_UNSELECTED_IMAGE_PATH, categoryModel.getUnselected_path());
        values.put(KEY_UPDATED_AT, categoryModel.getUpdated_at().toString());
        values.put(KEY_CATEGORY_PARENT_ID, categoryModel.getParent_id().toString());
        // insert row
        try {
            category_id = db.insertOrThrow(TABLE_CATEGORY, null, values);

        } catch (SQLiteConstraintException e) {

            update_category(categoryModel, categoryModel.getCategory_id().toString());

        }
        return category_id;
    }


    /*delete a row */

    public boolean DeleteCategory(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_CATEGORY, KEY_CATEGORY_ID + "=" + id, null) > 0;
    }


    /*get category_id from name*/
    public String GetCategory_id(String category_name) {
        String category_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY + " WHERE " + KEY_CATEGORY_NAME + " = " + "'" + category_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                category_id = c.getString(c.getColumnIndex(KEY_CATEGORY_ID));

            } while (c.moveToNext());
        }

        c.close();


        return category_id;
    }

    /*
 * getting all categories
 * */
    public ArrayList<ItemCategoryModel> getAllCategoryItems() {
        ArrayList<ItemCategoryModel> categorylist = new ArrayList<ItemCategoryModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemCategoryModel categoryModel = new ItemCategoryModel(c.getString(c.getColumnIndex(KEY_CATEGORY_ID)), c.getString(c.getColumnIndex(KEY_CATEGORY_NAME)), c.getString(c.getColumnIndex(KEY_SELECTED_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_UNSELECTED_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)), c.getString(c.getColumnIndex(KEY_CATEGORY_PARENT_ID)));
                // adding to category list
                categorylist.add(categoryModel);
            } while (c.moveToNext());
        }

        c.close();
        return categorylist;
    }

    /*getting datas based on parent id*/
    public ArrayList<ItemCategoryModel> getSubCategoryData(String parent_id) {

        ArrayList<ItemCategoryModel> categorylist = new ArrayList<ItemCategoryModel>();

        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY + " WHERE " + KEY_CATEGORY_PARENT_ID + " = " + "'" + parent_id + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemCategoryModel categoryModel = new ItemCategoryModel(c.getString(c.getColumnIndex(KEY_CATEGORY_ID)), c.getString(c.getColumnIndex(KEY_CATEGORY_NAME)), c.getString(c.getColumnIndex(KEY_SELECTED_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_UNSELECTED_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)), c.getString(c.getColumnIndex(KEY_CATEGORY_PARENT_ID)));
                // adding to category list
                categorylist.add(categoryModel);
            } while (c.moveToNext());
        }

        c.close();
        return categorylist;
    }

    /*Get parent id*/
    /*getting datas based on parent id*/
    public String get_category_item_from_parent_id(String name) {

        String parent_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY + " WHERE " + KEY_CATEGORY_NAME + " = " + "'" + name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                parent_id = c.getString(c.getColumnIndex(KEY_CATEGORY_PARENT_ID));
                // adding to category list
            } while (c.moveToNext());
        }

        c.close();
        return parent_id;
    }
    /*methods for item style*/
    /*inserting*/

    public long update_style(ItemStyleModel styleModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STYLE_ID, styleModel.getStyle_id().toString());
        values.put(KEY_STYLE_NAME, styleModel.getStyle_name().toString());
        values.put(KEY_UPDATED_AT, styleModel.getUpdated_at().toString());
        // insert row
        long style_id = db.update(TABLE_STYLE, values, KEY_STYLE_ID + "=" + id, null);
        return style_id;
    }

    public long insert_style(ItemStyleModel styleModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        long style_id = 0;

        ContentValues values = new ContentValues();
        values.put(KEY_STYLE_ID, styleModel.getStyle_id().toString());
        values.put(KEY_STYLE_NAME, styleModel.getStyle_name().toString());
        values.put(KEY_UPDATED_AT, styleModel.getUpdated_at().toString());
        // insert row
        try {

            style_id = db.insertOrThrow(TABLE_STYLE, null, values);

        } catch (SQLiteConstraintException e) {

            update_style(styleModel, styleModel.getStyle_id().toString());

        }
        return style_id;
    }

        /*delete a row */

    public boolean DeleteStyle(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_STYLE, KEY_STYLE_ID + "=" + id, null) > 0;
    }

    /*fetching dstyle based on id*/
    public String GetStyle_id(String style_name) {
        String style_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_STYLE + " WHERE " + KEY_STYLE_NAME + " = " + "'" + style_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                style_id = c.getString(c.getColumnIndex(KEY_STYLE_ID));

            } while (c.moveToNext());
        }

        c.close();


        return style_id;
    }

    /*fetching all data*/
    public ArrayList<ItemStyleModel> getAllStyleItems() {
        ArrayList<ItemStyleModel> stylelist = new ArrayList<ItemStyleModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_STYLE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemStyleModel styleModel = new ItemStyleModel(c.getString(c.getColumnIndex(KEY_STYLE_ID)), c.getString(c.getColumnIndex(KEY_STYLE_NAME)), null, null, c.getString(c.getColumnIndex(KEY_UPDATED_AT)));
                // adding to category list
                stylelist.add(styleModel);
            } while (c.moveToNext());
        }

        c.close();
        return stylelist;
    }

    /*methods for care tips*/
    /*inserting*/

    public long update_care_tip(ItemCareTipsModel careTipsModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CARE_TIP_ID, careTipsModel.getCare_tip_id().toString());
        values.put(KEY_CARE_TIP_NAME, careTipsModel.getName().toString());
        values.put(KEY_UPDATED_AT, careTipsModel.getUpdated_at().toString());
        values.put(KEY_IMAGE_PATH, careTipsModel.getImage_path().toString());
        // insert row
        long care_tip_id = db.update(TABLE_CARE_TIP, values, KEY_CARE_TIP_ID + "=" + id, null);
        return care_tip_id;
    }

    public long insert_care_tip(ItemCareTipsModel careTipsModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        long care_tip_id = 0;

        ContentValues values = new ContentValues();
        values.put(KEY_CARE_TIP_ID, careTipsModel.getCare_tip_id().toString());
        values.put(KEY_CARE_TIP_NAME, careTipsModel.getName().toString());
        values.put(KEY_UPDATED_AT, careTipsModel.getUpdated_at().toString());
        values.put(KEY_IMAGE_PATH, careTipsModel.getImage_path().toString());
        // insert row
        try {

            care_tip_id = db.insertOrThrow(TABLE_CARE_TIP, null, values);

        } catch (SQLiteConstraintException e) {

            update_care_tip(careTipsModel, careTipsModel.getCare_tip_id().toString());

        }
        return care_tip_id;
    }

        /*delete a row */

    public boolean DeleteCareTip(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_CARE_TIP, KEY_CARE_TIP_ID + "=" + id, null) > 0;
    }

/*get caret tip image--- parameter name*/

    public String GetCareTipImage(String care_tip_name) {
        String care_Tip_mage = "";
        String selectQuery = "SELECT  * FROM " + TABLE_CARE_TIP + " WHERE " + KEY_CARE_TIP_NAME + " = " + "'" + care_tip_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                care_Tip_mage = c.getString(c.getColumnIndex(KEY_IMAGE_PATH));

            } while (c.moveToNext());
        }

        c.close();


        return care_Tip_mage;
    }

    /*fetching all data*/
    public ArrayList<ItemCareTipsModel> getAllcaretip() {
        ArrayList<ItemCareTipsModel> caretiplist = new ArrayList<ItemCareTipsModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_CARE_TIP;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemCareTipsModel caretipmodel = new ItemCareTipsModel(c.getString(c.getColumnIndex(KEY_CARE_TIP_ID)), c.getString(c.getColumnIndex(KEY_CARE_TIP_NAME)), c.getString(c.getColumnIndex(KEY_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));
                // adding to category list
                caretiplist.add(caretipmodel);
            } while (c.moveToNext());
        }

        c.close();
        return caretiplist;
    }

    /*method for color table*/

    public long update_color(ItemColorModel colorModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_COLOR_ID, colorModel.getColor_id().toString());
        values.put(KEY_COLOR_NAME, colorModel.getName().toString());
        values.put(KEY_HEX_CODE, colorModel.getHex_code().toString());
        values.put(KEY_UPDATED_AT, colorModel.getUpdated_at().toString());
        // insert row
        long color_id = db.update(TABLE_COLOR, values, KEY_COLOR_ID + "=" + id, null);
        return color_id;
    }

    public long insert_color(ItemColorModel colorModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        long color_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_COLOR_ID, colorModel.getColor_id().toString());
        values.put(KEY_COLOR_NAME, colorModel.getName().toString());
        values.put(KEY_HEX_CODE, colorModel.getHex_code().toString());
        values.put(KEY_UPDATED_AT, colorModel.getUpdated_at().toString());
        // insert row
        try {

            color_id = db.insertOrThrow(TABLE_COLOR, null, values);

        } catch (SQLiteConstraintException e) {

            update_color(colorModel, colorModel.getColor_id().toString());

        }
        return color_id;
    }

        /*delete a row */

    public boolean DeleteColor(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_COLOR, KEY_COLOR_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemColorModel> getAllcolors() {
        ArrayList<ItemColorModel> colorlist = new ArrayList<ItemColorModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_COLOR;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemColorModel colorModel = new ItemColorModel(c.getString(c.getColumnIndex(KEY_COLOR_ID)), c.getString(c.getColumnIndex(KEY_COLOR_NAME)), c.getString(c.getColumnIndex(KEY_HEX_CODE)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));
                // adding to category list
                colorlist.add(colorModel);
            } while (c.moveToNext());
        }

        c.close();
        return colorlist;
    }

    /*get hex code from color id*/

    /*fetching color based on id*/
    public String GetHexCode(String color_id) {
        String hexcode = "";
        String selectQuery = "SELECT  * FROM " + TABLE_COLOR + " WHERE " + KEY_COLOR_ID + " = " + "'" + color_id + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                hexcode = c.getString(c.getColumnIndex(KEY_HEX_CODE));

            } while (c.moveToNext());
        }

        c.close();

//        hardcoded value of color is null

        if (hexcode.equalsIgnoreCase("")) {

            hexcode = "5e5f5f";
        }

        return hexcode;
    }

    /*ends*/

    /*get colors id list based on name*/

    /*fetching color based on id*/
    public String GetColor_id(String color_name) {
        String color_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_COLOR + " WHERE " + KEY_COLOR_NAME + " = " + "'" + color_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                color_id = c.getString(c.getColumnIndex(KEY_COLOR_ID));

            } while (c.moveToNext());
        }

        c.close();


        return color_id;
    }

    /*method for occassion*/
    public long update_occassion(ItemOccasionModel occasionModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_OCCASSION_ID, occasionModel.getOccassion_id().toString());
        values.put(KEY_OCCASSION_NAME, occasionModel.getOccasion_name().toString());
        values.put(KEY_DESCRIPTION, occasionModel.getDescription().toString());
        values.put(KEY_UPDATED_AT, occasionModel.getUpdated_at().toString());
        // insert row
        long occassion_id = db.update(TABLE_OCCASION, values, KEY_OCCASSION_ID + "=" + id, null);
        return occassion_id;
    }

    public long insert_occassion(ItemOccasionModel occasionModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        long occassion_id = 0;

        ContentValues values = new ContentValues();
        values.put(KEY_OCCASSION_ID, occasionModel.getOccassion_id().toString());
        values.put(KEY_OCCASSION_NAME, occasionModel.getOccasion_name().toString());
        values.put(KEY_DESCRIPTION, occasionModel.getDescription().toString());
        values.put(KEY_UPDATED_AT, occasionModel.getUpdated_at().toString());
        // insert row
        try {

            occassion_id = db.insertOrThrow(TABLE_OCCASION, null, values);

        } catch (SQLiteConstraintException e) {

            update_occassion(occasionModel, occasionModel.getOccassion_id().toString());

        }
        return occassion_id;
    }

        /*delete a row */

    public boolean DeleteOccassion(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_OCCASION, KEY_OCCASSION_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemOccasionModel> getAlloccasion() {
        ArrayList<ItemOccasionModel> occasionlist = new ArrayList<ItemOccasionModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_OCCASION;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemOccasionModel occassionModel = new ItemOccasionModel(c.getString(c.getColumnIndex(KEY_OCCASSION_ID)), c.getString(c.getColumnIndex(KEY_OCCASSION_NAME)), c.getString(c.getColumnIndex(KEY_DESCRIPTION)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));
                // adding to category list
                occasionlist.add(occassionModel);
            } while (c.moveToNext());
        }

        c.close();
        return occasionlist;
    }


    public String Get_Occassion_Id(String occassion_name) {
        String occassion_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_OCCASION + " WHERE " + KEY_OCCASSION_NAME + " = " + "'" + occassion_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                occassion_id = c.getString(c.getColumnIndex(KEY_OCCASSION_ID));

            } while (c.moveToNext());
        }

        c.close();


        return occassion_id;
    }

    /*method for occassion ends*/

    /*methods for size starts*/
    public long update_size(ItemSizeModel sizeModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        long size_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_SIZE_ID, sizeModel.getSize_id().toString());
        values.put(KEY_SIZE_VALUE, sizeModel.getValue().toString());
        values.put(KEY_UPDATED_AT, sizeModel.getUpdated_at().toString());
        // insert row
        size_id = db.update(TABLE_SIZE, values, KEY_SIZE_ID + "=" + id, null);

        return size_id;
    }

    public long insert_size(ItemSizeModel sizeModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        long size_id = 0;

        ContentValues values = new ContentValues();
        values.put(KEY_SIZE_ID, sizeModel.getSize_id().toString());
        values.put(KEY_SIZE_VALUE, sizeModel.getValue().toString());
        values.put(KEY_UPDATED_AT, sizeModel.getUpdated_at().toString());
        // insert row
        try {
            size_id = db.insertOrThrow(TABLE_SIZE, null, values);

        } catch (SQLiteConstraintException e) {
            //  Log.e("status","Already added");
            update_size(sizeModel, sizeModel.getSize_id().toString());

        }
        return size_id;
    }

        /*delete a row */

    public boolean DeleteSize(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_SIZE, KEY_SIZE_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemSizeModel> getAllsize() {
        ArrayList<ItemSizeModel> sizelist = new ArrayList<ItemSizeModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_SIZE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemSizeModel sizeModel = new ItemSizeModel(c.getString(c.getColumnIndex(KEY_SIZE_ID)), c.getString(c.getColumnIndex(KEY_SIZE_VALUE)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)), c.getString(c.getColumnIndex(KEY_CATEGORY_ID)));
                // adding to category list
                sizelist.add(sizeModel);
            } while (c.moveToNext());
        }

        c.close();
        return sizelist;
    }

/*size based on category or subcategory*/

    public ArrayList<ItemSizeModel> GetSizeBasedOnCategory(String id) {
        ArrayList<ItemSizeModel> sizelist = new ArrayList<ItemSizeModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_SIZE + " WHERE " + "category_id" + " = " + "'" + id + "'";
        ;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemSizeModel sizeModel = new ItemSizeModel(c.getString(c.getColumnIndex(KEY_SIZE_ID)), c.getString(c.getColumnIndex(KEY_SIZE_VALUE)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)), c.getString(c.getColumnIndex(KEY_CATEGORY_ID)));
                // adding to category list
                sizelist.add(sizeModel);
            } while (c.moveToNext());
        }

        c.close();
        return sizelist;
    }

    /*Method to get size based on id*/


    public String GetSizeBasedOnId(String id) {
        String size = "";
        String selectQuery = "SELECT  * FROM " + TABLE_SIZE + " WHERE " + KEY_SIZE_ID + " = " + "'" + id + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                size = c.getString(c.getColumnIndex(KEY_SIZE_VALUE));

            } while (c.moveToNext());
        }

        c.close();


        return size;
    }

    /*end*/

    /*methods for additional tags*/

    public long update_tags(ItemTagModel tagModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TAG_ID, tagModel.getTag_id().toString());
        values.put(KEY_TAG_NAME, tagModel.getTag_name().toString());
        values.put(KEY_UPDATED_AT, tagModel.getUpdated_at().toString());
        // insert row
        long tag_id = db.update(TABLE_TAG, values, KEY_TAG_ID + "=" + id, null);
        return tag_id;
    }


    public long insert_tags(ItemTagModel tagModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        long tag_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_TAG_ID, tagModel.getTag_id().toString());
        values.put(KEY_TAG_NAME, tagModel.getTag_name().toString());
        values.put(KEY_UPDATED_AT, tagModel.getUpdated_at().toString());
        // insert row
        try {

            tag_id = db.insertOrThrow(TABLE_TAG, null, values);

        } catch (SQLiteConstraintException e) {

            update_tags(tagModel, tagModel.getTag_id().toString());

        }
        return tag_id;
    }


        /*delete a row */

    public boolean DeleteTags(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_TAG, KEY_TAG_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemTagModel> getAlltags() {
        ArrayList<ItemTagModel> taglist = new ArrayList<ItemTagModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_TAG;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemTagModel tagModel = new ItemTagModel(c.getString(c.getColumnIndex(KEY_TAG_ID)), c.getString(c.getColumnIndex(KEY_TAG_NAME)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));
                // adding to category list
                taglist.add(tagModel);
            } while (c.moveToNext());
        }

        c.close();
        return taglist;
    }

    /*tags based on id*/

    public String Get_tag_id(String tag_name) {
        String tag_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_TAG + " WHERE " + KEY_TAG_NAME + " = " + "'" + tag_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                tag_id = c.getString(c.getColumnIndex(KEY_TAG_ID));

            } while (c.moveToNext());
        }

        c.close();


        return tag_id;
    }


    public long update_brands(ItemBrandModel brandModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_BRAND_ID, brandModel.getBrand_id().toString());
        values.put(KEY_BRAND_NAME, brandModel.getBrand_name().toString());
        values.put(KEY_UPDATED_AT, brandModel.getUpdated_at().toString());
        values.put(KEY_IMAGE_PATH, brandModel.getImage_path().toString());

        // insert row
        long brand_id = db.update(TABLE_BRAND, values, KEY_BRAND_ID + "=" + id, null);
        return brand_id;
    }


    public long insert_brands(ItemBrandModel brandModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        long tag_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_BRAND_ID, brandModel.getBrand_id().toString());
        values.put(KEY_BRAND_NAME, brandModel.getBrand_name().toString());
        values.put(KEY_UPDATED_AT, brandModel.getUpdated_at().toString());
        values.put(KEY_IMAGE_PATH, brandModel.getImage_path().toString());

        // insert row
        try {

            tag_id = db.insertOrThrow(TABLE_BRAND, null, values);

        } catch (SQLiteConstraintException e) {

            update_brands(brandModel, brandModel.getBrand_id().toString());

        }
        return tag_id;
    }

        /*delete a row */

    public boolean DeleteBrand(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_BRAND, KEY_BRAND_ID + "=" + id, null) > 0;
    }


    /*fetching all data*/
    public ArrayList<ItemBrandModel> getAllbrnads() {
        ArrayList<ItemBrandModel> brandlist = new ArrayList<ItemBrandModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_BRAND;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemBrandModel brandModel = new ItemBrandModel(c.getString(c.getColumnIndex(KEY_BRAND_ID)), c.getString(c.getColumnIndex(KEY_BRAND_NAME)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)), c.getString(c.getColumnIndex(KEY_IMAGE_PATH)));

                // adding to category list
                brandlist.add(brandModel);
            } while (c.moveToNext());
        }

        c.close();
        return brandlist;
    }

    /*get category_id from name*/
    public String GetBrand_id(String brand_name) {
        String brand_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_BRAND + " WHERE " + KEY_BRAND_NAME + " = " + "'" + brand_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                brand_id = c.getString(c.getColumnIndex(KEY_BRAND_ID)) + "_" + c.getString(c.getColumnIndex(KEY_BRAND_NAME));

            } while (c.moveToNext());
        }

        c.close();


        return brand_id;
    }

    public String GetBrand_id_without_name(String brand_name) {
        String brand_id = "";
        String selectQuery = "SELECT  * FROM " + TABLE_BRAND + " WHERE " + KEY_BRAND_NAME + " = " + "'" + brand_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                brand_id = c.getString(c.getColumnIndex(KEY_BRAND_ID));

            } while (c.moveToNext());
        }

        c.close();


        return brand_id;
    }

    /*get brand image*/

    public String GetBrandLogo(String brand_name) {
        String brand_logo = "";
        String selectQuery = "SELECT  * FROM " + TABLE_BRAND + " WHERE " + KEY_BRAND_NAME + " = " + "'" + brand_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                brand_logo = c.getString(c.getColumnIndex(KEY_IMAGE_PATH));

            } while (c.moveToNext());
        }

        c.close();


        return brand_logo;
    }
    /*end*/

    public long update_store(ItemStoreModel storeModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STORE_ID, storeModel.getStore_id().toString());
        values.put(KEY_STORE_NAME, storeModel.getStore_name().toString());
        values.put(KEY_MODE_REF_TYPE, storeModel.getMode_ref_type());
        values.put(KEY_MODE_REF, storeModel.getMode_ref().toString());
        values.put(KEY_UPDATED_AT, storeModel.getUpdated_at().toString());
        // insert row
        long store_id = db.update(TABLE_STORE, values, KEY_STORE_ID + "=" + id, null);
        return store_id;
    }

    public long insert_store(ItemStoreModel storeModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        long store_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_STORE_ID, storeModel.getStore_id().toString());
        values.put(KEY_STORE_NAME, storeModel.getStore_name().toString());
        values.put(KEY_MODE_REF_TYPE, storeModel.getMode_ref_type());
        values.put(KEY_MODE_REF, storeModel.getMode_ref().toString());
        values.put(KEY_UPDATED_AT, storeModel.getUpdated_at().toString());
        // insert row
        try {

            store_id = db.insertOrThrow(TABLE_STORE, null, values);

        } catch (SQLiteConstraintException e) {
            update_store(storeModel, storeModel.getStore_id().toString());
        }
        return store_id;
    }

        /*delete a row */

    public boolean DeleteStore(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_STORE, KEY_STORE_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemStoreModel> getAllstores() {
        ArrayList<ItemStoreModel> storelist = new ArrayList<ItemStoreModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_STORE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemStoreModel storeModel = new ItemStoreModel(c.getString(c.getColumnIndex(KEY_STORE_ID)), c.getString(c.getColumnIndex(KEY_STORE_NAME)), c.getString(c.getColumnIndex(KEY_MODE_REF_TYPE)), c.getString(c.getColumnIndex(KEY_MODE_REF)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));

                // adding to category list
                storelist.add(storeModel);
            } while (c.moveToNext());
        }

        c.close();
        return storelist;
    }

    /*get store based on brandid*/
    public ArrayList<ItemStoreModel> GetStoreBasedOnBrandId(String brand_id) {
        ArrayList<ItemStoreModel> storelist = new ArrayList<ItemStoreModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_STORE + " WHERE " + KEY_MODE_REF + " = " + "'" + brand_id + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemStoreModel storeModel = new ItemStoreModel(c.getString(c.getColumnIndex(KEY_STORE_ID)), c.getString(c.getColumnIndex(KEY_STORE_NAME)), c.getString(c.getColumnIndex(KEY_MODE_REF_TYPE)), c.getString(c.getColumnIndex(KEY_MODE_REF)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));

                // adding to category list
                storelist.add(storeModel);
            } while (c.moveToNext());
        }

        c.close();
        return storelist;
    }

    /*get store based on brandid*/
    public String GetStoreId(String store_name) {
        String store_id = null;
        String selectQuery = "SELECT  * FROM " + TABLE_STORE + " WHERE " + KEY_STORE_NAME + " = " + "'" + store_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                store_id = c.getString(c.getColumnIndex(KEY_STORE_ID));
                // adding to category list
            } while (c.moveToNext());
        }

        c.close();
        return store_id;
    }

//    methods for body type


    public long update_body_type(ItemBodyTypeModel bodyTypeModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_BODY_TYPE_ID, bodyTypeModel.getBody_type_id().toString());
        values.put(KEY_BODY_TYPE_NAME, bodyTypeModel.getName().toString());
        values.put(KEY_BODY_TYPE_DESCRIPTION, bodyTypeModel.getDescription().toString());
        values.put(KEY_IMAGE_PATH, bodyTypeModel.getFilename().toString());
        values.put(KEY_IMAGE_WIDTH, bodyTypeModel.getImage_width().toString());
        values.put(KEY_IMAGE_HEIGHT, bodyTypeModel.getImage_height().toString());
        values.put(KEY_GENDER, bodyTypeModel.getGender().toString());
        values.put(KEY_UPDATED_AT, bodyTypeModel.getUpdated_at().toString());
        // insert row
        long body_type_id = db.update(TABLE_BODY_TYPE, values, KEY_BODY_TYPE_ID + "=" + id, null);
        return body_type_id;
    }

    public long insert_body_type(ItemBodyTypeModel bodyTypeModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        long body_type_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_BODY_TYPE_ID, bodyTypeModel.getBody_type_id().toString());
        values.put(KEY_BODY_TYPE_NAME, bodyTypeModel.getName().toString());
        values.put(KEY_BODY_TYPE_DESCRIPTION, bodyTypeModel.getDescription().toString());
        values.put(KEY_IMAGE_PATH, bodyTypeModel.getFilename().toString());
        values.put(KEY_IMAGE_WIDTH, bodyTypeModel.getImage_width().toString());
        values.put(KEY_IMAGE_HEIGHT, bodyTypeModel.getImage_height().toString());
        values.put(KEY_GENDER, bodyTypeModel.getGender().toString());
        values.put(KEY_UPDATED_AT, bodyTypeModel.getUpdated_at().toString());
        // insert row
        try {

            body_type_id = db.insertOrThrow(TABLE_BODY_TYPE, null, values);

        } catch (SQLiteConstraintException e) {
            update_body_type(bodyTypeModel, bodyTypeModel.getBody_type_id().toString());
        }
        return body_type_id;
    }


        /*delete a row */

    public boolean DeleteBodyType(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_BODY_TYPE, KEY_BODY_TYPE_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemBodyTypeModel> getAllBodyTypes() {
        ArrayList<ItemBodyTypeModel> bodyTypeList = new ArrayList<ItemBodyTypeModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_BODY_TYPE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemBodyTypeModel bodyTypeModel = new ItemBodyTypeModel(c.getString(c.getColumnIndex(KEY_BODY_TYPE_ID)), c.getString(c.getColumnIndex(KEY_GENDER)), c.getString(c.getColumnIndex(KEY_BODY_TYPE_NAME)), c.getString(c.getColumnIndex(KEY_DESCRIPTION)), c.getString(c.getColumnIndex(KEY_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_IMAGE_WIDTH)), c.getString(c.getColumnIndex(KEY_IMAGE_HEIGHT)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));

                // adding to category list
                bodyTypeList.add(bodyTypeModel);
            } while (c.moveToNext());
        }

        c.close();
        return bodyTypeList;
    }


    /*get body_type  based on name*/

    public String GetBody_type_id(String body_type_name) {
        String body_type_id = null;
        String selectQuery = "SELECT  * FROM " + TABLE_BODY_TYPE + " WHERE " + KEY_BODY_TYPE_NAME + " = " + "'" + body_type_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                body_type_id = c.getString(c.getColumnIndex(KEY_BODY_TYPE_ID));
                // adding to category list
            } while (c.moveToNext());
        }

        c.close();
        return body_type_id;
    }

//    ends

    /*methods for skin tone*/

    public long update_skin_tone(ItemSkinToneModel skinToneModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SKIN_TONE_ID, skinToneModel.getId().toString());
        values.put(KEY_SKIN_TONE_NAME, skinToneModel.getName().toString());
        values.put(KEY_SKIN_TONE_DESCRIPTION, skinToneModel.getDescription().toString());
        values.put(KEY_IMAGE_PATH, skinToneModel.getFilename().toString());
        values.put(KEY_IMAGE_WIDTH, skinToneModel.getImage_width().toString());
        values.put(KEY_IMAGE_HEIGHT, skinToneModel.getImage_height().toString());
        values.put(KEY_UPDATED_AT, skinToneModel.getUpdated_at().toString());
        // insert row
        long skin_tone_id = db.update(TABLE_SKIN_TONE, values, KEY_SKIN_TONE_ID + "=" + id, null);
        return skin_tone_id;
    }

    public long insert_skin_tone(ItemSkinToneModel skin_tone_model) {
        SQLiteDatabase db = this.getWritableDatabase();

        long skin_tone_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_SKIN_TONE_ID, skin_tone_model.getId().toString());
        values.put(KEY_SKIN_TONE_NAME, skin_tone_model.getName().toString());
        values.put(KEY_SKIN_TONE_DESCRIPTION, skin_tone_model.getDescription().toString());
        values.put(KEY_IMAGE_PATH, skin_tone_model.getFilename().toString());
        values.put(KEY_IMAGE_WIDTH, skin_tone_model.getImage_width().toString());
        values.put(KEY_IMAGE_HEIGHT, skin_tone_model.getImage_height().toString());
        values.put(KEY_UPDATED_AT, skin_tone_model.getUpdated_at().toString());
        // insert row
        try {

            skin_tone_id = db.insertOrThrow(TABLE_SKIN_TONE, null, values);

        } catch (SQLiteConstraintException e) {
            update_skin_tone(skin_tone_model, skin_tone_model.getId().toString());
        }
        return skin_tone_id;
    }

    public boolean DeleteSkinTone(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_SKIN_TONE, KEY_SKIN_TONE_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemSkinToneModel> getAllSkinTones() {
        ArrayList<ItemSkinToneModel> skintonelist = new ArrayList<ItemSkinToneModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_SKIN_TONE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemSkinToneModel skinToneModel = new ItemSkinToneModel(c.getString(c.getColumnIndex(KEY_SKIN_TONE_ID)), c.getString(c.getColumnIndex(KEY_SKIN_TONE_NAME)), c.getString(c.getColumnIndex(KEY_DESCRIPTION)), c.getString(c.getColumnIndex(KEY_IMAGE_PATH)), c.getString(c.getColumnIndex(KEY_IMAGE_WIDTH)), c.getString(c.getColumnIndex(KEY_IMAGE_HEIGHT)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));

                // adding to category list
                skintonelist.add(skinToneModel);
            } while (c.moveToNext());
        }

        c.close();
        return skintonelist;
    }


    /*get body_type  based on name*/

    public String GetSkintoneid(String skin_tone_name) {
        String skin_tone_id = null;
        String selectQuery = "SELECT  * FROM " + TABLE_SKIN_TONE + " WHERE " + KEY_SKIN_TONE_NAME + " = " + "'" + skin_tone_name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                skin_tone_id = c.getString(c.getColumnIndex(KEY_SKIN_TONE_ID));
                // adding to category list
            } while (c.moveToNext());
        }

        c.close();
        return skin_tone_id;
    }

    /*ends*/


    /*Method for Bust size*/


    public long update_bust_size(ItemBustSizeModel itemBustSizeModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SKIN_TONE_ID, itemBustSizeModel.getBust_size_id().toString());
        values.put(KEY_BAND, itemBustSizeModel.getBand().toString());
        values.put(KEY_CUP, itemBustSizeModel.getCup().toString());
        values.put(KEY_DISPLAY, itemBustSizeModel.getDisplay().toString());
        values.put(KEY_UPDATED_AT, itemBustSizeModel.getUpdated_at().toString());
        // insert row
        long bust_tone_id = db.update(TABLE_BUST_SIZE, values, KEY_BUST_SIZE_ID + "=" + id, null);
        return bust_tone_id;
    }

    public long insert_bust_size(ItemBustSizeModel itemBustSizeModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        long bust_size_id = 0;
        ContentValues values = new ContentValues();
        values.put(KEY_BUST_SIZE_ID, itemBustSizeModel.getBust_size_id().toString());
        values.put(KEY_BAND, itemBustSizeModel.getBand().toString());
        values.put(KEY_CUP, itemBustSizeModel.getCup().toString());
        values.put(KEY_DISPLAY, itemBustSizeModel.getDisplay().toString());
        values.put(KEY_UPDATED_AT, itemBustSizeModel.getUpdated_at().toString());
        // insert row
        try {

            bust_size_id = db.insertOrThrow(TABLE_BUST_SIZE, null, values);

        } catch (SQLiteConstraintException e) {

            update_bust_size(itemBustSizeModel, itemBustSizeModel.getBust_size_id().toString());

        }
        return bust_size_id;
    }

        /*delete a row */

    public boolean DeleteBust(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_BUST_SIZE, KEY_BUST_SIZE_ID + "=" + id, null) > 0;
    }

    /*fetching all data*/
    public ArrayList<ItemBustSizeModel> getAllBustSizes() {
        ArrayList<ItemBustSizeModel> bustsizelist = new ArrayList<ItemBustSizeModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_BUST_SIZE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ItemBustSizeModel bustSizeModel = new ItemBustSizeModel(c.getString(c.getColumnIndex(KEY_BUST_SIZE_ID)), c.getString(c.getColumnIndex(KEY_BAND)), c.getString(c.getColumnIndex(KEY_CUP)), c.getString(c.getColumnIndex(KEY_DISPLAY)), c.getString(c.getColumnIndex(KEY_UPDATED_AT)));

                // adding to category list
                bustsizelist.add(bustSizeModel);
            } while (c.moveToNext());
        }

        c.close();
        return bustsizelist;
    }

    /*end*/


    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
