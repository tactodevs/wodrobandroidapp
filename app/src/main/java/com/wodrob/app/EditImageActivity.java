package com.wodrob.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.ByteArrayOutputStream;
import java.io.File;

public class EditImageActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView im_undo,im_enhance,im_crop,im_cut,im_rotate,im_edit_image_view;
    File file;
    private Toolbar toolbar;
    private TextView tv_activity_title;
    Bitmap original_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_image);
        init();
        original_image = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 450);
        im_edit_image_view.setImageBitmap(Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 450));
        im_crop.setOnClickListener(this);
        im_rotate.setOnClickListener(this);
        im_undo.setOnClickListener(this);
        im_enhance.setOnClickListener(this);
        im_cut.setOnClickListener(this);
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_activity_title.setTypeface(face);
        file = new File(Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg");
        //get bitmap from path with size of
        im_undo = (ImageView) findViewById(R.id.im_undo);
        im_enhance = (ImageView) findViewById(R.id.im_enhance);
        im_cut = (ImageView) findViewById(R.id.im_cut);
        im_rotate = (ImageView) findViewById(R.id.im_rotate);
        im_crop = (ImageView) findViewById(R.id.im_crop);
        im_edit_image_view = (ImageView) findViewById(R.id.im_edit_image_view);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.im_crop){
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg");
            Intent intent = new Intent("com.android.camera.action.CROP");
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "image/*");
            startActivityForResult(getCropIntent(intent), 1);
        }else if(v.getId() == R.id.im_rotate){

            Bitmap bitmap = ((BitmapDrawable)im_edit_image_view.getDrawable()).getBitmap();
           im_edit_image_view.setImageBitmap(Wodrob.RotateBitmap(bitmap,90));

        }else if(v.getId() == R.id.im_undo){
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg");
            //get bitmap from path with size of
            im_edit_image_view.setImageBitmap(original_image);
        }else if(v.getId() == R.id.im_enhance){
            String path = Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg";
            Intent intent = new Intent(EditImageActivity.this, EnhanceActivity.class);
            intent.putExtra("image_path",path);
            startActivityForResult(intent, 5);

        }else if(v.getId() == R.id.im_cut){

            String path = Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg";
            File file = new File(path);
            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 600);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            Intent intent = new Intent(EditImageActivity.this,CropActivity.class);
            intent.putExtra("image",byteArray);
            startActivityForResult(intent,3);
        }

    }

    private Intent getCropIntent(Intent intent) {
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 320);
        intent.putExtra("outputY", 320);
        intent.putExtra("return-data", true);
        return intent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode != RESULT_CANCELED) {
                Wodrob.SaveImage(getBitmapFromData(data));
                im_edit_image_view.setImageBitmap(getBitmapFromData(data));
            }
            // this bitmap is the finish image
        }else if(requestCode == 3){
            byte[] byteArray = data.getByteArrayExtra("edited_image");
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            im_edit_image_view.setImageBitmap(bmp);

        }else if(requestCode ==5){

            byte[] byteArray = data.getByteArrayExtra("enhancedimage");
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            im_edit_image_view.setImageBitmap(bmp);

        }
        super.onActivityResult(requestCode, resultCode, data);

    }



    public static Bitmap getBitmapFromData(Intent data) {
        Bitmap photo = null;
        Uri photoUri = data.getData();
        if (photoUri != null) {
            photo = BitmapFactory.decodeFile(photoUri.getPath());
        }
        if (photo == null) {
            Bundle extra = data.getExtras();
            if (extra != null) {
                photo = (Bitmap) extra.get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            }
        }

        return photo;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_edit_image) {
            save();
            return true;
        }else if(id == android.R.id.home){

            onBackPressed();

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Bitmap bitmap = original_image;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Wodrob.SaveImage(original_image);
        byte[] byteArray = stream.toByteArray();
        Intent intent = new Intent();
        intent.putExtra("edited_image",byteArray);
        setResult(2, intent);

        super.onBackPressed();
        //  save();
        //finish();
    }

    private void save() {
        Bitmap bitmap = ((BitmapDrawable)im_edit_image_view.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Wodrob.SaveImage(bitmap);
        byte[] byteArray = stream.toByteArray();
        Intent intent = new Intent();
        intent.putExtra("edited_image", byteArray);
        setResult(2, intent);
        finish();

    }
}
