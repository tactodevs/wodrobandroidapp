package com.wodrob.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class EnhanceActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView im_magic_wand,im_brightness,im_contrast,im_enhance_image_view;
    SeekBar sb_brightness,sb_contrast;
    Toolbar toolbar;
    File file;
    int brightness = 0;
    Double contrast = Double.valueOf(0);
    private TextView tv_activity_title;
    private int smoothnessFactor = 10;
    Bitmap bitmap,original_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enhance);
        init();
        im_brightness.setOnClickListener(this);
        im_contrast.setOnClickListener(this);
        im_magic_wand.setOnClickListener(this);
        original_image = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 450);
        im_enhance_image_view.setImageBitmap(Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 450));

       // bitmap = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 600, 450);
        bitmap = ((BitmapDrawable)im_enhance_image_view.getDrawable()).getBitmap();

        sb_brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

             //   brightness = Math.abs(progress - brightness);

                brightness = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                Bitmap newBitMap = Wodrob.doBrightness(bitmap, brightness);
                im_enhance_image_view.setImageBitmap(Wodrob.ChangeContrast(newBitMap,contrast));
               // bitmap = newBitMap;

            }
        });

        sb_contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

               // contrast = (double) Math.abs(progress - (new Double(contrast).intValue()));
              contrast = (double) progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                Bitmap newBitMap = Wodrob.ChangeContrast(bitmap, contrast);
                im_enhance_image_view.setImageBitmap( Wodrob.doBrightness(newBitMap, brightness));
               // bitmap = newBitMap;

            }
        });

    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_activity_title.setTypeface(face);

        file = new File(Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg");
        im_magic_wand = (ImageView) findViewById(R.id.im_magic_wand);
        im_brightness = (ImageView) findViewById(R.id.im_brightness);
        im_contrast = (ImageView) findViewById(R.id.im_contrast);
        im_enhance_image_view = (ImageView) findViewById(R.id.im_enhance_image_view);
        sb_brightness = (SeekBar) findViewById(R.id.sb_brightness);
        sb_contrast = (SeekBar) findViewById(R.id.sb_contrast);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.im_magic_wand){

            sb_brightness.setVisibility(View.GONE);
            sb_contrast.setVisibility(View.GONE);

        }else if(v.getId() == R.id.im_brightness){

            sb_brightness.setVisibility(View.VISIBLE);
            sb_contrast.setVisibility(View.GONE);
           // bitmap = ((BitmapDrawable)im_enhance_image_view.getDrawable()).getBitmap();


        }else if(v.getId() == R.id.im_contrast){

            sb_brightness.setVisibility(View.GONE);
            sb_contrast.setVisibility(View.VISIBLE);
           // bitmap = ((BitmapDrawable)im_enhance_image_view.getDrawable()).getBitmap();


        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_edit_image) {

            Bitmap bitmap = ((BitmapDrawable)im_enhance_image_view.getDrawable()).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Intent intent = new Intent();
            intent.putExtra("enhancedimage",byteArray);
            setResult(5, intent);
            Wodrob.SaveImage(bitmap);
            finish();
            return true;
        }else if(id == android.R.id.home){
            Bitmap bitmap = original_image;
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            Intent intent = new Intent();
            intent.putExtra("enhancedimage",byteArray);
            setResult(5, intent);
            finish();

            return true;
        }


        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {

        Bitmap bitmap = original_image;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        Intent intent = new Intent();
        intent.putExtra("enhancedimage",byteArray);
        setResult(5, intent);

        super.onBackPressed();
        //  save();
      //  finish();
    }

   /* @Override
    pr_public void finish() {

        Bitmap bitmap = original_image;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        Intent intent = new Intent();
        intent.putExtra("enhancedimage",byteArray);
        setResult(5, intent);
        super.finish();
    }*/

}
