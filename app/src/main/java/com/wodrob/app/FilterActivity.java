package com.wodrob.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.wodrob.app.fragment.OtherFilters;
import com.wodrob.app.fragment.OutfitCategoryFilter;
import java.util.ArrayList;

public class FilterActivity extends AppCompatActivity {

    Toolbar toolbar;
    public  static TextView tv_filter_title;
    OutfitCategoryFilter outfitcategoryfilter;
    OtherFilters otherFilters;
    FragmentManager fragmentManager;
    String current_fragment;
    ProgressDialog pDialog;
    DbHelper db;

    public static String selected_category_id="";
    public static ArrayList<String> selected_style_list;
    public static ArrayList<String> selected_brand_list;
    public static ArrayList<String> selected_size_list;
    public static ArrayList<String> selected_color_list;
    public static ArrayList<String> selected_tags_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        init();

        if (outfitcategoryfilter != null) {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.container, outfitcategoryfilter, "CategoryFilter");
            fragmentTransaction.commit();
            tv_filter_title.setText("Select Category");
            current_fragment = "category";
        }
    }


    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        db = new DbHelper(getApplicationContext());
        fragmentManager = getSupportFragmentManager();
        outfitcategoryfilter = new OutfitCategoryFilter();
        otherFilters = new OtherFilters();

        pDialog = new ProgressDialog(this);

        tv_filter_title = (TextView) findViewById(R.id.tv_filter_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_filter_title.setTypeface(face);

        selected_style_list = new ArrayList<>();
        selected_brand_list = new ArrayList<>();;
        selected_size_list = new ArrayList<>();
        selected_color_list = new ArrayList<>();
        selected_tags_list = new ArrayList<>();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_ok) {

            Intent intent = new Intent();
            setResult(5, intent);
            finish();

            return true;

        } else if (id == android.R.id.home) {

            finish();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
