package com.wodrob.app;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 21/12/15.
 */
public class GetAddItemOutPut {


    public static String GetOccassionlist(Activity activity){

        DbHelper dbHelper;

        dbHelper = new DbHelper(activity);

        String out_occassion_list = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = PublishActivity.selected_ocassion_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString()+"_"+dbHelper.Get_Occassion_Id(out_array_List.get(i).toString()));

            } else {

                sb.append(out_array_List.get(i).toString()+"_"+dbHelper.Get_Occassion_Id(out_array_List.get(i).toString())+",");

            }
        }

        out_occassion_list=sb.toString();

        return out_occassion_list;
    }


    public static String GetAdditionalTag(){

        String out_additional_tag = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_additional_tags;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        out_additional_tag=sb.toString();

        return out_additional_tag;
    }
    public static String GetOutCareTip(){

        String out_care_tip = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_washing_instrcutions;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        out_care_tip = sb.toString();

        return out_care_tip;
    }

    public static String GetCategory(){

        String out_category = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_category_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        //out_category = sb.toString();

        out_category = AddItem.selected_category_name;

        return out_category;
    }

    public static String GetOutCategoryID(Activity activity){

        String out_category_id = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_category_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        DbHelper db = new DbHelper(activity);
        //out_category_id = db.GetCategory_id(sb.toString());
        out_category_id = sb.toString();

        return out_category_id;
    }
    public static String GetOutColor(){

        String out_color="";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.pixel_array_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString().substring(1));

            } else {

                sb.append(out_array_List.get(i).toString().substring(1)+",");

            }
        }

        out_color = sb.toString();
        return out_color;
    }

    public static String GetOutDate(){

        String out_date = "";

        if(AddItem.selected_purchase_date_list.size() != 0){

            out_date = AddItem.selected_purchase_date_list.get(0);

        }

        return out_date;
    }

    public static String GetOutPrice(){

        String out_price = "";

        if(AddItem.selected_price_list.size() != 0){

            out_price = AddItem.selected_price_list.get(0);

        }
        return out_price;
    }
    public static String GetOutSize(){

        String out_size = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_size_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        out_size = sb.toString();

        return out_size;
    }
    public static String GetOutStyle(){

        String out_style = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_style_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        out_style = sb.toString();

        return out_style;
    }

    public static String GetOutBrand(){

        String out_brand = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_brand_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        out_brand = sb.toString();

        return out_brand;
}
    public static String GetOutBrandID(Activity activity){

        String out_brand = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_brand_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        DbHelper db = new DbHelper(activity);
        out_brand = db.GetBrand_id(sb.toString());
        String[] out_brand_id = out_brand.split("_");

        return out_brand_id[0];
    }

    public static String GetOutStore(){

        String out_brand = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_store_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        out_brand = sb.toString();

        return out_brand;
    }
    public static String GetOutStoreID(Activity activity){

        String out_store_id = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = AddItem.selected_store_list;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        DbHelper db = new DbHelper(activity);
        out_store_id = db.GetStoreId(sb.toString());

        return out_store_id;
    }
}
