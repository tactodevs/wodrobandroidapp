package com.wodrob.app;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.wodrob.app.interfaces.EditDialogListener;


/**
 * Created by rameesfazal on 25/12/15.
 */
public class ImageEditDialog extends Dialog implements View.OnClickListener{

    Matrix matrix;
    public Activity activity;
    ImageView im_edit_image;
    RelativeLayout image_layout;


    private static final int GETIMAGE = 1;
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;

    boolean isclicked = false;



    RelativeLayout.LayoutParams layoutParams;
    Button btn_remove,btn_flip,btn_clone,btn_cutout,btn_forward,btn_backward;
    float angle;
    Bitmap image_bitmap;
    Bitmap backward_image = null;
    Bitmap forward_image = null;
    boolean istransparent;
    float selected_image_depth;
    int item_count;

    public ImageEditDialog(Activity activity, Matrix matrix, RelativeLayout.LayoutParams layoutParams, float angle, Bitmap image_bitmap, boolean istransparent, float selected_image_depth, int item_count) {
        super(activity, R.style.free_floating_dialog);
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.matrix = matrix;
        this.layoutParams = layoutParams;
        this.angle = angle;
        this.image_bitmap = image_bitmap;
        this.istransparent = istransparent;
        this.selected_image_depth = selected_image_depth;
        this.item_count = item_count;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();

        int screenWidth = (int) (metrics.widthPixels);
        int screenHeight = (int) (metrics.heightPixels);

        setContentView(R.layout.edit_image_popup);

        getWindow().setLayout(screenWidth, screenHeight);

        init();


        /*image on touch listener*/

        im_edit_image.setOnTouchListener(new View.OnTouchListener() {

            RelativeLayout.LayoutParams parms;
            float dx = 0, dy = 0, x = 0, y = 0;
            float angle = 0;


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final ImageView view = (ImageView) v;
                ((BitmapDrawable) view.getDrawable()).setAntiAlias(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:

                        parms = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        dx = event.getRawX() - parms.leftMargin;
                        dy = event.getRawY() - parms.topMargin;

                        savedMatrix.set(matrix);
                        start.set(event.getX(), event.getY());
                        mode = DRAG;
                        lastEvent = null;
                       // lastTouchDown = System.currentTimeMillis();
                        break;

                    case MotionEvent.ACTION_POINTER_DOWN:
                        oldDist = spacing(event);
                        if (oldDist > 10f) {
                            savedMatrix.set(matrix);
                            midPoint(mid, event);
                            mode = ZOOM;
                        }
                        lastEvent = new float[4];
                        lastEvent[0] = event.getX(0);
                        lastEvent[1] = event.getX(1);
                        lastEvent[2] = event.getY(0);
                        lastEvent[3] = event.getY(1);
                        d = rotation(event);
                        break;
                    case MotionEvent.ACTION_UP:


                        break;

                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        lastEvent = null;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG) {

                         //   view.setScaleType(ImageView.ScaleType.FIT_XY);


                            x = event.getRawX();
                            y = event.getRawY();


                            parms.leftMargin = (int) (x - dx);
                            parms.topMargin = (int) (y - dy);

                            parms.rightMargin = 0 ;
                            parms.bottomMargin = 0;

                            if(parms.leftMargin > 400){

                                parms.rightMargin = parms.leftMargin+parms.width;

                            }

                            if(parms.topMargin > 500){

                                parms.bottomMargin = parms.topMargin + parms.height;

                            }

                            //parms.rightMargin = parms.leftMargin+parms.width;



                           // matrix.set(savedMatrix);

                            view.setLayoutParams(parms);
                            ((EditDialogListener) activity).OnDrag(view.getImageMatrix(), parms, view.getRotation());

                        } else if (mode == ZOOM) {

                           view.setScaleType(ImageView.ScaleType.FIT_XY);
                            float newDist = spacing(event);


                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);


                            if (oldDist < newDist) {

                                parms.width = parms.width + ((int) scale * 5);
                                parms.height = parms.height + ((int) scale * 5);
                                view.setLayoutParams(parms);
                                ((EditDialogListener) activity).OnDrag(view.getImageMatrix(), parms, view.getRotation());

                            } else {

                                float tempscale = (oldDist / newDist);

                                parms.width = parms.width - ((int) tempscale * 5);
                                parms.height = parms.height - ((int) tempscale * 5);

                                view.setLayoutParams(parms);
                                ((EditDialogListener) activity).OnDrag(view.getImageMatrix(), parms, view.getRotation());

                            }

                            //  }
                            view.setImageMatrix(matrix);
                            if (lastEvent != null && event.getPointerCount() == 2) {
                                // view.setScaleType(ImageView.ScaleType.MATRIX);
                                newRot = rotation(event);
                                float r = newRot - d;
                                float[] values = new float[9];
                                matrix.getValues(values);
                                float tx = values[2];
                                float ty = values[5];
                                float sx = values[0];
                                float xc = (view.getWidth() / 2) * sx;
                                float yc = (view.getHeight() / 2) * sx;

                                float imageWidth = view.getDrawable().getIntrinsicWidth();
                                float imageHeight = view.getDrawable().getIntrinsicHeight();
                                RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
                                RectF viewRect = new RectF(0, 0, view.getWidth(),
                                        view.getHeight());

                                angle = r;
                                matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
                               // matrix.postRotate(r, tx + xc, ty + yc);
                                view.setLayoutParams(parms);
                                view.setImageMatrix(matrix);
                                view.animate().rotationBy(angle).setDuration(0).setInterpolator(new LinearInterpolator()).start();
                                ((EditDialogListener) activity).OnDrag(view.getImageMatrix(), parms, view.getRotation());

                            }
                        }
                        break;
                }

               // view.setLayoutParams(parms);
                //view.setRotation(r);
                return false;

            }
        });



    }

    private void init() {

        image_layout = (RelativeLayout) findViewById(R.id.image_layout);
        im_edit_image = (ImageView) findViewById(R.id.im_edit_image);
        im_edit_image.setImageBitmap(image_bitmap);
        im_edit_image.setImageMatrix(matrix);
        im_edit_image.setLayoutParams(layoutParams);
        im_edit_image.setRotation(angle);
        im_edit_image.setScaleType(ImageView.ScaleType.FIT_XY);
        forward_image = image_bitmap;
        backward_image = image_bitmap;
      //  im_edit_image.setAlpha(100);
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            im_edit_image.setZ(selected_image_depth);

        }

        if(istransparent){

            im_edit_image.setAlpha(255);

        }else {

            im_edit_image.setAlpha(127);

        }*/

        btn_remove = (Button) findViewById(R.id.btn_remove);
        btn_flip = (Button) findViewById(R.id.btn_filp);
        btn_clone = (Button) findViewById(R.id.btn_clone);
        btn_cutout = (Button) findViewById(R.id.btn_cutout);
        btn_forward = (Button) findViewById(R.id.btn_forward);
        btn_backward = (Button) findViewById(R.id.btn_backward);


        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "fonts/opensansregular.ttf");

        btn_remove.setTypeface(face);
        btn_flip.setTypeface(face);
        btn_clone.setTypeface(face);
        btn_cutout.setTypeface(face);
        btn_forward.setTypeface(face);
        btn_backward.setTypeface(face);

        if(item_count<2){

            btn_forward.setCompoundDrawablesWithIntrinsicBounds(null, getContext().getResources().getDrawable(R.drawable.canvas_forward), null, null);
            btn_backward.setCompoundDrawablesWithIntrinsicBounds(null, getContext().getResources().getDrawable(R.drawable.canvas_backward), null, null);

        }else {

            btn_forward.setCompoundDrawablesWithIntrinsicBounds(null, getContext().getResources().getDrawable(R.drawable.canvas_forward_selected), null, null);
            btn_backward.setCompoundDrawablesWithIntrinsicBounds( null, getContext().getResources().getDrawable( R.drawable.canvas_backward_selected), null, null );

        }

        btn_remove.setOnClickListener(this);
        btn_flip.setOnClickListener(this);
        btn_clone.setOnClickListener(this);
        btn_cutout.setOnClickListener(this);
        btn_forward.setOnClickListener(this);
        btn_backward.setOnClickListener(this);
        image_layout.setOnClickListener(this);
        im_edit_image.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_remove){
            Bitmap bitmap = ((BitmapDrawable)im_edit_image.getDrawable()).getBitmap();
            ((EditDialogListener) activity).onReturnValue(bitmap,"Remove");
            dismiss();

        }
        else if(v.getId() == R.id.btn_filp){

            Bitmap bitmap = ((BitmapDrawable)im_edit_image.getDrawable()).getBitmap();

            backward_image = bitmap;

            im_edit_image.setImageBitmap(Wodrob.flip(bitmap ,2));

            bitmap = ((BitmapDrawable)im_edit_image.getDrawable()).getBitmap();

            ((EditDialogListener) activity).onReturnValue(bitmap,"flip");

        }
        else if(v.getId() == R.id.btn_clone){

            Bitmap bitmap = ((BitmapDrawable)im_edit_image.getDrawable()).getBitmap();

            ((EditDialogListener) activity).onReturnValue(bitmap,"clone");
            dismiss();

        }
        else if(v.getId() == R.id.btn_cutout){


        }
        else if(v.getId() == R.id.btn_forward){

            if(item_count > 1) {
                Bitmap bitmap = ((BitmapDrawable) im_edit_image.getDrawable()).getBitmap();
                ((EditDialogListener) activity).onReturnValue(bitmap, "forward");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    selected_image_depth = selected_image_depth + 1;
                    im_edit_image.setZ(selected_image_depth);
                }
                im_edit_image.setAlpha(255);
            }

        }
        else if(v.getId() == R.id.btn_backward){

            if(item_count > 1) {

                Bitmap bitmap = ((BitmapDrawable) im_edit_image.getDrawable()).getBitmap();
                ((EditDialogListener) activity).onReturnValue(bitmap, "backward");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    selected_image_depth = selected_image_depth + 1;
                    im_edit_image.setZ(selected_image_depth);
                }

                im_edit_image.setAlpha(127);

            }

        }else if(v.getId() == R.id.image_layout){

            dismiss();

        }else if(v.getId() == R.id.im_edit_image){

//do nothing
        }

    }



    /*methods for on touch listener*/

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
        // return FloatMath.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }

}