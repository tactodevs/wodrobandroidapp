package com.wodrob.app;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.adapter.SimilarItemsAdapter;
import com.wodrob.app.model.WearSimilarItemsModel;
import com.wodrob.app.views.CircularImageView;
import com.wodrob.app.views.SingleSpacingDecoration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class LookBookView extends AppCompatActivity implements View.OnClickListener {

    private CollapsingToolbarLayout collapsingToolbar;
    TextView tv_outfit_name, tv_created_by, tv_created_time, tv_occassion_types, tv_likes, tv_items_in_outfit, tv_items_in_closet, tv_no_of_shares, tv_no_of_views;
    TextView tv_items_in_outfits_title, tv_items_in_closet_title, tv_no_of_shares_title, tv_no_of_views_title;
    CircularImageView im_profile_pic;
    Toolbar toolbar;
    private ProgressDialog pDialog;
    ImageView im_outfit, im_privacy, im_like;
    Menu menu;

    TextView tv_suggested_outfit_title,tv_items_in_outfit_title;

    RecyclerView rv_items_in_outfits, rv_suggested_outfit;

    WearSimilarItemsModel wearSimilarItemsModel;

    ArrayList<WearSimilarItemsModel> similarItemsModelArrayList;
    ArrayList<WearSimilarItemsModel> suggestedoutfitarraylist;

    private SimilarItemsAdapter similaritemadapter;
    private SimilarItemsAdapter suggestedoutfitadapter;
    private LinearLayoutManager itemsinoutfitslayoutmanager;
    private LinearLayoutManager outfitlayoutmanager;
    private LinearLayout ll_additional_tags;
    String story_status;
    String isliked;
    TextView tv_activity_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_book_view);

        init();

        setlayout();


        if(getIntent().getExtras().getString("type").equals("Add")){

            HideLayout();
        }

        GetData();

    }

    private void GetData() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchOutfitItem().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "lookbook/view");

            } else {

                new FetchOutfitItem().execute(WodrobConstant.BASE_URL + "lookbook/view");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);
        tv_suggested_outfit_title = (TextView) findViewById(R.id.tv_suggested_outfit_title);
        tv_items_in_outfit_title = (TextView) findViewById(R.id.tv_items_in_outfit_title);

        pDialog = new ProgressDialog(this);

        tv_outfit_name = (TextView) findViewById(R.id.tv_outfit_name);
        tv_created_by = (TextView) findViewById(R.id.tv_created_by);
        tv_created_time = (TextView) findViewById(R.id.tv_created_time);

        tv_occassion_types = (TextView) findViewById(R.id.tv_occassion_types);
        tv_likes = (TextView) findViewById(R.id.tv_likes);
        tv_items_in_outfit = (TextView) findViewById(R.id.tv_items_in_outfits);
        tv_items_in_closet = (TextView) findViewById(R.id.tv_items_in_closet);
        tv_no_of_shares = (TextView) findViewById(R.id.tv_no_of_shares);
        tv_no_of_views = (TextView) findViewById(R.id.tv_no_of_views);

        /*title initialisation fot setting font*/
        tv_items_in_outfits_title = (TextView) findViewById(R.id.items_in_outfits_title);
        tv_items_in_closet_title = (TextView) findViewById(R.id.items_in_outfits_closet_title);
        tv_no_of_shares_title = (TextView) findViewById(R.id.no_of_shares);
        tv_no_of_views_title = (TextView) findViewById(R.id.no_of_views);

        ll_additional_tags = (LinearLayout) findViewById(R.id.ll_additional_tags);


        im_profile_pic = (CircularImageView) findViewById(R.id.im_profile_image);
        im_outfit = (ImageView) findViewById(R.id.im_outfit);
        im_privacy = (ImageView) findViewById(R.id.im_privacy);
        im_like = (ImageView) findViewById(R.id.im_like);
        im_like.setOnClickListener(this);

        suggestedoutfitarraylist = new ArrayList<>();
        similarItemsModelArrayList = new ArrayList<>();

        rv_items_in_outfits = (RecyclerView) findViewById(R.id.rv_items_in_outfits);
        rv_suggested_outfit = (RecyclerView) findViewById(R.id.rv_suggested_outfits);

        itemsinoutfitslayoutmanager = new LinearLayoutManager(LookBookView.this);
        itemsinoutfitslayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);

        outfitlayoutmanager = new LinearLayoutManager(LookBookView.this);
        outfitlayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);

        SetFonts();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_outfit_view, menu);
        this.menu = menu;

        MenuItem add_to_canvas = menu.findItem(R.id.mi_add_to_canvas);
        MenuItem mi_outfit_of_the_day = menu.findItem(R.id.mi_outfit_of_the_day);


        if(getIntent().getExtras().getString("type").equals("Add")){

            add_to_canvas.setVisible(true);
            mi_outfit_of_the_day.setVisible(false);
            invalidateOptionsMenu();

        }else {

            add_to_canvas.setVisible(false);
            mi_outfit_of_the_day.setVisible(true);
            invalidateOptionsMenu();

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            case R.id.mi_outfit_of_the_day:
                //Write your logic here
                if (!story_status.equals("1")) {

                    Story();


                }

                return true;
            case R.id.mi_add_to_canvas:
                //Write your logic here
                Intent intent = new Intent();
                intent.putExtra("status","added");
                setResult(1, intent);
                finish();


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void Story() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new AddToStory().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "story/add");

            } else {

                new AddToStory().execute(WodrobConstant.BASE_URL + "story/add");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void Like() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new AddLike().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "outfit/like");

            } else {

                new AddLike().execute(WodrobConstant.BASE_URL + "outfit/like");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    private void setlayout() {
        similaritemadapter = new SimilarItemsAdapter(similarItemsModelArrayList, R.layout.recomendations_item_layout, "outfititems");
        rv_items_in_outfits.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        rv_items_in_outfits.setAdapter(similaritemadapter);
        rv_items_in_outfits.setLayoutManager(itemsinoutfitslayoutmanager);

        suggestedoutfitadapter = new SimilarItemsAdapter(suggestedoutfitarraylist, R.layout.recomendations_item_layout, "outfits");
        rv_suggested_outfit.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        rv_suggested_outfit.setAdapter(suggestedoutfitadapter);
        rv_suggested_outfit.setLayoutManager(outfitlayoutmanager);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.im_like) {

            if(isliked.equalsIgnoreCase("1")){

                Toast.makeText(getApplicationContext(),"You have already liked this outfit",Toast.LENGTH_LONG).show();

            }else {

                Like();

            }

        }
    }

    private class FetchOutfitItem extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("outfit_id", "UTF-8") + "=" + getIntent().getExtras().getString("outfit_id")+"&"
                        + URLEncoder.encode("patron_id", "UTF-8")+"="+WodrobConstant.patron_id;

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            /*fetching onbjects and array*/

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray occassion_type_array = dataobject.getJSONArray("occasion");
                            JSONArray tag_array = dataobject.getJSONArray("tags");
                            JSONArray productarray = dataobject.getJSONArray("products");

                            String occassion_types = "";
                            for (int i = 0; i < occassion_type_array.length(); i++) {

                                if (i == occassion_type_array.length() - 1) {

                                    occassion_types = occassion_types + occassion_type_array.getString(i).split("_")[0];

                                } else {

                                    occassion_types = occassion_types + occassion_type_array.getString(i).split("_")[0] + ",";

                                }

                            }

                            for (int k = 0; k < tag_array.length(); k++) {

                                CreateTags(tag_array.get(k).toString().split("_")[0]);

                            }
                            /*fetching data*/

                            String id;
                            String name = dataobject.getString("name");
                            String outfit_image_path = dataobject.getString("image");
                            String profile_pic_image_path;
                            String created_by = dataobject.getString("created_by");
                            String created_by_profile_pic = dataobject.getString("profile_pic");
                           // story_status = dataobject.getString("story_is_added");

                            String created_time = dataobject.getString("created_at");
                            String likes = dataobject.getString("like_cnt");
                            String itmes_in_outfit = dataobject.getString("itemsInOutfit");
                            String items_in_closet = dataobject.getString("itemsInCloset");
                            String no_of_shares = dataobject.getString("share_count");
                            String no_of_views = dataobject.getString("views");
                            String privacy = dataobject.getString("privacy");
                            isliked = dataobject.getString("isLiked");

                            tv_outfit_name.setText(name);
                            tv_created_by.setText(created_by.split("_")[1]);
                            tv_created_time.setText(date_difference(created_time));
                            tv_occassion_types.setText(occassion_types);
                            tv_likes.setText(likes);
                            tv_items_in_outfit.setText(itmes_in_outfit);
                            tv_items_in_closet.setText(items_in_closet);
                            tv_no_of_shares.setText(no_of_shares);
                            tv_no_of_views.setText(no_of_views);
                            tv_activity_title.setText(name);

                            SetPrivacy(privacy);

                            Picasso.with(im_outfit.getContext()).load(outfit_image_path).into(im_outfit);
                            Picasso.with(im_outfit.getContext()).load(created_by_profile_pic).into(im_profile_pic);
                            /*if (story_status.equalsIgnoreCase("1")) {
                                menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.canvas_cancel));

                            }*/
                            if(isliked.equalsIgnoreCase("1")){

                                im_like.setImageDrawable(getResources().getDrawable(R.drawable.pr_private));

                            }else {

                                im_like.setImageDrawable(getResources().getDrawable(R.drawable.heart));


                            }


//                            fetching similar,suggested items,outlookk

                            JSONArray suggestedOutfits = dataobject.getJSONArray("suggestedOutfits");

                            //similar items
                            for (int i = 0; i < productarray.length(); i++) {

                                JSONObject similar_items_object = productarray.getJSONObject(i);
                                String similar_item_product_id = similar_items_object.getString("product_id");
                                String similar_item_name = similar_items_object.getString("name");
                                String similar_item_price = similar_items_object.getString("price");
                                String similar_item_image_path = similar_items_object.getString("imageURL");

                                wearSimilarItemsModel = new WearSimilarItemsModel(similar_item_product_id, similar_item_name, similar_item_image_path, similar_item_price, "");
                                similarItemsModelArrayList.add(wearSimilarItemsModel);

                            }

                            //suggested outfits
                            for (int i = 0; i < suggestedOutfits.length(); i++) {

                                JSONObject suggested_outfit_object = suggestedOutfits.getJSONObject(i);

                                if (suggested_outfit_object.has("image")) {

                                    String suggested_outfit_product_id = suggested_outfit_object.getString("_id");
                                    String suggested_outfit_name = suggested_outfit_object.getString("name");
                                    String suggested_created_by = suggested_outfit_object.getString("created_by");

                                    String suggested_outfit_image_path = suggested_outfit_object.getString("image");

                                    //    String suggested_outfit_createdby = suggested_outfit_object.getString("createdby");
                                    wearSimilarItemsModel = new WearSimilarItemsModel(suggested_outfit_product_id, suggested_outfit_name, suggested_outfit_image_path, "", suggested_created_by);
                                    suggestedoutfitarraylist.add(wearSimilarItemsModel);

                                }

                            }


                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();

            }
        }
    }

    private void SetPrivacy(String privacy) {

        if (privacy.equalsIgnoreCase("public")) {

            im_privacy.setImageDrawable(getResources().getDrawable(R.drawable.pr_public));

        } else if (privacy.equalsIgnoreCase("private")) {

            im_privacy.setImageDrawable(getResources().getDrawable(R.drawable.pr_private));


        } else if (privacy.equalsIgnoreCase("protected")) {

            im_privacy.setImageDrawable(getResources().getDrawable(R.drawable.pr_protected));


        }
    }

//    add to outfit of the day starts


    private class AddToStory extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("outfit_id", "UTF-8") + "=" + getIntent().getExtras().getString("outfit_id") + "&"
                        + URLEncoder.encode("date", "UTF-8") + "=" + getDateTime();

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.canvas_cancel));


                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();

            }
        }
    }

//add to outfit of the day ends

    //    like image background starts


    private class AddLike extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("outfit_id", "UTF-8") + "=" + getIntent().getExtras().getString("outfit_id")+"&"
                        + URLEncoder.encode("patron_id", "UTF-8")+"="+WodrobConstant.patron_id;

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            im_like.setImageDrawable(getResources().getDrawable(R.drawable.pr_private));
                            isliked = "1";


                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();

            }
        }
    }

    //like ends
    @SuppressLint("NewApi")
    private void CreateTags(String tags) {
        final LinearLayout selected_item_textview_layout = new LinearLayout(this);
        selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
        selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        selected_item_textview_layout.setLayoutParams(params);

        TextView tv_seleected_text = new TextView(this);
        tv_seleected_text.setText(tags);
        tv_seleected_text.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        //tv_seleected_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        tv_seleected_text.setTextSize(16);
        selected_item_textview_layout.addView(tv_seleected_text);
        ll_additional_tags.addView(selected_item_textview_layout);

    }

    public String date_difference(String serverdate) {

        String out = "";
        // String strCurrentDate = getDateTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-M-DD HH:mm:SS");

        Date newDate = (Date) Calendar.getInstance().getTime();
        Date date2 = null;
        try {

            //Date newDates= format.parse(strCurrentDate);

            Calendar c = Calendar.getInstance();
            c.setTime(format.parse(serverdate));
            c.add(Calendar.MONTH, 1);

            // date2 = format.parse(serverdate);
            date2 = c.getTime();  // dt is now the new date


        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = newDate.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        if (days != 0) {

            out = String.valueOf(days) + "days ago";
            return out;

        } else if (hours != 0) {

            out = String.valueOf(hours) + "hours ago";
            return out;

        } else if (minutes != 0) {

            out = String.valueOf(minutes) + "minutes ago";
            return out;

        } else {

            out = String.valueOf(seconds) + "seconds ago";
            return out;
        }

    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void SetFonts() {

        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_outfit_name);
        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_activity_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_items_in_outfits_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_items_in_closet_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_no_of_shares_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_no_of_views_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_created_time);

    }

    public void HideLayout(){

        rv_items_in_outfits.setVisibility(View.GONE);
        rv_suggested_outfit.setVisibility(View.GONE);
        tv_suggested_outfit_title.setVisibility(View.GONE);
        tv_items_in_outfit_title.setVisibility(View.GONE);
        im_like.setVisibility(View.GONE);

    }
}
