package com.wodrob.app;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wodrob.app.fragment.Explore;
import com.wodrob.app.fragment.Feed;
import com.wodrob.app.fragment.StyleChat;
import com.wodrob.app.fragment.Wear;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener{

    FloatingActionButton fab;

    private TextView tv_title;
    LinearLayout ll_feed,ll_wear,ll_explore,ll_style_chat;
    ImageView im_feed,im_wear,im_explore,im_style_chat;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);;
        init();

        /*Display first fragment*/
        Fragment feed_fragment = new Feed();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, feed_fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        CreateMenu();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        toolbar.setNavigationIcon(R.drawable.closet_main_menu);

    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.closet_main_menu);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("WEAR");
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_title.setTypeface(face);

        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_wear = (LinearLayout) findViewById(R.id.ll_wear);
        ll_explore = (LinearLayout) findViewById(R.id.ll_explore);
        ll_style_chat = (LinearLayout) findViewById(R.id.ll_style_chat);

        ll_feed.setOnClickListener(this);
        ll_wear.setOnClickListener(this);
        ll_explore.setOnClickListener(this);
        ll_style_chat.setOnClickListener(this);

        im_feed = (ImageView) findViewById(R.id.im_feed);
        im_wear = (ImageView) findViewById(R.id.im_wear);
        im_explore = (ImageView) findViewById(R.id.im_explore);
        im_style_chat = (ImageView) findViewById(R.id.im_style_chat);

    }

    private void CreateMenu() {

        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);

// repeat many times:
        ImageView itemIcon = new ImageView(this);
        itemIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_gallery));
        SubActionButton button1 = itemBuilder.setContentView(itemIcon).build();

        ImageView itemIcon1 = new ImageView(this);
        itemIcon1.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_manage));
        SubActionButton button2 = itemBuilder.setContentView(itemIcon1).build();

        ImageView itemIcon3 = new ImageView(this);
        itemIcon3.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_send));
        SubActionButton button3 = itemBuilder.setContentView(itemIcon3).build();


        FloatingActionMenu actionMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(button1)
                .addSubActionView(button2)
                .addSubActionView(button3)
                .attachTo(fab)
                .build();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {


        if(v.getId() == R.id.ll_feed){

            try{

                FilterActivity.selected_category_id="";
                FilterActivity.selected_style_list.clear();
                FilterActivity.selected_brand_list.clear();
                FilterActivity.selected_size_list.clear();
                FilterActivity.selected_color_list.clear();
                FilterActivity.selected_tags_list.clear();

            }catch (Exception e){

            }

            im_feed.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_feed_selected));
            im_wear.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_closet));
            im_explore.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_lookbook));
            im_style_chat.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_chat));

            Fragment feed_fragment = new Feed();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, feed_fragment);
            transaction.addToBackStack(null);

// Commit the transaction
            transaction.commit();
            tv_title.setText("FEED");

        }else if(v.getId() == R.id.ll_wear){

            try{

                FilterActivity.selected_category_id="";
                FilterActivity.selected_style_list.clear();
                FilterActivity.selected_brand_list.clear();
                FilterActivity.selected_size_list.clear();
                FilterActivity.selected_color_list.clear();
                FilterActivity.selected_tags_list.clear();

            }catch (Exception e){

            }

            im_feed.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_feed));
            im_wear.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_closet_selected));
            im_explore.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_lookbook));
            im_style_chat.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_chat));

            Fragment wear_fragment = new Wear();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, wear_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
            tv_title.setText("Wear");

        }else if(v.getId() == R.id.ll_explore){

            try{

                FilterActivity.selected_category_id="";
                FilterActivity.selected_style_list.clear();
                FilterActivity.selected_brand_list.clear();
                FilterActivity.selected_size_list.clear();
                FilterActivity.selected_color_list.clear();
                FilterActivity.selected_tags_list.clear();

            }catch (Exception e){

            }

            im_feed.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_feed));
            im_wear.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_closet));
            im_explore.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_lookbook_selected));
            im_style_chat.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_chat));

            Fragment explore_fragment = new Explore();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, explore_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
            tv_title.setText("EXPLORE");


        }else if(v.getId() == R.id.ll_style_chat){

            try{

                FilterActivity.selected_category_id="";
                FilterActivity.selected_style_list.clear();
                FilterActivity.selected_brand_list.clear();
                FilterActivity.selected_size_list.clear();
                FilterActivity.selected_color_list.clear();
                FilterActivity.selected_tags_list.clear();

            }catch (Exception e){

            }

            im_feed.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_feed));
            im_wear.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_closet));
            im_explore.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_lookbook));
            im_style_chat.setImageDrawable(getResources().getDrawable(R.drawable.closet_main_chat_selected));

            Fragment feed_style_chat = new StyleChat();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, feed_style_chat);
            transaction.addToBackStack(null);
            transaction.commit();
            tv_title.setText("STYLE");

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
