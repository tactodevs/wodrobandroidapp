package com.wodrob.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PickColorActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private static final int GETIMAGE = 1;
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;

    boolean isclicked = false;

    Toolbar toolbar;
    TextView tv_color1, tv_color2, tv_color3, tv_color4, tv_color5;
    String color1, color2, color3, color4, color5;
    ImageView im_color_pick_view;
    ImageView save;
    Bitmap bitmap_image;
    TextView selected_textView;
    int postion = 1;

    ArrayList<String> color_array_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_color);

        init();
        bitmap_image = ((BitmapDrawable) AddItem.im_item.getDrawable()).getBitmap();
        im_color_pick_view.setImageBitmap(bitmap_image);
        SetColors();

        tv_color1.setOnClickListener(this);
        tv_color2.setOnClickListener(this);
        tv_color3.setOnClickListener(this);
        tv_color4.setOnClickListener(this);
        tv_color5.setOnClickListener(this);

        save.setOnClickListener(this);


        im_color_pick_view.setOnTouchListener(new View.OnTouchListener() {

            RelativeLayout.LayoutParams parms;
            float dx = 0, dy = 0, x = 0, y = 0;
            float angle = 0;

            private long lastTouchDown;
            private int CLICK_ACTION_THRESHHOLD = 100;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final ImageView view = (ImageView) v;
                ((BitmapDrawable) view.getDrawable()).setAntiAlias(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:

                        parms = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        dx = event.getRawX() - parms.leftMargin;
                        dy = event.getRawY() - parms.topMargin;

                        savedMatrix.set(matrix);
                        start.set(event.getX(), event.getY());
                        mode = DRAG;
                        lastEvent = null;
                        lastTouchDown = System.currentTimeMillis();
                        break;

                    case MotionEvent.ACTION_POINTER_DOWN:
                        oldDist = spacing(event);
                        if (oldDist > 10f) {
                            savedMatrix.set(matrix);
                            midPoint(mid, event);
                            mode = ZOOM;
                        }
                        lastEvent = new float[4];
                        lastEvent[0] = event.getX(0);
                        lastEvent[1] = event.getX(1);
                        lastEvent[2] = event.getY(0);
                        lastEvent[3] = event.getY(1);
                        d = rotation(event);
                        break;
                    case MotionEvent.ACTION_UP:

                        if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_THRESHHOLD) {

                            float eventX = event.getX();
                            float eventY = event.getY();
                            float[] eventXY = new float[]{eventX, eventY};

                            Matrix invertMatrix = new Matrix();
                            ((ImageView) v).getImageMatrix().invert(invertMatrix);

                            invertMatrix.mapPoints(eventXY);
                            int x = Integer.valueOf((int) eventXY[0]);
                            int y = Integer.valueOf((int) eventXY[1]);


                            Drawable imgDrawable = ((ImageView) v).getDrawable();
                            Bitmap bitmap = ((BitmapDrawable) imgDrawable).getBitmap();

                            //Limit x, y range within bitmap
                            if (x < 0) {
                                x = 0;
                            } else if (x > bitmap.getWidth() - 1) {
                                x = bitmap.getWidth() - 1;
                            }

                            if (y < 0) {
                                y = 0;
                            } else if (y > bitmap.getHeight() - 1) {
                                y = bitmap.getHeight() - 1;
                            }

                            int touchedRGB = bitmap.getPixel(x, y);

                            if (postion == 1) {

                                color1 = "#" + Integer.toHexString(touchedRGB);
                                tv_color1.setBackgroundColor(touchedRGB);

                            } else if (postion == 2) {

                                color2 = "#" + Integer.toHexString(touchedRGB);
                                tv_color2.setBackgroundColor(touchedRGB);


                            } else if (postion == 3) {

                                color3 = "#" + Integer.toHexString(touchedRGB);
                                tv_color3.setBackgroundColor(touchedRGB);


                            } else if (postion == 4) {

                                color4 = "#" + Integer.toHexString(touchedRGB);
                                tv_color4.setBackgroundColor(touchedRGB);


                            } else if (postion == 5) {

                                color5 = "#" + Integer.toHexString(touchedRGB);
                                tv_color5.setBackgroundColor(touchedRGB);


                            }


                        }

                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        lastEvent = null;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG) {

                            matrix.set(savedMatrix);
                            matrix.postTranslate(event.getX() - start.x, event.getY()
                                    - start.y);
                        } else if (mode == ZOOM) {

                            // view.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            float newDist = spacing(event);
                            //if (newDist > 10f) {

                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);

                            //  }
                            view.setImageMatrix(matrix);
                            if (lastEvent != null && event.getPointerCount() == 2) {
                                view.setScaleType(ImageView.ScaleType.MATRIX);
                                newRot = rotation(event);
                                float r = newRot - d;
                                float[] values = new float[9];
                                matrix.getValues(values);
                                float tx = values[2];
                                float ty = values[5];
                                float sx = values[0];
                                float xc = (view.getWidth() / 2) * sx;
                                float yc = (view.getHeight() / 2) * sx;

                                float imageWidth = view.getDrawable().getIntrinsicWidth();
                                float imageHeight = view.getDrawable().getIntrinsicHeight();
                                RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
                                RectF viewRect = new RectF(0, 0, view.getWidth(),
                                        view.getHeight());

                                angle = r;
                                view.setImageMatrix(matrix);

                            }
                        }
                        break;
                }
                view.setImageMatrix(matrix);
                return true;

            }
        });

    }

    public void SetColors() {

        for (int i = 0; i < color_array_list.size(); i++) {

            if (i == 0) {

                tv_color1.setBackgroundColor(Color.parseColor(color_array_list.get(i).toString()));
                color1 = color_array_list.get(i).toString();

            } else if (i == 1) {

                tv_color2.setBackgroundColor(Color.parseColor(color_array_list.get(i).toString()));
                color2 = color_array_list.get(i).toString();

            } else if (i == 2) {

                tv_color3.setBackgroundColor(Color.parseColor(color_array_list.get(i).toString()));
                color3 = color_array_list.get(i).toString();

            } else if (i == 3) {

                tv_color4.setBackgroundColor(Color.parseColor(color_array_list.get(i).toString()));
                color4 = color_array_list.get(i).toString();

            } else if (i == 4) {

                tv_color5.setBackgroundColor(Color.parseColor(color_array_list.get(i).toString()));
                color5 = color_array_list.get(i).toString();

            }
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_color1 = (TextView) findViewById(R.id.tv_color1);
        tv_color2 = (TextView) findViewById(R.id.tv_color2);
        tv_color3 = (TextView) findViewById(R.id.tv_color3);
        tv_color4 = (TextView) findViewById(R.id.tv_color4);
        tv_color5 = (TextView) findViewById(R.id.tv_color5);

        im_color_pick_view = (ImageView) findViewById(R.id.im_color_pick_view);
        save = (ImageView) findViewById(R.id.im_ok);
        color_array_list = AddItem.pixel_array_list;


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_color1) {

            selected_textView = tv_color1;
            postion = 1;

        } else if (v.getId() == R.id.tv_color2) {

            selected_textView = tv_color2;
            postion = 2;

        } else if (v.getId() == R.id.tv_color3) {

            selected_textView = tv_color3;
            postion = 3;

        } else if (v.getId() == R.id.tv_color4) {

            selected_textView = tv_color4;
            postion = 4;


        } else if (v.getId() == R.id.tv_color5) {

            selected_textView = tv_color5;
            postion = 5;

        } else if (v.getId() == R.id.im_ok) {

            color_array_list.clear();
            AddItem.pixel_array_list.clear();
            AddItem.pixel_array_list.add(color1);
            AddItem.pixel_array_list.add(color2);
            AddItem.pixel_array_list.add(color3);
            AddItem.pixel_array_list.add(color4);
            AddItem.pixel_array_list.add(color5);

            Toast.makeText(getApplicationContext(), "" + AddItem.pixel_array_list.size(), Toast.LENGTH_LONG).show();

            Intent intent = new Intent();
            setResult(6, intent);
            finish();


        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (v.getId() == R.id.im_color_pick_view) {

            if (postion == 1) {

                int x = (int) event.getX();
                int y = (int) event.getY();
                int pixel = bitmap_image.getPixel(x, y);
                color1 = String.format("#%08X", pixel);
                tv_color1.setBackgroundColor(Color.parseColor(color1));

            } else if (postion == 2) {

                int x = (int) event.getX();
                int y = (int) event.getY();
                int pixel = bitmap_image.getPixel(x, y);
                color2 = String.format("#%08X", pixel);
                tv_color2.setBackgroundColor(Color.parseColor(color2));


            } else if (postion == 3) {

                int x = (int) event.getX();
                int y = (int) event.getY();
                int pixel = bitmap_image.getPixel(x, y);
                color3 = String.format("#%08X", pixel);
                tv_color3.setBackgroundColor(Color.parseColor(color3));


            } else if (postion == 4) {

                int x = (int) event.getX();
                int y = (int) event.getY();
                int pixel = bitmap_image.getPixel(x, y);
                color4 = String.format("#%08X", pixel);
                tv_color4.setBackgroundColor(Color.parseColor(color4));


            } else if (postion == 5) {

                int x = (int) event.getX();
                int y = (int) event.getY();
                int pixel = bitmap_image.getPixel(x, y);
                color5 = String.format("#%08X", pixel);
                tv_color5.setBackgroundColor(Color.parseColor(color5));


            }


        }
        return false;
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
        // return FloatMath.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }

}
