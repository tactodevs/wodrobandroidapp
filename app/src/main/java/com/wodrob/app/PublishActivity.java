package com.wodrob.app;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.wodrob.app.adapter.PublishOutfitViewPagerAdapter;
import com.wodrob.app.interfaces.FilterListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PublishActivity extends AppCompatActivity implements View.OnClickListener, FilterListener {

    ImageView im_publish_image;
    Toolbar toolbar;
    ViewPager vp_publish;
    PublishOutfitViewPagerAdapter publishOutfitViewPagerAdapter;
    LinearLayout selected_item_display_layout;
    HorizontalScrollView hr_scroll_layout;
    String type = "title";
    String title = "";
    int title_count = 0;
    boolean istitleset = false;
    public static ArrayList<String> selected_ocassion_list;
    public static ArrayList<String> selected_tags_list;

    TextView tv_title;
    ImageView im_forward, im_backward, im_add_tag;
    private ProgressDialog pDialog;

    LinearLayout new_tag_layout,indicator_layout;
    EditText ed_new_tag;
    Bitmap bmp;
    private String search_string;
    TextView[] indicator_array;
    String out_occassion_list="",out_tag_list="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);

        init();

        byte[] byteArray = getIntent().getByteArrayExtra("publish_image");
        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        im_publish_image.setImageBitmap(bmp);

        AddIndicators();
        DisplaySelectedItem("title");

        ed_new_tag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() != 0) {

                    search_string = s.toString();
                    if (type.equals("occasion")) {

                        publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, search_string, type);
                        vp_publish.setAdapter(publishOutfitViewPagerAdapter);
                        vp_publish.setCurrentItem(1);

                    } else if (type.equals("tags")) {
                        publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, search_string, type);
                        vp_publish.setAdapter(publishOutfitViewPagerAdapter);
                        vp_publish.setCurrentItem(2);
                    }

                } else {

                    search_string = "";
                    if (type.equals("occasion")) {
                        publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, search_string, type);
                        vp_publish.setAdapter(publishOutfitViewPagerAdapter);
                        vp_publish.setCurrentItem(1);
                    } else if (type.equals("tags")) {
                        publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, search_string, type);
                        vp_publish.setAdapter(publishOutfitViewPagerAdapter);
                        vp_publish.setCurrentItem(2);
                    }
                }
                // Field2.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    @SuppressLint("NewApi")
    private void AddIndicators() {


        for(int i = 0; i < 4; i++) {
            indicator_array[i] = new TextView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(10,10);
            params.setMargins(4, 4, 4, 4);
            indicator_array[i].setLayoutParams(params);
            indicator_layout.addView(indicator_array[i]);
        }
        Setindicator(0);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void Setindicator(int position) {

        for(int i = 0; i < 4; i++) {

            if(i == position){

                indicator_array[i].setBackground(getResources().getDrawable(R.drawable.selecteditem_dot));

            }else {

                indicator_array[i].setBackground(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            }
        }
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        pDialog = new ProgressDialog(this);

        new_tag_layout = (LinearLayout) findViewById(R.id.new_tag_layout);
        // im_add_tag = (ImageView) findViewById(R.id.im_add_tag);
        ed_new_tag = (EditText) findViewById(R.id.ed_new_tag);

        // im_add_tag.setOnClickListener(this);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        im_publish_image = (ImageView) findViewById(R.id.im_publish_image);
        vp_publish = (ViewPager) findViewById(R.id.vp_publish);
        hr_scroll_layout = (HorizontalScrollView) findViewById(R.id.hr_scroll_layout);
        selected_ocassion_list = new ArrayList<>();
        selected_tags_list = new ArrayList<>();

        tv_title = (TextView) findViewById(R.id.tv_title);
        im_backward = (ImageView) findViewById(R.id.im_backward);
        im_forward = (ImageView) findViewById(R.id.im_forward);
        im_forward.setOnClickListener(this);
        im_backward.setOnClickListener(this);

        publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(this, "", "");
        vp_publish.setAdapter(publishOutfitViewPagerAdapter);
        selected_item_display_layout = (LinearLayout) findViewById(R.id.selected_layout);
        indicator_layout = (LinearLayout) findViewById(R.id.indicator_layout);

        indicator_array = new TextView[4];

        ed_new_tag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if(!ed_new_tag.getText().toString().equals("")) {
                        update_Search(type, ed_new_tag.getText().toString());
                        publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, "", type);
                        vp_publish.setAdapter(publishOutfitViewPagerAdapter);
                        ed_new_tag.setText("");
                        if (type.equalsIgnoreCase("occasion")) {

                            vp_publish.setCurrentItem(1);

                        } else if (type.equalsIgnoreCase("tags")) {

                            vp_publish.setCurrentItem(2);


                        }
                    }
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(ed_new_tag.getWindowToken(), 0);
                }
                return handled;
            }
        });

        vp_publish.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0) {

                    type = "title";
                    tv_title.setText("TITLE");
                    DisplaySelectedItem("title");
                    new_tag_layout.setVisibility(View.VISIBLE);
                    ed_new_tag.setText("");
                    if (title.equals("")) {
                        ed_new_tag.setVisibility(View.VISIBLE);

                    } else {
                        ed_new_tag.setVisibility(View.GONE);

                    }
                    Setindicator(0);
                    //  im_add_tag.setVisibility(View.GONE);

                } else if (position == 1) {

                    type = "occasion";
                    tv_title.setText("OCCASION TYPE");
                    DisplaySelectedItem("occasion");
                    // im_add_tag.setVisibility(View.VISIBLE);
                    //  ed_new_tag.setText("");
                    new_tag_layout.setVisibility(View.VISIBLE);
                    ed_new_tag.setVisibility(View.VISIBLE);
                    Setindicator(1);


                } else if (position == 2) {

                    type = "tags";
                    tv_title.setText("ADDITIONAL TAGS");
                    DisplaySelectedItem("tags");
                    // im_add_tag.setVisibility(View.VISIBLE);
                    //  ed_new_tag.setText("");
                    new_tag_layout.setVisibility(View.VISIBLE);
                    ed_new_tag.setVisibility(View.VISIBLE);
                    Setindicator(2);

                } else if (position == 3) {

                    type = "share";
                    tv_title.setText("SHARE");
                    DisplaySelectedItem("share");
                    ed_new_tag.setVisibility(View.GONE);
                    //   im_add_tag.setVisibility(View.VISIBLE);
                   // ed_new_tag.setText("");
                    new_tag_layout.setVisibility(View.GONE);
                    Setindicator(3);

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @SuppressLint("NewApi")
    public void DisplaySelectedItem(String type) {

        selected_item_display_layout.removeAllViews();

        if (type.equalsIgnoreCase("title")) {

            if (!title.equals("")) {
                final LinearLayout selected_item_textview_layout = new LinearLayout(PublishActivity.this);
                selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                selected_item_textview_layout.removeView(ed_new_tag);
                selected_item_display_layout.removeAllViews();

                final TextView tv_price = new TextView(PublishActivity.this);
                tv_price.setBackgroundColor(Color.parseColor("#00000000"));
                tv_price.setText(title);
                ImageView im_price_remove = new ImageView(PublishActivity.this);
                im_price_remove.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                selected_item_textview_layout.addView(tv_price);
                selected_item_textview_layout.addView(im_price_remove);
                selected_item_display_layout.addView(selected_item_textview_layout);
                istitleset = true;
                ed_new_tag.setVisibility(View.GONE);


                im_price_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        title = "";
                        selected_item_textview_layout.removeAllViews();
                        selected_item_display_layout.removeAllViews();
                        istitleset = false;
                        title_count = 0;
                        DisplaySelectedItem("title");
                        ed_new_tag.setVisibility(View.VISIBLE);

                    }
                });
            }


        } else if (type.equalsIgnoreCase("occasion")) {

            if (selected_ocassion_list.size() != 0) {
                for (int j = 0; j < selected_ocassion_list.size(); j++) {

                    final LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_ocassion_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_ocassion_list.remove(((ImageView) v).getId());
                            DisplaySelectedItem("occasion");
                            RefreshView("occasion");
                        }
                    });
                }
            }

        } else if (type.equalsIgnoreCase("tags")) {

            if (selected_tags_list.size() != 0) {
                for (int j = 0; j < selected_tags_list.size(); j++) {

                    LinearLayout selected_item_textview_layout = new LinearLayout(this);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(this);
                    tv_seleected_text.setText(selected_tags_list.get(j).toString());
                    ImageView imageView = new ImageView(this);
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected_tags_list.remove(((ImageView) v).getId());
                            DisplaySelectedItem("tags");
                            RefreshView("tags");
                        }
                    });
                }
            }

        }
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.im_backward) {

            if (type.equalsIgnoreCase("tags")) {

                vp_publish.setCurrentItem(1);

            } else if (type.equalsIgnoreCase("occasion")) {

                vp_publish.setCurrentItem(0);

            } else if(type.equalsIgnoreCase("share")){

                vp_publish.setCurrentItem(2);

            }

        } else if (v.getId() == R.id.im_forward) {

            if (type.equalsIgnoreCase("title")) {

                vp_publish.setCurrentItem(1);

            } else if (type.equalsIgnoreCase("occasion")) {

                vp_publish.setCurrentItem(2);

            } else if (type.equalsIgnoreCase("tags")){

                vp_publish.setCurrentItem(3);

            }

        }


    }

    private void PublishOutfit() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new PublishOutfit().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "outfit/create");
            } else {
                new PublishOutfit().execute(WodrobConstant.BASE_URL + "outfit/create");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void DoFilter(String type) {

        DisplaySelectedItem(type);
    }

   /* private class PublishOutfit extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String outfitProducts;
        JSONObject dataobject;
        JSONArray jsonArray;
        JSONObject jObjectData;

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            jsonArray = new JSONArray();

            for (int i = 0; i < CanvasActivity.canvasitems.size(); i++) {
                jObjectData = new JSONObject();

                try {

                    String image_tag = CanvasActivity.canvasitems.get(i).getTag().toString();
                    String[] tag_array = image_tag.split("/");

                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) CanvasActivity.canvasitems.get(i).getLayoutParams();
                    jObjectData.put("product_id", tag_array[0]);
                    jObjectData.put("product_img_id", tag_array[1]);
                    jObjectData.put("pref_store", "1");
                    jObjectData.put("angle", CanvasActivity.canvasitems.get(i).getRotation());
                    jObjectData.put("depth", "0");
                    jObjectData.put("x", layoutParams.leftMargin);
                    jObjectData.put("y", layoutParams.rightMargin);
                    jObjectData.put("height", layoutParams.height);
                    jObjectData.put("width", layoutParams.width);
                    jObjectData.put("is_flipped", Integer.parseInt(tag_array[2]));
                    jObjectData.put("in_wishlist",Integer.parseInt(tag_array[3]));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jObjectData);

            }
            dataobject = new JSONObject();

            try {

                dataobject.put("abcd", jsonArray);

            } catch (JSONException e) {

                e.printStackTrace();

            }

            outfitProducts = jsonArray.toString();
            Log.d("JSon Out", outfitProducts);


        }

        @Override
        protected Void doInBackground(String... urls) {

            String charset = "UTF-8";
            File uploadFile1 = new File(Environment.getExternalStorageDirectory() + File.separator + "outfitimages/outfit.jpg");


            try {

                MultipartUtility multipart = new MultipartUtility(urls[0], charset);

                multipart.addFormField("outfitProducts", outfitProducts.toString());
                multipart.addFormField("src", "app");
                multipart.addFilePart("image", uploadFile1);
                multipart.addFormField("name", "outfit1");
                multipart.addFormField("tags", out_tag_list);
                multipart.addFormField("occasion",out_occassion_list);
                multipart.addFormField("creator", "");

                List<String> response = multipart.finish();

                System.out.println("SERVER REPLIED:");
                StringBuilder sb = new StringBuilder();

                for (String line : response) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (IOException ex) {

                String error = ex.toString();
                System.err.println(ex);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), "Outfit Published Successfully", Toast.LENGTH_LONG).show();
                            finish();
                            ;

                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Somthing is wrong ! Please try again after some time", Toast.LENGTH_LONG).show();

                }
            }
            pDialog.dismiss();

        }
    }*/


    private class PublishOutfit extends AsyncTask<String, Void, Void> {

        String out;
        String data = "";
        Bitmap bitmap = null;
        int height = 0;
        int width = 0;


        private String Content;
        private String Error = null;
        String outfitProducts;
        JSONObject dataobject;
        JSONArray jsonArray;
        JSONObject jObjectData;

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            jsonArray = new JSONArray();

            for (int i = 0; i < CanvasActivity.canvasitems.size(); i++) {
                jObjectData = new JSONObject();

                try {

                    String image_tag = CanvasActivity.canvasitems.get(i).getTag().toString();
                    String[] tag_array = image_tag.split("/");

                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) CanvasActivity.canvasitems.get(i).getLayoutParams();
                    jObjectData.put("product_id", tag_array[0]);
                    jObjectData.put("product_img_id", tag_array[1]);
                    jObjectData.put("pref_store", "1");
                    jObjectData.put("angle", CanvasActivity.canvasitems.get(i).getRotation());
                    jObjectData.put("depth", "0");
                    jObjectData.put("x", layoutParams.leftMargin);
                    jObjectData.put("y", layoutParams.rightMargin);
                    jObjectData.put("height", layoutParams.height);
                    jObjectData.put("width", layoutParams.width);
                    jObjectData.put("is_flipped", Integer.parseInt(tag_array[2]));
                    jObjectData.put("in_wishlist",Integer.parseInt(tag_array[3]));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jObjectData);

            }
            dataobject = new JSONObject();

            try {

                dataobject.put("abcd", jsonArray);

            } catch (JSONException e) {

                e.printStackTrace();

            }

            outfitProducts = jsonArray.toString();



                bitmap = ((BitmapDrawable) im_publish_image.getDrawable()).getBitmap();
                height = bitmap.getHeight();
                width = bitmap.getWidth();


            try {

                data +="&"+ URLEncoder.encode("outfitProducts", "UTF-8")+"="+ outfitProducts.toString()+"&"
                        + URLEncoder.encode("src", "UTF-8")+"="+"app"+"&"
                        + URLEncoder.encode("image", "UTF-8")+"="+Wodrob.image_to_String(bitmap)+"&"
                        + URLEncoder.encode("name", "UTF-8")+"="+title+"&"
                        + URLEncoder.encode("tags", "UTF-8")+"="+out_tag_list+"&"
                        + URLEncoder.encode("occasion", "UTF-8")+"="+out_occassion_list+"&"
                        + URLEncoder.encode("creator", "UTF-8")+"="+"";

                  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equals("true")){

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else{

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_publish_outfit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_ok) {

            if (title.equals("")) {

                Toast.makeText(getApplicationContext(), "Please add a title", Toast.LENGTH_LONG).show();


            } else if (selected_ocassion_list.size() == 0) {

                Toast.makeText(getApplicationContext(), "Please select ocassion type", Toast.LENGTH_LONG).show();

            } else {

                Wodrob.SaveImage(bmp);
                out_occassion_list = GetAddItemOutPut.GetOccassionlist(PublishActivity.this);
                out_tag_list = GetAddItemOutPut.GetOccassionlist(PublishActivity.this);
                GenereateData();
                PublishOutfit();

            }


            return true;
        } else if (id == android.R.id.home) {

            onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    private void GenereateData() {


    }

    public void update_Search(String type, String data) {

        if (type.equalsIgnoreCase("tags")) {

            selected_tags_list.add(data);
            DisplaySelectedItem("tags");

        } else if (type.equalsIgnoreCase("occasion")) {

            selected_ocassion_list.add(data);
            DisplaySelectedItem("occasion");


        } else if (type.equalsIgnoreCase("title")) {

            title = ed_new_tag.getText().toString();
            DisplaySelectedItem("title");

        }
    }

    public void RefreshView(String type) {
        if (type.equalsIgnoreCase("tags")) {

            publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, "", type);
            vp_publish.setAdapter(publishOutfitViewPagerAdapter);
            ed_new_tag.setText("");
            vp_publish.setCurrentItem(2);

        } else if (type.equalsIgnoreCase("occasion")) {


            publishOutfitViewPagerAdapter = new PublishOutfitViewPagerAdapter(PublishActivity.this, "", type);
            vp_publish.setAdapter(publishOutfitViewPagerAdapter);
            ed_new_tag.setText("");
            vp_publish.setCurrentItem(1);


        } else if (type.equalsIgnoreCase("title")) {


        }
    }

}
