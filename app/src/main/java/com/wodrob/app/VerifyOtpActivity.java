package com.wodrob.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class VerifyOtpActivity extends AppCompatActivity implements View.OnClickListener{

    EditText ed_otp;
    Button btn_verify,btn_resend;
    ProgressDialog pDialog;
    TextView tv_phone;
    int otp;
    Toolbar toolbar;
    private BroadcastReceiver mIntentReceiver;
    private TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());

        init();


        tv_phone.setText(WodrobConstant.otp_phone);

    }


    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        mIntentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getStringExtra("get_msg");

                msg = msg.replace("\n", "");
                String body = msg.substring(msg.lastIndexOf(":")+1, msg.length());
                String pNumber = msg.substring(0,msg.lastIndexOf(":"));

                ed_otp.setText(body.split(" ")[0]);
                otp = Integer.parseInt(ed_otp.getText().toString());
                Verify();

            }
        };
        this.registerReceiver(mIntentReceiver, intentFilter);
    }

    @Override
    protected void onPause() {

        super.onPause();
        this.unregisterReceiver(this.mIntentReceiver);
    }


    private void init() {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);
       // getSupportActionBar().hide();


        tv_title = (TextView) findViewById(R.id.tv_activity_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_title.setTypeface(face);

        ed_otp = (EditText) findViewById(R.id.ed_otp);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        btn_verify = (Button) findViewById(R.id.btn_verify);
        btn_resend = (Button) findViewById(R.id.btn_resend);
        btn_resend.setOnClickListener(this);
        btn_verify.setOnClickListener(this);
        pDialog = new ProgressDialog(this);
        
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_verify){

            otp = Integer.parseInt(ed_otp.getText().toString());
            Verify();

        }else if(v.getId() == R.id.btn_resend){

            ResendOtp();

        }
    }



    private void ResendOtp() {


        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new Resend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "otp/generate");

            }
            else {

                new Resend().execute(WodrobConstant.BASE_URL+"otp/generate");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void Verify() {

        if(getIntent().getExtras().getString("type").equalsIgnoreCase("Login")){


            try {
                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                    new VeriftyOtp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "login/otp/verify");

                }
                else {

                    new VeriftyOtp().execute(WodrobConstant.BASE_URL+"login/otp/verify");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }else {



            try {
                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                    new VeriftyOtp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "otp/verify");

                }
                else {

                    new VeriftyOtp().execute(WodrobConstant.BASE_URL+"otp/verify");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


    }

    /*verify otp*/


    private class VeriftyOtp extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                if(getIntent().getExtras().getString("type").equalsIgnoreCase("Login")){


                    data +="&"+ URLEncoder.encode("client_id", "UTF-8")+"="+ WodrobConstant.client_id+"&"
                            + URLEncoder.encode("client_secret", "UTF-8")+"="+WodrobConstant.client_secret+"&"
                            + URLEncoder.encode("email", "UTF-8")+"="+Wodrob.getPreference(getApplicationContext(),"email")+"&"
                            + URLEncoder.encode("otp", "UTF-8")+"="+otp+"&"
                            + URLEncoder.encode("uuid", "UTF-8")+"="+Wodrob.GetUuid(VerifyOtpActivity.this);

                }else{

                    data +="&"+ URLEncoder.encode("mobile", "UTF-8")+"="+ getIntent().getExtras().getString("phone")+"&"
                            + URLEncoder.encode("country_code", "UTF-8")+"="+getIntent().getExtras().getString("country_code")+"&"
                            + URLEncoder.encode("type", "UTF-8")+"="+getIntent().getExtras().getString("type")+"&"
                            + URLEncoder.encode("otp", "UTF-8")+"="+otp;

                }



                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                //conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(VerifyOtpActivity.this));
                conn.setRequestProperty("Authorization", "Bearer qG3Tq3oN6ixE0OhAT6ZsZzfRRMgDr0IUf4CtH2jy");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {

                Error = ex.getMessage();

            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if(jsonObject.has("error")){

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else if(jsonObject.has("status")){

                            String status = jsonObject.getString("status");
                            if(status.equals("true")){

                                if(getIntent().getExtras().getString("type").equalsIgnoreCase("Login")){
                                    JSONObject data_object = jsonObject.getJSONObject("data");

                                    JSONObject token_object = data_object.getJSONObject("token");

                                    String access_token = token_object.getString("access_token");

                                    String token_type = token_object.getString("token_type");

                                    String expires_in = token_object.getString("expires_in");

                                    String refresh_token = token_object.getString("refresh_token");


                                    Wodrob.setPreference(getApplicationContext(),getIntent().getExtras().getString("phone"),"phone");

                                    Intent resultData = new Intent();
                                    resultData.putExtra("status", "true");
                                    resultData.putExtra("access_token", access_token);
                                    resultData.putExtra("token_type", token_type);
                                    resultData.putExtra("expires_in", expires_in);
                                    resultData.putExtra("refresh_token", refresh_token);

                                    setResult(1, resultData);
                                    finish();

                                }else{

                                    Wodrob.setPreference(getApplicationContext(),getIntent().getExtras().getString("phone"),"phone");

                                    Intent resultData = new Intent();
                                    resultData.putExtra("status", "true");
                                    setResult(1, resultData);
                                    finish();
                                }



                            }else {

                                Intent resultData = new Intent();
                                resultData.putExtra("status", "false");
                                setResult(1, resultData);
                                finish();

                            }

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }
    /*ends*/
     /* sending otp request*/

    private class Resend extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data +="&"+ URLEncoder.encode("mobile", "UTF-8")+"="+ getIntent().getExtras().getString("phone")+"&"
                        + URLEncoder.encode("country_code", "UTF-8")+"="+getIntent().getExtras().getString("country_code")+"&"
                        + URLEncoder.encode("type", "UTF-8")+"="+getIntent().getExtras().getString("type");

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
               // conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(VerifyOtpActivity.this));
                conn.setRequestProperty("Authorization", "Bearer qG3Tq3oN6ixE0OhAT6ZsZzfRRMgDr0IUf4CtH2jy");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if(jsonObject.has("error")){

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else if(jsonObject.has("status")){

                            String status = jsonObject.getString("status");
                            if(status.equals("true")){

                                Toast.makeText(getApplicationContext(),"Otp has requested successfully",Toast.LENGTH_LONG).show();

                            }else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                            }

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }
    /*ends*/
}
