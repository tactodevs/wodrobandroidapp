package com.wodrob.app;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.adapter.SimilarItemsAdapter;
import com.wodrob.app.model.WearSimilarItemsModel;
import com.wodrob.app.views.SingleSpacingDecoration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ViewCatalog extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    private ProgressDialog pDialog;
    TextView tv_status, tv_name, tv_category, tv_brand, tv_size, tv_prize, tv_items_purchased, tv_wishlist_price;
    TextView tv_items_purchased_title, tv_outfits_created_title, tv_care_tips_title, tv_size_title,tv_outfits_created;
    ImageView im_item_image;
    RecyclerView similar_items, rv_suggested_outfit;
    private LinearLayoutManager layoutManager;
    private LinearLayoutManager outfitlayputmanager;
    private LinearLayoutManager lookbooklayoutmanager;
    TextView tv_activity_title;
    TextView tv_similar_items_title,tv_suggested_outfit_title,tv_suggested_loobook_title;


    LinearLayout ll_additional_tags, ll_care_tip_item_layout,ll_color_layout;
    DbHelper db;


    WearSimilarItemsModel wearSimilarItemsModel;
    ArrayList<WearSimilarItemsModel> similarItemsModelArrayList;
    ArrayList<WearSimilarItemsModel> suggestedoutfitarraylist;
    ArrayList<WearSimilarItemsModel> suggestedlooksarraylist;
    private SimilarItemsAdapter similaritemadapter;
    private SimilarItemsAdapter suggestedoutfitadapter;
    private SimilarItemsAdapter suggestedlookbookadapter;
    Button btn_buy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_catalog);

        init();
        btn_buy.setOnClickListener(this);

        setlayout();
        SetFonts();

        if(getIntent().getExtras().getString("type").equals("Add")){

            HideLayout();
        }

        GetData();

    }

    @SuppressLint("NewApi")
    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
        db = new DbHelper(this);
        pDialog = new ProgressDialog(this);
        btn_buy = (Button) findViewById(R.id.btn_buy);

        tv_similar_items_title = (TextView) findViewById(R.id.tv_similar_items_title);
        tv_suggested_outfit_title = (TextView) findViewById(R.id.tv_suggested_outfit_title);
        //tv_suggested_loobook_title = (TextView) findViewById(R.id.tv_suggested_look_title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_name = (TextView) findViewById(R.id.tv_title);
        tv_category = (TextView) findViewById(R.id.tv_category_name);
        tv_brand = (TextView) findViewById(R.id.tv_brand);
        tv_size = (TextView) findViewById(R.id.tv_size);
        tv_prize = (TextView) findViewById(R.id.tv_price);
      //  tv_items_purchased = (TextView) findViewById(R.id.tv_no_of_items_purchased);
        tv_wishlist_price = (TextView) findViewById(R.id.tv_wishlist_price);

      //  tv_items_purchased_title = (TextView) findViewById(R.id.item_purchase_title);
        tv_outfits_created_title = (TextView) findViewById(R.id.tv_outfits_created_title);
        tv_outfits_created = (TextView) findViewById(R.id.tv_outfits_created);
        tv_care_tips_title = (TextView) findViewById(R.id.tv_care_tip_title);
        tv_size_title = (TextView) findViewById(R.id.tv_size_title);

        similar_items = (RecyclerView) findViewById(R.id.rv_similar_items);
       // rv_suggested_lookbook = (RecyclerView) findViewById(R.id.rv_suggested_looks);
        rv_suggested_outfit = (RecyclerView) findViewById(R.id.rv_suggested_outfits);

        layoutManager = new LinearLayoutManager(ViewCatalog.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        lookbooklayoutmanager = new LinearLayoutManager(ViewCatalog.this);
        lookbooklayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);

        outfitlayputmanager = new LinearLayoutManager(ViewCatalog.this);
        outfitlayputmanager.setOrientation(LinearLayoutManager.HORIZONTAL);

        im_item_image = (ImageView) findViewById(R.id.im_item_image);
        similarItemsModelArrayList = new ArrayList<>();
        suggestedoutfitarraylist = new ArrayList<>();
        suggestedlooksarraylist = new ArrayList<>();

        ll_additional_tags = (LinearLayout) findViewById(R.id.ll_additional_tags);
        ll_care_tip_item_layout = (LinearLayout) findViewById(R.id.ll_care_tip_item_layout);
        ll_color_layout = (LinearLayout) findViewById(R.id.ll_color_layout);

    }

    private void setlayout() {

        similaritemadapter = new SimilarItemsAdapter(similarItemsModelArrayList, R.layout.recomendations_item_layout, "items");
        similar_items.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        similar_items.setAdapter(similaritemadapter);
        similar_items.setLayoutManager(layoutManager);

        suggestedlookbookadapter = new SimilarItemsAdapter(suggestedlooksarraylist, R.layout.recomendations_item_layout, "lookbook");


        suggestedoutfitadapter = new SimilarItemsAdapter(suggestedoutfitarraylist, R.layout.recomendations_item_layout, "outfits");
        rv_suggested_outfit.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        rv_suggested_outfit.setAdapter(suggestedoutfitadapter);
        rv_suggested_outfit.setLayoutManager(outfitlayputmanager);

    }

    private void GetData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "product/view");

            } else {

                new FetchItemData().execute(WodrobConstant.BASE_URL + "product/view");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_catalog, menu);

        MenuItem add_to_canvas = menu.findItem(R.id.mi_add_to_Canvas);
        MenuItem mi_add_to_wishlist = menu.findItem(R.id.mi_add_to_wishlist);

        if(getIntent().getExtras().getString("type").equals("Add")){

            add_to_canvas.setVisible(true);
            mi_add_to_wishlist.setVisible(false);
            invalidateOptionsMenu();

        }else {

            add_to_canvas.setVisible(false);
            mi_add_to_wishlist.setVisible(true);
            invalidateOptionsMenu();

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mi_add_to_wishlist) {

            UpdateStatus();

            return true;
        } else if (id == android.R.id.home) {

            onBackPressed();

        }else if(id == R.id.mi_add_to_Canvas){

            Intent intent = new Intent();
            intent.putExtra("status", "added");
            setResult(1, intent);
            finish();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_buy) {

            //  UpdateStatus();

        }
    }

    private void UpdateStatus() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new AddToWishList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "wishlist/add");

            } else {

                new AddToWishList().execute(WodrobConstant.BASE_URL + "wishlist/add");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /*get item data*/


    private class AddToWishList extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("product_id", "UTF-8") + "=" + getIntent().getStringExtra("product_id");

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            finish();


                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();
                suggestedlookbookadapter.notifyDataSetChanged();

            }
        }
    }

    private class FetchItemData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("product_id", "UTF-8") + "=" + getIntent().getStringExtra("product_id");

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray size_array = dataobject.getJSONArray("size");
                            JSONArray image_array = dataobject.getJSONArray("images");
                            JSONObject image_object = image_array.getJSONObject(0);
                            JSONArray tag_array = dataobject.getJSONArray("tags");
                            /*JSONArray care_tip_array = dataobject.getJSONArray("caretips");
                            for(int l = 0; l<care_tip_array.length();l++){

                                //CreateCareTip(db.GetCareTipImage(care_tip_array.get(l).toString()));
                                CreateCareTip(db.GetCareTipImage("Dry Clean: Any Solvent Except Trichloroethylene"));


                            }*/
                            if (tag_array.length() > 0) {

                                ll_additional_tags.setVisibility(View.VISIBLE);

                                for (int k = 0; k < tag_array.length(); k++) {

                                    CreateTags(tag_array.get(k).toString());

                                }

                            }


                            String name = dataobject.getString("name");
                            String id = dataobject.getString("_id");
                            String category = dataobject.getString("category");
                            String brand = dataobject.getString("brand");
                            String filename = image_object.getString("filename");
                            String price = dataobject.getString("mrp");
                            String outfits_created = dataobject.getString("outfit_count");
                            JSONArray color_array = dataobject.getJSONArray("color");

                            ll_color_layout.setWeightSum(color_array.length());
                            for(int i = 0;i<color_array.length();i++){

                                CreateColor(color_array.get(i).toString().split("_")[0]);
                            }


                            tv_name.setText(name);
                            tv_category.setText(category.split("_")[1]);
                            if (!brand.equals("")) {

                                tv_brand.setText(brand.split("_")[1]);

                            }
                            tv_size.setText(size_array.getString(0));
                          //  tv_items_purchased.setText("");
                            tv_prize.setText("₹" + price);
                            tv_activity_title.setText(name);
                            tv_wishlist_price.setText("₹" + price);
                            tv_outfits_created.setText(outfits_created);

                            Picasso.with(im_item_image.getContext()).load(filename).into(im_item_image);

//                            fetching similar,suggested items,outlookk

                            JSONArray similar_items = dataobject.getJSONArray("suggestedItems");
                            JSONArray suggestedOutfits = dataobject.getJSONArray("suggestedOutfits");
                            JSONArray suggestedLooks = dataobject.getJSONArray("suggestedLooks");

                            //similar items
                            for (int i = 0; i < similar_items.length(); i++) {

                                JSONObject similar_items_object = similar_items.getJSONObject(i);
                                String similar_item_product_id = similar_items_object.getString("product_id");

                                JSONObject similar_items_product_object = similar_items_object.getJSONObject("product");
                                String similar_item_name = similar_items_product_object.getString("name");
                                //  String similar_item_price = similar_items_product_object.getString("price");


                                JSONArray similar_items_image_array = similar_items_product_object.getJSONArray("images");
                                JSONObject similar_items_image_object = similar_items_image_array.getJSONObject(0);

                                String similar_item_image_path = similar_items_image_object.getString("filename");

                                wearSimilarItemsModel = new WearSimilarItemsModel(similar_item_product_id, similar_item_name, similar_item_image_path, "", "");
                                similarItemsModelArrayList.add(wearSimilarItemsModel);

                            }

                            //suggested looks
                            for (int i = 0; i < suggestedLooks.length(); i++) {

                                JSONObject suggestedLooksJSONObject = suggestedLooks.getJSONObject(i);
                                String suggested_looks_product_id = suggestedLooksJSONObject.getString("_id");
                                JSONObject suggested_looks_image_object = suggestedLooksJSONObject.getJSONObject("image");
                                String suggested_looks_image_path = suggested_looks_image_object.getString("url");
                                // String suggested_look_createdby = suggestedLooksJSONObject.getString("createdby");

                                wearSimilarItemsModel = new WearSimilarItemsModel(suggested_looks_product_id, "", suggested_looks_image_path, "", "");
                                suggestedlooksarraylist.add(wearSimilarItemsModel);

                            }

                            //suggested outfits
                            for (int i = 0; i < suggestedOutfits.length(); i++) {

                                JSONObject suggested_outfit_object = suggestedOutfits.getJSONObject(i);

                                if (suggested_outfit_object.has("image")) {

                                    String suggested_outfit_product_id = suggested_outfit_object.getString("_id");
                                    String suggested_outfit_name = suggested_outfit_object.getString("name");
                                    String suggested_created_by = suggested_outfit_object.getString("created_by");

                                    String suggested_outfit_image_path = suggested_outfit_object.getString("image");

                                    //    String suggested_outfit_createdby = suggested_outfit_object.getString("createdby");
                                    wearSimilarItemsModel = new WearSimilarItemsModel(suggested_outfit_product_id, suggested_outfit_name, suggested_outfit_image_path, "", suggested_created_by);
                                    suggestedoutfitarraylist.add(wearSimilarItemsModel);

                                }

                            }


                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();
                suggestedlookbookadapter.notifyDataSetChanged();

            }
        }
    }

    @SuppressLint("NewApi")
    private void CreateTags(String tags) {
        final LinearLayout selected_item_textview_layout = new LinearLayout(this);
        selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
        selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        selected_item_textview_layout.setLayoutParams(params);

        TextView tv_seleected_text = new TextView(this);
        tv_seleected_text.setText(tags);
        tv_seleected_text.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        //tv_seleected_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        tv_seleected_text.setTextSize(16);
        selected_item_textview_layout.addView(tv_seleected_text);
        ll_additional_tags.addView(selected_item_textview_layout);

    }

    private void CreateCareTip(String image_path) {
        ImageView im_caretip = new ImageView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        im_caretip.setLayoutParams(params);
        if (!image_path.equals("")) {

            Picasso.with(im_caretip.getContext()).load(image_path).into(im_caretip);

        }
        ll_care_tip_item_layout.addView(im_caretip);

    }

    public void SetFonts() {

        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_name);
        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_activity_title);
        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_brand);
       // Wodrob.AddFont(this, "opensans_italic.ttf", tv_items_purchased_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_outfits_created_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_care_tips_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_size_title);
    }

    public void HideLayout(){

        similar_items.setVisibility(View.GONE);
        rv_suggested_outfit.setVisibility(View.GONE);

        tv_similar_items_title.setVisibility(View.GONE);
        tv_suggested_outfit_title.setVisibility(View.GONE);
        //tv_suggested_loobook_title.setVisibility(View.GONE);

    }

    private void CreateColor(String color_id) {

        String hexcode = db.GetHexCode(color_id);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, 1);
        LinearLayout ll_color = new LinearLayout(this);
        ll_color.setLayoutParams(param);
        ll_color.setBackgroundColor(Color.parseColor("#" + hexcode));
        ll_color_layout.addView(ll_color);
    }

}
