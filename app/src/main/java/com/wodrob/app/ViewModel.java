package com.wodrob.app;

/**
 * Created by Ramees on 1/17/2016.
 */
public class ViewModel {
    String title;

    public ViewModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
