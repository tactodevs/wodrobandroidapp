package com.wodrob.app;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ActionMenuView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.adapter.SimilarItemsAdapter;
import com.wodrob.app.model.WearSimilarItemsModel;
import com.wodrob.app.views.SingleSpacingDecoration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class WearViewItem extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolbar;
    private ProgressDialog pDialog;
    TextView tv_status, tv_name, tv_category, tv_brand, tv_size, tv_prize, tv_date, tv_wishlist_price,tv_last_worn,tv_no_of_times_worn,tv_paired_outfits;
    TextView tv_date_of_purchase_title,tv_last_worn_title,tv_no_of_times_worn_title,tv_paired_outfits_title,tv_care_tip_title;
    ImageView im_item_image,im_brand_icon;
    ImageView im_rating_star1,im_rating_star2,im_rating_star3,im_rating_star4,im_rating_star5;
    ArrayList<ImageView> ar_rating;
    RecyclerView similar_items, rv_suggested_outfit;
    private LinearLayoutManager layoutManager;
    private LinearLayoutManager outfitlayputmanager;
    private LinearLayoutManager lookbooklayoutmanager;
    Button btn_delete;

    LinearLayout ll_additional_tags,ll_care_tip_item_layout,ll_color_layout;
    RelativeLayout ll_status_layout;
    RelativeLayout ll_buy_layout;
    DbHelper db;

    TextView tv_activity_title;
    TextView tv_suggested_look_title,tv_suggested_outfit_title,tv_similar_items_title;
    WearSimilarItemsModel wearSimilarItemsModel;
    ArrayList<WearSimilarItemsModel> similarItemsModelArrayList;
    ArrayList<WearSimilarItemsModel> suggestedoutfitarraylist;
    ArrayList<WearSimilarItemsModel> suggestedlooksarraylist;
    private SimilarItemsAdapter similaritemadapter;
    private SimilarItemsAdapter suggestedoutfitadapter;
    private SimilarItemsAdapter suggestedlookbookadapter;
    Spinner sp_status;
    String selected_spinner_item;
    int sp_prev_item_position;
    boolean sp_isFirst_time = true;
    int spinner_count = 0;
    int rating_count,rating_to_Send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wear_view_item);
        //FontHelper.applyFont(this, findViewById(R.id.root_layout), "fonts/opensansregular.ttf");


       init();
       setlayout();
       SetFonts();
        if(getIntent().getExtras().getString("type").equalsIgnoreCase("add")){

            HideLayout();
        }
       GetData();

    }

    @SuppressLint("NewApi")
    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        db = new DbHelper(this);
        pDialog = new ProgressDialog(this);
        sp_status = (Spinner) findViewById(R.id.sp_status);
        im_brand_icon = (ImageView) findViewById(R.id.im_brand_icon);
        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
       // tv_suggested_look_title = (TextView) findViewById(R.id.tv_suggested_look_title);
        tv_suggested_outfit_title = (TextView) findViewById(R.id.tv_suggested_outfit_title);
        tv_similar_items_title = (TextView) findViewById(R.id.tv_similar_items_title);
        btn_delete = (Button) findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(this);
        ll_color_layout = (LinearLayout) findViewById(R.id.ll_color_layout);

        sp_prev_item_position = sp_status.getSelectedItemPosition();
        sp_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.divider));
                ((TextView) parent.getChildAt(0)).setTextSize(20);
                ((TextView) parent.getChildAt(0)).setGravity(Gravity.TOP);

                selected_spinner_item = sp_status.getSelectedItem().toString().toLowerCase();
                if (!sp_isFirst_time) {

                    sendsatus();

                }
                if (spinner_count > 0) {

                    sp_isFirst_time = false;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       // sp_status.getBackground().setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_name = (TextView) findViewById(R.id.tv_title);
        tv_category = (TextView) findViewById(R.id.tv_category_name);
        tv_brand = (TextView) findViewById(R.id.tv_brand);
        tv_size = (TextView) findViewById(R.id.tv_size);
        tv_prize = (TextView) findViewById(R.id.tv_price);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_wishlist_price = (TextView) findViewById(R.id.tv_wishlist_price);
        tv_last_worn = (TextView) findViewById(R.id.tv_last_worn);
        tv_no_of_times_worn = (TextView) findViewById(R.id.tv_worn_count);
        tv_paired_outfits = (TextView) findViewById(R.id.tv_paired_outfits);

        tv_date_of_purchase_title = (TextView) findViewById(R.id.tv_date_of_purchase_title);
        tv_last_worn_title = (TextView) findViewById(R.id.last_worn);
        tv_no_of_times_worn_title = (TextView) findViewById(R.id.tv_no_of_times_worn_title);
        tv_paired_outfits_title = (TextView) findViewById(R.id.paired_outfit_title);
        tv_care_tip_title = (TextView) findViewById(R.id.tv_care_tip_title);

        similar_items = (RecyclerView) findViewById(R.id.rv_similar_items);
       // rv_suggested_lookbook = (RecyclerView) findViewById(R.id.rv_suggested_looks);
        rv_suggested_outfit = (RecyclerView) findViewById(R.id.rv_suggested_outfits);

        layoutManager = new LinearLayoutManager(WearViewItem.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        lookbooklayoutmanager = new LinearLayoutManager(WearViewItem.this);
        lookbooklayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);

        outfitlayputmanager = new LinearLayoutManager(WearViewItem.this);
        outfitlayputmanager.setOrientation(LinearLayoutManager.HORIZONTAL);

        im_item_image = (ImageView) findViewById(R.id.im_item_image);
        similarItemsModelArrayList = new ArrayList<>();
        suggestedoutfitarraylist = new ArrayList<>();
        suggestedlooksarraylist = new ArrayList<>();

        ll_additional_tags = (LinearLayout) findViewById(R.id.ll_additional_tags);
        ll_care_tip_item_layout = (LinearLayout) findViewById(R.id.ll_care_tip_item_layout);
        ll_buy_layout = (RelativeLayout) findViewById(R.id.ll_buy_layout);
        ll_status_layout = (RelativeLayout) findViewById(R.id.ll_status_layout);

        if(!getIntent().getExtras().getString("is_whishlist").equalsIgnoreCase("WISHLIST")) {

            ll_status_layout.setVisibility(View.VISIBLE);
            ll_buy_layout.setVisibility(View.GONE);

        }else {

            ll_status_layout.setVisibility(View.GONE);
            ll_buy_layout.setVisibility(View.VISIBLE);

        }

        /*rating stars imageview initialisation*/
        ar_rating = new ArrayList<>();
        im_rating_star1 = (ImageView) findViewById(R.id.im_rating_star1);
        im_rating_star2 = (ImageView) findViewById(R.id.im_rating_star2);
        im_rating_star3 = (ImageView) findViewById(R.id.im_rating_star3);
        im_rating_star4 = (ImageView) findViewById(R.id.im_rating_star4);
        im_rating_star5 = (ImageView) findViewById(R.id.im_rating_star5);

        ar_rating.add(im_rating_star1);
        ar_rating.add(im_rating_star2);
        ar_rating.add(im_rating_star3);
        ar_rating.add(im_rating_star4);
        ar_rating.add(im_rating_star5);
        /*rating on click listener*/

        for(int i = 0;i<ar_rating.size();i++){

            final int finalI = i;
            ar_rating.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(rating_count == 1 && finalI == 0){

                        rating_to_Send = 0;
                    }else{

                        rating_to_Send = finalI +1;

                    }
                    update_rating();

                }
            });
        }


    }

    private void update_rating() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new UpdateRating().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "item/rate");

            } else {

                new UpdateRating().execute(WodrobConstant.BASE_URL + "item/rate");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void Delete() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new DeleteItem().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "item/delete");

            } else {

                new DeleteItem().execute(WodrobConstant.BASE_URL + "item/delete");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void sendsatus(){

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new UpdateStatus().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "item/status");

            } else {

                new UpdateStatus().execute(WodrobConstant.BASE_URL + "item/status");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void setlayout() {

        similaritemadapter = new SimilarItemsAdapter(similarItemsModelArrayList, R.layout.recomendations_item_layout, "items");
        similar_items.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        similar_items.setAdapter(similaritemadapter);
        similar_items.setLayoutManager(layoutManager);

        suggestedlookbookadapter = new SimilarItemsAdapter(suggestedlooksarraylist, R.layout.recomendations_item_layout, "lookbook");

        suggestedoutfitadapter = new SimilarItemsAdapter(suggestedoutfitarraylist, R.layout.recomendations_item_layout, "outfits");
        rv_suggested_outfit.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        rv_suggested_outfit.setAdapter(suggestedoutfitadapter);
        rv_suggested_outfit.setLayoutManager(outfitlayputmanager);

    }

    private void GetData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "item/view");

            } else {

                new FetchItemData().execute(WodrobConstant.BASE_URL + "item/view");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_item, menu);

        MenuItem add_to_canvas = menu.findItem(R.id.mi_add_to_canvas);

        if(getIntent().getExtras().getString("type").equalsIgnoreCase("Add")){

            add_to_canvas.setVisible(true);
            invalidateOptionsMenu();

        }else {

            add_to_canvas.setVisible(false);
            invalidateOptionsMenu();

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
         if (id == android.R.id.home) {

            onBackPressed();

        }else if(id == R.id.mi_add_to_canvas){

            Intent intent = new Intent();
            intent.putExtra("status","added");
            setResult(1, intent);
            finish();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_delete){

            //code to delete
            Delete();

        }
    }

    /*get item data*/

    private class FetchItemData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("item_id", "UTF-8") + "=" + getIntent().getExtras().getString("item_id");

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONObject product_object = dataobject.getJSONObject("product");
                            JSONObject attribute_obj = dataobject.getJSONObject("attributes");
                            JSONArray image_array = product_object.getJSONArray("images");
                            JSONArray tag_array = attribute_obj.getJSONArray("tags");
                            JSONArray care_tip_array = product_object.getJSONArray("care_tips");
                            JSONArray color_array = product_object.getJSONArray("color");
                            ll_color_layout.setWeightSum(color_array.length());
                            for(int i = 0;i<color_array.length();i++){

                                CreateColor(color_array.get(i).toString().split("_")[0]);
                            }



                            String name = product_object.getString("name");
                            String category = product_object.getString("category");
                            String brand = product_object.getString("brand");
                            String worn_count = dataobject.getString("wear_cnt");
                            String last_worn = dataobject.getString("last_worn");
                            String item_status = attribute_obj.getString("status");
                            rating_count = Integer.parseInt(attribute_obj.getString("rating"));
                            String paired_outfits = dataobject.getString("pairedOutfitsCnt");



                            //  collapsetoolbar.setTitle(name);
                            tv_activity_title.setText(name);
                            setitemstatus(item_status);
                            setRating(rating_count);

                            String size = attribute_obj.getString("size");
                            String date = attribute_obj.getString("purchase_date");
                            String price = attribute_obj.getString("price");
                            JSONObject image_obj = image_array.getJSONObject(0);
                            String filename = image_obj.getString("filename");

                            for (int k = 0 ;k<tag_array.length();k++){

                                CreateTags(tag_array.get(k).toString());

                            }

                            for(int l = 0; l<care_tip_array.length();l++){

                               //CreateCareTip(db.GetCareTipImage(care_tip_array.get(l).toString()));
                               CreateCareTip(db.GetCareTipImage("Dry Clean: Any Solvent Except Trichloroethylene"));


                            }

//                            fetching similar,suggested items,outlookk

                            JSONArray similar_items = dataobject.getJSONArray("suggestedItems");
                            JSONArray suggestedOutfits = dataobject.getJSONArray("suggestedOutfits");
                            JSONArray suggestedLooks = dataobject.getJSONArray("suggestedLooks");

                            //similar items
                            for (int i = 0; i < similar_items.length(); i++) {

                                JSONObject similar_items_object = similar_items.getJSONObject(i);
                                String similar_item_product_id = similar_items_object.getString("product_id");

                                JSONObject similar_items_product_object = similar_items_object.getJSONObject("product");
                                String similar_item_name = similar_items_product_object.getString("name");
                              //  String similar_item_price = similar_items_product_object.getString("price");


                                JSONArray similar_items_image_array = similar_items_product_object.getJSONArray("images");
                                JSONObject similar_items_image_object = similar_items_image_array.getJSONObject(0);

                                String similar_item_image_path = similar_items_image_object.getString("filename");

                                wearSimilarItemsModel = new WearSimilarItemsModel(similar_item_product_id, similar_item_name, similar_item_image_path, "", "");
                                similarItemsModelArrayList.add(wearSimilarItemsModel);

                            }


                            //suggested outfits
                            for (int i = 0; i < suggestedOutfits.length(); i++) {

                                JSONObject suggested_outfit_object = suggestedOutfits.getJSONObject(i);

                                if (suggested_outfit_object.has("image")) {

                                    String suggested_outfit_product_id = suggested_outfit_object.getString("_id");
                                    String suggested_outfit_name = suggested_outfit_object.getString("name");
                                    String suggested_created_by = suggested_outfit_object.getString("created_by");

                                    String suggested_outfit_image_path = suggested_outfit_object.getString("image");

                                //    String suggested_outfit_createdby = suggested_outfit_object.getString("createdby");
                                    wearSimilarItemsModel = new WearSimilarItemsModel(suggested_outfit_product_id, suggested_outfit_name, suggested_outfit_image_path, "", suggested_created_by);
                                    suggestedoutfitarraylist.add(wearSimilarItemsModel);

                                }

                            }

                            tv_name.setText(name);
                            tv_category.setText(category.split("_")[1]);
                            if (!brand.equals("")) {

                                tv_brand.setText(brand.split("_")[1]);

                              String brand_image_path = db.GetBrandLogo(brand.split("_")[1]);
                                if(!brand_image_path.equalsIgnoreCase("")){
                                    Picasso.with(getApplicationContext())
                                            .load(brand_image_path)
                                            .into(im_brand_icon);
                                }


                            }
                            tv_size.setText(size);
                            tv_date.setText(date);
                            tv_prize.setText("₹"+price);
                            tv_wishlist_price.setText("₹"+price);
                            tv_last_worn.setText(last_worn);
                            tv_no_of_times_worn.setText(worn_count);
                            tv_paired_outfits.setText(paired_outfits);

                            Picasso.with(im_item_image.getContext()).load(filename).into(im_item_image);


                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();
                suggestedlookbookadapter.notifyDataSetChanged();

            }
        }
    }

    private void CreateColor(String color_id) {

        String hexcode = db.GetHexCode(color_id);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, 1);
        LinearLayout ll_color = new LinearLayout(this);
        ll_color.setLayoutParams(param);
        ll_color.setBackgroundColor(Color.parseColor("#"+hexcode));
        ll_color_layout.addView(ll_color);
    }


    /*update status of the item*/


    private class UpdateStatus extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("item_id", "UTF-8") + "=" + getIntent().getExtras().getString("item_id")+"&"
                        + URLEncoder.encode("status", "UTF-8")+"="+selected_spinner_item;


                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
                Log.d("res",Content);

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                sp_status.setSelection(sp_prev_item_position);
                sp_isFirst_time = true;
                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();


                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            sp_status.setSelection(sp_prev_item_position);
                            sp_isFirst_time = true;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    sp_status.setSelection(sp_prev_item_position);
                    sp_isFirst_time = true;
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();
                suggestedlookbookadapter.notifyDataSetChanged();

            }
        }
    }



    /*update rate of the item ends*/

    private class UpdateRating extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("item_id", "UTF-8") + "=" + getIntent().getExtras().getString("item_id")+"&"
                        + URLEncoder.encode("rating", "UTF-8")+"="+rating_to_Send;


                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
                Log.d("res",Content);

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            rating_count = rating_to_Send;
                            setRating(rating_count);


                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    sp_status.setSelection(sp_prev_item_position);
                    sp_isFirst_time = true;
                }

                pDialog.dismiss();
                similaritemadapter.notifyDataSetChanged();
                suggestedoutfitadapter.notifyDataSetChanged();
                suggestedlookbookadapter.notifyDataSetChanged();

            }
        }
    }

    /*update rate of the item ends*/



    /*Delete rate of the item starts*/

    private class DeleteItem extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("item_id", "UTF-8") + "=" + getIntent().getExtras().getString("item_id");


                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
                Log.d("res",Content);

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equalsIgnoreCase("true")) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            finish();

                        } else {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                }

                pDialog.dismiss();

            }
        }
    }

    /*Delete item ends*/

    @SuppressLint("NewApi")
    private void CreateTags(String tags) {
        final LinearLayout selected_item_textview_layout = new LinearLayout(this);
        selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
        selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        selected_item_textview_layout.setLayoutParams(params);

        TextView tv_seleected_text = new TextView(this);
        tv_seleected_text.setText(tags);
        tv_seleected_text.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        //tv_seleected_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        tv_seleected_text.setTextSize(16);
        selected_item_textview_layout.addView(tv_seleected_text);
        ll_additional_tags.addView(selected_item_textview_layout);

    }

    private void CreateCareTip(String image_path) {
        ImageView im_caretip = new ImageView(this);
        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        im_caretip.setLayoutParams(params);
        if(!image_path.equals("")){

            Picasso.with(im_caretip.getContext()).load(image_path).into(im_caretip);

        }
        ll_care_tip_item_layout.addView(im_caretip);

    }

    public void SetFonts() {

        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_name);
        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_brand);
        Wodrob.AddFont(this, "opensans_semibold.ttf", tv_activity_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_date_of_purchase_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_last_worn_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_no_of_times_worn_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_paired_outfits_title);
        Wodrob.AddFont(this, "opensans_italic.ttf", tv_care_tip_title);

    }

    public void HideLayout(){

        similar_items.setVisibility(View.GONE);
        rv_suggested_outfit.setVisibility(View.GONE);
        ll_status_layout.setVisibility(View.GONE);
        tv_suggested_outfit_title.setVisibility(View.GONE);
        tv_similar_items_title.setVisibility(View.GONE);
    }
    public void setitemstatus(String status){

        if(status.equalsIgnoreCase("AVAILABLE")){

            sp_status.setSelection(0);

        }else if(status.equalsIgnoreCase("FOR REPAIR")){

            sp_status.setSelection(1);


        }else if(status.equalsIgnoreCase("DISCARD")){

            sp_status.setSelection(1);


        }else if(status.equalsIgnoreCase("RENTED")){

            sp_status.setSelection(1);


        }else if(status.equalsIgnoreCase("FOR WASH")){

            sp_status.setSelection(1);


        }

        spinner_count = spinner_count+1;

    }

    public void setRating(int ratingcout){

        for(int i = 0 ;i<ar_rating.size();i++){

            if(i<ratingcout){
                ar_rating.get(i).setImageDrawable(getResources().getDrawable(R.drawable.itemdetailed_goldstar));
            }else {
                ar_rating.get(i).setImageDrawable(getResources().getDrawable(R.drawable.itemdetailed_greystar));
            }
        }

    }
}
