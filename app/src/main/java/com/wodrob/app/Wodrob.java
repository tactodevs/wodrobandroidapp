package com.wodrob.app;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wodrob.app.gcm.QuickstartPreferences;
import com.wodrob.app.model.ItemCategoryModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rameesfazal on 19/1/16.
 */
public class Wodrob {

    private static String PREFS_NAME;
    final static int FLIP_VERTICAL = 1;
    final static int FLIP_HORIZONTAL = 2;

    public static void copyDirectoryOneLocationToAnotherLocation(Activity activity)
            throws IOException {

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/" + activity.getPackageName() + "/databases/wodrob";
                String backupDBPath = "wodrob.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public static String getCurrentTimeStamp() {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTimeStamp = dateFormat.format(new Date()); // Find todays date

            return currentTimeStamp;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap flip(Bitmap src, int type) {


        // create new matrix for transformation
        Matrix matrix = new Matrix();
        // if vertical
        if(type == FLIP_VERTICAL) {
            matrix.preScale(1.0f, -1.0f);
        }
        // if horizonal
        else if(type == FLIP_HORIZONTAL) {
            matrix.preScale(-1.0f, 1.0f);
            // unknown type
        } else {
            return null;
        }

        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }


    /*canvas methods*/

    public static void sendViewToBack(final View child) {
        final ViewGroup parent = (ViewGroup)child.getParent();
        if (null != parent) {
            parent.removeView(child);
            parent.addView(child, 0);
        }
    }


    public static void applyFont(final Context context, final View root, final String fontName) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(context, viewGroup.getChildAt(i), fontName);
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getAssets(), fontName));
        } catch (Exception e) {
            Log.e("ProjectName", String.format("Error occured when trying to apply %s font for %s view", fontName, root));
            e.printStackTrace();
        }
    }

    static public boolean setPreference(Context c, String value, String key) {
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        settings = c.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    static public String getPreference(Context c, String key) {
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        settings = c.getSharedPreferences(PREFS_NAME, 0);
        String value = settings.getString(key, "");
        return value;
    }

    public static boolean setbooleanpreference(Context c, boolean value, String key) {
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        settings = c.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static boolean getbooleanpreference(Context c, String key) {
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        settings = c.getSharedPreferences(PREFS_NAME, 0);
        boolean value = settings.getBoolean(key, false);
        return value;

    }

//    get uuid -- used in Reistration intent service .java

    public static String GetUuid(Context activity) {

        TelephonyManager tManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = tManager.getDeviceId();
        return  uuid;

    }

    public static String isTablet(Context context) {
        String device_type = "";
        if((context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE){

        device_type = "Android Tablet" ;

        }else{

            device_type = "Android Phone" ;


        }

        return device_type;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String GenerateFiterString(Activity activity) {

        String filter_category_id = "";
        String filter_style = "";
        String filter_brands = "";
        String filter_size = "";
        String filter_color = "";
        String filter_tags = "";

        DbHelper db = new DbHelper(activity.getApplicationContext());

        try {

            if(!FilterActivity.selected_category_id.equals("")){

                filter_category_id = FilterActivity.selected_category_id;

            }

            if(FilterActivity.selected_style_list.size() != 0){

                ArrayList<String> data_array_list = new ArrayList<>();

                for(int i = 0; i<FilterActivity.selected_style_list.size();i++){

                    data_array_list.add(db.GetStyle_id(FilterActivity.selected_style_list.get(i).toString()));
                }
                //generate string
                filter_style = Wodrob.BuildString(data_array_list);

            }

            if(FilterActivity.selected_brand_list.size() != 0){

                //generate string
                ArrayList<String> data_array_list = new ArrayList<>();

                for(int i = 0; i<FilterActivity.selected_brand_list.size();i++){

                    data_array_list.add(db.GetBrand_id_without_name(FilterActivity.selected_brand_list.get(i).toString()));
                }
                filter_brands = Wodrob.BuildString(data_array_list);

            }
            if(FilterActivity.selected_size_list.size() != 0){

                //generate String
                filter_size = Wodrob.BuildString(FilterActivity.selected_size_list);

            }

            if(FilterActivity.selected_color_list.size() != 0){

                //generate string
                ArrayList<String> data_array_list = new ArrayList<>();

                for(int i = 0; i<FilterActivity.selected_color_list.size();i++){

                    data_array_list.add(db.GetColor_id(FilterActivity.selected_color_list.get(i).toString()));
                }

                filter_color = Wodrob.BuildString(data_array_list);

            }

            if(FilterActivity.selected_tags_list.size() != 0){

                //generate string
                ArrayList<String> data_array_list = new ArrayList<>();

                for(int i = 0; i<FilterActivity.selected_tags_list.size();i++){

                    data_array_list.add(db.Get_tag_id(FilterActivity.selected_tags_list.get(i).toString()));
                }

                filter_tags = Wodrob.BuildString(FilterActivity.selected_tags_list);

            }
        }catch (NullPointerException e){

            Log.d("null","Empty filter");

        }

        return filter_category_id+"/"+filter_style+"/"+filter_color+"/"+filter_size+"/"+filter_brands+"/"+filter_tags;

    }


    public static String BuildString(ArrayList<String> arraylist){

        String output = "";

        StringBuilder sb = new StringBuilder();

        ArrayList<String> out_array_List = arraylist;
        for (int i=0; i<out_array_List.size();i++){

            if(i==(out_array_List.size()-1)) {

                sb.append(out_array_List.get(i).toString());

            } else {

                sb.append(out_array_List.get(i).toString()+",");

            }
        }

        output=sb.toString();

        return output;
    }

    public static int getDominantColor1(Bitmap bitmap) {

        if (bitmap == null)
            throw new NullPointerException();

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = width * height;
        int pixels[] = new int[size];

        Bitmap bitmap2 = bitmap.copy(Bitmap.Config.ARGB_4444, false);

        bitmap2.getPixels(pixels, 0, width, 0, 0, width, height);

        final List<HashMap<Integer, Integer>> colorMap = new ArrayList<HashMap<Integer, Integer>>();
        colorMap.add(new HashMap<Integer, Integer>());
        colorMap.add(new HashMap<Integer, Integer>());
        colorMap.add(new HashMap<Integer, Integer>());

        int color = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        Integer rC, gC, bC;
        for (int i = 0; i < pixels.length; i++) {
            color = pixels[i];

            r = Color.red(color);
            g = Color.green(color);
            b = Color.blue(color);

            rC = colorMap.get(0).get(r);
            if (rC == null)
                rC = 0;
            colorMap.get(0).put(r, ++rC);

            gC = colorMap.get(1).get(g);
            if (gC == null)
                gC = 0;
            colorMap.get(1).put(g, ++gC);

            bC = colorMap.get(2).get(b);
            if (bC == null)
                bC = 0;
            colorMap.get(2).put(b, ++bC);
        }

        int[] rgb = new int[3];
        for (int i = 0; i < 3; i++) {
            int max = 0;
            int val = 0;
            for (Map.Entry<Integer, Integer> entry : colorMap.get(i).entrySet()) {
                if (entry.getValue() > max) {
                    max = entry.getValue();
                    val = entry.getKey();
                }
            }
            rgb[i] = val;
        }

        int dominantColor = Color.rgb(rgb[0], rgb[1], rgb[2]);

        return dominantColor;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path,
                                                     int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        //Query bitmap without allocating memory
        options.inJustDecodeBounds = true;
        //decode file from path
        BitmapFactory.decodeFile(path, options);
        // Calculate inSampleSize
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        //decode according to configuration or according best match
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;
        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }
        //if value is greater than 1,sub sample the original image
        options.inSampleSize = inSampleSize;
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    /*do brigntness image view*/
    public static Bitmap doBrightness(Bitmap src, int value) {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        // color information
        int A, R, G, B;
        int pixel;

        // scan through all pixels
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);

                // increase/decrease each channel
                R += value;
                if(R > 255) { R = 255; }
                else if(R < 0) { R = 0; }

                G += value;
                if(G > 255) { G = 255; }
                else if(G < 0) { G = 0; }

                B += value;
                if(B > 255) { B = 255; }
                else if(B < 0) { B = 0; }

                // apply new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }


    /*code to change contrast!! used in enhanceImageActitivty.java*/
    public static Bitmap ChangeContrast(Bitmap src, double value) {
        // src image size
        double contrast = 0;
        int width = src.getWidth();
        int height = src.getHeight();
        // create output bitmap with original size
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        // color information
        int A, R, G, B;
        int pixel;
        // get contrast value
        if((int) value != 0){

            contrast = Math.pow((100 + value) / 100, 2);
            // scan through all pixels
            for(int x = 0; x < width; ++x) {
                for(int y = 0; y < height; ++y) {
                    // get pixel color
                    pixel = src.getPixel(x, y);
                    A = Color.alpha(pixel);
                    // apply filter contrast for every channel R, G, B
                    R = Color.red(pixel);
                    R = (int)(((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                    if(R < 0) { R = 0; }
                    else if(R > 255) { R = 255; }

                    G = Color.red(pixel);
                    G = (int)(((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                    if(G < 0) { G = 0; }
                    else if(G > 255) { G = 255; }

                    B = Color.red(pixel);
                    B = (int)(((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                    if(B < 0) { B = 0; }
                    else if(B > 255) { B = 255; }

                    // set new pixel color to output bitmap
                    bmOut.setPixel(x, y, Color.argb(A, R, G, B));
                }
            }

            // return final image
            return bmOut;

        }else{
            return src;
        }


    }

    public static void SaveImage(Bitmap finalBitmap) {
        File file = new File (Environment.getExternalStorageDirectory()+File.separator + "wodrob.jpg");
        if (file.exists ()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*code to rotate image view !! used in add item activity*/
    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

   public static ArrayList<ItemCategoryModel> GetCategoryfilterlist(Activity activity,ArrayList<ItemCategoryModel> itemCategoryModels,String search){

       ArrayList<ItemCategoryModel> filtered_list = new ArrayList<>();

       for(int i= 0; i < itemCategoryModels.size();i++){

           if(itemCategoryModels.get(i).getCategory_name().toLowerCase().startsWith(search.toLowerCase())){

               filtered_list.add(itemCategoryModels.get(i));
           }

       }
       return filtered_list;
    }

    public static String image_to_String(Bitmap image){

        String image_str = "";
        if(image != null){

            ByteArrayOutputStream stream=new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 90, stream);
            byte[] imageByteArray=stream.toByteArray();
            image_str = Base64.encodeToString(imageByteArray, 0);


        }
        return image_str;

    }

    public static void AddFont(Activity activity,String font_name,TextView view){
        Typeface face= Typeface.createFromAsset(activity.getAssets(), "fonts/"+font_name);
        view.setTypeface(face);

    }

    public static String GetAccessToken(Activity activity){

        String token = "Bearer "+Wodrob.getPreference(activity.getApplicationContext(),"access_token");
        return token;

    }
}
