package com.wodrob.app;

/**
 * Created by Android Dev on 21-03-2016.
 */
public class WodrobApiConstant {
    public static String BASE_URL = "http://app.wodrob.net/api/v1/";
    public static String MASTER_SYNC = BASE_URL + "masters/sync";

}
