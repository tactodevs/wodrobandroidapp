package com.wodrob.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.wodrob.app.AddItem;
import com.wodrob.app.DbHelper;
import com.wodrob.app.PickColorActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.model.ItemBrandModel;
import com.wodrob.app.model.ItemCareTipsModel;
import com.wodrob.app.model.ItemCategoryModel;
import com.wodrob.app.model.ItemSizeModel;
import com.wodrob.app.model.ItemStoreModel;
import com.wodrob.app.model.ItemStyleModel;
import com.wodrob.app.model.ItemTagModel;
import com.wodrob.app.views.PredicateLayout;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 8/1/16.
 */
public class AddItemPagerAdapter extends PagerAdapter {

    Activity activity;
    LayoutInflater mLayoutInflater;

    ArrayList<ItemCategoryModel> categoryModelArrayList;
    ArrayList<ItemCategoryModel> subcategoryModelArrayList;
    ArrayList<ItemStyleModel> styleModelArrayList;
    ArrayList<ItemBrandModel> brandModelArrayList;
    ArrayList<String> colorModelArrayList;
    ArrayList<ItemSizeModel> sizeModelArrayList;
    ArrayList<ItemStoreModel> storeModelArrayList;
    ArrayList<ItemCareTipsModel> careTipsModelArrayList;
    ArrayList<ItemTagModel> tagModelArrayList;
    FetchCategoryAdapter fetchCategoryAdapter;

    TextView[] filter_item;

    DbHelper dbHelper;
    String type;
    LinearLayout.LayoutParams lp;
    String search_string;

    public AddItemPagerAdapter(Activity activity, String search_string, String type) {

        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dbHelper = new DbHelper(activity.getApplicationContext());
        categoryModelArrayList = new ArrayList<>();

        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(24, 12, 24, 12);
        this.search_string = search_string;
        this.type = type;

    }

    @Override
    public int getCount() {
        return 11;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.add_item_pager, container, false);
        ScrollView bottom_scroll_layout = (ScrollView) itemView.findViewById(R.id.bottom_scroll_layout);
        final PredicateLayout data_layout = (PredicateLayout) itemView.findViewById(R.id.filter_items_layout);
        LinearLayout category_layout = (LinearLayout) itemView.findViewById(R.id.category_layout);

        if (position == 0) {

            bottom_scroll_layout.setVisibility(View.GONE);
            data_layout.setVisibility(View.GONE);
            category_layout.setVisibility(View.VISIBLE);
            type = "Category";
            DisplayCategoryLayout(category_layout, search_string);

        } else if (position == 1) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "SubCategories";
            DisplayItems(data_layout, search_string);

        } else if (position == 2) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "style";
            DisplayItems(data_layout, search_string);

        } else if (position == 3) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "brand";
            DisplayItems(data_layout, search_string);

        } else if (position == 4) {

            bottom_scroll_layout.setVisibility(View.GONE);
            data_layout.setVisibility(View.GONE);
            category_layout.setVisibility(View.VISIBLE);
            type = "color";
            DisplayColorLayput(category_layout, search_string);

        } else if (position == 5) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "size";
            DisplayItems(data_layout, search_string);

        } else if (position == 6) {

            /*type = "price";
            DisplayItems(data_layout,search_string);*/

        } else if (position == 7) {

//            type = "date";
//            DisplayItems(data_layout,search_string);

        } else if (position == 8) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "store";
            DisplayItems(data_layout, search_string);

        } else if (position == 9) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "washinginstructions";
            DisplayItems(data_layout, search_string);

        } else if (position == 10) {

            bottom_scroll_layout.setVisibility(View.VISIBLE);
            data_layout.setVisibility(View.VISIBLE);
            category_layout.setVisibility(View.GONE);

            type = "tags";
            DisplayItems(data_layout, search_string);

        }

        container.addView(itemView);

        return itemView;
    }

    public void DisplayColorLayput(LinearLayout data_layout, String search_string) {
        if (type.equalsIgnoreCase("Color")) {


            LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.add_item_color_layout, null);

            LinearLayout color_layout = (LinearLayout) view.findViewById(R.id.ll_color_layout);
            LinearLayout ll_color1 = (LinearLayout) view.findViewById(R.id.ll_color1);
            LinearLayout ll_color2 = (LinearLayout) view.findViewById(R.id.ll_color2);
            LinearLayout ll_color3 = (LinearLayout) view.findViewById(R.id.ll_color3);
            LinearLayout ll_color4 = (LinearLayout) view.findViewById(R.id.ll_color4);
            LinearLayout ll_color5 = (LinearLayout) view.findViewById(R.id.ll_color5);

            if (AddItem.pixel_array_list.size() != 0) {

                ll_color1.setBackgroundColor(Color.parseColor(AddItem.pixel_array_list.get(0).toString()));
                ll_color2.setBackgroundColor(Color.parseColor(AddItem.pixel_array_list.get(1).toString()));
                ll_color3.setBackgroundColor(Color.parseColor(AddItem.pixel_array_list.get(2).toString()));
                ll_color4.setBackgroundColor(Color.parseColor(AddItem.pixel_array_list.get(3).toString()));
                ll_color5.setBackgroundColor(Color.parseColor(AddItem.pixel_array_list.get(4).toString()));

            }

            ll_color1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(activity, PickColorActivity.class);
                    intent.putExtra("position", 1);
                    activity.startActivityForResult(intent, 6);

                }
            });
            ll_color2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(activity, PickColorActivity.class);
                    intent.putExtra("position", 2);
                    activity.startActivityForResult(intent, 6);
                    ;


                }
            });
            ll_color3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(activity, PickColorActivity.class);
                    intent.putExtra("position", 3);
                    activity.startActivityForResult(intent, 6);


                }
            });
            ll_color4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(activity, PickColorActivity.class);
                    intent.putExtra("position", 4);
                    activity.startActivityForResult(intent, 6);


                }
            });
            ll_color5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(activity, PickColorActivity.class);
                    intent.putExtra("position", 5);
                    activity.startActivityForResult(intent, 6);


                }
            });

            data_layout.addView(view);


        }
    }


    public void DisplayCategoryLayout(LinearLayout data_layout, String search_string) {
        if (type.equalsIgnoreCase("Category")) {

            if(search_string.equals("")){

                categoryModelArrayList.clear();
                categoryModelArrayList = dbHelper.getAllCategoryItems();

              fetchCategoryAdapter = new FetchCategoryAdapter(activity, categoryModelArrayList);

            }else{

                categoryModelArrayList.clear();
                categoryModelArrayList = Wodrob.GetCategoryfilterlist(activity,dbHelper.getAllCategoryItems(), search_string);
                fetchCategoryAdapter = new FetchCategoryAdapter(activity, categoryModelArrayList);

            }

            LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.add_item_category_layout, null);

            GridView category_grid = (GridView) view.findViewById(R.id.gr_category);
            category_grid.setAdapter(fetchCategoryAdapter);

            category_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    AddItem.selected_category_list.clear();
                    AddItem.selected_category_list.add(categoryModelArrayList.get(position).getCategory_id());
                    AddItem.selected_category_name = categoryModelArrayList.get(position).getCategory_name();
                    ((AddItem) activity).clearsearch();
                    ((AddItem) activity).RefreshView("SubCategory", "");

                }
            });

            data_layout.addView(view);


        }
    }

    @SuppressLint("NewApi")
    public void DisplayItems(PredicateLayout data_layout, String search_string) {

        if (type.equalsIgnoreCase("SubCategory")) {

            subcategoryModelArrayList = dbHelper.getSubCategoryData(AddItem.selected_category_list.get(0).toString());
            data_layout.removeAllViews();

            if (search_string.equals("")) {

                filter_item = new TextView[subcategoryModelArrayList.size()];
                for (int i = 0; i < subcategoryModelArrayList.size(); i++) {

                    filter_item[i] = new TextView(activity);
                    filter_item[i].setText(subcategoryModelArrayList.get(i).getCategory_name().toString());
                    filter_item[i].setId(i);
                    filter_item[i].setTextSize(15);
                    filter_item[i].setLayoutParams(lp);
                    filter_item[i].setPadding(15, 15, 15, 15);
                    filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                    filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                    filter_item[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddItem.selected_sub_category_list.clear();
                            AddItem.selected_sub_category_list.add(((TextView) v).getText().toString());
                            ((AddItem) activity).RefreshView("SubCategory", "");


                        }
                    });

                    data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                }

            } else {

                filter_item = new TextView[subcategoryModelArrayList.size()];
                for (int i = 0; i < subcategoryModelArrayList.size(); i++) {
                    if (subcategoryModelArrayList.get(i).getCategory_name().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(subcategoryModelArrayList.get(i).getCategory_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_sub_category_list.clear();
                                AddItem.selected_sub_category_list.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("SubCategory", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }
                }
            }

        } else if (type.equalsIgnoreCase("style")) {

            styleModelArrayList = dbHelper.getAllStyleItems();
            data_layout.removeAllViews();

            if (search_string.equals("")) {

                filter_item = new TextView[styleModelArrayList.size()];
                for (int i = 0; i < styleModelArrayList.size(); i++) {

                    if(AddItem.selected_style_list.contains(styleModelArrayList.get(i).getStyle_name().toString())){

                    }else{

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(styleModelArrayList.get(i).getStyle_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_style_list.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Style", "");


                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }

                }

            } else {

                filter_item = new TextView[styleModelArrayList.size()];
                for (int i = 0; i < styleModelArrayList.size(); i++) {

                    if(AddItem.selected_style_list.contains(styleModelArrayList.get(i).getStyle_name().toString())){



                        }else{

                        if (styleModelArrayList.get(i).getStyle_name().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                            filter_item[i] = new TextView(activity);
                            filter_item[i].setText(styleModelArrayList.get(i).getStyle_name().toString());
                            filter_item[i].setId(i);
                            filter_item[i].setTextSize(15);
                            filter_item[i].setLayoutParams(lp);
                            filter_item[i].setPadding(15, 15, 15, 15);
                            filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                            filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                            filter_item[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    AddItem.selected_style_list.add(((TextView) v).getText().toString());
                                    ((AddItem) activity).RefreshView("Style", "");

                                }
                            });

                            data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                    }

                    }
                }
            }


        } else if (type.equalsIgnoreCase("brand")) {

            brandModelArrayList = dbHelper.getAllbrnads();

            data_layout.removeAllViews();

            if (search_string.equals("")) {

                filter_item = new TextView[brandModelArrayList.size()];
                for (int i = 0; i < brandModelArrayList.size(); i++) {

                    filter_item[i] = new TextView(activity);
                    filter_item[i].setText(brandModelArrayList.get(i).getBrand_name().toString());
                    filter_item[i].setId(i);
                    filter_item[i].setTextSize(15);
                    filter_item[i].setLayoutParams(lp);
                    filter_item[i].setPadding(15, 15, 15, 15);
                    filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                    filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                    filter_item[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddItem.selected_brand_list.clear();
                            AddItem.selected_brand_list.add(((TextView) v).getText().toString());
                            AddItem.selected_brand_id = dbHelper.GetBrand_id(((TextView) v).getText().toString());
                            ((AddItem) activity).RefreshView("Brand", "");

                        }
                    });

                    data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                }

            } else {

                filter_item = new TextView[brandModelArrayList.size()];
                for (int i = 0; i < brandModelArrayList.size(); i++) {
                    if (brandModelArrayList.get(i).getBrand_name().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(brandModelArrayList.get(i).getBrand_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_brand_list.clear();
                                AddItem.selected_brand_list.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Brand", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }
                }
            }


        } else if (type.equalsIgnoreCase("color")) {


            colorModelArrayList = AddItem.pixel_array_list;
            data_layout.removeAllViews();

            if (search_string.equals("")) {

                filter_item = new TextView[colorModelArrayList.size()];
                for (int i = 0; i < colorModelArrayList.size(); i++) {

                    filter_item[i] = new TextView(activity);
                    filter_item[i].setText(colorModelArrayList.get(i).toString());

                    filter_item[i].setId(i);
                    filter_item[i].setTextSize(15);
                    filter_item[i].setLayoutParams(lp);
                    filter_item[i].setPadding(15, 15, 15, 15);
                    filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                    filter_item[i].setBackgroundColor(Color.parseColor(colorModelArrayList.get(i).toString()));
                    filter_item[i].setTextColor(Color.parseColor(colorModelArrayList.get(i).toString()));
                    filter_item[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddItem.selected_color_list.clear();
                            AddItem.selected_color_list.add(((TextView) v).getText().toString());
                            ((AddItem) activity).RefreshView("color", "");


                        }
                    });

                    data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                }

            } else {

                filter_item = new TextView[colorModelArrayList.size()];
                for (int i = 0; i < colorModelArrayList.size(); i++) {
                    if (colorModelArrayList.get(i).toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(colorModelArrayList.get(i).toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_size_list.clear();
                                AddItem.selected_size_list.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Color", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }
                }
            }

        } else if (type.equalsIgnoreCase("size")) {

            sizeModelArrayList = dbHelper.getAllsize();
            data_layout.removeAllViews();

            if (search_string.equals("")) {

                filter_item = new TextView[sizeModelArrayList.size()];
                for (int i = 0; i < sizeModelArrayList.size(); i++) {

                    filter_item[i] = new TextView(activity);
                    filter_item[i].setText(sizeModelArrayList.get(i).getValue().toString());
                    filter_item[i].setId(i);
                    filter_item[i].setTextSize(15);
                    filter_item[i].setLayoutParams(lp);
                    filter_item[i].setPadding(15, 15, 15, 15);
                    filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                    filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                    filter_item[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddItem.selected_size_list.clear();
                            AddItem.selected_size_list.add(((TextView) v).getText().toString());
                            ((AddItem) activity).RefreshView("Size", "");


                        }
                    });

                    data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                }

            } else {

                filter_item = new TextView[sizeModelArrayList.size()];
                for (int i = 0; i < sizeModelArrayList.size(); i++) {
                    if (sizeModelArrayList.get(i).getValue().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(sizeModelArrayList.get(i).getValue().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_size_list.clear();
                                AddItem.selected_size_list.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Size", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }
                }
            }


        } else if (type.equalsIgnoreCase("date")) {

        } else if (type.equalsIgnoreCase("price")) {

        } else if (type.equalsIgnoreCase("store")) {

            // storeModelArrayList = dbHelper.getAllstores();

            storeModelArrayList = dbHelper.GetStoreBasedOnBrandId(AddItem.selected_brand_id);
            data_layout.removeAllViews();

            if (search_string.equals("")) {

                filter_item = new TextView[storeModelArrayList.size()];
                for (int i = 0; i < storeModelArrayList.size(); i++) {

                    filter_item[i] = new TextView(activity);
                    filter_item[i].setText(storeModelArrayList.get(i).getStore_name().toString());
                    filter_item[i].setId(i);
                    filter_item[i].setTextSize(15);
                    filter_item[i].setLayoutParams(lp);
                    filter_item[i].setPadding(15, 15, 15, 15);
                    filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                    filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                    filter_item[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddItem.selected_store_list.clear();
                            AddItem.selected_store_list.add(((TextView) v).getText().toString());
                            ((AddItem) activity).RefreshView("Store", "");

                        }
                    });

                    data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                }

            } else {

                filter_item = new TextView[storeModelArrayList.size()];
                for (int i = 0; i < storeModelArrayList.size(); i++) {
                    if (storeModelArrayList.get(i).getStore_name().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(storeModelArrayList.get(i).getStore_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_store_list.clear();
                                AddItem.selected_store_list.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Store", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }
                }
            }


        } else if (type.equalsIgnoreCase("washinginstructions")) {

            data_layout.removeAllViews();
            careTipsModelArrayList = dbHelper.getAllcaretip();

            if (search_string.equals("")) {

                filter_item = new TextView[careTipsModelArrayList.size()];
                for (int i = 0; i < careTipsModelArrayList.size(); i++) {

                    if(AddItem.selected_washing_instrcutions.contains(careTipsModelArrayList.get(i).getName().toString())){

                    }else {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(careTipsModelArrayList.get(i).getName().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_washing_instrcutions.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Washinginstructions", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }

                }

            } else {

                filter_item = new TextView[careTipsModelArrayList.size()];
                for (int i = 0; i < careTipsModelArrayList.size(); i++) {

                    if(AddItem.selected_washing_instrcutions.contains(careTipsModelArrayList.get(i).getName().toString())){

                    }else {

                        if (careTipsModelArrayList.get(i).getName().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                            filter_item[i] = new TextView(activity);
                            filter_item[i].setText(careTipsModelArrayList.get(i).getName().toString());
                            filter_item[i].setId(i);
                            filter_item[i].setTextSize(15);
                            filter_item[i].setLayoutParams(lp);
                            filter_item[i].setPadding(15, 15, 15, 15);
                            filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                            filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                            filter_item[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    AddItem.selected_washing_instrcutions.add(((TextView) v).getText().toString());
                                    ((AddItem) activity).RefreshView("Washinginstructions", "");

                                }
                            });

                            data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                        }

                    }

                }
            }

        } else if (type.equalsIgnoreCase("tags")) {

            data_layout.removeAllViews();
            tagModelArrayList = dbHelper.getAlltags();

            if (search_string.equals("")) {

                filter_item = new TextView[tagModelArrayList.size()];
                for (int i = 0; i < tagModelArrayList.size(); i++) {

                    if(AddItem.selected_additional_tags.contains(tagModelArrayList.get(i).getTag_name().toString())){

                    }else {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(tagModelArrayList.get(i).getTag_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AddItem.selected_additional_tags.add(((TextView) v).getText().toString());
                                ((AddItem) activity).RefreshView("Tags", "");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));


                    }

                }

            } else {

                filter_item = new TextView[tagModelArrayList.size()];
                for (int i = 0; i < tagModelArrayList.size(); i++) {

                    if(AddItem.selected_additional_tags.contains(tagModelArrayList.get(i).getTag_name().toString())){

                    }else {

                        if (tagModelArrayList.get(i).getTag_name().toString().toLowerCase().startsWith(search_string.toLowerCase())) {

                            filter_item[i] = new TextView(activity);
                            filter_item[i].setText(tagModelArrayList.get(i).getTag_name().toString());
                            filter_item[i].setId(i);
                            filter_item[i].setTextSize(15);
                            filter_item[i].setLayoutParams(lp);
                            filter_item[i].setPadding(15, 15, 15, 15);
                            filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                            filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                            filter_item[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    AddItem.selected_additional_tags.add(((TextView) v).getText().toString());
                                    ((AddItem) activity).RefreshView("Tags", "");

                                }
                            });

                            data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));

                    }

                    }
                }
            }
        }

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
