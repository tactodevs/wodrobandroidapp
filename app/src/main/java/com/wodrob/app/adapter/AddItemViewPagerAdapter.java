package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 24/12/15.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wodrob.app.fragment.AddOutfitFragment;
import com.wodrob.app.fragment.AllItems;
import com.wodrob.app.fragment.Catalog;
import com.wodrob.app.fragment.Closet;


/**
 * Created by hp1 on 21-01-2015.
 */
public class AddItemViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when AddItemViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the AddItemViewPagerAdapter is created

    String type;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public AddItemViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, String type) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.type = type;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (position == 0) // if the position is 0 we are returning the First tab
        {
            if (type.equalsIgnoreCase("item")) {

                Catalog catalog = new Catalog();
                return catalog;

            } else {

                AddOutfitFragment outfitFragment = new AddOutfitFragment("outfit");
                return outfitFragment;

                //outfit
            }


        } else if (position == 1) {

            if (type.equalsIgnoreCase("item")) {

                Closet closet = new Closet();
                return closet;

            } else {

                AddOutfitFragment outfitFragment = new AddOutfitFragment("lookbook");
                return outfitFragment;
                //outfit
            }

        } else {

            if (type.equalsIgnoreCase("item")) {

                AllItems allItems = new AllItems();
                return allItems;

            } else {

                AddOutfitFragment outfitFragment = new AddOutfitFragment("suggested");
                return outfitFragment;
                //outfit
            }


        }


    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
