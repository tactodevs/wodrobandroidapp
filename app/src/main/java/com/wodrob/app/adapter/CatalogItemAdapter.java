package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 16/12/15.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.wodrob.app.R;
import com.wodrob.app.model.CatalogItem;

import java.util.ArrayList;

public class CatalogItemAdapter extends BaseAdapter{
    private Context mContext;
    ArrayList<CatalogItem> catalogItems;

    public CatalogItemAdapter(Context c, ArrayList<CatalogItem> catalogItems) {
        mContext = c;
        this.catalogItems = catalogItems;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return catalogItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    private static class ViewHolder {
        private ImageView item_image;

        public ViewHolder(View v) {
            item_image = (ImageView) v.findViewById(R.id.im_item);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

      /*  if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.catalog_grid_item, null);
            ImageView im_image_item = (ImageView) grid.findViewById(R.id.im_item);
           Picasso.with(mContext)
                    .load(catalogItems.get(position).getImage_path().toString())
                    .into(im_image_item);
            //im_category.setImageResource(Imageid[position]);
           // im_image_item.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_add_selection_24dp));
        } else {
            grid = (View) convertView;
        }*/

        ViewHolder holder = null;
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.catalog_grid_item, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        }  else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(mContext)
                .load(catalogItems.get(position).getImage_path().toString())
                .into(holder.item_image);


        return convertView;
    }
}