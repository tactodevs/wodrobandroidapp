package com.wodrob.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wodrob.app.R;
import com.wodrob.app.model.ExploreCatalogModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 19/1/16.
 */
public class ExploreCatalogAdapter extends RecyclerView.Adapter<ExploreCatalogAdapter.ViewHolder>{

    private ArrayList<ExploreCatalogModel> items;
    private int itemLayout;

    public ExploreCatalogAdapter(ArrayList<ExploreCatalogModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ExploreCatalogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext())
                .inflate(itemLayout, parent, false);

        return new ExploreCatalogAdapter.ViewHolder(view);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {

        ExploreCatalogModel item = items.get(position);

        holder.text.setText(item.getName());
        Picasso.with(holder.image.getContext()).load(item.getImage_path().toString()).into(holder.image);
        holder.itemView.setTag(item);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image_view);
            text = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
