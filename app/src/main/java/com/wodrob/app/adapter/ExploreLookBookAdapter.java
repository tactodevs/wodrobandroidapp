package com.wodrob.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.wodrob.app.R;
import com.wodrob.app.model.ExploreLookBookModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class ExploreLookBookAdapter extends RecyclerView.Adapter<ExploreLookBookAdapter.ViewHolder>  {


    private ArrayList<ExploreLookBookModel> items;
    private int itemLayout;

    public ExploreLookBookAdapter(ArrayList<ExploreLookBookModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        ExploreLookBookModel item = items.get(position);
        holder.tv_name.setText(item.getName());
        holder.tv_occassion_type.setText(item.getOccassion_type());
        holder.tv_created_by.setText("Created by "+item.getCreated_by().split("_")[1]);
        holder.tv_likes.setText(item.getLikes());
        holder.image.setImageBitmap(null);
        Picasso.with(holder.image.getContext()).cancelRequest(holder.image);
        Picasso.with(holder.image.getContext()).load(item.getImage_path().toString()).into(holder.image);
        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView tv_name,tv_occassion_type,tv_created_by,tv_likes;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image_view);
            tv_name = (TextView) itemView.findViewById(R.id.title);
            tv_occassion_type = (TextView) itemView.findViewById(R.id.tv_occassion_types);
            tv_created_by = (TextView) itemView.findViewById(R.id.tv_created_by);
            tv_likes = (TextView) itemView.findViewById(R.id.tv_likes);
        }
    }
}
