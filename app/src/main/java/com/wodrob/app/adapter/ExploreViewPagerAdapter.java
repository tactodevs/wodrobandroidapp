package com.wodrob.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by rameesfazal on 12/1/16.
 */

import android.support.v4.app.FragmentStatePagerAdapter;

import com.wodrob.app.fragment.ExploreCatalogFragment;
import com.wodrob.app.fragment.ExploreLookbookFragment;

public class ExploreViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ExploreViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                ExploreCatalogFragment exploreCatalogFragment = new ExploreCatalogFragment();
                return exploreCatalogFragment;

            case 1:

                ExploreLookbookFragment exploreLookbookFragment = new ExploreLookbookFragment();
                return exploreLookbookFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
