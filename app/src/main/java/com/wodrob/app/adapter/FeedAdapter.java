package com.wodrob.app.adapter;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wodrob.app.R;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.model.ExploreLookBookModel;
import com.squareup.picasso.Picasso;
import com.wodrob.app.model.FeedEmptyClosetItemModel;
import com.wodrob.app.model.FeedEmptyClosetModel;
import com.wodrob.app.model.FeedModel;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {


    private ArrayList<FeedModel> items;
    private int itemLayout;
    Activity activity;

    public FeedAdapter(Activity activity, ArrayList<FeedModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final FeedModel item = items.get(position);

        if (item.getCard_type().equalsIgnoreCase("story")) {

            holder.tv_patron_name.setText(item.getFeedStoryModel().getPatron_name());
            holder.tv_story_time_stamp.setText(item.getFeedStoryModel().getStory_time_stamp());
            holder.tv_story_title.setText(item.getFeedStoryModel().getStory_title());
            holder.tv_story_description.setText(item.getFeedStoryModel().getStory_description());
            holder.tv_like_count.setText(item.getFeedStoryModel().getLike_count());
            holder.tv_comment_count.setText(item.getFeedStoryModel().getComment_count());

            Picasso.with(holder.im_story_avatar.getContext()).cancelRequest(holder.im_story_avatar);
            Picasso.with(holder.im_story_avatar.getContext()).load(item.getFeedStoryModel().getPatron_avatar_path().toString()).into(holder.im_story_avatar);
            Picasso.with(holder.im_story_image.getContext()).load(item.getFeedStoryModel().getStory_image_path().toString()).into(holder.im_story_image);

        } else if (item.getCard_type().equalsIgnoreCase("style_request")) {


            holder.tv_style_request_patron_name.setText(item.getFeedStyleRequest().getStyle_request_patron_name());
            holder.tv_style_request_time_stamp.setText(item.getFeedStyleRequest().getStyle_request_time_stamp());
            holder.tv_style_request_outfit_title.setText(item.getFeedStyleRequest().getStyle_request_title());
            holder.tv_style_request_outfit_description.setText(item.getFeedStyleRequest().getStyle_request_description());
            holder.tv_style_request_participant_Count.setText(item.getFeedStyleRequest().getParticipants_count() + " Participants");

            Picasso.with(holder.im_request_outfit_image.getContext()).load(item.getFeedStyleRequest().getStyle_request_outfit_image().toString()).into(holder.im_request_outfit_image);
            Picasso.with(holder.im_style_request_avatar.getContext()).load(item.getFeedStyleRequest().getStyle_request_patron_avatar().toString()).into(holder.im_style_request_avatar);

        } else if (item.getCard_type().equalsIgnoreCase("BrandSuggestion")) {

            FeedbrandyouMayLikeAdapter feedbrandyoumaylikeadapter = new FeedbrandyouMayLikeAdapter(item.getFeedBrandYouMayLike().getFeedBrandYouMayLikeItemModelArrayList(), R.layout.brand_you_may_like_item);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            holder.rv_brands_you_may_like.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
            holder.rv_brands_you_may_like.setAdapter(feedbrandyoumaylikeadapter);
            holder.rv_brands_you_may_like.setLayoutManager(layoutManager);

        } else if (item.getCard_type().equalsIgnoreCase("weather")) {

            holder.cv_weather.setVisibility(View.VISIBLE);
            holder.tv_weather_title.setText("What are you wearing today ?");
            holder.tv_weather_value.setText(item.getFeedWeatherModel().getWeather_value());
            Picasso.with(holder.im_weather_suggested_outfit1.getContext()).load(item.getFeedWeatherModel().getOutfit_one_image().toString()).into(holder.im_weather_suggested_outfit1);
            Picasso.with(holder.im_weather_suggested_outfit2.getContext()).load(item.getFeedWeatherModel().getOutfit_two_image().toString()).into(holder.im_weather_suggested_outfit2);

            //btn_weather_add_outfit

        }else if (item.getCard_type().equalsIgnoreCase("wishlist_item")) {

            FeedWishlistAdapter feedWishlistAdapter = new FeedWishlistAdapter(item.getFeedWishListModel().getArrayList(), R.layout.feed_wishlist_item);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            holder.rv_wish_list_items.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
            holder.rv_wish_list_items.setAdapter(feedWishlistAdapter);
            holder.rv_wish_list_items.setLayoutManager(layoutManager);


        }else if(item.getCard_type().equalsIgnoreCase("empty_closet")){

            holder.cv_empty_closet.setVisibility(View.VISIBLE);
            Picasso.with(holder.im_empty_closet_item1.getContext()).load(item.getFeedEmptyClosetModel().getItem_image1()).into(holder.im_empty_closet_item1);
            Picasso.with(holder.im_empty_closet_item2.getContext()).load(item.getFeedEmptyClosetModel().getItem_image2()).into(holder.im_empty_closet_item2);
            Picasso.with(holder.im_empty_closet_item3.getContext()).load(item.getFeedEmptyClosetModel().getItem_image3()).into(holder.im_empty_closet_item3);

        }


        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        /*declaration of story card items*/

        public ImageView im_story_avatar, im_story_image, im_like, im_comment, im_share, im_repost;
        public TextView tv_story_time_stamp, tv_patron_name, tv_story_title, tv_story_description, tv_like_count, tv_comment_count;

        /*end*/

        /*declaration of style request card*/
        public ImageView im_request_outfit_image, im_style_request_avatar;
        public TextView tv_style_request_patron_name, tv_style_request_time_stamp, tv_style_request_outfit_title, tv_style_request_outfit_description, tv_style_request_participant_Count;


        /*decleration of brands you may like*/

        RecyclerView rv_brands_you_may_like;

        /*decleration of items in wish list card*/

        RecyclerView rv_wish_list_items;

        /*declaration of weather layout items*/
        public TextView tv_weather_title,tv_weather_value,tv_weather_card_weather_type,tv_weather_card_day,tv_weather_card_date;
        public ImageView im_weather_outfit, im_weather_suggested_outfit1, im_weather_suggested_outfit2,im_weather_icon;
        public Button btn_weather_add_outfit;
        public CardView cv_weather;

        /*decleration of empty closet card*/

        public TextView tv_emptycloset_card_title;
        public ImageView im_empty_closet_outfit,im_empty_closet_item1,im_empty_closet_item2,im_empty_closet_item3;
        public RecyclerView rv_empty_closet_suggested_items;
        public Button btn_empty_closet_add_item;
        public CardView cv_empty_closet;



        public ViewHolder(View itemView) {
            super(itemView);

           /*initialisation of story items*/
            im_story_avatar = (ImageView) itemView.findViewById(R.id.im_story_avatar);
            im_story_image = (ImageView) itemView.findViewById(R.id.im_story_image);
            im_like = (ImageView) itemView.findViewById(R.id.im_like);
            im_comment = (ImageView) itemView.findViewById(R.id.im_comment);
            im_share = (ImageView) itemView.findViewById(R.id.im_share);
            im_repost = (ImageView) itemView.findViewById(R.id.im_repost);

            tv_story_time_stamp = (TextView) itemView.findViewById(R.id.tv_story_time_stamp);
            tv_patron_name = (TextView) itemView.findViewById(R.id.tv_patron_name);
            tv_story_title = (TextView) itemView.findViewById(R.id.tv_story_title);
            tv_story_description = (TextView) itemView.findViewById(R.id.tv_story_description);
            tv_like_count = (TextView) itemView.findViewById(R.id.tv_like_count);
            tv_comment_count = (TextView) itemView.findViewById(R.id.tv_comment_count);

            /*end*/

            /*initialisation of style request items*/
            im_request_outfit_image = (ImageView) itemView.findViewById(R.id.im_request_outfit_image);
            im_style_request_avatar = (ImageView) itemView.findViewById(R.id.im_style_request_avatar);

            tv_style_request_patron_name = (TextView) itemView.findViewById(R.id.tv_style_request_patron_name);
            tv_style_request_time_stamp = (TextView) itemView.findViewById(R.id.tv_style_request_time_stamp);
            tv_style_request_outfit_title = (TextView) itemView.findViewById(R.id.tv_style_request_outfit_title);
            tv_style_request_outfit_description = (TextView) itemView.findViewById(R.id.tv_style_request_outfit_description);
            tv_style_request_participant_Count = (TextView) itemView.findViewById(R.id.tv_style_request_participant_Count);


            /*initialisation of brand you may like items*/

            rv_brands_you_may_like = (RecyclerView) itemView.findViewById(R.id.rv_brands_you_may_like);

            /*initialisation of weather layout items*/
            tv_weather_title = (TextView) itemView.findViewById(R.id.tv_weather_title);
            tv_weather_value = (TextView) itemView.findViewById(R.id.tv_weather_value);
            tv_weather_card_weather_type = (TextView) itemView.findViewById(R.id.tv_weather_card_weather_type);
            tv_weather_card_day = (TextView) itemView.findViewById(R.id.tv_weather_card_day);
            tv_weather_card_date = (TextView) itemView.findViewById(R.id.tv_weather_card_date);
            im_weather_outfit = (ImageView) itemView.findViewById(R.id.im_weather_icon);

            im_weather_outfit = (ImageView) itemView.findViewById(R.id.im_weather_outfit);
            im_weather_suggested_outfit1 = (ImageView) itemView.findViewById(R.id.im_weather_suggested_outfit1);
            im_weather_suggested_outfit2 = (ImageView) itemView.findViewById(R.id.im_weather_suggested_outfit2);
            btn_weather_add_outfit = (Button) itemView.findViewById(R.id.btn_weather_add_outfit);
            cv_weather = (CardView) itemView.findViewById(R.id.cv_weather);

            /*initialisation of items in wishlist card*/
            rv_wish_list_items = (RecyclerView) itemView.findViewById(R.id.rv_wish_list_items);

            /*initialisation if emty closet items*/

            tv_emptycloset_card_title = (TextView) itemView.findViewById(R.id.tv_emptycloset_card_title);
            im_empty_closet_outfit = (ImageView) itemView.findViewById(R.id.im_empty_closet_outfit);
            btn_empty_closet_add_item = (Button) itemView.findViewById(R.id.btn_empty_closet_add_item);
            cv_empty_closet = (CardView) itemView.findViewById(R.id.cv_empty_closet);
            im_empty_closet_item1 = (ImageView)itemView.findViewById(R.id.im_empty_closet_item1);
            im_empty_closet_item2 = (ImageView)itemView.findViewById(R.id.im_empty_closet_item2);
            im_empty_closet_item3 = (ImageView)itemView.findViewById(R.id.im_empty_closet_item3);

        }
    }


}
