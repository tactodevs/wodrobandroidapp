package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 1/3/16.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wodrob.app.R;
import com.wodrob.app.model.FeedEmptyClosetItemModel;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class FeedEmptyClosetAdapter extends RecyclerView.Adapter<FeedEmptyClosetAdapter.ViewHolder>  {


    private ArrayList<FeedEmptyClosetItemModel> items;
    private int itemLayout;

    public FeedEmptyClosetAdapter(ArrayList<FeedEmptyClosetItemModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(vh);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        FeedEmptyClosetItemModel item = items.get(position);

        try {

            String item_image = item.getImage().toString();
            Log.d("e",item_image);
            Log.d("e",item_image);
            Log.d("e",item_image);


/*        holder.tv_title.setVisibility(View.GONE);
        holder.tv_price.setVisibility(View.GONE);*/
        //    Picasso.with(holder.im_suggested_outfit.getContext()).load(R.mipmap.ic_launcher).into(holder.im_suggested_outfit);
          //  holder.itemView.setTag(item);

        }catch (Exception e){

            Log.d("e",e.toString());
        }

    }

    @Override public int getItemCount() {
        //return items.size();
        return  5;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title,tv_price;
        public ImageView im_suggested_outfit;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            im_suggested_outfit = (ImageView) itemView.findViewById(R.id.im_suggested_outfit);

        }
    }
}


