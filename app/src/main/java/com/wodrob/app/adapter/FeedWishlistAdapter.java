package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 1/3/16.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wodrob.app.R;
import com.wodrob.app.model.FeedBrandYouMayLikeItemModel;
import com.wodrob.app.model.FeedWishListModel;
import com.wodrob.app.model.WishListItemModel;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class FeedWishlistAdapter extends RecyclerView.Adapter<FeedWishlistAdapter.ViewHolder>  {


    private ArrayList<WishListItemModel> items;
    private int itemLayout;

    public FeedWishlistAdapter(ArrayList<WishListItemModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        WishListItemModel item = items.get(position);

        holder.tv_title.setText(item.getTitle().toString());
        holder.tv_price.setText(item.getPrice().toString());
        Picasso.with(holder.im_suggested_outfit.getContext()).load(item.getImage_path()).into(holder.im_suggested_outfit);

        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title,tv_price;
        public ImageView im_suggested_outfit;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            im_suggested_outfit = (ImageView) itemView.findViewById(R.id.im_suggested_outfit);

        }
    }
}


