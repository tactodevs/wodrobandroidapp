package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 29/2/16.
 */

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wodrob.app.R;
import com.wodrob.app.model.FeedBrandYouMayLikeItemModel;
import com.wodrob.app.model.StyleItemModel;

import java.util.ArrayList;


/**
 * Created by rameesfazal on 20/1/16.
 */
public class FeedbrandyouMayLikeAdapter extends RecyclerView.Adapter<FeedbrandyouMayLikeAdapter.ViewHolder>  {


    private ArrayList<FeedBrandYouMayLikeItemModel> items;
    private int itemLayout;

    public FeedbrandyouMayLikeAdapter(ArrayList<FeedBrandYouMayLikeItemModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        FeedBrandYouMayLikeItemModel item = items.get(position);
        /*holder.tv_title.setText(item.getTitle());
        holder.tv_occassion_type.setText(item.getOccassion());*/
        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_brand_patron_name,tv_brand_time_stamp;
        public ImageView im_brand_you_may_like_avatar;
        public Button btn_follow;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_brand_patron_name = (TextView) itemView.findViewById(R.id.tv_brand_patron_name);
            tv_brand_time_stamp = (TextView) itemView.findViewById(R.id.tv_brand_time_stamp);
            im_brand_you_may_like_avatar = (ImageView) itemView.findViewById(R.id.im_brand_you_may_like_avatar);
            btn_follow = (Button) itemView.findViewById(R.id.btn_follow);

        }
    }
}

