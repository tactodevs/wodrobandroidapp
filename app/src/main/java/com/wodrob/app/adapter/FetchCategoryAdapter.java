package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 16/12/15.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wodrob.app.AddItem;
import com.wodrob.app.R;
import com.wodrob.app.model.ItemCategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FetchCategoryAdapter extends BaseAdapter{
    private Context mContext;
    ArrayList<ItemCategoryModel> ar_category;

    public FetchCategoryAdapter(Context c, ArrayList<ItemCategoryModel> ar_category) {
        mContext = c;
        this.ar_category = ar_category;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return ar_category.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        ViewHolder viewHolder;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.category_grid_item, null);
            viewHolder.tv_category_name = (TextView) convertView.findViewById(R.id.tv_category_name);
            viewHolder.im_category = (ImageView)convertView.findViewById(R.id.im_category);

            convertView.setTag(viewHolder);

        } else {
            //grid = (View) convertView;
            viewHolder = (ViewHolder) convertView.getTag();

        }
        viewHolder.tv_category_name.setText(ar_category.get(position).getCategory_name());
        Picasso.with(mContext)
                .load(ar_category.get(position).getUnselected_path())
                .into(viewHolder.im_category);
        if(AddItem.selected_category_list.contains(ar_category.get(position).getCategory_id())){

            Picasso.with(mContext)
                    .load(ar_category.get(position).getSelected_path())
                    .into(viewHolder.im_category);
        }
       /* if(AddItem.selected_category_list.size() != 0){





        }else{

            Picasso.with(mContext)
                    .load(ar_category.get(position).getUnselected_path())
                    .into(viewHolder.im_category);
        }*/


        return convertView;
    }

    private static class ViewHolder {
        TextView tv_category_name;
        ImageView im_category;
    }
}