package com.wodrob.app.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.model.FollowItemModel;
import com.wodrob.app.model.StyleItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class FollowItemAdapter extends RecyclerView.Adapter<FollowItemAdapter.ViewHolder>  {


    private ArrayList<FollowItemModel> items;
    private int itemLayout;
    int pager_position;
    Activity activity;
    ProgressDialog pDialog;
    String invite_id,follow_id,friend_id;

    public FollowItemAdapter(Activity activity,ArrayList<FollowItemModel> items, int itemLayout,int pager_position ){
        this.items = items;
        this.itemLayout = itemLayout;
        this.pager_position = pager_position;
        this.activity = activity;
        pDialog= new ProgressDialog(activity);
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, final int position) {
        final FollowItemModel item = items.get(position);

        if(pager_position == 0){

            holder.btn_invite.setVisibility(View.VISIBLE);

        }else if(pager_position == 1){

            holder.btn_follow.setVisibility(View.VISIBLE);

        }else if(pager_position == 2){

            holder.btn_friends.setVisibility(View.VISIBLE);

        }
        holder.tv_name.setText(item.getName());
        holder.tv_description.setText(item.getDescription());
        Picasso.with(holder.im_profile_pic.getContext()).load(R.mipmap.ic_launcher).into(holder.im_profile_pic);

        holder.btn_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //
                invite_id = items.get(position).getId();

                UpdateRequest();

            }
        });

        holder.btn_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //
                follow_id = items.get(position).getId();

                UpdateRequest();

            }
        });


        holder.btn_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //
                 friend_id = items.get(position).getId();

                 UpdateRequest();

            }
        });

        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name,tv_description;
        public ImageView im_profile_pic;
        Button btn_invite,btn_friends,btn_follow;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
            im_profile_pic = (ImageView) itemView.findViewById(R.id.im_profile_pic);
            btn_invite = (Button) itemView.findViewById(R.id.btn_invite);
            btn_friends = (Button) itemView.findViewById(R.id.btn_friends);
            btn_follow = (Button) itemView.findViewById(R.id.btn_follow);



        }
    }

    public void UpdateRequest(){
        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new SendRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"connection/process");

            }
            else {

                new SendRequest().execute(WodrobConstant.BASE_URL+"connection/process");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*background class for sending request*/

    private class SendRequest extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            //no parameters are required // only headers are sending

            try {

                if (pager_position == 0){

                    data +="&"+ URLEncoder.encode("invite", "UTF-8")+"="+ invite_id;


                }else if(pager_position == 1){

                    data +="&"+ URLEncoder.encode("follow", "UTF-8")+"="+ follow_id;


                }else if(pager_position == 2){

                    data +="&"+ URLEncoder.encode("friend", "UTF-8")+"="+ friend_id;

                }


            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();

            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(activity));
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        String id = "",display_name;

                        if(status.equalsIgnoreCase("true")){

                            Toast.makeText(activity.getApplicationContext(), message, Toast.LENGTH_LONG).show();


                        }else{

                            Toast.makeText(activity.getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }

    /*end*/
}
