package com.wodrob.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wodrob.app.fragment.FollowItem;

/**
 * Created by rameesfazal on 12/1/16.
 */


public class FollowPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public FollowPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        FollowItem invite;
        FollowItem follows;
        FollowItem friends;

        switch (position) {
            case 0:

                invite = new FollowItem(position);
                return invite;

            case 1:

               follows = new FollowItem(position);
                return follows;

            case 2:

                 friends = new FollowItem(position);
                return friends;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

