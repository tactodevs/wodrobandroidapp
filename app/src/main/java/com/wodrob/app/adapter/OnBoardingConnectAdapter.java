package com.wodrob.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wodrob.app.R;
import com.wodrob.app.fragment.OnBoardingConnect;
import com.wodrob.app.model.ConnectModel;

import java.util.ArrayList;
import java.util.Map;

public class OnBoardingConnectAdapter extends BaseAdapter {
    private final ArrayList<ConnectModel> mData;
    Context context;
    Boolean connectFlag = false;
    OnBoardingConnect connect;
    String type;

    public OnBoardingConnectAdapter(Context context, ArrayList<ConnectModel> mData, OnBoardingConnect connect,String type) {
        this.context = context;
        this.mData = mData;
        this.connect = connect;
        this.type = type;

    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public ConnectModel getItem(int position) {

        return mData.get(position);

    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
       // final View result;

        ViewHolderItem viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_connect, parent, false);
            viewHolder = new ViewHolderItem();

            viewHolder.ivSocialMediaIcon = (ImageView) convertView.findViewById(R.id.ivSocialMediaIcon);
            viewHolder.txtSocialMediaName = (TextView) convertView.findViewById(R.id.txtSocialMediaName);
            viewHolder.btn_connect = (Button) convertView.findViewById(R.id.btn_connect);
            viewHolder.im_connected = (ImageView) convertView.findViewById(R.id.im_connected);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolderItem) convertView.getTag();

        }


        viewHolder.txtSocialMediaName.setText(mData.get(position).getName());
        if(type.equalsIgnoreCase("connect")){

            viewHolder.btn_connect.setVisibility(View.VISIBLE);
            viewHolder.im_connected.setVisibility(View.GONE);

        }else{

            viewHolder.btn_connect.setVisibility(View.GONE);
            viewHolder.im_connected.setVisibility(View.VISIBLE);

        }

        viewHolder.btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mData.get(position).getName().equalsIgnoreCase("facebook")){

                    connect.connect("facebook");

                }else if(mData.get(position).getName().equalsIgnoreCase("google")){

                    connect.connect("google");

                }
            }
        });

        // TODO replace findViewById by ViewHolder

        return convertView;
    }


    static class ViewHolderItem {

        ImageView ivSocialMediaIcon;
        TextView txtSocialMediaName;
        Button btn_connect;
        ImageView im_connected;
    }
}
