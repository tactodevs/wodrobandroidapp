package com.wodrob.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wodrob.app.R;
import java.util.ArrayList;
import java.util.Map;

public class OnBoardingConnectedAdapter extends BaseAdapter {
    private final ArrayList mData;
    Context context;
    Boolean connectFlag = false;

    public OnBoardingConnectedAdapter(Context context, Map<String, Boolean> map) {
        this.context = context;
        mData = new ArrayList();
        mData.addAll(map.entrySet());
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, Boolean> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_connect, parent, false);
        } else {
            result = convertView;
        }
      /*  TextView txtConnected = (TextView) result.findViewById(R.id.txtConnected);
        TextView txtConnect = (TextView) result.findViewById(R.id.txtConnect);

        ImageView ivSocialMediaIcon = (ImageView) result.findViewById(R.id.ivSocialMediaIcon);
        TextView txtSocialMediaName = (TextView) result.findViewById(R.id.txtSocialMediaName);
        ImageView ivConnectionStatusIcon = (ImageView) result.findViewById(R.id.ivConnectionStatusIcon);*/

//        Map.Entry<String, Boolean> item = getItem(position);
//        Map.Entry<String, Boolean> ConnectedItems;
//        Map.Entry<String, Boolean> ConnectItems;
//
//        item.getValue()
//
//        if(item.getValue() == true){
//            ConnectedItems = getItem(position);
//
//        }else{
//
//            ConnectItems = getItem(position);
//
//        }
//
//        txtSocialMediaName.setText(item.getKey());
//        Picasso.with(ivSocialMediaIcon.getContext()).load(R.mipmap.ic_launcher).into(ivSocialMediaIcon);
//        Picasso.with(ivConnectionStatusIcon.getContext()).load(R.mipmap.ic_launcher).into(ivConnectionStatusIcon);
        // TODO replace findViewById by ViewHolder

        return result;
    }
}
