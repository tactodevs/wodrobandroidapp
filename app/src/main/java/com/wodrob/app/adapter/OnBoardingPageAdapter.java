package com.wodrob.app.adapter;

/**
 * Created by rameesfazal on 20/2/16.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wodrob.app.fragment.GetOtp;
import com.wodrob.app.fragment.OnBoardingConnect;
import com.wodrob.app.fragment.OnBoardingFollow;
import com.wodrob.app.fragment.OnboardingPersona;
import com.wodrob.app.fragment.OnboardingPrivacy;


/**
 * Created by rameesfazal on 12/1/16.
 */


public class OnBoardingPageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public OnBoardingPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

            OnboardingPersona persona = new OnboardingPersona();
                return persona;

            case 1:

                OnboardingPrivacy onboardingPrivacy = new OnboardingPrivacy();
                return onboardingPrivacy;

            case 2:

                GetOtp getOtp = new GetOtp("Onboarding");
                return getOtp;

            case 3:

               OnBoardingConnect onBoardingConnect = new OnBoardingConnect();
                return onBoardingConnect;

            case 4:

                OnBoardingFollow onBoardingFollow = new OnBoardingFollow();
                return onBoardingFollow;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

