package com.wodrob.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.wodrob.app.DbHelper;
import com.wodrob.app.PublishActivity;
import com.wodrob.app.R;
import com.wodrob.app.interfaces.FilterListener;
import com.wodrob.app.model.ItemOccasionModel;
import com.wodrob.app.model.ItemTagModel;
import com.wodrob.app.views.PredicateLayout;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 8/1/16.
 */
public class PublishOutfitViewPagerAdapter extends PagerAdapter {

    Activity activity;
    LayoutInflater mLayoutInflater;
    ArrayList<ItemOccasionModel> occassion_items;
    ArrayList<ItemTagModel> tagModelArrayList;
    TextView[] filter_item;
    DbHelper dbHelper;
    String type;
    LinearLayout.LayoutParams lp;
    String search_string;

    public PublishOutfitViewPagerAdapter(Activity activity, String search_string, String type) {

        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dbHelper = new DbHelper(activity.getApplicationContext());
        occassion_items = dbHelper.getAlloccasion();
        tagModelArrayList = dbHelper.getAlltags();
        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(24, 12, 24, 12);
        this.search_string = search_string;
        this.type = type;

    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.outfit_publish_item, container, false);
        ScrollView bottom_scroll_layout = (ScrollView) itemView.findViewById(R.id.bottom_scroll_layout);
        final PredicateLayout data_layout = (PredicateLayout) itemView.findViewById(R.id.filter_items_layout);
        LinearLayout share_layout = (LinearLayout) itemView.findViewById(R.id.share);

        if (position == 1) {


            type = "occasion";
            DisplayItems(data_layout,search_string);

        } else if (position == 2) {


            type = "tags";
            DisplayItems(data_layout,search_string);

        }else if (position == 3) {

            bottom_scroll_layout.setVisibility(View.GONE);
            data_layout.setVisibility(View.GONE);
            share_layout.setVisibility(View.VISIBLE);
            type = "share";
            DisplayShare(share_layout);

        }
        container.addView(itemView);

        return itemView;
    }
    public void DisplayShare(LinearLayout share_layout){

        share_layout.setVisibility(View.VISIBLE);
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.share_layout,null);
        share_layout.addView(view);


    }


    @SuppressLint("NewApi")
    public void DisplayItems(final PredicateLayout data_layout,String search_string){
        if(type.equalsIgnoreCase("occasion")){

            data_layout.removeAllViews();
            if(search_string.equals("")){
                filter_item = new TextView[occassion_items.size()];
                for(int i = 0; i<occassion_items.size(); i++){

                    if(!PublishActivity.selected_ocassion_list.contains(occassion_items.get(i).getOccasion_name().toString())) {

                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(occassion_items.get(i).getOccasion_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                PublishActivity.selected_ocassion_list.add(((TextView) v).getText().toString());

                                ((FilterListener) activity).DoFilter("occasion");
                                ((PublishActivity) activity).RefreshView("occasion");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                    }
                 }

            }else{

                filter_item = new TextView[occassion_items.size()];
                for(int i = 0; i<occassion_items.size(); i++){

                    if(occassion_items.get(i).getOccasion_name().toString().toLowerCase().startsWith(search_string.toLowerCase())){

                        if(!PublishActivity.selected_ocassion_list.contains(occassion_items.get(i).getOccasion_name().toString())) {
                            filter_item[i] = new TextView(activity);
                            filter_item[i].setText(occassion_items.get(i).getOccasion_name().toString());
                            filter_item[i].setId(i);
                            filter_item[i].setTextSize(15);
                            filter_item[i].setLayoutParams(lp);
                            filter_item[i].setPadding(15, 15, 15, 15);
                            filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                            filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                            filter_item[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    PublishActivity.selected_ocassion_list.add(((TextView) v).getText().toString());
                                    //type = "occasion";

                                    ((FilterListener) activity).DoFilter("occasion");
                                    ((PublishActivity) activity).RefreshView("occasion");

                                    // DisplaySelectedItem();

                                }
                            });

                            data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                        }
                    }

                }


            }

        }else if(type.equalsIgnoreCase("tags")) {


            data_layout.removeAllViews();
            if(search_string.equals("")){

                filter_item = new TextView[tagModelArrayList.size()];
                for (int i = 0; i < tagModelArrayList.size(); i++) {

                    if(!PublishActivity.selected_tags_list.contains(tagModelArrayList.get(i).getTag_name().toString())) {
                        filter_item[i] = new TextView(activity);
                        filter_item[i].setText(tagModelArrayList.get(i).getTag_name().toString());
                        filter_item[i].setId(i);
                        filter_item[i].setTextSize(15);
                        filter_item[i].setLayoutParams(lp);
                        filter_item[i].setPadding(15, 15, 15, 15);
                        filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                        filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                        filter_item[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                PublishActivity.selected_tags_list.add(((TextView) v).getText().toString());
                                ((FilterListener) activity).DoFilter("tags");
                                ((PublishActivity) activity).RefreshView("tags");

                            }
                        });

                        data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                    }
                }

            }else{

                filter_item = new TextView[tagModelArrayList.size()];
                for (int i = 0; i < tagModelArrayList.size(); i++) {
                    if(tagModelArrayList.get(i).getTag_name().toString().toLowerCase().startsWith(search_string.toLowerCase())){

                        if(!PublishActivity.selected_tags_list.contains(tagModelArrayList.get(i).getTag_name().toString())) {
                            filter_item[i] = new TextView(activity);
                            filter_item[i].setText(tagModelArrayList.get(i).getTag_name().toString());
                            filter_item[i].setId(i);
                            filter_item[i].setTextSize(15);
                            filter_item[i].setLayoutParams(lp);
                            filter_item[i].setPadding(15, 15, 15, 15);
                            filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                            filter_item[i].setBackground(activity.getResources().getDrawable(R.drawable.style_layout_border));

                            filter_item[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    PublishActivity.selected_tags_list.add(((TextView) v).getText().toString());
                                    ((FilterListener) activity).DoFilter("tags");
                                    ((PublishActivity) activity).RefreshView("tags");

                                }
                            });

                            data_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
                        }

                    }
                }
            }


            }else if (type.equalsIgnoreCase("share")) {


                LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View view = inflater.inflate(R.layout.share_layout, null);
                data_layout.addView(view);


        }

    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }
}
