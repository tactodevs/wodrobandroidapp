package com.wodrob.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wodrob.app.R;
import com.wodrob.app.model.WearSimilarItemsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class SimilarItemsAdapter extends RecyclerView.Adapter<SimilarItemsAdapter.ViewHolder>  {


    private ArrayList<WearSimilarItemsModel> items;
    private int itemLayout;
    String type;

    public SimilarItemsAdapter(ArrayList<WearSimilarItemsModel> items, int itemLayout,String type) {
        this.items = items;
        this.itemLayout = itemLayout;
        this.type = type;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        WearSimilarItemsModel item = items.get(position);

        holder.image.setImageBitmap(null);
        Picasso.with(holder.image.getContext()).cancelRequest(holder.image);
        Picasso.with(holder.image.getContext()).load(item.getImage_path().toString()).into(holder.image);

        if(type.equals("items")) {

            holder.tv_text1.setText(item.getProduct_name());
            holder.tv_text2.setText("");
        }else if(type.equalsIgnoreCase("outfits")){
            holder.tv_text1.setText(item.getProduct_name());
            holder.tv_text2.setText("Created By "+ item.getCreatedby());
        }else if(type.equalsIgnoreCase("outfititems")){
            holder.tv_text1.setText(item.getProduct_name());
            holder.tv_text2.setText("₹"+item.getPrice());
        }

        holder.itemView.setTag(item);

    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView tv_text1,tv_text2;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image_view);
            tv_text2 = (TextView) itemView.findViewById(R.id.tv_text2);
            tv_text1 = (TextView) itemView.findViewById(R.id.tv_text1);

        }
    }
}
