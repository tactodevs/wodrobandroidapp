package com.wodrob.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.wodrob.app.R;
import com.wodrob.app.model.StyleItemModel;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class StyleItemAdapter extends RecyclerView.Adapter<StyleItemAdapter.ViewHolder>  {


    private ArrayList<StyleItemModel> items;
    private int itemLayout;

    public StyleItemAdapter(ArrayList<StyleItemModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        StyleItemModel item = items.get(position);
        holder.tv_title.setText(item.getTitle());
        holder.tv_occassion_type.setText(item.getOccassion());
        holder.tv_responses.setText(item.getResponses());
        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title,tv_occassion_type,tv_responses;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_occassion_type = (TextView) itemView.findViewById(R.id.tv_occassion);
            tv_responses = (TextView) itemView.findViewById(R.id.tv_responses);

        }
    }
}
