package com.wodrob.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by rameesfazal on 12/1/16.
 */

import android.support.v4.app.FragmentStatePagerAdapter;

import com.wodrob.app.fragment.StyleDraftFragment;
import com.wodrob.app.fragment.StyleRequestFragment;

public class StylePageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public StylePageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                StyleRequestFragment styleRequestFragment = new StyleRequestFragment();
                return styleRequestFragment;

            case 1:

                StyleDraftFragment styleDraftFragment = new StyleDraftFragment();
                return styleDraftFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
