package com.wodrob.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wodrob.app.R;
import com.wodrob.app.model.WearItemModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/1/16.
 */
public class WearItemAdapter extends RecyclerView.Adapter<WearItemAdapter.ViewHolder>  {


    private ArrayList<WearItemModel> items;
    private int itemLayout;
    ArrayList<String> positionlist;

    public WearItemAdapter(ArrayList<WearItemModel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
        positionlist = new ArrayList<>();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {

        if(holder.itemView.getId() != 0) {
            WearItemModel item = items.get(position);

            holder.tv_category.setText(item.getCategory_name().split("_")[1]);
            holder.tv_brand.setText(item.getBrand().split("_")[1]);
            holder.tv_size.setText(item.getSize().toString());
            holder.tv_price.setText("₹ "+item.getPrice().toString());
            holder.tv_status.setText(item.getStatus().toString());
            if(!item.getStatus().toString().equalsIgnoreCase("WISHLIST")){
                holder.im_edit.setVisibility(View.VISIBLE);
                holder.btn_buy.setVisibility(View.GONE);
            }else {
                holder.btn_buy.setVisibility(View.VISIBLE);
                holder.im_edit.setVisibility(View.GONE);
            }
            //holder.image.setImageBitmap(null);
           // Picasso.with(holder.image.getContext()).cancelRequest(holder.image);
            if (item.getImagepath().toString().equals("")) {

                Picasso.with(holder.image.getContext()).load(R.mipmap.ic_launcher).into(holder.image);


            } else {
                Picasso.with(holder.image.getContext()).load(items.get(position).getImagepath().toString()).into(holder.image);

            }
            //holder.itemView.setTag(items.get(position));
        }

    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image,im_edit;
        public TextView tv_category,tv_size,tv_brand,tv_price,tv_status;
        Button btn_buy;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image_view);
            tv_category = (TextView) itemView.findViewById(R.id.title);
            tv_size = (TextView) itemView.findViewById(R.id.tv_size);
            tv_brand = (TextView) itemView.findViewById(R.id.tv_brand);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
            im_edit = (ImageView) itemView.findViewById(R.id.im_edit);
            btn_buy = (Button) itemView.findViewById(R.id.btn_buy);


        }
    }

}

