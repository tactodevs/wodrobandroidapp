package com.wodrob.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by rameesfazal on 12/1/16.
 */

import android.support.v4.app.FragmentStatePagerAdapter;

import com.wodrob.app.fragment.WearItemFragment;
import com.wodrob.app.fragment.WearOutFitFragment;

public class WearViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public WearViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                WearItemFragment wearItemFragment = new WearItemFragment();
                return wearItemFragment;

            case 1:
                
                WearOutFitFragment wearOutFitFragment = new WearOutFitFragment();
                return wearOutFitFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
