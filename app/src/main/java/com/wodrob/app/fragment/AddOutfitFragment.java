package com.wodrob.app.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.LookBookView;
import com.wodrob.app.OutfitView;
import com.wodrob.app.R;
import com.wodrob.app.ViewCatalog;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.ExploreCatalogAdapter;
import com.wodrob.app.model.ExploreCatalogModel;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 22/2/16.
 */
public class AddOutfitFragment extends Fragment{

    ProgressDialog pDialog;

    ArrayList<ExploreCatalogModel> exploreCatalogModelArrayList;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    ExploreCatalogAdapter singleadapter;
    ExploreCatalogModel exploreCatalogModel;
    DbHelper db;
    String next_page_url;
    boolean loading = true;
    private GridLayoutManager gridlayoutmanager;
    String type;
    int selected_item_position;


    public AddOutfitFragment() {
    }

    @SuppressLint("ValidFragment")
    public AddOutfitFragment(String type) {

        this.type = type;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_add_story_image, container, false);

        init(v);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it
                selected_item_position = position;
                if(type.equalsIgnoreCase("outfit")){

                    Intent intent = new Intent(getActivity(), OutfitView.class);
                    intent.putExtra("outfit_id",exploreCatalogModelArrayList.get(position).getProduct_id());
                    intent.putExtra("type","Add");
                    startActivityForResult(intent, 1);
                }else {
                    Intent intent = new Intent(getActivity(), LookBookView.class);
                    intent.putExtra("outfit_id",exploreCatalogModelArrayList.get(position).getProduct_id());
                    intent.putExtra("type","Add");
                    startActivityForResult(intent, 1);
                }
            }
        });


        return v;
    }

    private void init(View v) {

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        recyclerView.addItemDecoration(new SingleSpacingDecoration(2, "parent"));

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        pDialog = new ProgressDialog(getActivity());
        gridlayoutmanager = new GridLayoutManager(getActivity(), 2);
        exploreCatalogModelArrayList = new ArrayList<>();
        recyclerView.setLayoutManager(gridlayoutmanager);

        setlayout();
        GetData(type);

    }

    private void setlayout() {

        singleadapter = new ExploreCatalogAdapter(exploreCatalogModelArrayList, R.layout.item_grid_layout);
        recyclerView.setAdapter(singleadapter);
    }


    private void GetData(String type) {

        if (type.equalsIgnoreCase("lookbook")) {

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "lookbook");

                } else {

                    new FetchItemData().execute(WodrobConstant.BASE_URL + "lookbook");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else if (type.equalsIgnoreCase("outfit")) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "outfit");

                } else {

                    new FetchItemData().execute(WodrobConstant.BASE_URL + "outfit");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else if (type.equalsIgnoreCase("suggestedoutfit")) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "outfit");

                } else {

                    new FetchItemData().execute(WodrobConstant.BASE_URL + "outfit");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*backkground class for fetching data*/

    private class FetchItemData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            try {

                if(type.equalsIgnoreCase("lookbook")){

                    data +="&"+ URLEncoder.encode("patron_id", "UTF-8")+"="+ WodrobConstant.patron_id;

                }


                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equalsIgnoreCase("true")) {


                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray data_array = dataobject.getJSONArray("data");

                            for (int i = 0; i < data_array.length(); i++) {

                                JSONObject data_obj = data_array.getJSONObject(i);
                                JSONArray occassio_array = data_obj.getJSONArray("occasion");
                                String name = data_obj.getString("name");
                                String image_path = data_obj.getString("image");
                                String outfit_id = data_obj.getString("_id");


                                exploreCatalogModel = new ExploreCatalogModel(outfit_id, name, image_path);
                                exploreCatalogModelArrayList.add(exploreCatalogModel);
                                //  cataloglist.add(catalogItem);po
                            }
                            singleadapter.notifyDataSetChanged();


                        } else {

                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1) {

            if(data != null) {
                Log.d("data",data.toString());


                String selected_image_path = exploreCatalogModelArrayList.get(selected_item_position).getImage_path();
                String selected_product_id = exploreCatalogModelArrayList.get(selected_item_position).getProduct_id();
                //String selected_image_id = exploreCatalogModelArrayList.get(selected_item_position).get;

                Intent intent = new Intent();
                intent.putExtra("image_path", selected_image_path);
                intent.putExtra("selected_product_id", selected_product_id);
                //intent.putExtra("selected_image_id", selected_image_id);
                intent.putExtra("in_wish_list", "0");
                getActivity().setResult(2, intent);
                getActivity().finish();

            }else{

                Intent intent = new Intent();
                intent.putExtra("image_path", "");
                getActivity().setResult(2, intent);
                getActivity().finish();
            }


        }
    }
}
