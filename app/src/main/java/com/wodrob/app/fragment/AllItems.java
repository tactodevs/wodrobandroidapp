package com.wodrob.app.fragment;

/**
 * Created by rameesfazal on 24/12/15.
 */
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wodrob.app.R;


public class AllItems extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.items_tab_layout,container,false);
        return v;
    }
}
