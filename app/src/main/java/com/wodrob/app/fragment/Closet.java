package com.wodrob.app.fragment;

/**
 * Created by rameesfazal on 24/12/15.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.wodrob.app.AddCanvasItem;
import com.wodrob.app.DbHelper;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.ViewCatalog;
import com.wodrob.app.WearViewItem;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.CatalogItemAdapter;
import com.wodrob.app.model.CatalogItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by rameesfazal on 24/12/15.
 */

public class Closet extends Fragment implements AdapterView.OnItemClickListener{

    GridView item_grid;
    CatalogItemAdapter closetItemAdapter;
    ArrayList<CatalogItem> closetitemlist;
    CatalogItem closetItems;
    ProgressDialog pDialog;
    String next_page_url = "";
    int count = 0;
    int selected_item_position;

    String filter_category_id = "";
    String filter_style = "";
    String filter_brands = "";
    String filter_size = "";
    String filter_color = "";
    String filter_tags = "";

    DbHelper db;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.items_tab_layout,container,false);

        init(v);

        if(AddCanvasItem.isfilterclicked) {

            GenerateFiterString();

        }

        GetData();

        item_grid.setNumColumns(2);
        item_grid.setVerticalSpacing(5);
        item_grid.setHorizontalSpacing(5);
        item_grid.setAdapter(closetItemAdapter);
        item_grid.setOnItemClickListener(this);

        item_grid.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (count == 1) {

                    int position = firstVisibleItem+visibleItemCount;
                    int limit = totalItemCount;

                    // Check if bottom has been reached
                    if ((position) >= limit && totalItemCount > 0) {
                        //scroll end reached
                        //Write your code here
                        count = 0;

                        //  item_grid.setSelection(limit);
                        if(!next_page_url.equals("")){
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                    new FetchClosetImages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, next_page_url);

                                } else {

                                    new FetchClosetImages().execute(next_page_url);

                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }else {

                            Toast.makeText(getActivity().getApplicationContext(),"All Items are Listed",Toast.LENGTH_LONG).show();

                        }


                    }


                }
            }
        });

        return v;
    }

    private void GenerateFiterString() {

        if(!FilterActivity.selected_category_id.equals("")){

            filter_category_id = FilterActivity.selected_category_id;

        }

        if(FilterActivity.selected_style_list.size() != 0){

            ArrayList<String> data_array_list = new ArrayList<>();

            for(int i = 0; i<FilterActivity.selected_style_list.size();i++){

                data_array_list.add(db.GetStyle_id(FilterActivity.selected_style_list.get(i).toString()));
            }
            //generate string
            filter_style = Wodrob.BuildString(data_array_list);

        }

        if(FilterActivity.selected_brand_list.size() != 0){

            //generate string
            ArrayList<String> data_array_list = new ArrayList<>();

            for(int i = 0; i<FilterActivity.selected_brand_list.size();i++){

                data_array_list.add(db.GetBrand_id_without_name(FilterActivity.selected_brand_list.get(i).toString()));
            }
            filter_brands = Wodrob.BuildString(data_array_list);

        }
        if(FilterActivity.selected_size_list.size() != 0){

            //generate String
            filter_size = Wodrob.BuildString(FilterActivity.selected_size_list);

        }

        if(FilterActivity.selected_color_list.size() != 0){

            //generate string
            ArrayList<String> data_array_list = new ArrayList<>();

            for(int i = 0; i<FilterActivity.selected_color_list.size();i++){

                data_array_list.add(db.GetColor_id(FilterActivity.selected_color_list.get(i).toString()));
            }

            filter_color = Wodrob.BuildString(data_array_list);

        }

        if(FilterActivity.selected_tags_list.size() != 0){

            //generate string
            ArrayList<String> data_array_list = new ArrayList<>();

            for(int i = 0; i<FilterActivity.selected_tags_list.size();i++){

                data_array_list.add(db.Get_tag_id(FilterActivity.selected_tags_list.get(i).toString()));
            }

            filter_tags = Wodrob.BuildString(FilterActivity.selected_tags_list);


        }

    }

    private void GetData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {
                new FetchClosetImages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"item");
            }
            else {
                new FetchClosetImages().execute(WodrobConstant.BASE_URL+"item");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void init(View v) {

        db = new DbHelper(getActivity().getApplicationContext());
        item_grid = (GridView) v.findViewById(R.id.item_grid);
        closetitemlist = new ArrayList<CatalogItem>();

        pDialog = new ProgressDialog(getActivity());

        /*temperory data*/
        closetItemAdapter = new CatalogItemAdapter(getActivity(), closetitemlist);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Toast.makeText(getActivity().getApplicationContext(),""+position,Toast.LENGTH_LONG).show();

        /*String selected_image_path = closetitemlist.get(position).getImage_path();
        String selected_product_id = closetitemlist.get(position).getProduct_id();
        String selected_image_id = closetitemlist.get(position).getImage_id();

        Intent intent = new Intent();
        intent.putExtra("image_path",selected_image_path);
        intent.putExtra("selected_product_id",selected_product_id);
        intent.putExtra("selected_image_id",selected_image_id);
        intent.putExtra("in_wish_list","1");
        getActivity().setResult(2, intent);
        getActivity().finish();*/

        selected_item_position = position;
        String ids = closetitemlist.get(position).getProduct_id();
        Log.d("id",ids);


        Intent intent  = new Intent(getActivity(), WearViewItem.class);
        intent.putExtra("item_id",closetitemlist.get(position).getProduct_id());
        intent.putExtra("type", "Add");
        intent.putExtra("is_whishlist","AVAILABLE");
        startActivityForResult(intent ,1);

        // ((AddITemInterface) getActivity()).NewItem(selected_image_path);

    }


    private class FetchClosetImages extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            if(!FilterActivity.selected_category_id.equals("")) {
                try{
                    //    progress.setVisibility(View.VISIBLE);
                    //    tvError.setVisibility(View.GONE);
                    // cataloglist.clear();
                    data +="&"+ URLEncoder.encode("category_id", "UTF-8")+"="+ filter_category_id+"&"
                            + URLEncoder.encode("styles", "UTF-8")+"="+filter_style+"&"
                            + URLEncoder.encode("brand", "UTF-8")+"="+filter_brands+"&"
                            + URLEncoder.encode("sizes", "UTF-8")+"="+filter_size+"&"
                            + URLEncoder.encode("colors", "UTF-8")+"="+filter_color;

                    Log.d("data", data);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }




        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {
                pDialog.dismiss();

              //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        if(status.equalsIgnoreCase("true")){

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray data_array = dataobject.getJSONArray("data");
                            next_page_url = dataobject.getString("next_page_url");
                            count = 1;
                            if(next_page_url.equals(null)){

                                next_page_url = "";
                                count = 0;

                            }

                            for(int i = 0;i<data_array.length();i++){

                                JSONObject dataobj = data_array.getJSONObject(i);
                                String product_id = dataobj.getString("product_id");
                                JSONObject product_obj = dataobj.getJSONObject("product");
                                JSONArray image_array = product_obj.getJSONArray("images");

                                JSONObject filename_obj = image_array.getJSONObject(0);
                                String filename = filename_obj.getString("filename");
                                String image_id = filename_obj.getString("_id");

                                closetItems = new CatalogItem(product_id,image_id,filename);
                                closetitemlist.add(closetItems);

                            }


                        }else{

                            Toast.makeText(getActivity().getApplicationContext(),"Unable To Get Data",Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();
            closetItemAdapter.notifyDataSetChanged();
            //item_grid.setAdapter(closetItemAdapter);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1) {

            if(data != null) {
                Log.d("data", data.toString());


                String selected_image_path = closetitemlist.get(selected_item_position).getImage_path();
                String selected_product_id = closetitemlist.get(selected_item_position).getProduct_id();
                String selected_image_id = closetitemlist.get(selected_item_position).getImage_id();

                Intent intent = new Intent();
                intent.putExtra("image_path", selected_image_path);
                intent.putExtra("selected_product_id", selected_product_id);
                intent.putExtra("selected_image_id", selected_image_id);
                intent.putExtra("in_wish_list", "0");
                getActivity().setResult(2, intent);
                getActivity().finish();

            }


        }
    }
}

