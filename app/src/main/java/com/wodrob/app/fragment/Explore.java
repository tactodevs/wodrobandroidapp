package com.wodrob.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.adapter.ExploreViewPagerAdapter;


/**
 * Created by rameesfazal on 12/1/16.
 */
public class Explore extends Fragment {

    ViewPager vp_outfit;
    TabLayout tabLayout;
    ExploreViewPagerAdapter exploreViewPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_explore_layout,container,false);

        init(view);

        exploreViewPagerAdapter = new ExploreViewPagerAdapter(getFragmentManager(),tabLayout.getTabCount());
        vp_outfit.setAdapter(exploreViewPagerAdapter);

        vp_outfit.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp_outfit.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    private void init(View v) {

        tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("CATALOG"));
        tabLayout.addTab(tabLayout.newTab().setText("LOOKBOOK"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        vp_outfit = (ViewPager) v.findViewById(R.id.vp_outfit);

        Wodrob.applyFont(getContext(), tabLayout, "fonts/montserratregular.ttf");
    }

}
