package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.ViewCatalog;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.ExploreCatalogAdapter;
import com.wodrob.app.model.ExploreCatalogModel;
import com.wodrob.app.views.EndlessOnScrollListener;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 19/1/16.
 */
public class ExploreCatalogFragment extends Fragment {

    ArrayList<ExploreCatalogModel> exploreCatalogModelArrayList;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    ExploreCatalogAdapter singleadapter;
    ExploreCatalogModel exploreCatalogModel;
    ProgressDialog pDialog;
    DbHelper db;
    String next_page_url;
    boolean loading = true;
    private GridLayoutManager gridlayoutmanager;

    String filter_category_id = "";
    String filter_style = "";
    String filter_brands = "";
    String filter_size = "";
    String filter_color = "";
    String filter_tags = "";
    boolean isfirstime = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.item_list_layout, container, false);

        db = new DbHelper(getContext());

        init(rootView);
        setHasOptionsMenu(true);

        GetFilterString();
        GetData();

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it
                Intent intent = new Intent(getActivity(), ViewCatalog.class);
                intent.putExtra("product_id",exploreCatalogModelArrayList.get(position).getProduct_id());
                intent.putExtra("type","view");
                startActivity(intent);
            }
        });


        recyclerView.addOnScrollListener(new EndlessOnScrollListener(gridlayoutmanager) {

            @Override
            public void onScrolledToEnd() {
                if (!loading) {
                    loading = true;
                    // add 10 by 10 to tempList then notify changing in data
                    if (!next_page_url.equals("")) {
                        singleadapter.notifyDataSetChanged();
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, next_page_url);

                            } else {

                                new FetchItemData().execute(next_page_url);

                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
                loading = false;
            }
        });

        return rootView;
    }

    private void GetFilterString() {

        String[] filterstring = Wodrob.GenerateFiterString(getActivity()).split("/");

        try{

            filter_category_id = filterstring[0];
            filter_style = filterstring[1];
            filter_color = filterstring[2];
            filter_size = filterstring[3];
            filter_brands = filterstring[4];
            filter_tags = filterstring[5];

        }catch (ArrayIndexOutOfBoundsException e){

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isfirstime){

            isfirstime = false;

        }
        isfirstime = false;


    }

    private void init(View rootView) {

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        recyclerView.addItemDecoration(new SingleSpacingDecoration(2, "parent"));


        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        pDialog = new ProgressDialog(getActivity());
        gridlayoutmanager = new GridLayoutManager(getActivity(), 2);
        exploreCatalogModelArrayList = new ArrayList<>();
        recyclerView.setLayoutManager(gridlayoutmanager);

        setlayout();

    }


    private void setlayout() {

        singleadapter = new ExploreCatalogAdapter(exploreCatalogModelArrayList, R.layout.item_grid_layout);
        recyclerView.setAdapter(singleadapter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
                // Not implemented here
            case R.id.mi_filter:
                // Do Fragment menu item stuff here
                Intent intent = new Intent(getActivity(),FilterActivity.class);
                startActivityForResult(intent, 5);
                return true;
            default:
                break;
        }

        return false;
    }


    private void GetData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "product");

            } else {

                new FetchItemData().execute(WodrobConstant.BASE_URL + "product");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /*get item data*/

    private class FetchItemData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();
            try{

                data +="&"+ URLEncoder.encode("categories", "UTF-8")+"="+ filter_category_id+"&"
                        + URLEncoder.encode("styles", "UTF-8")+"="+filter_style+"&"
                        + URLEncoder.encode("brand", "UTF-8")+"="+filter_brands+"&"
                        + URLEncoder.encode("sizes", "UTF-8")+"="+filter_size+"&"
                        + URLEncoder.encode("colors", "UTF-8")+"="+filter_color;

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                  pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray data_array = dataobject.getJSONArray("data");
                            next_page_url = dataobject.getString("next_page_url");
                            //count = 1;
                            if(next_page_url.equals(null)){

                                next_page_url = "";
                                loading = false;
                                // count = 0;

                            }

                            for(int i = 0;i<data_array.length();i++){

                                JSONObject data_obj = data_array.getJSONObject(i);
                                String name = data_obj.getString("name");
                                String product_id = data_obj.getString("_id");
                                JSONArray image_array = data_obj.getJSONArray("images");
                                JSONObject image_obj = image_array.getJSONObject(0);

                                String image_path = image_obj.getString("filename");

                                exploreCatalogModel = new ExploreCatalogModel(product_id,name,image_path);
                                exploreCatalogModelArrayList.add(exploreCatalogModel);
                                //  cataloglist.add(catalogItem);po

                            }
                           singleadapter.notifyDataSetChanged();


                        }else{

                            Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pDialog.dismiss();

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        exploreCatalogModelArrayList.clear();
        singleadapter.notifyDataSetChanged();
        GetFilterString();
        GetData();

    }
}
