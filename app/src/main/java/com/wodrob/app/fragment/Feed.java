package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.LookBookView;
import com.wodrob.app.R;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.FeedAdapter;
import com.wodrob.app.model.FeedEmptyClosetItemModel;
import com.wodrob.app.model.FeedEmptyClosetModel;
import com.wodrob.app.model.FeedFriendsYouMayFolow;
import com.wodrob.app.model.FeedModel;
import com.wodrob.app.model.FeedProfileComplete;
import com.wodrob.app.model.FeedStoryModel;
import com.wodrob.app.model.FeedStyleRequest;
import com.wodrob.app.model.FeedWeatherModel;
import com.wodrob.app.model.FeedWishListModel;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by rameesfazal on 12/1/16.
 */
public class Feed extends Fragment {

    LinearLayoutManager layoutManager;
    FeedAdapter feedAdapter;
    ProgressDialog pDialog;
    DbHelper db;
    FeedModel feedModel;
    RecyclerView rv_feed;
    ArrayList<FeedModel> feedModelArrayList;

    ArrayList<FeedEmptyClosetItemModel> feedEmptyClosetItemModelArrayList;
    /*decleration of feed model classes*/
    FeedWeatherModel feedWeatherModel;
    FeedEmptyClosetModel feedEmptyClosetModel;
    FeedEmptyClosetItemModel feedEmptyClosetItemModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_feed_layout, container, false);

        init(view);
        GetData();
        return view;
    }

    private void GetData() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new FetchFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "feed/emptyCloset");
            } else {
                new FetchFeed().execute(WodrobConstant.BASE_URL + "feed/emptyCloset");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    private void init(View rootView) {

        rv_feed = (RecyclerView) rootView.findViewById(R.id.rv_feed);

        feedModelArrayList = new ArrayList<>();
        feedEmptyClosetItemModelArrayList = new ArrayList<>();

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        pDialog = new ProgressDialog(getActivity());

        setlayout();

    }


    private void setlayout() {

        feedAdapter = new FeedAdapter(getActivity(), feedModelArrayList, R.layout.feed_item);
        rv_feed.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        rv_feed.setAdapter(feedAdapter);
        rv_feed.setLayoutManager(layoutManager);

    }

    /*backgorund class for fetching feed data*/

    private class FetchFeed extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data += "&" + URLEncoder.encode("patron_id", "UTF-8") + "=" + WodrobConstant.patron_id;

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization", "Bearer qG3Tq3oN6ixE0OhAT6ZsZzfRRMgDr0IUf4CtH2jy");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);

                        // card weather starts

                       /* String weather = jsonObject.getString("weather");
                        JSONArray weather_suggested_outfits = jsonObject.getJSONArray("suggested_outfits");
                        JSONObject weather_outfit_image_1_obj = weather_suggested_outfits.getJSONObject(0);
                        JSONObject weather_outfit_image_2_obj = weather_suggested_outfits.getJSONObject(0);

                        JSONObject weather_outfit_id_1_obj = weather_outfit_image_1_obj.getJSONObject("_id");
                        JSONObject weather_outfit_id_2_obj = weather_outfit_image_1_obj.getJSONObject("_id");


                        String weather_outfit_image_1 = weather_outfit_image_1_obj.getString("image");
                        String weather_outfit_image_2 = weather_outfit_image_2_obj.getString("image");
                        String weather_outfit_id_1 = weather_outfit_id_1_obj.getString("$id");
                        String weather_outfit_id_2 = weather_outfit_id_2_obj.getString("$id");

                        feedWeatherModel = new FeedWeatherModel(weather_outfit_id_1,weather_outfit_id_2,weather_outfit_image_1,weather_outfit_image_2,weather,"");

                        feedModel = new FeedModel("weather",null,feedWeatherModel,null,null,null,null,null,null);
                        feedModelArrayList.add(feedModel);*/

                        //cards empty closet starts

                       /* JSONArray empty_closet_recommended_products_arrray = jsonObject.getJSONArray("recommended_products");

                        JSONObject empty_closet_recommended_products_obj1 = empty_closet_recommended_products_arrray.getJSONObject(0);
                        JSONObject empty_closet_recommended_products_obj2 = empty_closet_recommended_products_arrray.getJSONObject(0);
                        JSONObject empty_closet_recommended_products_obj3 = empty_closet_recommended_products_arrray.getJSONObject(0);

                        JSONArray image_array1 = empty_closet_recommended_products_obj1.getJSONArray("images");
                        JSONArray image_array2 = empty_closet_recommended_products_obj2.getJSONArray("images");
                        JSONArray image_array3 = empty_closet_recommended_products_obj3.getJSONArray("images");

                        JSONObject id_obj1 = empty_closet_recommended_products_obj1.getJSONObject("_id");
                        JSONObject id_obj2 = empty_closet_recommended_products_obj1.getJSONObject("_id");
                        JSONObject id_obj3 = empty_closet_recommended_products_obj1.getJSONObject("_id");

                        String id1 = id_obj1.getString("$id");
                        String id2 = id_obj2.getString("$id");
                        String id3 = id_obj3.getString("$id");

                        JSONObject image_object1 = image_array1.getJSONObject(0);
                        JSONObject image_object2 = image_array2.getJSONObject(0);
                        JSONObject image_object3 = image_array3.getJSONObject(0);

                        String image1 = image_object1.getString("filename");
                        String image2 = image_object2.getString("filename");
                        String image3 = image_object3.getString("filename");


                        feedEmptyClosetModel = new FeedEmptyClosetModel(image1, image2, image3, id1, id2, id3);
                        feedModel = new FeedModel("empty_closet", null, null, null, null, null, null, null, feedEmptyClosetModel);
                        feedModelArrayList.add(feedModel);
*/




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();
            feedAdapter.notifyDataSetChanged();

        }
    }

}

