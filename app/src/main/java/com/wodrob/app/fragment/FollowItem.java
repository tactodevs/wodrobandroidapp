package com.wodrob.app.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.FollowItemAdapter;
import com.wodrob.app.model.FollowItemModel;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/2/16.
 */
public class FollowItem extends Fragment {

    int pager_postion;
    LinearLayoutManager layoutManager;
    FollowItemAdapter singleadapter;
    ProgressDialog pDialog;
    DbHelper db;
    FollowItemModel followItemModel;
    RecyclerView recyclerView;
    ArrayList<FollowItemModel> followItemModelArrayList;

    public FollowItem() {
    }

    @SuppressLint("ValidFragment")
    public FollowItem(int position) {

        this.pager_postion = position;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.follow_fragment, container, false);

        init(view);

        FetchData();

        return view;

    }

    private void FetchData() {

        if(pager_postion == 0){

            try {
                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                    new GetData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"recommendation/invite");

                }
                else {

                    new GetData().execute(WodrobConstant.BASE_URL+"recommendation/invite");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        else if(pager_postion == 1){

            try {
                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                    new GetData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"recommendation/follow");

                }
                else {

                    new GetData().execute(WodrobConstant.BASE_URL+"recommendation/follow");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }else if(pager_postion == 2){

            try {
                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                    new GetData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"recommendation/friend");

                }
                else {

                    new GetData().execute(WodrobConstant.BASE_URL+"recommendation/friend");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private void init(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        followItemModelArrayList = new ArrayList<>();

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        pDialog = new ProgressDialog(getActivity());
        setlayout();
    }

    private void setlayout() {

        singleadapter = new FollowItemAdapter(getActivity(),followItemModelArrayList, R.layout.follow_item_layout,pager_postion);
        recyclerView.addItemDecoration(new SingleSpacingDecoration(10,"parent"));
        recyclerView.setAdapter(singleadapter);
        recyclerView.setLayoutManager(layoutManager);

    }

     /*Fetch  data*/

    private class GetData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            //no parameters are required // only headers are sending

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization",Wodrob.GetAccessToken(getActivity()));
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                       // String message = jsonObject.getString("message");

                        String id = "",display_name;

                        if(status.equalsIgnoreCase("true")){

                            JSONArray dataarray = jsonObject.getJSONArray("data");


                            for(int i = 0;i<dataarray.length();i++){

                                JSONObject dataobject = dataarray.getJSONObject(i);

                                if(pager_postion == 0){

                                    id = dataobject.getString("id");

                                }else if(pager_postion == 1){

                                    id = dataobject.getString("followee_id");


                                }else if(pager_postion == 2){

                                    id = dataobject.getString("friend_id");

                                }

                                display_name = dataobject.getString("display_name");

                                followItemModel = new FollowItemModel(id,display_name,"","");
                                followItemModelArrayList.add(followItemModel);

                            }
                            singleadapter.notifyDataSetChanged();

                        }else{

                         //   Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }

}
