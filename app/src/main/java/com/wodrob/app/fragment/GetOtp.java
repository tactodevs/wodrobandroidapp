package com.wodrob.app.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wodrob.app.R;
import com.wodrob.app.VerifyOtpActivity;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by rameesfazal on 16/2/16.
 */
public class GetOtp extends Fragment implements View.OnClickListener{

    EditText ed_phone;
    Button btn_get_otp;
    String phone ="";
    ProgressDialog pDialog;
    String phone_no;

    LinearLayout ll_message_layout,ll_main_layout;

    String source;

    public GetOtp() {


    }

    @SuppressLint("ValidFragment")
    public GetOtp(String source) {

        this.source = source;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_getotp,container,false);

        init(view);

        if(phone_no.equalsIgnoreCase("")){

        ll_main_layout.setVisibility(View.VISIBLE);
        ll_message_layout.setVisibility(View.GONE);

        }else{

            ll_message_layout.setVisibility(View.VISIBLE);
            ll_main_layout.setVisibility(View.GONE);

        }

        return view;
    }

    private void init(View view) {

        ed_phone = (EditText) view.findViewById(R.id.ed_phone);
        btn_get_otp = (Button) view.findViewById(R.id.btn_getotp);
        pDialog = new ProgressDialog(getActivity());
        btn_get_otp.setOnClickListener(this);

        phone_no = Wodrob.getPreference(getActivity().getApplicationContext(),"phone");


        ll_message_layout = (LinearLayout) view.findViewById(R.id.ll_message_layout);
        ll_main_layout = (LinearLayout) view.findViewById(R.id.ll_main_layout);


    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_getotp){

            phone = ed_phone.getText().toString();
            SendOtpRequest();

        }


    }

    private void SendOtpRequest() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new SendOtp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "otp/generate");

            }
            else {

                new SendOtp().execute(WodrobConstant.BASE_URL+"otp/generate");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

     /* sending otp request*/

    private class SendOtp extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data +="&"+ URLEncoder.encode("mobile", "UTF-8")+"="+ phone+"&"
                        + URLEncoder.encode("country_code", "UTF-8")+"="+"IN"+"&"
                        + URLEncoder.encode("type", "UTF-8")+"="+"mobile_verification";

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization", "Bearer qG3Tq3oN6ixE0OhAT6ZsZzfRRMgDr0IUf4CtH2jy");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        if(jsonObject.has("error")){

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                           Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else if(jsonObject.has("status")){

                            String status = jsonObject.getString("status");
                            if(status.equals("true")){


                                Intent intent = new Intent(getActivity(), VerifyOtpActivity.class);
                                intent.putExtra("phone", phone);
                                intent .putExtra("country_code", "IN"); // hard coded ...

                                if(source.equalsIgnoreCase("Login")){

                                    intent.putExtra("type", "Login");

                                }else if(source.equalsIgnoreCase("Onboarding")){

                                    intent.putExtra("type", "mobile_verification");

                                }

                                getActivity().startActivityForResult(intent, 1);

                            }else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                            }

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getActivity().getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }
    /*ends*/

}
