package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.wodrob.app.MainActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.onboarding.LoginActivity;
import com.wodrob.app.onboarding.OnboardingActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;

/**
 * Created by rameesfazal on 16/2/16.
 */
public class Login extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    EditText ed_email, ed_password;
    Button btn_login, btn_getotp, btn_forget_password, btn_signup, btn_send_mail;
    ProgressDialog pDialog;
    String email, password;
    CallbackManager callbackManager;
    LoginButton loginButton;
    String fb_id, accessToken, fb_email, first_name, last_name, dob, gender, picture;
    TextInputLayout email_layout, passwoed_layout;


    //Signin button
    private SignInButton signInButton;

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    private GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View view = inflater.inflate(R.layout.layout_login, container, false);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken("1023513032563-bjkb3ms47kbvumk7k4kaio1ok308lcas.apps.googleusercontent.com")
                .requestEmail()
                .build();

        init(view);

        /*google sign in*/

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API).build();

        signInButton.setOnClickListener(this);

        //        ends

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                pDialog.setMessage("Fetching Details . . .");
                pDialog.show();
                accessToken = loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Log.v("LoginActivity", response.toString());

                                getFacebookData(object);
                                pDialog.dismiss();
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        return view;
    }


    public void getFacebookData(JSONObject object) {


        try {

            fb_id = object.getString("id");

            URL profile_pic = new URL("https://graph.facebook.com/" + fb_id + "/picture?width=200&height=150");
            picture = profile_pic.toString();

            if (object.has("first_name")) {

                first_name = object.getString("first_name");

            }
            if (object.has("last_name")) {

                last_name = object.getString("last_name");

            }
            if (object.has("birthday")) {

                dob = object.getString("birthday");

            }
            if (object.has("gender")) {

                gender = object.getString("gender");

            }
            if (object.has("email")) {

                fb_email = object.getString("email");

                DoFacebookLogin();


            } else {

                //  LoginManager.getInstance().logOut();
                Toast.makeText(getActivity().getApplicationContext(), "Sorry ! Unable to connect With facebook", Toast.LENGTH_LONG).show();
                disconnectFromFacebook();

            }

            pDialog.dismiss();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void DoFacebookLogin() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new PreSignUp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/presignup");

            } else {

                new PreSignUp().execute(WodrobConstant.BASE_URL + "account/presignup");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void init(View view) {

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends, user_events, user_likes, user_location, user_photos"));
        loginButton.setFragment(this);


        email_layout = (TextInputLayout) view.findViewById(R.id.email_layout);
        passwoed_layout = (TextInputLayout) view.findViewById(R.id.password_layout);
        /*google sign in*/

        //Initializing signinbutton for google
        signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());

        //ends

        ed_email = (EditText) view.findViewById(R.id.ed_email);
        ed_password = (EditText) view.findViewById(R.id.ed_password);
        btn_login = (Button) view.findViewById(R.id.btn_login);
        btn_getotp = (Button) view.findViewById(R.id.btn_getotp);
        btn_forget_password = (Button) view.findViewById(R.id.btn_forget_password);
        btn_signup = (Button) view.findViewById(R.id.btn_sign_up);
        btn_send_mail = (Button) view.findViewById(R.id.btn_send_mail);

        btn_signup.setOnClickListener(this);
        btn_getotp.setOnClickListener(this);
        btn_forget_password.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_send_mail.setOnClickListener(this);

        pDialog = new ProgressDialog(getActivity());
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_login) {

            email = ed_email.getText().toString();
            password = ed_password.getText().toString();

            if (email.equals("")) {

                email_layout.setError("Please fill the email");
                email_layout.setHint("");

            }else if (!Wodrob.isValidEmail(ed_email.getText().toString())) {

                email_layout.setError("Please enter a valid email");
                email_layout.setHint("");

            } else if (password.equals("")) {

                passwoed_layout.setError("Please fill the password");
                passwoed_layout.setHint("");

            } else {

                DoLogin();

            }

        } else if (v.getId() == R.id.btn_sign_up) {

            ((LoginActivity) getActivity()).SetFragment("Signup");

        } else if (v.getId() == R.id.btn_forget_password) {

            ed_password.setVisibility(View.GONE);
            btn_login.setVisibility(View.GONE);
            btn_signup.setVisibility(View.GONE);
            btn_getotp.setVisibility(View.GONE);
            btn_forget_password.setVisibility(View.GONE);
            passwoed_layout.setVisibility(View.GONE);
            btn_send_mail.setVisibility(View.VISIBLE);
            LoginActivity.tv_title.setText("Forgot Password");

        } else if (v.getId() == R.id.btn_send_mail) {

            email = ed_email.getText().toString();

            if (email.equals("")) {

                email_layout.setError("Please fill the email");
                email_layout.setHint("");

            }else if (!Wodrob.isValidEmail(ed_email.getText().toString())) {

                email_layout.setError("Please enter a valid email");
                email_layout.setHint("");

            } else {

                sendemail();

            }

        } else if (v.getId() == R.id.btn_getotp) {

            ((LoginActivity) getActivity()).SetFragment("GetOtp");


        } else if (v.getId() == R.id.sign_in_button) {

            signIn();
            // mGoogleApiClient.connect();

        }

    }

    private void sendemail() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new SendEmail().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/forget_password");

            } else {

                new SendEmail().execute(WodrobConstant.BASE_URL + "account/forget_password");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void DoLogin() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new LoginUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/login");

            } else {

                new LoginUser().execute(WodrobConstant.BASE_URL + "account/login");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    @Override
    public void onConnected(Bundle bundle) {

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

            Person currentPerson = Plus.PeopleApi
                    .getCurrentPerson(mGoogleApiClient);

            first_name = currentPerson.getDisplayName().split(" ")[0];
            last_name = currentPerson.getDisplayName().split(" ")[1];
            fb_id = currentPerson.getId();
            email = Plus.AccountApi.getAccountName(mGoogleApiClient);


            try {
                int genderInt = currentPerson.getGender();
                String goolegender = String.valueOf(genderInt);
                if (goolegender.equals("0")) {

                    gender = "male";

                } else {

                    gender = "female";

                }
                Person.Image googlepicture = currentPerson.getImage();
                if (googlepicture != null) {

                    picture = googlepicture.getUrl();

                }
                dob = currentPerson.getBirthday();


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public void onConnectionFailed(ConnectionResult result) {
        //if (!mIntentInProgress && result.hasResolution()) {
        try {
            //mIntentInProgress = true;
            getActivity().startIntentSenderForResult(result.getResolution()
                    .getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {

            // mIntentInProgress = false;
            mGoogleApiClient.connect();
        }
        //}
    }

    @Override
    public void onConnectionSuspended(int i) {

        mGoogleApiClient.connect();

    }



    /*forget password sending email*/

    private class SendEmail extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data += "&" + URLEncoder.encode("client_id", "UTF-8") + "=" + WodrobConstant.client_id + "&"
                        + URLEncoder.encode("client_secret", "UTF-8") + "=" + WodrobConstant.client_secret + "&"
                        + URLEncoder.encode("email", "UTF-8") + "=" + email;

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if (jsonObject.has("error")) {

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        } else if (jsonObject.has("status")) {

                            String status = jsonObject.getString("status");
                            if (status.equals("true")) {

                                ed_password.setVisibility(View.VISIBLE);
                                btn_login.setVisibility(View.VISIBLE);
                                btn_signup.setVisibility(View.VISIBLE);
                                btn_getotp.setVisibility(View.VISIBLE);
                                btn_forget_password.setVisibility(View.VISIBLE);
                                passwoed_layout.setVisibility(View.VISIBLE);
                                btn_send_mail.setVisibility(View.GONE);

                            } else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            }

                        }

                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(getActivity().getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();

                }

            }

        }
    }

    /*ends*/
    private class LoginUser extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data += "&" + URLEncoder.encode("client_id", "UTF-8") + "=" + WodrobConstant.client_id + "&"
                        + URLEncoder.encode("client_secret", "UTF-8") + "=" + WodrobConstant.client_secret + "&"
                        + URLEncoder.encode("email", "UTF-8") + "=" + email + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + password + "&"
                        + URLEncoder.encode("uuid", "UTF-8") + "=" + Wodrob.getPreference(getActivity().getApplicationContext(), "uuid");

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if (jsonObject.has("error")) {

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        } else if (jsonObject.has("status")) {

                            String status = jsonObject.getString("status");
                            if (status.equals("true")) {

                                JSONObject token_object = jsonObject.getJSONObject("token");

                                String access_token = token_object.getString("access_token");

                                String token_type = token_object.getString("token_type");

                                String expires_in = token_object.getString("expires_in");

                                String refresh_token = token_object.getString("refresh_token");


                                Wodrob.setPreference(getActivity().getApplicationContext(), access_token, "access_token");
                                Wodrob.setPreference(getActivity().getApplicationContext(), token_type, "token_type");
                                Wodrob.setPreference(getActivity().getApplicationContext(), expires_in, "expires_in");
                                Wodrob.setPreference(getActivity().getApplicationContext(), refresh_token, "refresh_token");


                                    Intent intent = new Intent(getActivity(),MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                            } else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            }

                        }

                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(getActivity().getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();

                }

            }

        }
    }

    /*ends*/
    private class PreSignUp extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data += "&" + URLEncoder.encode("client_id", "UTF-8") + "=" + WodrobConstant.client_id + "&"
                        + URLEncoder.encode("client_secret", "UTF-8") + "=" + WodrobConstant.client_secret + "&"
                        + URLEncoder.encode("fbid", "UTF-8") + "=" + fb_id + "&"
                        + URLEncoder.encode("access_token", "UTF-8") + "=" + accessToken + "&"
                        + URLEncoder.encode("email", "UTF-8") + "=" + fb_email + "&"
                        + URLEncoder.encode("first_name", "UTF-8") + "=" + first_name + "&"
                        + URLEncoder.encode("last_name", "UTF-8") + "=" + last_name + "&"
                        + URLEncoder.encode("dob", "UTF-8") + "=" + dob + "&"
                        + URLEncoder.encode("gender", "UTF-8") + "=" + gender + "&"
                        + URLEncoder.encode("picture", "UTF-8") + "=" + picture;
                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if (jsonObject.has("error")) {

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        } else if (jsonObject.has("status")) {

                            String status = jsonObject.getString("status");
                            if (status.equals("true")) {

                                JSONObject token_object = jsonObject.getJSONObject("token");

                                String access_token = token_object.getString("access_token");

                                String token_type = token_object.getString("token_type");

                                String expires_in = token_object.getString("expires_in");

                                String refresh_token = token_object.getString("refresh_token");


                                Wodrob.setPreference(getActivity().getApplicationContext(), access_token, "access_token");
                                Wodrob.setPreference(getActivity().getApplicationContext(), token_type, "token_type");
                                Wodrob.setPreference(getActivity().getApplicationContext(), expires_in, "expires_in");
                                Wodrob.setPreference(getActivity().getApplicationContext(), refresh_token, "refresh_token");

                                if(Wodrob.getbooleanpreference(getActivity().getApplicationContext(),"isFirstTime")) {

                                    Wodrob.setbooleanpreference(getActivity().getApplicationContext(), true, "isFirstTime");
                                    Intent intent = new Intent(getActivity(), OnboardingActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                                }else{

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                                }


                            } else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            }

                        }

                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(getActivity().getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();

                }

            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {

            callbackManager.onActivityResult(requestCode, resultCode, data);

        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            first_name = acct.getDisplayName().split(" ")[0];
            last_name = acct.getDisplayName().split(" ")[1];
            accessToken = acct.getIdToken();
            fb_id = acct.getId();
            email = acct.getEmail();
            Uri picture_uri = acct.getPhotoUrl();
            if (picture_uri != null) {

                picture = picture_uri.toString();
            }

            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);

                try {
                    int genderInt = currentPerson.getGender();
                    String goolegender = String.valueOf(genderInt);
                    if (goolegender.equals("0")) {

                        gender = "male";

                    } else {

                        gender = "female";

                    }
                    if (picture_uri == null) {

                        Person.Image googlepicture = currentPerson.getImage();
                        if (googlepicture != null) {

                            picture = googlepicture.getUrl();

                        }

                    }

                    dob = currentPerson.getBirthday();


                } catch (Exception e) {

                }

            } else {
                // Signed out, show unauthenticated UI.

            }
        }
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }
}
