package com.wodrob.app.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.wodrob.app.MainActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.onboarding.LoginActivity;
import com.wodrob.app.onboarding.OnboardingActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;

/**
 * Created by rameesfazal on 16/2/16.
 */
public class LoginHome extends Fragment implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks {

    Button btn_login;
    Button btn_sign_up;
    //Signin button
    private SignInButton signInButton;
    String email;

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    public GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    CallbackManager callbackManager;
    LoginButton loginButton;
    String fb_id="", accessToken="", fb_email, first_name="", last_name="", dob="", gender="", picture="";
    ProgressDialog pDialog;

    String login_type;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View view = inflater.inflate(R.layout.layout_home, container, false);


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken("1023513032563-bjkb3ms47kbvumk7k4kaio1ok308lcas.apps.googleusercontent.com")
                .requestEmail()
                .build();

        init(view);
         /*google sign in*/

        loginButton.getLoginBehavior();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this)
                .addApi(Plus.API).build();


       // mGoogleApiClient.connect();


        signInButton.setOnClickListener(this);


        //authorising facebook login


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                login_type = "facebook";
                pDialog.setMessage("Fetching Details . . .");
                pDialog.show();
                accessToken = loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Log.v("LoginActivity", response.toString());

                                getFacebookData(object);
                                pDialog.dismiss();
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location, age_range"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        return view;
    }

    public void getFacebookData(JSONObject object) {


        try {

            fb_id = object.getString("id");

            URL profile_pic = new URL("https://graph.facebook.com/" + fb_id + "/picture?width=200&height=150");
            picture = profile_pic.toString();

            if (object.has("first_name")) {

                first_name = object.getString("first_name");

            }
            if (object.has("last_name")) {

                last_name = object.getString("last_name");

            }
            if (object.has("birthday")) {

                dob = object.getString("birthday");

            }
            if (object.has("gender")) {

                gender = object.getString("gender");

            }
            if (object.has("email")) {

                email = object.getString("email");

                DoFacebookLogin();


            } else {

                //  LoginManager.getInstance().logOut();
                Toast.makeText(getActivity().getApplicationContext(), "Sorry ! Unable to connect With facebook", Toast.LENGTH_LONG).show();
                disconnectFromFacebook();

            }

            pDialog.dismiss();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void DoFacebookLogin() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new PreSignUp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/social-login");

            } else {

                new PreSignUp().execute(WodrobConstant.BASE_URL + "account/social-login");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void signIn() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void init(View view) {

        btn_login = (Button) view.findViewById(R.id.btn_login);
        btn_sign_up = (Button) view.findViewById(R.id.btn_sign_up);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/montserratregular.ttf");
        btn_login.setTypeface(face);
        btn_sign_up.setTypeface(face);

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends, user_events, user_likes, user_location, user_photos"));
        loginButton.setFragment(this);

        /*google sign in*/

        //Initializing signinbutton for google
        signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());

        pDialog = new ProgressDialog(getActivity());

        btn_login.setOnClickListener(this);
        btn_sign_up.setOnClickListener(this);

    }

    @Override

    public void onClick(View v) {

        if (v.getId() == R.id.btn_login) {

            ((LoginActivity) getActivity()).SetFragment("Login");

        } else if (v.getId() == R.id.btn_sign_up) {

            ((LoginActivity) getActivity()).SetFragment("Signup");
        } else if (v.getId() == R.id.sign_in_button) {

            login_type = "Google";
            signIn();
            // mGoogleApiClient.connect();

        }

    }

    @Override
    public void onConnected(Bundle bundle) {

      //  CheckGoogleSignin();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class PreSignUp extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                if(login_type.equalsIgnoreCase("facebook")){

                    data += "&" + URLEncoder.encode("client_id", "UTF-8") + "=" + WodrobConstant.client_id + "&"
                            + URLEncoder.encode("client_secret", "UTF-8") + "=" + WodrobConstant.client_secret + "&"
                            + URLEncoder.encode("facebook_id", "UTF-8") + "=" + fb_id + "&"
                            + URLEncoder.encode("token", "UTF-8") + "=" + accessToken + "&"
                            + URLEncoder.encode("email", "UTF-8") + "=" + email + "&"
                            + URLEncoder.encode("first_name", "UTF-8") + "=" + first_name + "&"
                            + URLEncoder.encode("last_name", "UTF-8") + "=" + last_name + "&"
                            + URLEncoder.encode("dob", "UTF-8") + "=" + dob + "&"
                            + URLEncoder.encode("gender", "UTF-8") + "=" + gender + "&"
                            + URLEncoder.encode("avatar", "UTF-8") + "=" + picture + "&"
                            + URLEncoder.encode("uuid", "UTF-8") + "=" + Wodrob.GetUuid(getActivity());
                }else if(login_type.equalsIgnoreCase("google")){

                    data += "&" + URLEncoder.encode("client_id", "UTF-8") + "=" + WodrobConstant.client_id + "&"
                            + URLEncoder.encode("client_secret", "UTF-8") + "=" + WodrobConstant.client_secret + "&"
                            + URLEncoder.encode("google_id", "UTF-8") + "=" + fb_id + "&"
                            + URLEncoder.encode("token", "UTF-8") + "=" + accessToken + "&"
                            + URLEncoder.encode("email", "UTF-8") + "=" + email + "&"
                            + URLEncoder.encode("first_name", "UTF-8") + "=" + first_name + "&"
                            + URLEncoder.encode("last_name", "UTF-8") + "=" + last_name + "&"
                            + URLEncoder.encode("dob", "UTF-8") + "=" + dob + "&"
                            + URLEncoder.encode("gender", "UTF-8") + "=" + gender + "&"
                            + URLEncoder.encode("avatar", "UTF-8") + "=" + picture + "&"
                            + URLEncoder.encode("uuid", "UTF-8") + "=" + Wodrob.GetUuid(getActivity());


                }


                 Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if (jsonObject.has("error")) {

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        } else if (jsonObject.has("status")) {

                            String status = jsonObject.getString("status");
                            JSONObject data_object = jsonObject.getJSONObject("data");
                            if (status.equals("true")) {

                                JSONObject token_object = data_object.getJSONObject("token");

                                String access_token = token_object.getString("access_token");

                                String token_type = token_object.getString("token_type");

                                String expires_in = token_object.getString("expires_in");

                                String refresh_token = token_object.getString("refresh_token");


                                Wodrob.setPreference(getActivity().getApplicationContext(), access_token, "access_token");
                                Wodrob.setPreference(getActivity().getApplicationContext(), token_type, "token_type");
                                Wodrob.setPreference(getActivity().getApplicationContext(), expires_in, "expires_in");
                                Wodrob.setPreference(getActivity().getApplicationContext(), refresh_token, "refresh_token");
                                Wodrob.setbooleanpreference(getActivity().getApplicationContext(),true,"has_access_token");

                                if (!Wodrob.getbooleanpreference(getActivity().getApplicationContext(), "IsVisited_Onboarding")) {

                                    Wodrob.setbooleanpreference(getActivity().getApplicationContext(), true, "IsVisited_Onboarding");
                                    Intent intent = new Intent(getActivity(), OnboardingActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                                } else {

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                                }


                            } else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            }

                        }

                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(getActivity().getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();

                }

            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {

            callbackManager.onActivityResult(requestCode, resultCode, data);

        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            first_name = acct.getDisplayName().split(" ")[0];
            last_name = acct.getDisplayName().split(" ")[1];
            accessToken = acct.getIdToken();
            fb_id = acct.getId();
            email = acct.getEmail();
            Uri picture_uri = acct.getPhotoUrl();
            if (picture_uri != null) {

                picture = picture_uri.toString();
            }

            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);

                try {
                    int genderInt = currentPerson.getGender();
                    String goolegender = String.valueOf(genderInt);
                    if (goolegender.equals("0")) {

                        gender = "male";

                    } else {

                        gender = "female";

                    }
                    if (picture_uri == null) {

                        Person.Image googlepicture = currentPerson.getImage();
                        if (googlepicture != null) {

                            picture = googlepicture.getUrl();

                        }

                    }

                    dob = currentPerson.getBirthday();

                    DoFacebookLogin();

                } catch (Exception e) {

                }

            } else {
                // Signed out, show unauthenticated UI.

            }
        }
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }
}