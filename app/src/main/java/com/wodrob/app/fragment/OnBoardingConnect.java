package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.OnBoardingConnectAdapter;
import com.wodrob.app.adapter.OnBoardingConnectedAdapter;
import com.wodrob.app.model.ConnectModel;
import com.wodrob.app.onboarding.OnboardingActivity;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by rameesfazal on 20/2/16.
 */
public class OnBoardingConnect extends Fragment implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks{

    ProgressDialog pDialog;

    ArrayList<ConnectModel> connectModelArrayList;
    ConnectModel connectModels;
    ArrayList<ConnectModel> connectedModelArrayList;

    HashMap<String, Boolean> ConnectedMaps;
    HashMap<String, Boolean> ConnectMaps;
    OnBoardingConnectAdapter onBoardingConnectAdapter,onBoardingConnectedAdapter;
    ListView connectedlistView;
    ListView connectlistView;
    private CallbackManager callbackManager;
    String facebookaccesstoken,fb_id,upload_type,googletoken,googleid;
    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    public GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 100;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View view = inflater.inflate(R.layout.layout_connect, container, false);


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken("1023513032563-bjkb3ms47kbvumk7k4kaio1ok308lcas.apps.googleusercontent.com")
                .requestEmail()
                .build();


        init(view);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this)
                .addApi(Plus.API).build();
      //  GetPersonaData();

        return view;
    }

    private void init(View view) {

        connectedlistView = (ListView) view.findViewById(R.id.lvConnected);
        connectlistView = (ListView) view.findViewById(R.id.lvConnect);
        connectModelArrayList = new ArrayList<>();
        connectedModelArrayList = new ArrayList<>();

        connectModels = new ConnectModel("facebook" ,false);
        connectModelArrayList.add(connectModels);
        connectModels = new ConnectModel("google" ,false);
        connectModelArrayList.add(connectModels);
        connectModels = new ConnectModel("twitter" ,true);
        connectedModelArrayList.add(connectModels);

        onBoardingConnectAdapter = new OnBoardingConnectAdapter(getActivity(), connectModelArrayList,OnBoardingConnect.this,"connect");
        onBoardingConnectedAdapter  = new OnBoardingConnectAdapter(getActivity(), connectedModelArrayList,OnBoardingConnect.this,"connected");

        connectlistView.setAdapter(onBoardingConnectAdapter);
        connectedlistView.setAdapter(onBoardingConnectedAdapter);

        pDialog = new ProgressDialog(getActivity());

        callbackManager = CallbackManager.Factory.create();
    }

    private void GetPersonaData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new GetData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"account/profile");

            }
            else {

                new GetData().execute(WodrobConstant.BASE_URL+"account/profile");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class GetData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            //no parameters are required

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                //conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(getActivity()));
                conn.setRequestProperty("Authorization", "Bearer xV15QCT45Ikk6DMVDObiCTFAuzAzzyEiwGjnSG7b");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {


            if(Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                            JSONObject patron_object = jsonObject.getJSONObject("data");
                            if(patron_object.optBoolean("facebook")){

                                ConnectedMaps.put("facebook", patron_object.optBoolean("facebook"));

                            }else{

                                ConnectMaps.put("facebook", patron_object.optBoolean("facebook"));

                            }
                            if(patron_object.optBoolean("instagram")){

                                ConnectedMaps.put("instagram", patron_object.optBoolean("instagram"));

                            }else{

                                ConnectMaps.put("instagram", patron_object.optBoolean("instagram"));

                            }
                            if(patron_object.optBoolean("twitter")){

                                ConnectedMaps.put("twitter", patron_object.optBoolean("twitter"));

                            }else{

                                ConnectMaps.put("twitter", patron_object.optBoolean("twitter"));

                            }
                            if(patron_object.optBoolean("google")){

                                ConnectedMaps.put("google", patron_object.optBoolean("google"));

                            }else{

                                ConnectMaps.put("google", patron_object.optBoolean("google"));

                            }

                            onBoardingConnectAdapter.notifyDataSetChanged();
                            onBoardingConnectedAdapter.notifyDataSetChanged();
                          //notify here

                        }else{

                            // Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {

            callbackManager.onActivityResult(requestCode, resultCode, data);

        }


    }


    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            googletoken = acct.getIdToken();
            googleid = acct.getId();
            upload_type = "gooogle";
            UploadData();
        }
    }

    public void connect(String type){

        if(type.equalsIgnoreCase("facebook")){

            DoFaceboookLogin();

        }else if(type.equalsIgnoreCase("google")){

            DoGoogleLogin();

        }

    }

    private void DoGoogleLogin() {

       Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
       startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    //facebook login

    public void DoFaceboookLogin(){


        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        pDialog.setMessage("Fetching Details . . .");
                        pDialog.show();
                        facebookaccesstoken = loginResult.getAccessToken().getToken();

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Log.v("LoginActivity", response.toString());

                                        getFacebookData(object);
                                        pDialog.dismiss();
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location, age_range"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    public void getFacebookData(JSONObject object) {


        try {

            fb_id = object.getString("id");
            upload_type = "facebook";
            UploadData();
            pDialog.dismiss();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void UploadData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new UploadData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"account/profile/edit");

            }
            else {

                new UploadData().execute(WodrobConstant.BASE_URL+"account/profile/edit");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

        private class UploadData extends AsyncTask<String, Void, Void> {

            private String Content;
            private String Error = null;
            String data ="";

            @Override
            protected void onPreExecute() {

                pDialog.setMessage("Loading...");
                pDialog.show();


                try{

                        if(upload_type.equalsIgnoreCase("facebook")){

                            data +="&"+ URLEncoder.encode("facebook_id", "UTF-8")+"="+ fb_id+"&"
                                    + URLEncoder.encode("facebook_token", "UTF-8")+"="+facebookaccesstoken;


                        }else if(upload_type.equalsIgnoreCase("google")){

                            data +="&"+ URLEncoder.encode("google_id", "UTF-8")+"="+ googleid+"&"
                                    + URLEncoder.encode("google_token", "UTF-8")+"="+googletoken;

                        }


                    Log.d("data", data);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }

            @Override
            protected Void doInBackground(String... urls) {

                BufferedReader reader=null;

                try
                {

                    URL url = new URL(urls[0]);
                    URLConnection conn = url.openConnection();
                    conn.setDoOutput(true);
                    //conn.setRequestProperty("Authorization", "Bearer qG3Tq3oN6ixE0OhAT6ZsZzfRRMgDr0IUf4CtH2jy");
                    conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(getActivity()));
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write( data );
                    wr.flush();
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while((line = reader.readLine()) != null)
                    {
                        sb.append(line);
                    }

                    Content = sb.toString();
                }
                catch(Exception ex)
                {
                    Error = ex.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if(Error != null) {
                    pDialog.dismiss();

                    //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
                } else {

                    if(Content != null){
                        try {
                            JSONObject jsonObject = new JSONObject(Content);
                            String status = jsonObject.getString("status");
                            //   String message = jsonObject.getString("message");
                            if(status.equalsIgnoreCase("true")){

                                Toast.makeText(getActivity().getApplicationContext(), "Successfully connected", Toast.LENGTH_LONG).show();


                            }else{

                                //  Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                pDialog.dismiss();

            }
        }

    /*end*/

    }

