package com.wodrob.app.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.adapter.FollowPagerAdapter;
import com.wodrob.app.adapter.WearViewPagerAdapter;

/**
 * Created by rameesfazal on 20/2/16.
 */
public class OnBoardingFollow extends Fragment {

    ViewPager vp_follow;
    TabLayout tabLayout;
    FollowPagerAdapter followPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_follow, container, false);

        init(view);

        followPagerAdapter = new FollowPagerAdapter(getFragmentManager(),tabLayout.getTabCount());
        vp_follow.setOffscreenPageLimit(3);
        vp_follow.setAdapter(followPagerAdapter);

        vp_follow.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp_follow.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    private void init(View view) {

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("INVITE"));
        tabLayout.addTab(tabLayout.newTab().setText("FOLLOW"));
        tabLayout.addTab(tabLayout.newTab().setText("FRIENDS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        vp_follow = (ViewPager) view.findViewById(R.id.vp_follow);
        Wodrob.applyFont(getContext(), tabLayout, "fonts/montserratregular.ttf");
    }
}
