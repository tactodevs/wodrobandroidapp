package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.model.ItemBustSizeModel;
import com.wodrob.app.model.PersonaBodyTypeModel;
import com.wodrob.app.onboarding.OnboardingActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/2/16.
 */
public class OnboardingPersona extends Fragment implements View.OnClickListener{

    LinearLayout ll_apple, ll_hairglass, ll_coloumn, ll_pear, ll_lollipop, ll_gobiet, ll_trapezoid, ll_inverted_triangle,ll_triangle, ll_oval, ll_rectangle,ll_pale,ll_fair,ll_medium,ll_olive,ll_dark_black;
    ImageView im_apple, im_hairglass, im_coloumn, im_pear, im_lollipop, im_gobiet, im_trapezoid, im_inverted_triangle,im_triangle, im_oval, im_rectangle,im_pale,im_fair,im_medium,im_olive,im_dark_black;
    TextView tv_apple, tv_hairglass, tv_coloumn, tv_pear, tv_lollipop, tv_gobiet, tv_trapezoid, tv_inverted_triangle,tv_triangle, tv_oval, tv_rectangle,tv_pale,tv_fair,tv_medium,tv_olive,tv_dark_black;
    ArrayList<PersonaBodyTypeModel> ar_men_body_types,ar_women_body_types,ar_skin_color;
    String selected_body_type,selected_skin_color;
    EditText ed_height,ed_weight,ed_shirt_size,ed_waist,ed_chest,ed_shoe,ed_height_feet,ed_height_inches;
    Spinner sp_height,sp_weight,sp_waist,sp_bust,sp_chest;
    String chest_unit,body_type,skin_color,height,weight,shirt_size,waist_size,chest_size,shoe_size,height_unit,weight_unit,waist_unit,bust_size_id;

    ProgressDialog pDialog;
    DbHelper db;
    private ArrayAdapter<String> bust_spinner_adapter;
    ArrayList<ItemBustSizeModel> bust_size_array_list;
    int height_in_feet,height_in_inches;

    TextInputLayout height_layout,height_feet_layout,height_inches_layout;
    LinearLayout ll_body_type_men,ll_body_type_women;
    String gender;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_persona, container, false);
        setHasOptionsMenu(true);

        init(view);
        AddToList();

        GetPersonaData();


        sp_height.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0){

                    height_layout.setVisibility(View.VISIBLE);
                    height_feet_layout.setVisibility(View.GONE);
                    height_inches_layout.setVisibility(View.GONE);

                }else{

                    height_layout.setVisibility(View.GONE);
                    height_feet_layout.setVisibility(View.VISIBLE);
                    height_inches_layout.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.mi_next:

                GenerateOutPut();

                return true;
            default:
                break;
        }

        return false;
    }


    private void GetPersonaData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new GetData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"account/profile");

            }
            else {

                new GetData().execute(WodrobConstant.BASE_URL+"account/profile");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void AddToList() {

        /*women body types*/
        ar_women_body_types.add(new PersonaBodyTypeModel(ll_apple,"Apple"));
        ar_women_body_types.add(new PersonaBodyTypeModel(ll_hairglass,"Hourglass"));
        ar_women_body_types.add(new PersonaBodyTypeModel(ll_coloumn,"Column"));
        ar_women_body_types.add(new PersonaBodyTypeModel(ll_pear,"Pear"));
        ar_women_body_types.add(new PersonaBodyTypeModel(ll_lollipop,"Lollipop"));
        ar_women_body_types.add(new PersonaBodyTypeModel(ll_gobiet,"Goblet"));

        /*men body types*/
        ar_men_body_types.add(new PersonaBodyTypeModel(ll_trapezoid,"Trapezoid"));
        ar_men_body_types.add(new PersonaBodyTypeModel(ll_inverted_triangle,"Inverted Triangle"));
        ar_men_body_types.add(new PersonaBodyTypeModel(ll_triangle,"Rectangle"));
        ar_men_body_types.add(new PersonaBodyTypeModel(ll_oval,"Triangle"));
        ar_men_body_types.add(new PersonaBodyTypeModel(ll_rectangle,"Oval"));

        /*skin color*/

        ar_skin_color.add(new PersonaBodyTypeModel(ll_pale,"Pale"));
        ar_skin_color.add(new PersonaBodyTypeModel(ll_fair,"Fair"));
        ar_skin_color.add(new PersonaBodyTypeModel(ll_medium,"Medium"));
        ar_skin_color.add(new PersonaBodyTypeModel(ll_olive,"Olive"));
        ar_skin_color.add(new PersonaBodyTypeModel(ll_dark_black,"Dark"));


        /*on click listener for women body type items*/
        for(int i = 0; i<ar_women_body_types.size();i++){

            final int finalI = i;
            ar_women_body_types.get(i).getLayout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selected_body_type = ar_women_body_types.get(finalI).getValue().toString();
                    setBackGroundColor(ar_women_body_types,finalI);

                }
            });
        }

        /*end*/

        /*on click listener for women body type items*/
        for(int i = 0; i<ar_men_body_types.size();i++) {

            final int finalI = i;
            ar_men_body_types.get(i).getLayout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selected_body_type = ar_men_body_types.get(finalI).getValue().toString();
                    setBackGroundColor(ar_men_body_types,finalI);

                }
            });
        }

         /*on click listener for skin color*/

            /*on click listener for women body type items*/
            for(int i = 0; i<ar_skin_color.size();i++){

                final int finalI = i;
                ar_skin_color.get(i).getLayout().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        selected_skin_color = ar_skin_color.get(finalI).getValue().toString();
                        setBackGroundColor(ar_skin_color,finalI);

                    }
                });

            }

    }


    private void init(View view) {

        tv_apple = (TextView) view.findViewById(R.id.tv_apple);
        tv_hairglass = (TextView) view.findViewById(R.id.tv_hairglass);
        tv_coloumn = (TextView) view.findViewById(R.id.tv_coloumn);
        tv_pear = (TextView) view.findViewById(R.id.tv_pear);
        tv_lollipop = (TextView) view.findViewById(R.id.tv_lollipop);
        tv_gobiet = (TextView) view.findViewById(R.id.tv_gobiet);
        tv_trapezoid = (TextView) view.findViewById(R.id.tv_trapezoid);
        tv_inverted_triangle = (TextView) view.findViewById(R.id.tv_inverted_triangle);
        tv_triangle = (TextView) view.findViewById(R.id.tv_triangle);
        tv_oval = (TextView) view.findViewById(R.id.tv_oval);
        tv_rectangle = (TextView) view.findViewById(R.id.tv_rectangle);
        db = new DbHelper(getActivity());
        /*tv_pale = (TextView) view.findViewById(R.id.tv_pale);
        tv_fair = (TextView) view.findViewById(R.id.tv_fair);
        tv_medium = (TextView) view.findViewById(R.id.tv_medium);
        tv_olive = (TextView) view.findViewById(R.id.tv_olive);
        tv_dark_black = (TextView) view.findViewById(R.id.tv_dark_black);*/


        im_apple = (ImageView) view.findViewById(R.id.im_apple);
        im_hairglass = (ImageView) view.findViewById(R.id.im_hairglass);
        im_coloumn = (ImageView) view.findViewById(R.id.im_coloumn);
        im_pear = (ImageView) view.findViewById(R.id.im_pear);
        im_lollipop = (ImageView) view.findViewById(R.id.im_lollipop);
        im_gobiet = (ImageView) view.findViewById(R.id.im_gobiet);
        im_trapezoid = (ImageView) view.findViewById(R.id.im_trapezoid);
        im_inverted_triangle = (ImageView) view.findViewById(R.id.im_inverted_triangle);
        im_triangle = (ImageView) view.findViewById(R.id.im_triangle);
        im_oval = (ImageView) view.findViewById(R.id.im_oval);
        im_rectangle = (ImageView) view.findViewById(R.id.im_rectangle);
        im_pale = (ImageView) view.findViewById(R.id.im_pale);
        im_fair = (ImageView) view.findViewById(R.id.im_fair);
        im_medium = (ImageView) view.findViewById(R.id.im_medium);
        im_olive = (ImageView) view.findViewById(R.id.im_olive);
        im_dark_black = (ImageView) view.findViewById(R.id.im_dark_black);

        ll_apple = (LinearLayout) view.findViewById(R.id.ll_apple);
        ll_hairglass = (LinearLayout) view.findViewById(R.id.ll_hairglass);
        ll_coloumn = (LinearLayout) view.findViewById(R.id.ll_coloumn);
        ll_pear = (LinearLayout) view.findViewById(R.id.ll_pear);
        ll_lollipop = (LinearLayout) view.findViewById(R.id.ll_lollipop);
        ll_gobiet = (LinearLayout) view.findViewById(R.id.ll_gobiet);
        ll_trapezoid = (LinearLayout) view.findViewById(R.id.ll_trapezoid);
        ll_inverted_triangle = (LinearLayout) view.findViewById(R.id.ll_inverted_triangle);
        ll_triangle = (LinearLayout) view.findViewById(R.id.ll_triangle);
        ll_oval = (LinearLayout) view.findViewById(R.id.ll_oval);
        ll_rectangle = (LinearLayout) view.findViewById(R.id.ll_rectangle);
        ll_pale = (LinearLayout) view.findViewById(R.id.ll_pale);
        ll_fair = (LinearLayout) view.findViewById(R.id.ll_fair);
        ll_medium = (LinearLayout) view.findViewById(R.id.ll_medium);
        ll_olive = (LinearLayout) view.findViewById(R.id.ll_olive);
        ll_dark_black = (LinearLayout) view.findViewById(R.id.ll_dark_black);

        ll_body_type_men = (LinearLayout) view.findViewById(R.id.ll_body_type_men);
        ll_body_type_women = (LinearLayout) view.findViewById(R.id.ll_body_type_women);

        ed_height = (EditText) view.findViewById(R.id.ed_height);
        ed_weight = (EditText) view.findViewById(R.id.ed_weight);
        ed_shirt_size = (EditText) view.findViewById(R.id.ed_shirt);
        ed_waist = (EditText) view.findViewById(R.id.ed_waist);
        ed_chest = (EditText) view.findViewById(R.id.ed_chest);
        ed_shoe = (EditText) view.findViewById(R.id.ed_shoe);

        ed_height_feet = (EditText) view.findViewById(R.id.ed_height_feet);
        ed_height_inches = (EditText) view.findViewById(R.id.ed_height_inches);

        sp_height = (Spinner) view.findViewById(R.id.sp_height);
        sp_weight = (Spinner) view.findViewById(R.id.sp_weight);
        sp_waist = (Spinner) view.findViewById(R.id.sp_waist);
        sp_bust = (Spinner) view.findViewById(R.id.sp_bust);
        sp_chest  = (Spinner) view.findViewById(R.id.sp_chest);

        height_layout = (TextInputLayout) view.findViewById(R.id.height_layout);
        height_feet_layout = (TextInputLayout) view.findViewById(R.id.height_feet_layout);
        height_inches_layout = (TextInputLayout) view.findViewById(R.id.height_inches_layout);


        ArrayList<String> bust_display = new ArrayList<String>();
        bust_size_array_list = db.getAllBustSizes();

        for(int i = 0; i < bust_size_array_list.size(); i++){

            bust_display.add(bust_size_array_list.get(i).getDisplay().toString());

        }

        bust_spinner_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, bust_display);
        bust_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_bust.setAdapter(bust_spinner_adapter);


        ar_men_body_types = new ArrayList<>();
        ar_women_body_types = new ArrayList<>();
        ar_skin_color = new ArrayList<>();

        pDialog = new ProgressDialog(getActivity());

    }

    @Override
    public void onClick(View v) {

    }

    public void GenerateOutPut(){

        body_type = selected_body_type;
        skin_color = selected_skin_color;
        height = ed_height.getText().toString();
        weight = ed_weight.getText().toString();

        shirt_size = ed_shirt_size.getText().toString();
        waist_size = ed_waist.getText().toString();
        chest_size = ed_chest.getText().toString();
        shoe_size = ed_shoe.getText().toString();
        height_unit = sp_height.getSelectedItem().toString();
        weight_unit = sp_weight.getSelectedItem().toString();
        waist_unit = sp_waist.getSelectedItem().toString();
        chest_unit = sp_chest.getSelectedItem().toString();
        bust_size_id = bust_size_array_list.get(sp_bust.getSelectedItemPosition()).getBust_size_id();

        UploadPersonaData();

    }


    private void UploadPersonaData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new UploadData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"account/profile/edit");

            }
            else {

                new UploadData().execute(WodrobConstant.BASE_URL+"account/profile/edit");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
    /*Method to change background color of selected item*/
    public void setBackGroundColor(ArrayList<PersonaBodyTypeModel> list,int position){

        for(int i = 0;i<list.size(); i++){

            if(i == position){

                list.get(i).getLayout().setBackgroundColor(getResources().getColor(R.color.dialog_title_background));

            }else{

                list.get(i).getLayout().setBackgroundColor(getResources().getColor(R.color.White));

            }
        }

    }


    /*Upload persona data*/


    private class UploadData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


                try{

                    if(height_unit.equalsIgnoreCase("cm")){

                        if(gender.equalsIgnoreCase("male")){

                            data +="&"+ URLEncoder.encode("body_type_id", "UTF-8")+"="+ db.GetBody_type_id(selected_body_type)+"&"
                                    + URLEncoder.encode("skin_tone_id", "UTF-8")+"="+db.GetSkintoneid(selected_skin_color)+"&"
                                    + URLEncoder.encode("height", "UTF-8")+"="+Integer.parseInt(height)+"&"
                                    + URLEncoder.encode("height_unit_abbrv", "UTF-8")+"="+height_unit+"&"
                                    + URLEncoder.encode("weight", "UTF-8")+"="+Integer.parseInt(weight)+"&"
                                    + URLEncoder.encode("weight_unit_abbrv", "UTF-8")+"="+weight_unit+"&"
                                    + URLEncoder.encode("waist", "UTF-8")+"="+Integer.parseInt(waist_size)+"&"
                                    + URLEncoder.encode("waist_unit_abbrv", "UTF-8")+"="+waist_unit+"&"
                                    + URLEncoder.encode("chest", "UTF-8")+"="+Integer.parseInt(chest_size)+"&"
                                    + URLEncoder.encode("chest_unit_abbrv", "UTF-8")+"="+chest_unit;


                        }else{

                            data +="&"+ URLEncoder.encode("body_type_id", "UTF-8")+"="+ db.GetBody_type_id(selected_body_type)+"&"
                                    + URLEncoder.encode("skin_tone_id", "UTF-8")+"="+db.GetSkintoneid(selected_skin_color)+"&"
                                    + URLEncoder.encode("height", "UTF-8")+"="+Integer.parseInt(height)+"&"
                                    + URLEncoder.encode("height_unit_abbrv", "UTF-8")+"="+height_unit+"&"
                                    + URLEncoder.encode("weight", "UTF-8")+"="+Integer.parseInt(weight)+"&"
                                    + URLEncoder.encode("weight_unit_abbrv", "UTF-8")+"="+weight_unit+"&"
                                    + URLEncoder.encode("waist", "UTF-8")+"="+Integer.parseInt(waist_size)+"&"
                                    + URLEncoder.encode("waist_unit_abbrv", "UTF-8")+"="+waist_unit+"&"
                                    + URLEncoder.encode("bust_size_id", "UTF-8")+"="+bust_size_id;


                        }



                    }else if(height_unit.equalsIgnoreCase("ft")){

                       /* String feet = height.split("\\.")[0];
                        String inches = height.split("\\.")[1];*/
                        height_in_feet = Integer.parseInt(ed_height_feet.getText().toString());
                        height_in_inches = Integer.parseInt(ed_height_inches.getText().toString());

                        if(gender.equalsIgnoreCase("male")){

                            data +="&"+ URLEncoder.encode("body_type_id", "UTF-8")+"="+ db.GetBody_type_id(selected_body_type)+"&"
                                    + URLEncoder.encode("skin_tone_id", "UTF-8")+"="+db.GetSkintoneid(selected_skin_color)+"&"
                                    + URLEncoder.encode("height_feet", "UTF-8")+"="+height_in_feet+"&"
                                    + URLEncoder.encode("height_inch", "UTF-8")+"="+height_in_inches+"&"
                                    + URLEncoder.encode("height_unit_abbrv", "UTF-8")+"="+height_unit+"&"
                                    + URLEncoder.encode("weight", "UTF-8")+"="+Integer.parseInt(weight)+"&"
                                    + URLEncoder.encode("weight_unit_abbrv", "UTF-8")+"="+weight_unit+"&"
                                    + URLEncoder.encode("waist", "UTF-8")+"="+Integer.parseInt(waist_size)+"&"
                                    + URLEncoder.encode("waist_unit_abbrv", "UTF-8")+"="+waist_unit+"&"
                                    + URLEncoder.encode("chest", "UTF-8")+"="+Integer.parseInt(chest_size)+"&"
                                    + URLEncoder.encode("chest_unit_abbrv", "UTF-8")+"="+chest_unit;

                        }else{

                            data +="&"+ URLEncoder.encode("body_type_id", "UTF-8")+"="+ db.GetBody_type_id(selected_body_type)+"&"
                                    + URLEncoder.encode("skin_tone_id", "UTF-8")+"="+db.GetSkintoneid(selected_skin_color)+"&"
                                    + URLEncoder.encode("height_feet", "UTF-8")+"="+height_in_feet+"&"
                                    + URLEncoder.encode("height_inch", "UTF-8")+"="+height_in_inches+"&"
                                    + URLEncoder.encode("height_unit_abbrv", "UTF-8")+"="+height_unit+"&"
                                    + URLEncoder.encode("weight", "UTF-8")+"="+Integer.parseInt(weight)+"&"
                                    + URLEncoder.encode("weight_unit_abbrv", "UTF-8")+"="+weight_unit+"&"
                                    + URLEncoder.encode("waist", "UTF-8")+"="+Integer.parseInt(waist_size)+"&"
                                    + URLEncoder.encode("waist_unit_abbrv", "UTF-8")+"="+waist_unit+"&"
                                    + URLEncoder.encode("bust_size_id", "UTF-8")+"="+bust_size_id;

                        }


                    }



                    Log.d("data", data);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                //conn.setRequestProperty("Authorization", "Bearer qG3Tq3oN6ixE0OhAT6ZsZzfRRMgDr0IUf4CtH2jy");
                conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(getActivity()));
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                     //   String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                         //   Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            //move to next screen

                            ((OnboardingActivity)getActivity()).update_Page("privacy");


                        }else{

                          //  Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }

    /*end*/

    /*get persona data*/


    private class GetData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";
        String body_type;
        String skin_tone;
        String bust;

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            //no parameters are required

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization",Wodrob.GetAccessToken(getActivity()));
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                       // String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                            JSONObject patron_object = jsonObject.getJSONObject("data");
                            gender = patron_object.getString("gender");
                            if(gender.equalsIgnoreCase("male")){

                                ll_body_type_men.setVisibility(View.VISIBLE);
                                ll_body_type_women.setVisibility(View.GONE);
                                sp_bust.setVisibility(View.GONE);
                                ed_chest.setVisibility(View.VISIBLE);
                                sp_chest.setVisibility(View.VISIBLE);

                            }else{

                                ll_body_type_women.setVisibility(View.VISIBLE);
                                ll_body_type_men.setVisibility(View.GONE);
                                ed_chest.setVisibility(View.GONE);
                                sp_bust.setVisibility(View.VISIBLE);
                                sp_chest.setVisibility(View.GONE);

                            }
                           // JSONObject patron_object = data_object.getJSONObject("patron");
                            JSONObject measurement_object = patron_object.getJSONObject("measurements");

                            if (!measurement_object.isNull("weight")) {
                                JSONObject weight_object = measurement_object.getJSONObject("weight");
                                 String weight = weight_object.getString("value");

                                 weight_unit = weight_object.getString("unit").split("_")[1];
                                 ed_weight.setText(weight);

                                if(weight_unit.equalsIgnoreCase("kg")){

                                    sp_weight.setSelection(0);

                                }else {

                                    sp_weight.setSelection(1);
                                }

                            }

                            if (!measurement_object.isNull("height")) {
                                JSONObject heignt_object = measurement_object.getJSONObject("height");
                                 String height = heignt_object.getString("value");
                                 String height_unit = heignt_object.getString("unit").split("_")[1];

                                if(height_unit.equalsIgnoreCase("cm")){

                                    sp_height.setSelection(0);
                                    ed_height.setText(height);

                                }else {

                                    int height_ft = Integer.parseInt(height)/12;
                                    int height_inch = Integer.parseInt(height)%12;

                                    ed_height_feet.setText(""+height_ft);
                                    ed_height_inches.setText(""+height_inch);
                                    sp_height.setSelection(1);
                                }

                            }

                            if (!measurement_object.isNull("waist")) {
                                JSONObject waist_object = measurement_object.getJSONObject("waist");
                                String waist = waist_object.getString("value");
                                String waist_unit = waist_object.getString("unit").split("_")[1];
                                ed_waist.setText(waist);

                                if(waist_unit.equalsIgnoreCase("cm")){

                                    sp_waist.setSelection(0);

                                }else {

                                    sp_waist.setSelection(1);
                                }

                            }


                            if(!patron_object.isNull("body_type")){

                                final String  body_type = patron_object.getString("body_type").split("_")[1];

                                //setting body type for men

                             /*   for(int i = 0; i<ar_men_body_types.size();i++) {

                                    final int finalI = i;
                                    ar_men_body_types.get(i).getLayout().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            if(body_type.equalsIgnoreCase(ar_men_body_types.get(finalI).getValue())){

                                                selected_body_type = ar_men_body_types.get(finalI).getValue().toString();
                                                setBackGroundColor(ar_men_body_types, finalI);

                                            }
                                        }
                                    });

                                }*/

                            /*end*/



                            /*setting body type fpr women*/

                                for(int i = 0; i<ar_women_body_types.size();i++) {

                                    if(body_type.equalsIgnoreCase(ar_women_body_types.get(i).getValue())){

                                        selected_body_type = ar_women_body_types.get(i).getValue().toString();
                                        setBackGroundColor(ar_women_body_types, i);

                                    }
                                }

                            /*end*/


                            }

                            if(!patron_object.isNull("skin_tone")){

                              final String skin_tone = patron_object.getString("skin_tone").split("_")[1];

                                 /*setting skin tone*/

                                for(int i = 0; i<ar_skin_color.size();i++) {

                                    if(skin_tone.equalsIgnoreCase(ar_skin_color.get(i).getValue())){

                                        selected_skin_color = ar_skin_color.get(i).getValue().toString();
                                        setBackGroundColor(ar_skin_color, i);

                                    }

                                }

                            /*end*/


                            }

                            if(!measurement_object.isNull("bust")){

                              // bust = db.GetSizeBasedOnId(measurement_object.getString("bust"));

                                String bust = measurement_object.getString("bust").split("_")[0];

                                for(int i = 0;i< db.getAllBustSizes().size();i++){

                                    if(db.getAllBustSizes().get(i).getBust_size_id().equalsIgnoreCase(bust)){

                                        sp_bust.setSelection(i);

                                    }
                                }

                            }


                        }else{

                           // Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }


    /*end*/


}
