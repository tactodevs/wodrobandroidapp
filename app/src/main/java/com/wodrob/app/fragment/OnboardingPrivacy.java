package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.model.PersonaBodyTypeModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rameesfazal on 20/2/16.
 */
public class OnboardingPrivacy extends Fragment {

    LinearLayout ll_public,ll_protected,ll_personal;
    ArrayList<PersonaBodyTypeModel> ar_privacy;
    String selected_privacy;
    ProgressDialog pDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_privacy, container, false);

        setHasOptionsMenu(true);

        init(view);

        GetPrivacyData();

        return view;

    }


    private void GetPrivacyData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new GetData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"account/profile");

            }
            else {

                new GetData().execute(WodrobConstant.BASE_URL+"account/profile");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.mi_next:

                UploadPrivacyData();

                return true;
            default:
                break;
        }

        return false;
    }

    /*use in menu item next onclick*/

    private void UploadPrivacyData() {

        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {
                new UploadData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"account/profile/edit");
            }
            else {
                new UploadData().execute(WodrobConstant.BASE_URL+"account/profile/edit");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void init(View view) {

        ll_public = (LinearLayout) view.findViewById(R.id.ll_public);
        ll_protected = (LinearLayout) view.findViewById(R.id.ll_protected);
        ll_personal = (LinearLayout) view.findViewById(R.id.ll_personal);

        ar_privacy = new ArrayList<>();
        ar_privacy.add(new PersonaBodyTypeModel(ll_public,"public"));
        ar_privacy.add(new PersonaBodyTypeModel(ll_protected,"protected"));
        ar_privacy.add(new PersonaBodyTypeModel(ll_personal,"personal"));

        pDialog = new ProgressDialog(getActivity());

        /*linear layout onclick Listener*/

         /*on click listener for women body type items*/
        for(int i = 0; i<ar_privacy.size();i++){

            final int finalI = i;
            ar_privacy.get(i).getLayout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selected_privacy = ar_privacy.get(finalI).getValue().toString();
                    setBackGroundColor(ar_privacy,finalI);

                }
            });
        }

        /*end*/

    }

    /*Method to change background color of selected item*/
    public void setBackGroundColor(ArrayList<PersonaBodyTypeModel> list,int position){

        for(int i = 0;i<list.size(); i++){

            if(i == position){

                list.get(i).getLayout().setBackgroundColor(getResources().getColor(R.color.plum));

            }else{

                list.get(i).getLayout().setBackgroundColor(getResources().getColor(R.color.White));

            }
        }

    }

    /*upload privacy data*/

    private class UploadData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

                try{

                    data +="&"+ URLEncoder.encode("privacy_level", "UTF-8")+"="+ selected_privacy;

                    Log.d("data", data);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization", "Bearer "+Wodrob.getPreference(getActivity().getApplicationContext(),"access_token"));
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){



                        }else{

                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }

    /*get privacy data*/


     /*get persona data*/


    private class GetData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();

            //no parameters are required

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization",Wodrob.GetAccessToken(getActivity()));
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {
                pDialog.dismiss();

                //  Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if(Content != null){
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                      //  String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){


                            JSONObject patron_object = jsonObject.getJSONObject("data");
                           // JSONObject patron_object = data_object.getJSONObject("patron");

                            final String privacy_level = patron_object.getString("privacy_level");

                            for(int i = 0; i<ar_privacy.size();i++){

                                if(privacy_level.equalsIgnoreCase(ar_privacy.get(i).getValue())){

                                    selected_privacy = ar_privacy.get(i).getValue().toString();
                                    setBackGroundColor(ar_privacy,i);

                                }

                            }


                        }else{

                       //     Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

        }
    }



    /*ends*/
}
