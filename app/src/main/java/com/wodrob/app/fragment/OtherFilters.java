package com.wodrob.app.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wodrob.app.DbHelper;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.model.ItemBrandModel;
import com.wodrob.app.model.ItemColorModel;
import com.wodrob.app.model.ItemSizeModel;
import com.wodrob.app.model.ItemStyleModel;
import com.wodrob.app.model.ItemTagModel;
import com.wodrob.app.views.PredicateLayout;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 5/1/16.
 */
public class OtherFilters extends Fragment implements View.OnClickListener{

    ArrayList<ItemBrandModel> brand_array_list;
    ArrayList<ItemStyleModel> style_array_list;
    ArrayList<ItemSizeModel> size_array_list;
    ArrayList<ItemColorModel> color_array_list;
    ArrayList<ItemTagModel> tag_array_list;

    PredicateLayout filter_layout;
    TextView[] filter_item;
    LinearLayout.LayoutParams lp;
    LinearLayout.LayoutParams selected_item_lp;
    DbHelper db;
    LinearLayout ll_style,ll_brand,ll_color,ll_size,selected_item_display_layout,ll_tags,ll_clear;
    String type="style";

    ImageView im_style_icon,im_brand_icon,im_size_icon,im_color_icon,im_tags_icon;
    TextView tv_brand_badges,tv_style_badges,tv_color_badges,tv_size_badges,tv_tag_badges;
    TextView tv_brand_title,tv_style_title,tv_color_title,tv_size_title,tv_tag_title;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.other_filter_layout, null);

        init(view);

        DisplayItems();

        return view;
    }

    private void init(View view) {

        ll_style = (LinearLayout) view.findViewById(R.id.ll_style);
        ll_brand = (LinearLayout) view.findViewById(R.id.ll_brand);
        ll_color = (LinearLayout) view.findViewById(R.id.ll_color);
        ll_size = (LinearLayout) view.findViewById(R.id.ll_size);
        ll_tags = (LinearLayout) view.findViewById(R.id.ll_tags);
        ll_clear = (LinearLayout) view.findViewById(R.id.ll_clear);

        tv_brand_badges = (TextView) view.findViewById(R.id.tv_brand_badges);
        tv_style_badges = (TextView) view.findViewById(R.id.tv_style_badges);
        tv_color_badges = (TextView) view.findViewById(R.id.tv_color_badges);
        tv_size_badges = (TextView) view.findViewById(R.id.tv_size_badges);
        tv_tag_badges = (TextView) view.findViewById(R.id.tv_tags_badges);

        tv_brand_badges.setVisibility(View.GONE);
        tv_style_badges.setVisibility(View.GONE);
        tv_color_badges.setVisibility(View.GONE);
        tv_size_badges.setVisibility(View.GONE);
        tv_tag_badges.setVisibility(View.GONE);

        ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
        ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.plum));
        ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
        ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
        ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));


        tv_brand_title = (TextView) view.findViewById(R.id.tv_brand_title);
        tv_style_title = (TextView) view.findViewById(R.id.tv_style_title);
        tv_color_title = (TextView) view.findViewById(R.id.tv_color_title);
        tv_size_title = (TextView) view.findViewById(R.id.tv_size_title);
        tv_tag_title = (TextView) view.findViewById(R.id.tv_tags_title);

        im_brand_icon = (ImageView) view.findViewById(R.id.im_brand_icon);
        im_style_icon = (ImageView) view.findViewById(R.id.im_style_icon);
        im_color_icon = (ImageView) view.findViewById(R.id.im_color_icon);
        im_size_icon = (ImageView) view.findViewById(R.id.im_size_icon);
        im_tags_icon = (ImageView) view.findViewById(R.id.im_additionaltags_icon);

        /*initial state*/
        im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style_selected));
        //tv_style_badges.setTextColor(getResources().getColor(R.color.plum));
        tv_style_title.setTextColor(getResources().getColor(R.color.plum));

        selected_item_display_layout = (LinearLayout) view.findViewById(R.id.selected_layout);

        ll_style.setOnClickListener(this);
        ll_brand.setOnClickListener(this);
        ll_color.setOnClickListener(this);
        ll_size.setOnClickListener(this);
        ll_tags.setOnClickListener(this);
        ll_clear.setOnClickListener(this);

        filter_layout = (PredicateLayout) view.findViewById(R.id.filter_items_layout);
        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(24, 12, 24, 12);
        selected_item_lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        selected_item_lp.setMargins(5, 0, 0, 0);

        db = new DbHelper(getActivity());
        brand_array_list = db.getAllbrnads();
        style_array_list = db.getAllStyleItems();
        color_array_list = db.getAllcolors();
        size_array_list = db.GetSizeBasedOnCategory(FilterActivity.selected_category_id);
        tag_array_list = db.getAlltags();

    }

    @SuppressLint("NewApi")
    public void DisplayItems() {

        filter_layout.removeAllViews();

        if(type.equalsIgnoreCase("style")){

            //style_array_list = db.getAllStyleItems();
            filter_item = new TextView[style_array_list.size()];
            for(int i = 0; i<style_array_list.size(); i++){

                filter_item[i] = new TextView(getActivity());
                filter_item[i].setText(style_array_list.get(i).getStyle_name().toString());
                filter_item[i].setId(i);
                filter_item[i].setTextSize(15);
                filter_item[i].setLayoutParams(lp);
                filter_item[i].setPadding(15, 15, 15, 15);
                filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                filter_item[i].setBackground(getResources().getDrawable(R.drawable.style_layout_border));

            filter_item[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FilterActivity.selected_style_list.add(((TextView) v).getText().toString());
                    //filter_layout.removeView(v);
                    style_array_list.remove(v.getId());
                    DisplayItems();
                    DisplaySelectedItem();

                    //  Toast.makeText(getActivity().getApplicationContext(),((TextView) v).getText().toString(),Toast.LENGTH_LONG).show();
                }
            });

                filter_layout.addView(filter_item[i],new PredicateLayout.LayoutParams(10, 10));
            }



        }else if(type.equalsIgnoreCase("brand")){

           // brand_array_list = db.getAllbrnads();

            filter_item = new TextView[brand_array_list.size()];
            for(int i = 0; i<brand_array_list.size(); i++){

                filter_item[i] = new TextView(getActivity());
                filter_item[i].setText(brand_array_list.get(i).getBrand_name().toString());
                filter_item[i].setId(i);
                filter_item[i].setTextSize(15);
                filter_item[i].setLayoutParams(lp);
                filter_item[i].setPadding(15, 15, 15, 15);
                filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                filter_item[i].setBackground(getResources().getDrawable(R.drawable.style_layout_border));

            filter_item[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DisplaySelectedItem(size_value[i].getText().toString());
                    FilterActivity.selected_brand_list.add(((TextView) v).getText().toString());
                    brand_array_list.remove(v.getId());
                    DisplayItems();
                    DisplaySelectedItem();
                }
            });

                filter_layout.addView(filter_item[i],new PredicateLayout.LayoutParams(10, 10));
            }

        }else if(type.equalsIgnoreCase("color")){

            //filter_layout.removeAllViews();
            //color_array_list = db.getAllcolors();

            filter_item = new TextView[color_array_list.size()];
            for(int i = 0; i<color_array_list.size(); i++){

                filter_item[i] = new TextView(getActivity());
                filter_item[i].setText(color_array_list.get(i).getName().toString());
                filter_item[i].setId(i);
                filter_item[i].setTextSize(15);
                filter_item[i].setLayoutParams(lp);
                filter_item[i].setPadding(15, 15, 15, 15);
                filter_item[i].setGravity(Gravity.CENTER);
                filter_item[i].setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                try{

                    filter_item[i].setBackgroundColor(Color.parseColor("#" + color_array_list.get(i).getHex_code().toString()));

                }catch (Exception error){
                    String temp = color_array_list.get(i).getHex_code().toString();
                    Log.e("Hexcode: ",color_array_list.get(i).getHex_code().toString());


                }
               // filter_item[i].setBackgroundColor(Color.parseColor("#" + color_array_list.get(i).getHex_code().toString()));

                filter_item[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // DisplaySelectedItem(size_value[i].getText().toString());
                        FilterActivity.selected_color_list.add(((TextView) v).getText().toString());
                        color_array_list.remove(v.getId());
                        DisplayItems();
                        DisplaySelectedItem();
                    }
                });

                filter_layout.addView(filter_item[i], new PredicateLayout.LayoutParams(10, 10));
            }


        }else if(type.equalsIgnoreCase("size")){

            //size_array_list = db.GetSizeBasedOnCategory(FilterActivity.selected_category_id);

            filter_item = new TextView[size_array_list.size()];
            for(int i = 0; i<size_array_list.size(); i++){

                filter_item[i] = new TextView(getActivity());
                filter_item[i].setText(size_array_list.get(i).getValue().toString());
                filter_item[i].setId(i);
                filter_item[i].setTextSize(15);
                filter_item[i].setLayoutParams(lp);
                filter_item[i].setPadding(15, 15, 15, 15);
                filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                filter_item[i].setBackground(getResources().getDrawable(R.drawable.style_layout_border));

            filter_item[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DisplaySelectedItem(size_value[i].getText().toString());
                   // FilterActivity.selected_size_list.clear();
                    FilterActivity.selected_size_list.add(((TextView) v).getText().toString());
                    size_array_list.remove(v.getId());
                    DisplayItems();
                    DisplaySelectedItem();
                }
            });

                filter_layout.addView(filter_item[i],new PredicateLayout.LayoutParams(10, 10));
            }



        }else if(type.equalsIgnoreCase("tags")){

           // tag_array_list = db.getAlltags();

            filter_item = new TextView[tag_array_list.size()];
            for(int i = 0; i<tag_array_list.size(); i++){

                filter_item[i] = new TextView(getActivity());
                filter_item[i].setText(tag_array_list.get(i).getTag_name().toString());
                filter_item[i].setId(i);
                filter_item[i].setTextSize(15);
                filter_item[i].setLayoutParams(lp);
                filter_item[i].setPadding(15, 15, 15, 15);
                filter_item[i].setGravity(Gravity.CENTER_HORIZONTAL);
                filter_item[i].setBackground(getResources().getDrawable(R.drawable.style_layout_border));

                filter_item[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // DisplaySelectedItem(size_value[i].getText().toString());
                        //FilterActivity.selected_tags_list.clear();
                        FilterActivity.selected_tags_list.add(((TextView) v).getText().toString());
                        tag_array_list.remove(v.getId());
                        DisplayItems();
                        DisplaySelectedItem();
                    }
                });

                filter_layout.addView(filter_item[i],new PredicateLayout.LayoutParams(10, 10));
            }


        }


    }

    @SuppressLint("NewApi")
    private void DisplaySelectedItem(){

        selected_item_display_layout.removeAllViews();

        if(FilterActivity.selected_brand_list.size()!=0) {

            tv_brand_badges.setText("" + FilterActivity.selected_brand_list.size());
            tv_brand_badges.setVisibility(View.VISIBLE);

        }else {

            tv_brand_badges.setVisibility(View.GONE);

        }

        if(FilterActivity.selected_size_list.size()!=0) {

            tv_size_badges.setText("" + FilterActivity.selected_size_list.size());
            tv_size_badges.setVisibility(View.VISIBLE);

        }else {

            tv_size_badges.setVisibility(View.GONE);

        }

        if(FilterActivity.selected_color_list.size()!=0) {

            tv_color_badges.setText("" + FilterActivity.selected_color_list.size());
            tv_color_badges.setVisibility(View.VISIBLE);

        }else {

            tv_color_badges.setVisibility(View.GONE);

        }

        if(FilterActivity.selected_style_list.size()!=0) {

            tv_style_badges.setText("" + FilterActivity.selected_style_list.size());
            tv_style_badges.setVisibility(View.VISIBLE);

        }else {

            tv_style_badges.setVisibility(View.GONE);

        }

        if(FilterActivity.selected_tags_list.size()!=0) {

            tv_tag_badges.setText("" + FilterActivity.selected_tags_list.size());
            tv_tag_badges.setVisibility(View.VISIBLE);

        }else {

            tv_tag_badges.setVisibility(View.GONE);

        }


        if(type.equals("style")){

            if(FilterActivity.selected_style_list.size() != 0) {
                for (int j = 0; j < FilterActivity.selected_style_list.size(); j++) {

                    LinearLayout selected_item_textview_layout = new LinearLayout(getActivity());
                    selected_item_textview_layout.setLayoutParams(selected_item_lp);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(getActivity());
                    tv_seleected_text.setText(FilterActivity.selected_style_list.get(j).toString());
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    imageView.setTag(FilterActivity.selected_style_list.get(j).toString());
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity().getApplicationContext(),"selected"+((ImageView)v).getId(),Toast.LENGTH_LONG).show();
                            FilterActivity.selected_style_list.remove(((ImageView) v).getId());
                            ItemStyleModel styleModel = new ItemStyleModel("", ((ImageView) v).getTag().toString(), "", "", "");
                            style_array_list.add(styleModel);
                            DisplayItems();
                            DisplaySelectedItem();
                        }
                    });
                }
            }

        }else if(type.equals("brand")){

            if(FilterActivity.selected_brand_list.size() != 0) {
                for (int j = 0; j < FilterActivity.selected_brand_list.size(); j++) {

                    LinearLayout selected_item_textview_layout = new LinearLayout(getActivity());
                    selected_item_textview_layout.setLayoutParams(selected_item_lp);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(getActivity());
                    tv_seleected_text.setText(FilterActivity.selected_brand_list.get(j).toString());
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    imageView.setTag(FilterActivity.selected_brand_list.get(j).toString());
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity().getApplicationContext(),"selected"+((ImageView)v).getId(),Toast.LENGTH_LONG).show();
                            FilterActivity.selected_brand_list.remove(((ImageView) v).getId());
                            ItemBrandModel brandModel = new ItemBrandModel("", ((ImageView) v).getTag().toString(), "", "");
                            brand_array_list.add(brandModel);
                            DisplayItems();
                            DisplaySelectedItem();
                        }
                    });
                }
            }

        }else if(type.equals("color")){

            if(FilterActivity.selected_color_list.size() != 0) {
                for (int j = 0; j < FilterActivity.selected_color_list.size(); j++) {

                    LinearLayout selected_item_textview_layout = new LinearLayout(getActivity());
                    selected_item_textview_layout.setLayoutParams(selected_item_lp);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(getActivity());
                    tv_seleected_text.setText(FilterActivity.selected_color_list.get(j).toString());
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    imageView.setTag(FilterActivity.selected_color_list.get(j).toString());
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity().getApplicationContext(),"selected"+((ImageView)v).getId(),Toast.LENGTH_LONG).show();
                            FilterActivity.selected_color_list.remove(((ImageView) v).getId());
                            ItemColorModel colorModel = new ItemColorModel("", ((ImageView) v).getTag().toString(), "", "");
                            color_array_list.add(colorModel);
                            DisplayItems();
                            DisplaySelectedItem();
                        }
                    });
                }
            }


        }else if(type.equals("size")){

            if(FilterActivity.selected_size_list.size() != 0) {
                for (int j = 0; j < FilterActivity.selected_size_list.size(); j++) {

                    LinearLayout selected_item_textview_layout = new LinearLayout(getActivity());
                    selected_item_textview_layout.setLayoutParams(selected_item_lp);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(getActivity());
                    tv_seleected_text.setText(FilterActivity.selected_size_list.get(j).toString());
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    imageView.setTag(FilterActivity.selected_size_list.get(j).toString());
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity().getApplicationContext(),"selected"+((ImageView)v).getId(),Toast.LENGTH_LONG).show();
                            FilterActivity.selected_size_list.remove(((ImageView) v).getId());
                            ItemSizeModel sizeModel = new ItemSizeModel("", ((ImageView) v).getTag().toString(), "", "");
                            size_array_list.add(sizeModel);
                            DisplayItems();
                            DisplaySelectedItem();
                        }
                    });
                }
            }


        }else if(type.equals("tags")){

            if(FilterActivity.selected_tags_list.size() != 0) {
                for (int j = 0; j < FilterActivity.selected_tags_list.size(); j++) {

                    LinearLayout selected_item_textview_layout = new LinearLayout(getActivity());
                    selected_item_textview_layout.setLayoutParams(selected_item_lp);
                    selected_item_textview_layout.setOrientation(LinearLayout.HORIZONTAL);
                    selected_item_textview_layout.setBackground(getResources().getDrawable(R.drawable.style_layout_border));
                    TextView tv_seleected_text = new TextView(getActivity());
                    tv_seleected_text.setText(FilterActivity.selected_tags_list.get(j).toString());
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.filter_tag_cancel));
                    imageView.setId(j);
                    imageView.setTag(FilterActivity.selected_tags_list.get(j).toString());
                    selected_item_textview_layout.addView(tv_seleected_text);
                    selected_item_textview_layout.addView(imageView);
                    selected_item_display_layout.addView(selected_item_textview_layout);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity().getApplicationContext(),"selected"+((ImageView)v).getId(),Toast.LENGTH_LONG).show();
                            FilterActivity.selected_tags_list.remove(((ImageView) v).getId());
                            ItemTagModel tagModel = new ItemTagModel("", ((ImageView) v).getTag().toString(),"");
                            tag_array_list.add(tagModel);
                            DisplayItems();
                            DisplaySelectedItem();
                        }
                    });
                }
            }


        }
    }
    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.ll_style) {

            type = "style";

            im_brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_brands));
            im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style_selected));
            im_color_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_colour));
            im_size_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_size));
            im_tags_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_additional_tags));



            ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.plum));
            ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));


            tv_brand_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_style_title.setTextColor(getResources().getColor(R.color.plum));
            tv_color_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_size_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_tag_title.setTextColor(getResources().getColor(R.color.dark_smoke));

            DisplayItems();
            DisplaySelectedItem();


        } else if(v.getId() == R.id.ll_brand) {

            type = "brand";

            im_brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_brands_selected));
            im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style));
            im_color_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_colour));
            im_size_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_size));
            im_tags_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_additional_tags));


            ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.plum));
            ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));


            tv_brand_title.setTextColor(getResources().getColor(R.color.plum));
            tv_style_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_color_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_size_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_tag_title.setTextColor(getResources().getColor(R.color.dark_smoke));

            DisplayItems();
            DisplaySelectedItem();

        } else if(v.getId() == R.id.ll_color) {

            type = "color";

            im_brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_brands));
            im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style));
            im_color_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_colour_selected));
            im_size_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_size));
            im_tags_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_additional_tags));


            ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.plum));
            ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));



            tv_brand_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_style_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_color_title.setTextColor(getResources().getColor(R.color.plum));
            tv_size_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_tag_title.setTextColor(getResources().getColor(R.color.dark_smoke));

            DisplayItems();
            DisplaySelectedItem();

        } else if(v.getId() == R.id.ll_size) {

            type = "size";

            im_brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_brands));
            im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style));
            im_color_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_colour));
            im_size_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_size_selected));
            im_tags_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_additional_tags));


            ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.plum));
            ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));



            tv_brand_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_style_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_color_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_size_title.setTextColor(getResources().getColor(R.color.plum));
            tv_tag_title.setTextColor(getResources().getColor(R.color.dark_smoke));

            DisplayItems();
            DisplaySelectedItem();

        } else  if(v.getId() == R.id.ll_tags){

            type = "tags";

            im_brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_brands));
            im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style));
            im_color_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_colour));
            im_size_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_size));
            im_tags_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_additional_tags_selected));


            ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.plum));



            tv_brand_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_style_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_color_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_size_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_tag_title.setTextColor(getResources().getColor(R.color.plum));

            DisplayItems();
            DisplaySelectedItem();

        } else if(v.getId() == R.id.ll_clear){

            selected_item_display_layout.removeAllViews();

            im_brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_brands));
            im_style_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_style));
            im_color_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_colour));
            im_size_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_size));
            im_tags_icon.setImageDrawable(getResources().getDrawable(R.drawable.filter_additional_tags));

            tv_brand_badges.setText("");
            tv_style_badges.setText("");
            tv_color_badges.setText("");
            tv_size_badges.setText("");
            tv_tag_badges.setText("");

            tv_brand_badges.setVisibility(View.GONE);
            tv_style_badges.setVisibility(View.GONE);
            tv_color_badges.setVisibility(View.GONE);
            tv_size_badges.setVisibility(View.GONE);
            tv_tag_badges.setVisibility(View.GONE);

            brand_array_list = db.getAllbrnads();
            style_array_list = db.getAllStyleItems();
            color_array_list = db.getAllcolors();
            size_array_list = db.GetSizeBasedOnCategory(FilterActivity.selected_category_id);
            tag_array_list = db.getAlltags();

            DisplayItems();

            ((GradientDrawable) tv_brand_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_style_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_color_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_size_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));
            ((GradientDrawable) tv_tag_badges.getBackground()).setColor(getResources().getColor(R.color.dark_smoke));


            tv_brand_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_style_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_color_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_size_title.setTextColor(getResources().getColor(R.color.dark_smoke));
            tv_tag_title.setTextColor(getResources().getColor(R.color.dark_smoke));

            FilterActivity.selected_style_list.clear();
            FilterActivity.selected_brand_list.clear();
            FilterActivity.selected_tags_list.clear();
            FilterActivity.selected_color_list.clear();
            FilterActivity.selected_size_list.clear();

        }

    }
}

