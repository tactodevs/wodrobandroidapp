package com.wodrob.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.wodrob.app.DbHelper;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.adapter.FetchCategoryAdapter;
import com.wodrob.app.model.ItemCategoryModel;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 5/1/16.
 */
public class OutfitCategoryFilter extends Fragment {

    GridView item_grid;
    FetchCategoryAdapter fetchCategoryAdapter;
    ArrayList<ItemCategoryModel> ar_category_model;
    DbHelper db;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.filter_category_layout, null);

        init(view);
        item_grid.setAdapter(fetchCategoryAdapter);
        item_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FilterActivity.selected_category_id = ar_category_model.get(position).getCategory_id().toString();

                getActivity().getSupportFragmentManager().beginTransaction().remove(new OutfitCategoryFilter()).commit();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.container, new OtherFilters(), "OtherFilters");
                FilterActivity.tv_filter_title.setText("Filter");
                fragmentTransaction.commit();

            }
        });

        return view;
    }

    private void init(View view) {

        db = new DbHelper(getActivity().getApplicationContext());

        item_grid = (GridView) view.findViewById(R.id.gr_category);
        ar_category_model = db.getAllCategoryItems();
        fetchCategoryAdapter = new FetchCategoryAdapter(getActivity(), ar_category_model);

    }

}
