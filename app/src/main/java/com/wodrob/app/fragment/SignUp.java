package com.wodrob.app.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.wodrob.app.CropActivity;
import com.wodrob.app.MainActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.onboarding.LoginActivity;
import com.wodrob.app.onboarding.OnboardingActivity;
import com.wodrob.app.views.DatePickerFragment;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;

/**
 * Created by rameesfazal on 16/2/16.
 */
public class SignUp extends Fragment implements View.OnClickListener {

    private static final int RESULT_LOAD_IMAGE = 2;
    private static final int PICK_FROM_CAMERA = 1;
    ImageView im_profile_pic;
    EditText ed_email, ed_first_name, ed_last_name, ed_password, ed_confirm_password, ed_date_of_birth;
    RadioButton gn_male;
    RadioButton gn_female;
    Button btn_get_started,btn_login;
    TextInputLayout email_layout, firstname_layout, last_name_layout, passwoed_layout, confirmpassword_layout, dob_layout;
    ProgressDialog pDialog;
    String email, first_name, last_name, password, gender, date_of_birth,avatar = "";
    ImageView im_select_date;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_sign_up, container, false);

        init(view);
        return view;
    }

    private void ShowDatePicker() {

        DatePickerFragment newFragment = new DatePickerFragment();

        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        newFragment.setArguments(args);
        newFragment.setCallBack(ondate);

        newFragment.show(getActivity().getFragmentManager(), "datePicker");

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            ed_date_of_birth.setText(dayOfMonth + "-" + (monthOfYear + 01) + "-" + year);


        }
    };

    private void UploadSignupdata() {


        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new UploadData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/register");

            } else {

                new UploadData().execute(WodrobConstant.BASE_URL + "account/register");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    private void ChooseItem() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_item_dialog_layout);
        // dialog.setTitle("Add Image");

        // set the custom dialog components - text, image and button
        LinearLayout ll_camera = (LinearLayout) dialog.findViewById(R.id.ll_camera);
        LinearLayout ll_camera_roll = (LinearLayout) dialog.findViewById(R.id.ll_camera_roll);
        LinearLayout ll_catalogue = (LinearLayout) dialog.findViewById(R.id.ll_catalogue);
        ll_catalogue.setVisibility(View.GONE);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        ll_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                String picturePath = Environment.getExternalStorageDirectory() + File.separator + "wodrobavatar.jpg";
                File file = new File(picturePath);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(file));
                startActivityForResult(intent, PICK_FROM_CAMERA);

            }
        });
        ll_camera_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, RESULT_LOAD_IMAGE);

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FROM_CAMERA) {
            if (resultCode != getActivity().RESULT_CANCELED) {
                String picturePath = Environment.getExternalStorageDirectory() + File.separator + "wodrobavatar.jpg";
                File file = new File(picturePath);
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 400, 400);

                im_profile_pic.setImageBitmap(bmp);

            }

        } else if (requestCode == RESULT_LOAD_IMAGE) {
            if (data != null) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                File file = new File(picturePath);
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                Bitmap bmp = Wodrob.decodeSampledBitmapFromFile(file.getAbsolutePath(), 400, 400);

                im_profile_pic.setImageBitmap(bmp);

            }
        }

    }


    /* sending otp request*/

    private class UploadData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data += "&" + URLEncoder.encode("client_id", "UTF-8") + "=" + WodrobConstant.client_id + "&"
                        + URLEncoder.encode("client_secret", "UTF-8") + "=" + WodrobConstant.client_secret + "&"
                        + URLEncoder.encode("first_name", "UTF-8") + "=" + first_name + "&"
                        + URLEncoder.encode("last_name", "UTF-8") + "=" + last_name + "&"
                        + URLEncoder.encode("email", "UTF-8") + "=" + email + "&"
                        + URLEncoder.encode("gender", "UTF-8") + "=" + gender + "&"
                        + URLEncoder.encode("dob", "UTF-8") + "=" + "18-03-2010" + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + password + "&"
                        + URLEncoder.encode("uuid", "UTF-8") + "=" + Wodrob.getPreference(getActivity().getApplicationContext(), "uuid") + "&"
                        + URLEncoder.encode("avatar", "UTF-8") + "=" + avatar;

                  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if (jsonObject.has("error")) {

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        } else if (jsonObject.has("status")) {

                            String status = jsonObject.getString("status");
                            if (status.equals("true")) {


                                //next screen

                                JSONObject token_object = jsonObject.getJSONObject("token");

                                String access_token = token_object.getString("access_token");

                                String token_type = token_object.getString("token_type");

                                String expires_in = token_object.getString("expires_in");

                                String refresh_token = token_object.getString("refresh_token");


                                Wodrob.setPreference(getActivity().getApplicationContext(), access_token, "access_token");
                                Wodrob.setPreference(getActivity().getApplicationContext(), token_type, "token_type");
                                Wodrob.setPreference(getActivity().getApplicationContext(), expires_in, "expires_in");
                                Wodrob.setPreference(getActivity().getApplicationContext(), refresh_token, "refresh_token");

                                if(Wodrob.getbooleanpreference(getActivity().getApplicationContext(),"isFirstTime")) {

                                    Wodrob.setbooleanpreference(getActivity().getApplicationContext(), true, "isFirstTime");
                                    Intent intent = new Intent(getActivity(),OnboardingActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                                }else{

                                    Intent intent = new Intent(getActivity(),MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();

                                }


                            } else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            }

                        }

                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(getActivity().getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();

                }

            }

        }
    }
    /*ends*/

    private void init(View view) {

        email_layout = (TextInputLayout) view.findViewById(R.id.email_layout);
        firstname_layout = (TextInputLayout) view.findViewById(R.id.firstname_layout);
        last_name_layout = (TextInputLayout) view.findViewById(R.id.lastname_layout);
        passwoed_layout = (TextInputLayout) view.findViewById(R.id.password_layout);
        confirmpassword_layout = (TextInputLayout) view.findViewById(R.id.confirmpassword_layout);
        dob_layout = (TextInputLayout) view.findViewById(R.id.dob_layout);
        gn_male = (RadioButton) view.findViewById(R.id.rd_male);
        gn_female = (RadioButton) view.findViewById(R.id.rd_female);

        pDialog = new ProgressDialog(getActivity());

        ed_email = (EditText) view.findViewById(R.id.ed_email);
        ed_first_name = (EditText) view.findViewById(R.id.ed_first_name);
        ed_last_name = (EditText) view.findViewById(R.id.ed_last_name);
        ed_password = (EditText) view.findViewById(R.id.ed_password);
        ed_confirm_password = (EditText) view.findViewById(R.id.ed_confirm_password);
        ed_date_of_birth = (EditText) view.findViewById(R.id.ed_dob);
        ed_date_of_birth.setOnClickListener(this);

        im_profile_pic = (ImageView) view.findViewById(R.id.im_profile_pic);
        im_profile_pic.setOnClickListener(this);
        btn_get_started = (Button) view.findViewById(R.id.btn_get_started);
        btn_login = (Button) view.findViewById(R.id.btn_login);
        btn_get_started.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_get_started) {

            ClearError();
            if (Validation()) {

                UploadSignupdata();
              //  Toast.makeText(getActivity().getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
            }

        } else if (v.getId() == R.id.ed_dob) {

            ShowDatePicker();

        } else if (v.getId() == R.id.im_profile_pic) {

            ChooseItem();

        }else if(v.getId() == R.id.btn_login){

            ((LoginActivity) getActivity()).SetFragment("Login");

        }

    }

    public boolean Validation() {

        if (ed_email.getText().toString().equals("")) {

            email_layout.setError("Please fill the email");
            email_layout.setHint("");
            return false;

        } else if (!Wodrob.isValidEmail(ed_email.getText().toString())) {

            email_layout.setError("Please enter a valid email");
            email_layout.setHint("");
            return false;

        } else if (ed_first_name.getText().toString().equals("")) {

            firstname_layout.setError("Please fill the first name");
            firstname_layout.setHint("");
            return false;

        } else if (ed_last_name.getText().toString().equals("")) {

            last_name_layout.setError("Please fill the last name");
            last_name_layout.setHint("");
            return false;

        } else if (ed_password.getText().toString().equals("")) {

            passwoed_layout.setError("Please Enter a password");
            passwoed_layout.setHint("");
            return false;

        } else if (ed_confirm_password.getText().toString().equals("")) {

            confirmpassword_layout.setError("Please fill the pasword");
            confirmpassword_layout.setHint("");
            return false;

        } else if (ed_date_of_birth.getText().toString().equals("")) {

            dob_layout.setError("Please fill the date of birth");
            dob_layout.setHint("");
            return false;

        } else if (!ed_password.getText().toString().equals(ed_confirm_password.getText().toString())) {

            confirmpassword_layout.setError("Password doesn't match");
            confirmpassword_layout.setHint("");

            return false;

        } else {

            email = ed_email.getText().toString();
            first_name = ed_first_name.getText().toString();
            last_name = ed_last_name.getText().toString();
            password = ed_password.getText().toString();
            date_of_birth = ed_date_of_birth.getText().toString();

            if(im_profile_pic != null){

               Bitmap bitmap = ((BitmapDrawable) im_profile_pic.getDrawable()).getBitmap();
                avatar = Wodrob.image_to_String(bitmap);

            }


            if (gn_female.isChecked()) {

                gender = "female";

            } else {

                gender = "male";

            }


            return true;
        }
    }

    public void ClearError() {

        email_layout.setError("");
        firstname_layout.setError("");
        last_name_layout.setError("");
        passwoed_layout.setError("");
        confirmpassword_layout.setError("");
        dob_layout.setError("");
    }
}
