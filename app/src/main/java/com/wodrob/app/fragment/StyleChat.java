package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.R;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.StyleItemAdapter;
import com.wodrob.app.model.StyleItemModel;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Ramees on 2/14/2016.
 */
public class StyleChat extends Fragment{

    LinearLayoutManager layoutManager;
    StyleItemAdapter singleadapter;
    ProgressDialog pDialog;
    DbHelper db;
    StyleItemModel styleItemModel;
    RecyclerView recyclerView;
    ArrayList<StyleItemModel> styleItemModelArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_list_layout, container, false);
        init(rootView);
        //GetData();

        //comment after data integration
        for(int i = 0;i<2;i++){
            styleItemModel = new StyleItemModel("1","style1","occassion1","","","","");
            styleItemModelArrayList.add(styleItemModel);
            //  cataloglist.add(catalogItem);po
        }
        singleadapter.notifyDataSetChanged();

        return rootView;
    }

    private void GetData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "request");

            } else {

                new FetchRequest().execute(WodrobConstant.BASE_URL + "request");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void init(View rootView) {

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);

        styleItemModelArrayList = new ArrayList<>();

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        pDialog = new ProgressDialog(getActivity());

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it
                /*Intent intent = new Intent(getActivity(), LookBookView.class);
                intent.putExtra("outfit_id",styleItemModelArrayList.get(position).getId());
                startActivity(intent);*/
            }
        });


        setlayout();

    }

    private void setlayout() {

        singleadapter = new StyleItemAdapter(styleItemModelArrayList, R.layout.style_list_item);
        recyclerView.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
        recyclerView.setAdapter(singleadapter);
        recyclerView.setLayoutManager(layoutManager);

    }

      /*get item data*/


    private class FetchRequest extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";


        @Override
        protected void onPreExecute() {

            //pDialog.setMessage("Loading...");
            //pDialog.show();
            pDialog.setMessage("Loading...");
            pDialog.show();
            try {

                data += "&" + URLEncoder.encode("patron_id", "UTF-8") + "=" + WodrobConstant.patron_id;

                Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                //    pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray data_array = dataobject.getJSONArray("data");
                         /*   next_page_url = dataobject.getString("next_page_url");
                            count = 1;
                            if(next_page_url.equals(null)){

                                next_page_url = "";
                                count = 0;

                            }*/

                            for(int i = 0;i<data_array.length();i++){

                                JSONObject data_obj = data_array.getJSONObject(i);
                                JSONArray occassio_array = data_obj.getJSONArray("occasion");
                                String title = data_obj.getString("title");
                                String id= data_obj.getString("_id");
                                String occassion = occassio_array.getString(0).split("_")[0];
                                String no_of_entries = data_obj.getString("no_of_entries");
                                String no_of_participants = data_obj.getString("no_of_participants");

                                styleItemModel = new StyleItemModel(id,title,occassion,no_of_entries,no_of_participants,"","");
                                styleItemModelArrayList.add(styleItemModel);
                                //  cataloglist.add(catalogItem);po

                            }


                        }else{

                            Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }
                        singleadapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();


            // vp_choose.setAdapter(addItemPagerAdapter);
        }
    }

}
