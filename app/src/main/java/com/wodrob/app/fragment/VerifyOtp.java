package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wodrob.app.MainActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.onboarding.LoginActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by rameesfazal on 16/2/16.
 */
public class VerifyOtp extends Fragment implements View.OnClickListener{

    EditText ed_otp;
    Button btn_verify,btn_resend;
    ProgressDialog pDialog;
    TextView tv_phone;
    String otp;
    private BroadcastReceiver Receiver;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_verify_otp,container,false);

        init(view);
        tv_phone.setText(WodrobConstant.otp_phone);

        Receiver = new BroadcastReceiver(){
            public static final String SMS_BUNDLE = "pdus";

            @Override
            public void onReceive(Context context, Intent intent) {

                // TODO Auto-generated method stub

                Bundle intentExtras = intent.getExtras();
                if (intentExtras != null) {
                    Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);

                    for (int i = 0; i < sms.length; ++i) {
                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                        String smsBody = smsMessage.getMessageBody().toString();//it will give the whole sms body which is sent from the server


                    }

                }

            }};


        return view;
    }

    private void init(View view) {

        ed_otp = (EditText) view.findViewById(R.id.ed_otp);
        tv_phone = (TextView) view.findViewById(R.id.tv_phone);
        btn_verify = (Button) view.findViewById(R.id.btn_verify);
        btn_resend = (Button) view.findViewById(R.id.btn_resend);
        btn_resend.setOnClickListener(this);
        btn_verify.setOnClickListener(this);
        pDialog = new ProgressDialog(getActivity());


    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_verify){

            ResendOtp();


        }else if(v.getId() == R.id.btn_resend){

            otp = ed_otp.getText().toString();
            Verify();
        }

    }

    private void ResendOtp() {


        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new Resend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/getotp");

            }
            else {

                new Resend().execute(WodrobConstant.BASE_URL+"account/getotp");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void Verify() {


        try {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new VeriftyOtp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "account/verify_otp");

            }
            else {

                new VeriftyOtp().execute(WodrobConstant.BASE_URL+"account/verify_otp");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /*verify otp*/


    private class VeriftyOtp extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data +="&"+ URLEncoder.encode("client_id", "UTF-8")+"="+ WodrobConstant.client_id+"&"
                        + URLEncoder.encode("client_secret", "UTF-8")+"="+WodrobConstant.client_secret+"&"
                        + URLEncoder.encode("otp", "UTF-8")+"="+otp;

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if(jsonObject.has("error")){

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                           // String message = errorobject.getString("message");
                            //Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else if(jsonObject.has("status")){

                            String status = jsonObject.getString("status");
                            if(status.equals("true")){

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                                getActivity().finish();


                            }else {

                              //  String message = jsonObject.getString("message");
                                ((LoginActivity) getActivity()).SetFragment("Login");
                             //   Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                            }

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getActivity().getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }
    /*ends*/
     /* sending otp request*/

    private class Resend extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Loading...");
            pDialog.show();


            try {

                data +="&"+ URLEncoder.encode("client_id", "UTF-8")+"="+ WodrobConstant.client_id+"&"
                        + URLEncoder.encode("client_secret", "UTF-8")+"="+WodrobConstant.client_secret+"&"
                        + URLEncoder.encode("phone", "UTF-8")+"="+WodrobConstant.otp_phone;

                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if(jsonObject.has("error")){

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }else if(jsonObject.has("status")){

                            String status = jsonObject.getString("status");
                            if(status.equals("true")){


                                Toast.makeText(getActivity().getApplicationContext(),"Otp has send successfully",Toast.LENGTH_LONG).show();

                            }else {

                                String message = jsonObject.getString("message");
                                Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                            }

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getActivity().getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

        }
    }
    /*ends*/

}
