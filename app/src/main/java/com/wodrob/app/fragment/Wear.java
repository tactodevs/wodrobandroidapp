package com.wodrob.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.adapter.WearViewPagerAdapter;


/**
 * Created by rameesfazal on 12/1/16.
 */
public class Wear extends Fragment {

    ViewPager vp_wear;
    TabLayout tabLayout;
    WearViewPagerAdapter wearViewPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_wear_layout,container,false);

        init(view);

        wearViewPagerAdapter = new WearViewPagerAdapter(getFragmentManager(),tabLayout.getTabCount());
        vp_wear.setAdapter(wearViewPagerAdapter);

        vp_wear.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp_wear.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    private void init(View v) {

        tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("ITEM"));
        tabLayout.addTab(tabLayout.newTab().setText("OUTFIT"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        vp_wear = (ViewPager) v.findViewById(R.id.vp_wear);
        Wodrob.applyFont(getContext(), tabLayout, "fonts/montserratregular.ttf");
    }
}
