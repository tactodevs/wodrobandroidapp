package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.WearViewItem;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.WearItemAdapter;
import com.wodrob.app.model.WearItemModel;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Ramees on 1/17/2016.
 */
public class WearItemFragment extends Fragment {

    LinearLayoutManager layoutManager;
    WearItemAdapter singleadapter;
    ProgressDialog pDialog;
    DbHelper db;
    WearItemModel wearItemModel;
    RecyclerView recyclerView;
    ArrayList<WearItemModel> wearItemModelArrayList;
    String item_id;

    String filter_category_id = "";
    String filter_style = "";
    String filter_brands = "";
    String filter_size = "";
    String filter_color = "";
    String filter_tags = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.item_list_layout, container, false);

        db = new DbHelper(getContext());

        init(rootView);

        setHasOptionsMenu(true);

        GetFilterString();

        GetData();

        return rootView;
    }

    private void GetFilterString() {

        String[] filterstring = Wodrob.GenerateFiterString(getActivity()).split("/");

        try{

            filter_category_id = filterstring[0];
            filter_style = filterstring[1];
            filter_color = filterstring[2];
            filter_size = filterstring[3];
            filter_brands = filterstring[4];
            filter_tags = filterstring[5];

        }catch (ArrayIndexOutOfBoundsException e){

        }


    }


    private void init(View rootView) {

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);

        recyclerView.setItemViewCacheSize(0);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it
                Intent intent = new Intent(getActivity(),WearViewItem.class);
                intent.putExtra("item_id",wearItemModelArrayList.get(position).getProduct_id());
                intent.putExtra("type","view");
                intent.putExtra("is_whishlist",wearItemModelArrayList.get(position).getStatus());
                startActivity(intent);
            }
        });

        wearItemModelArrayList = new ArrayList<>();

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        pDialog = new ProgressDialog(getActivity());

        setlayout();

    }


    private void setlayout() {

            singleadapter = new WearItemAdapter(wearItemModelArrayList, R.layout.recycler_layout);
            recyclerView.addItemDecoration(new SingleSpacingDecoration(10, "parent"));
            recyclerView.setAdapter(singleadapter);
            recyclerView.setLayoutManager(layoutManager);

   }


    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Not implemented here
            case R.id.mi_filter:
                // Do Fragment menu item stuff here
                Intent intent = new Intent(getActivity(),FilterActivity.class);
                startActivityForResult(intent, 5);
                return true;
            default:
                break;
        }

        return false;
    }

    private void GetData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "item");

            } else {

                new FetchItemData().execute(WodrobConstant.BASE_URL + "item");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

        /*get item data*/

        private class FetchItemData extends AsyncTask<String, Void, Void> {

            private String Content;
            private String Error = null;
            String data = "";

            @Override
            protected void onPreExecute() {

                pDialog.setMessage("Loading...");
                pDialog.show();
                try{
                    data +="&"+ URLEncoder.encode("category_id", "UTF-8")+"="+ filter_category_id+"&"
                            + URLEncoder.encode("styles", "UTF-8")+"="+filter_style+"&"
                            + URLEncoder.encode("brand", "UTF-8")+"="+filter_brands+"&"
                            + URLEncoder.encode("sizes", "UTF-8")+"="+filter_size+"&"
                            + URLEncoder.encode("colors", "UTF-8")+"="+filter_color;

                    Log.d("data", data);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected Void doInBackground(String... urls) {

                BufferedReader reader = null;

                try {

                    URL url = new URL(urls[0]);
                    URLConnection conn = url.openConnection();
                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(data);
                    wr.flush();
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    Content = sb.toString();
                } catch (Exception ex) {
                    Error = ex.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if (Error != null) {
                    pDialog.dismiss();

                    Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
                } else {

                    if (Content != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(Content);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if(status.equalsIgnoreCase("true")){

                                JSONObject dataobject = jsonObject.getJSONObject("data");
                                JSONArray data_array = dataobject.getJSONArray("data");
                         /*   next_page_url = dataobject.getString("next_page_url");
                            count = 1;
                            if(next_page_url.equals(null)){

                                next_page_url = "";
                                count = 0;

                            }*/

                                for(int i = 0;i<data_array.length();i++){

                                    String wishlist_status;
                                    JSONObject product_obj = data_array.getJSONObject(i);
                                    JSONObject data_obj = product_obj.getJSONObject("product");
                                    JSONObject attribute_obj = product_obj.getJSONObject("attributes");

                                    String iswishlist = product_obj.getString("in_wishlist");
                                    String category = data_obj.getString("category");
                                    String brand = data_obj.getString("brand");
                                    item_id =  product_obj.getString("_id");
                                    String price = attribute_obj.getString("price");
                                    String size = attribute_obj.getString("size");
                                    String item_status = attribute_obj.getString("status");

                                    if(iswishlist.equals("0")){

                                        wishlist_status = item_status.toUpperCase();

                                    }else{

                                        wishlist_status = "WISHLIST";

                                    }
;
                                    JSONArray image_array = data_obj.getJSONArray("images");
                                    String filename = "";

                                    if(image_array.length() != 0){

                                        JSONObject filename_obj = image_array.getJSONObject(0);
                                        filename = filename_obj.getString("filename");
                                        String image_id = filename_obj.getString("_id");

                                    }

                                    wearItemModel = new WearItemModel(item_id,category,filename,brand,size,wishlist_status,price);
                                    wearItemModelArrayList.add(wearItemModel);
                                    //  cataloglist.add(catalogItem);po

                                }


                            }else{

                                Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                            }
                            singleadapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                 pDialog.dismiss();

            }
        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        wearItemModelArrayList.clear();
        singleadapter.notifyDataSetChanged();
        GetData();

    }

    }



