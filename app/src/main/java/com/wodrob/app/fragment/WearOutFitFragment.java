package com.wodrob.app.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wodrob.app.DbHelper;
import com.wodrob.app.OutfitView;
import com.wodrob.app.R;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.adapter.WearOutfitAdapter;
import com.wodrob.app.model.WearOutfitModel;
import com.wodrob.app.views.ItemClickSupport;
import com.wodrob.app.views.SingleSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Ramees on 1/17/2016.
 */
public class WearOutFitFragment extends Fragment {

    LinearLayoutManager layoutManager;
    WearOutfitAdapter singleadapter;
    ProgressDialog pDialog;
    DbHelper db;
    WearOutfitModel wearOutfitModel;
    RecyclerView recyclerView;
    ArrayList<WearOutfitModel> wearOutfitModelArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.item_list_layout, container, false);

        db = new DbHelper(getContext());

        init(rootView);
        setHasOptionsMenu(true);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it
                Intent intent = new Intent(getActivity(), OutfitView.class);
                intent.putExtra("outfit_id",wearOutfitModelArrayList.get(position).getOutfit_id());
                intent.putExtra("type","view");
                startActivity(intent);
            }
        });

        GetData();


        return rootView;
    }


    private void init(View rootView) {

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);

        wearOutfitModelArrayList = new ArrayList<>();

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        pDialog = new ProgressDialog(getActivity());

        setlayout();

    }


    private void setlayout() {

        singleadapter = new WearOutfitAdapter(wearOutfitModelArrayList, R.layout.outfit_list_item);
        recyclerView.addItemDecoration(new SingleSpacingDecoration(10,"parent"));
        recyclerView.setAdapter(singleadapter);
        recyclerView.setLayoutManager(layoutManager);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_filter : {
                // Log.i(TAG, "Save from fragment");

                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void GetData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchItemData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "outfit");

            } else {

                new FetchItemData().execute(WodrobConstant.BASE_URL + "outfit");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

        /*get item data*/


    private class FetchItemData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";


        @Override
        protected void onPreExecute() {

            //pDialog.setMessage("Loading...");
            //pDialog.show();

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                //    pDialog.dismiss();

                Toast.makeText(getActivity().getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONArray data_array = dataobject.getJSONArray("data");
                         /*   next_page_url = dataobject.getString("next_page_url");
                            count = 1;
                            if(next_page_url.equals(null)){

                                next_page_url = "";
                                count = 0;

                            }*/

                            for(int i = 0;i<data_array.length();i++){

                                JSONObject data_obj = data_array.getJSONObject(i);
                                JSONArray occassio_array = data_obj.getJSONArray("occasion");
                                String name = data_obj.getString("name");
                                String image_path = data_obj.getString("image");
                                String outfit_id = data_obj.getString("_id");
                                String occassion = occassio_array.getString(0).split("_")[0];
                                String created_by = data_obj.getString("created_by");
                                String likes = data_obj.getString("like_cnt");


                                wearOutfitModel = new WearOutfitModel(outfit_id,name,image_path,created_by,occassion,likes);
                                wearOutfitModelArrayList.add(wearOutfitModel);
                                //  cataloglist.add(catalogItem);po

                            }


                        }else{

                            Toast.makeText(getActivity().getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        }
                        singleadapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        singleadapter.notifyDataSetChanged();
                    }
                }
            }
            //  pDialog.dismiss();


            // vp_choose.setAdapter(addItemPagerAdapter);
        }
    }
}



