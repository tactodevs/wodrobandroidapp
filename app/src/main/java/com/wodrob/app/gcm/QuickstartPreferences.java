package com.wodrob.app.gcm;

/**
 * Created by Ramees on 2/3/2016.
 */

public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}
