package com.wodrob.app.gcm;

/**
 * Created by Ramees on 2/3/2016.
 */

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    SharedPreferences sharedPreferences;


    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {

            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);


            // TODO: Implement this method to send any registration to your app's servers.
            sendRegistrationToServer(token);
            // Subscribe to topic channels
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);

            Wodrob.setbooleanpreference(getApplicationContext(), false, QuickstartPreferences.SENT_TOKEN_TO_SERVER);
            Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
            LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        }
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.

        try{

            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {

                new SendToken(token).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL+"device/register");

            }
            else {

                new SendToken(token).execute(WodrobConstant.BASE_URL + "device/register");
            }


        }catch (Exception e){

            Wodrob.setbooleanpreference(getApplicationContext(),false,QuickstartPreferences.SENT_TOKEN_TO_SERVER);
            Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
            LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

        }


}


    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]
    private class SendToken extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data ="";
        String token;
        String device_type = Wodrob.isTablet(getApplicationContext());
        String device_os = android.os.Build.VERSION.RELEASE ;

        SendToken(String token){
            this.token = token;
        }
        @Override
        protected void onPreExecute() {

            try {

                data +="&"+ URLEncoder.encode("client_id", "UTF-8")+"="+ WodrobConstant.client_id+"&"
                        + URLEncoder.encode("client_secret", "UTF-8")+"="+WodrobConstant.client_secret+"&"
                        + URLEncoder.encode("uuid", "UTF-8")+"="+Wodrob.GetUuid(getApplicationContext())+"&"
                        + URLEncoder.encode("device_type", "UTF-8")+"="+device_type+"&"
                        + URLEncoder.encode("model_no", "UTF-8")+"="+Wodrob.getDeviceName()+"&"
                        + URLEncoder.encode("app_version", "UTF-8")+"="+1.0+"&"
                        + URLEncoder.encode("push_notification_key", "UTF-8")+"="+token+"&"
                        + URLEncoder.encode("device_os", "UTF-8")+"="+device_os;

                  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader=null;

            try
            {
                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            finally
            {
                try
                {
                    reader.close();
                }

                catch(Exception ex) {}
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(Error != null) {

                Wodrob.setbooleanpreference(getApplicationContext(), false, QuickstartPreferences.SENT_TOKEN_TO_SERVER);
                Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);

               // progressDialog.dismiss();
                // Toast.makeText(getActivity().getApplicationContext(), "Please Check Your Internet Connection", Toast.LENGTH_LONG).show();

            } else {

                if(Content != null){

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        if(jsonObject.has("error")){

                            Wodrob.setbooleanpreference(getApplicationContext(), false, QuickstartPreferences.SENT_TOKEN_TO_SERVER);
                            Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);

                        }else{

                            //subscribeTopics(token);

                            JSONArray device_array = jsonObject.getJSONArray("device");

                            JSONObject device_object = device_array.getJSONObject(0);
                           // JSONObject token_object = jsonObject.getJSONObject("token");

                            String patron_id = device_object.getString("patron_id");

                            String uuid = device_object.getString("uuid");

                            String device_type = device_object.getString("device_type");

                            String app_version = device_object.getString("app_version");

                            String model_number = device_object.getString("model_number");

                            String push_notification_key = device_object.getString("push_notification_key");

                            String _id = device_object.getString("_id");

                           // String access_token = token_object.getString("access_token");

                           // String token_type = token_object.getString("token_type");

                           // String expires_in = token_object.getString("expires_in");


                            Wodrob.setPreference(getApplicationContext(),patron_id,"patron_id");
                            Wodrob.setPreference(getApplicationContext(),uuid,"uuid");
                            Wodrob.setPreference(getApplicationContext(),device_type,"device_type");
                            Wodrob.setPreference(getApplicationContext(),app_version,"app_version");
                            Wodrob.setPreference(getApplicationContext(),model_number,"model_number");
                            Wodrob.setPreference(getApplicationContext(),push_notification_key,"push_notification_key");
                            Wodrob.setPreference(getApplicationContext(),_id,"_id");
                           // Wodrob.setPreference(getApplicationContext(),access_token,"access_token");
                           // Wodrob.setPreference(getApplicationContext(),token_type,"token_type");
                           // Wodrob.setPreference(getApplicationContext(),expires_in,"expires_in");

                            Wodrob.setbooleanpreference(getApplicationContext(), true, QuickstartPreferences.SENT_TOKEN_TO_SERVER);
                            Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);

                        }

                    }catch (Exception e){

                        Wodrob.setbooleanpreference(getApplicationContext(), false, QuickstartPreferences.SENT_TOKEN_TO_SERVER);
                        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);

                    }


                }else {

                    Wodrob.setbooleanpreference(getApplicationContext(), false, QuickstartPreferences.SENT_TOKEN_TO_SERVER);
                    Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);


                }

            }
        }
    }


}