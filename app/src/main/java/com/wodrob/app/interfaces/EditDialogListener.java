package com.wodrob.app.interfaces;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.widget.RelativeLayout;

/**
 * Created by rameesfazal on 29/12/15.
 */
public interface EditDialogListener {

    public void onReturnValue(Bitmap image, String action);

    public void OnDrag(Matrix matrix, RelativeLayout.LayoutParams params, Float rotation);

}
