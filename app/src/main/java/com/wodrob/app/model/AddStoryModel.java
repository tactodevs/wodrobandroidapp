package com.wodrob.app.model;

import android.widget.EditText;
import android.widget.ImageView;

/**
 * Created by Ramees on 2/14/2016.
 */
public class AddStoryModel {
    ImageView im_story;
    EditText ed_description;
    EditText ed_title;
    public AddStoryModel(ImageView im_story, EditText ed_description,EditText ed_title) {
        this.im_story = im_story;
        this.ed_description = ed_description;
        this.ed_title = ed_title;
    }

    public ImageView getIm_story() {
        return im_story;
    }

    public void setIm_story(ImageView im_story) {
        this.im_story = im_story;
    }

    public EditText getEd_description() {
        return ed_description;
    }

    public void setEd_description(EditText ed_description) {
        this.ed_description = ed_description;
    }

    public EditText getEd_title() {
        return ed_title;
    }

    public void setEd_title(EditText ed_title) {
        this.ed_title = ed_title;
    }
}
