package com.wodrob.app.model;

import android.graphics.Matrix;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by rameesfazal on 24/2/16.
 */
public class CanvasImage {
    ImageView view;
    Matrix matrix;
    RelativeLayout.LayoutParams params;
    String type;
    int left_margin,top_margin,width,height;
    float angle;
    float matrixx,matrixy;

    public CanvasImage(ImageView view, int left_margin, int top_margin, int width, int height, RelativeLayout.LayoutParams params, Matrix matrix, String type, float angle, float matrixx, float matrixy) {
        this.view = view;
        this.matrix = matrix;
        this.params = params;
        this.type = type;
        this.left_margin = left_margin;
        this.top_margin = top_margin;
        this.width = width;
        this.height = height;
        this.angle = angle;
        this.matrixx = matrixx;
        this.matrixy = matrixy;
    }

    public float getMatrixx() {
        return matrixx;
    }

    public void setMatrixx(float matrixx) {
        this.matrixx = matrixx;
    }

    public float getMatrixy() {
        return matrixy;
    }

    public void setMatrixy(float matrixy) {
        this.matrixy = matrixy;
    }

    public ImageView getView() {
        return view;
    }

    public void setView(ImageView view) {
        this.view = view;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }

    public RelativeLayout.LayoutParams getParams() {
        return params;
    }

    public void setParams(RelativeLayout.LayoutParams params) {
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLeft_margin() {
        return left_margin;
    }

    public void setLeft_margin(int left_margin) {
        this.left_margin = left_margin;
    }

    public int getTop_margin() {
        return top_margin;
    }

    public void setTop_margin(int top_margin) {
        this.top_margin = top_margin;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
