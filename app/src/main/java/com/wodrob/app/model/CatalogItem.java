package com.wodrob.app.model;

/**
 * Created by rameesfazal on 25/12/15.
 */
public class CatalogItem {

    String product_id;
    String image_path;
    String image_id;

    public CatalogItem(String product_id, String image_id, String image_path) {
        this.product_id = product_id;
        this.image_path = image_path;
        this.image_id = image_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getId() {
        return product_id;
    }

    public void setId(String product_id) {
        this.product_id = product_id;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
