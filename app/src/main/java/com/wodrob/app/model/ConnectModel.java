package com.wodrob.app.model;

/**
 * Created by rameesfazal on 21/3/16.
 */
public class ConnectModel {

    String name;
    boolean isconnected;


    public ConnectModel(String name, boolean isconnected) {
        this.name = name;
        this.isconnected = isconnected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isconnected() {
        return isconnected;
    }

    public void setIsconnected(boolean isconnected) {
        this.isconnected = isconnected;
    }
}
