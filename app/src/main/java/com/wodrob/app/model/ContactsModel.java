package com.wodrob.app.model;

/**
 * Created by rameesfazal on 17/3/16.
 */
public class ContactsModel {

    String id,firstname,lastname,email,phone;

    public ContactsModel(String email, String firstname, String lastname, String id, String phone) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.id = id;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
