package com.wodrob.app.model;

/**
 * Created by rameesfazal on 19/1/16.
 */
public class ExploreCatalogModel {

    String name;
    String image_path;
    String product_id;

    public ExploreCatalogModel(String product_id,String name, String image_path) {
        this.name = name;
        this.image_path = image_path;
        this.product_id = product_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
