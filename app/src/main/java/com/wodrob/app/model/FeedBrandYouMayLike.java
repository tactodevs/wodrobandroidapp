package com.wodrob.app.model;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedBrandYouMayLike {

    ArrayList<FeedBrandYouMayLikeItemModel> feedBrandYouMayLikeItemModelArrayList;

    public FeedBrandYouMayLike(ArrayList<FeedBrandYouMayLikeItemModel> feedBrandYouMayLikeItemModelArrayList) {

        this.feedBrandYouMayLikeItemModelArrayList = feedBrandYouMayLikeItemModelArrayList;
    }

    public ArrayList<FeedBrandYouMayLikeItemModel> getFeedBrandYouMayLikeItemModelArrayList() {
        return feedBrandYouMayLikeItemModelArrayList;
    }

    public void setFeedBrandYouMayLikeItemModelArrayList(ArrayList<FeedBrandYouMayLikeItemModel> feedBrandYouMayLikeItemModelArrayList) {
        this.feedBrandYouMayLikeItemModelArrayList = feedBrandYouMayLikeItemModelArrayList;
    }
}

