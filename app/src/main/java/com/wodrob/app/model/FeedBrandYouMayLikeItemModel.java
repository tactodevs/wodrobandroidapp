package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedBrandYouMayLikeItemModel {

    String avatar;
    String brand_name;
    String brand_followers;
    String brand_id;

    public FeedBrandYouMayLikeItemModel(String avatar, String brand_name, String brand_followers, String brand_id) {
        this.avatar = avatar;
        this.brand_name = brand_name;
        this.brand_followers = brand_followers;
        this.brand_id = brand_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_followers() {
        return brand_followers;
    }

    public void setBrand_followers(String brand_followers) {
        this.brand_followers = brand_followers;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }
}

