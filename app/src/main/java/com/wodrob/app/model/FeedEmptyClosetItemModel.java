package com.wodrob.app.model;

/**
 * Created by rameesfazal on 9/3/16.
 */
public class FeedEmptyClosetItemModel {

    String product_id;
    String image;

    public FeedEmptyClosetItemModel(String product_id, String image) {
        this.product_id = product_id;
        this.image = image;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
