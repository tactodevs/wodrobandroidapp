package com.wodrob.app.model;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 9/3/16.
 */
public class FeedEmptyClosetModel {

    String item_image1,item_image2,item_image3;
    String item_id1,item_id2,item_id3;

    public FeedEmptyClosetModel(String item_image1, String item_image2, String item_image3, String item_id1, String item_id2, String item_id3) {
        this.item_image1 = item_image1;
        this.item_image2 = item_image2;
        this.item_image3 = item_image3;
        this.item_id1 = item_id1;
        this.item_id2 = item_id2;
        this.item_id3 = item_id3;
    }

    public String getItem_image1() {
        return item_image1;
    }

    public void setItem_image1(String item_image1) {
        this.item_image1 = item_image1;
    }

    public String getItem_image2() {
        return item_image2;
    }

    public void setItem_image2(String item_image2) {
        this.item_image2 = item_image2;
    }

    public String getItem_image3() {
        return item_image3;
    }

    public void setItem_image3(String item_image3) {
        this.item_image3 = item_image3;
    }

    public String getItem_id1() {
        return item_id1;
    }

    public void setItem_id1(String item_id1) {
        this.item_id1 = item_id1;
    }

    public String getItem_id2() {
        return item_id2;
    }

    public void setItem_id2(String item_id2) {
        this.item_id2 = item_id2;
    }

    public String getItem_id3() {
        return item_id3;
    }

    public void setItem_id3(String item_id3) {
        this.item_id3 = item_id3;
    }
}
