package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedFriendsYouMayFolow {
    String avatar;
    String patron_name;
    String friends_count;
    String followers_count;
    String patron_id;

    public FeedFriendsYouMayFolow(String patron_id, String avatar, String patron_name, String friends_count, String followers_count) {
        this.patron_id = patron_id;
        this.avatar = avatar;
        this.patron_name = patron_name;
        this.friends_count = friends_count;
        this.followers_count = followers_count;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPatron_name() {
        return patron_name;
    }

    public void setPatron_name(String patron_name) {
        this.patron_name = patron_name;
    }

    public String getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(String friends_count) {
        this.friends_count = friends_count;
    }

    public String getPatron_id() {
        return patron_id;
    }

    public void setPatron_id(String patron_id) {
        this.patron_id = patron_id;
    }

    public String getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(String followers_count) {
        this.followers_count = followers_count;
    }
}
