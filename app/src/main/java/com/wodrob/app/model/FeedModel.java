package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedModel {

    String card_type;
    FeedStoryModel feedStoryModel;
    FeedWeatherModel feedWeatherModel;
    FeedStyleRequest feedStyleRequest;
    FeedBrandYouMayLike feedBrandYouMayLike;
    FeedFriendsYouMayFolow feedFriendsYouMayFolow;
    FeedProfileComplete feedProfileComplete;
    FeedWishListModel feedWishListModel;
    FeedEmptyClosetModel feedEmptyClosetModel;



    public FeedModel(String card_type, FeedStoryModel feedStoryModel, FeedWeatherModel feedWeatherModel, FeedStyleRequest feedStyleRequest, FeedBrandYouMayLike feedBrandYouMayLike, FeedFriendsYouMayFolow feedFriendsYouMayFolow, FeedProfileComplete feedProfileComplete, FeedWishListModel feedWishListModel,FeedEmptyClosetModel feedEmptyClosetModel) {
        this.feedStoryModel = feedStoryModel;
        this.card_type = card_type;
        this.feedWeatherModel = feedWeatherModel;
        this.feedStyleRequest = feedStyleRequest;
        this.feedBrandYouMayLike = feedBrandYouMayLike;
        this.feedFriendsYouMayFolow = feedFriendsYouMayFolow;
        this.feedProfileComplete = feedProfileComplete;
        this.feedWishListModel = feedWishListModel;
        this.feedEmptyClosetModel = feedEmptyClosetModel;
    }

    public FeedEmptyClosetModel getFeedEmptyClosetModel() {
        return feedEmptyClosetModel;
    }

    public void setFeedEmptyClosetModel(FeedEmptyClosetModel feedEmptyClosetModel) {
        this.feedEmptyClosetModel = feedEmptyClosetModel;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public FeedStoryModel getFeedStoryModel() {
        return feedStoryModel;
    }

    public void setFeedStoryModel(FeedStoryModel feedStoryModel) {
        this.feedStoryModel = feedStoryModel;
    }

    public FeedWeatherModel getFeedWeatherModel() {
        return feedWeatherModel;
    }

    public void setFeedWeatherModel(FeedWeatherModel feedWeatherModel) {
        this.feedWeatherModel = feedWeatherModel;
    }

    public FeedStyleRequest getFeedStyleRequest() {
        return feedStyleRequest;
    }

    public void setFeedStyleRequest(FeedStyleRequest feedStyleRequest) {
        this.feedStyleRequest = feedStyleRequest;
    }

    public FeedBrandYouMayLike getFeedBrandYouMayLike() {
        return feedBrandYouMayLike;
    }

    public void setFeedBrandYouMayLike(FeedBrandYouMayLike feedBrandYouMayLike) {
        this.feedBrandYouMayLike = feedBrandYouMayLike;
    }

    public FeedFriendsYouMayFolow getFeedFriendsYouMayFolow() {
        return feedFriendsYouMayFolow;
    }

    public void setFeedFriendsYouMayFolow(FeedFriendsYouMayFolow feedFriendsYouMayFolow) {
        this.feedFriendsYouMayFolow = feedFriendsYouMayFolow;
    }

    public FeedProfileComplete getFeedProfileComplete() {
        return feedProfileComplete;
    }

    public void setFeedProfileComplete(FeedProfileComplete feedProfileComplete) {
        this.feedProfileComplete = feedProfileComplete;
    }

    public FeedWishListModel getFeedWishListModel() {
        return feedWishListModel;
    }

    public void setFeedWishListModel(FeedWishListModel feedWishListModel) {
        this.feedWishListModel = feedWishListModel;
    }
}
