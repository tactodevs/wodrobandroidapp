package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedProfileComplete {

    String percentage;
    String patron_id;

    public FeedProfileComplete(String percentage,String patron_id) {
        this.percentage = percentage;
        this.patron_id = patron_id;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getPatron_id() {
        return patron_id;
    }

    public void setPatron_id(String patron_id) {
        this.patron_id = patron_id;
    }
}
