package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedStoryModel {

    String card_type,card_title,patron_id,story_id,is_sub_story,patron_avatar_path,patron_name,story_time_stamp,story_title,story_description,like_count,comment_count,story_image_path;

    public FeedStoryModel(String card_type, String card_title, String patron_id, String story_id, String is_sub_story, String patron_avatar_path, String patron_name, String story_time_stamp, String story_title, String story_description, String like_count, String comment_count, String story_image_path) {
        this.card_type = card_type;
        this.card_title = card_title;
        this.patron_id = patron_id;
        this.story_id = story_id;
        this.is_sub_story = is_sub_story;
        this.patron_avatar_path = patron_avatar_path;
        this.patron_name = patron_name;
        this.story_time_stamp = story_time_stamp;
        this.story_title = story_title;
        this.story_description = story_description;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.story_image_path = story_image_path;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getCard_title() {
        return card_title;
    }

    public void setCard_title(String card_title) {
        this.card_title = card_title;
    }

    public String getPatron_id() {
        return patron_id;
    }

    public void setPatron_id(String patron_id) {
        this.patron_id = patron_id;
    }

    public String getStory_id() {
        return story_id;
    }

    public void setStory_id(String story_id) {
        this.story_id = story_id;
    }

    public String getIs_sub_story() {
        return is_sub_story;
    }

    public void setIs_sub_story(String is_sub_story) {
        this.is_sub_story = is_sub_story;
    }

    public String getPatron_avatar_path() {
        return patron_avatar_path;
    }

    public void setPatron_avatar_path(String patron_avatar_path) {
        this.patron_avatar_path = patron_avatar_path;
    }

    public String getPatron_name() {
        return patron_name;
    }

    public void setPatron_name(String patron_name) {
        this.patron_name = patron_name;
    }

    public String getStory_time_stamp() {
        return story_time_stamp;
    }

    public void setStory_time_stamp(String story_time_stamp) {
        this.story_time_stamp = story_time_stamp;
    }

    public String getStory_title() {
        return story_title;
    }

    public void setStory_title(String story_title) {
        this.story_title = story_title;
    }

    public String getStory_description() {
        return story_description;
    }

    public void setStory_description(String story_description) {
        this.story_description = story_description;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getStory_image_path() {
        return story_image_path;
    }

    public void setStory_image_path(String story_image_path) {
        this.story_image_path = story_image_path;
    }
}
