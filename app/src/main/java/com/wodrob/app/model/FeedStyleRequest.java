package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedStyleRequest {

    String style_request_time_stamp,style_request_title,style_request_description,participants_count,Style_request_outfit_image;
    String style_request_patron_avatar,style_request_patron_name;

    public FeedStyleRequest(String style_request_patron_avatar, String style_request_patron_name, String style_request_time_stamp, String style_request_title, String style_request_description, String participants_count,String Style_request_outfit_image) {
        this.style_request_patron_avatar = style_request_patron_avatar;
        this.style_request_patron_name = style_request_patron_name;
        this.style_request_time_stamp = style_request_time_stamp;
        this.style_request_title = style_request_title;
        this.style_request_description = style_request_description;
        this.participants_count = participants_count;
        this.Style_request_outfit_image = Style_request_outfit_image;
    }

    public String getStyle_request_patron_avatar() {
        return style_request_patron_avatar;
    }

    public void setStyle_request_patron_avatar(String style_request_patron_avatar) {
        this.style_request_patron_avatar = style_request_patron_avatar;
    }

    public String getStyle_request_patron_name() {
        return style_request_patron_name;
    }

    public void setStyle_request_patron_name(String style_request_patron_name) {
        this.style_request_patron_name = style_request_patron_name;
    }

    public String getStyle_request_time_stamp() {
        return style_request_time_stamp;
    }

    public void setStyle_request_time_stamp(String style_request_time_stamp) {
        this.style_request_time_stamp = style_request_time_stamp;
    }


    public String getStyle_request_title() {
        return style_request_title;
    }

    public void setStyle_request_title(String style_request_title) {
        this.style_request_title = style_request_title;
    }

    public String getStyle_request_description() {
        return style_request_description;
    }

    public void setStyle_request_description(String style_request_description) {
        this.style_request_description = style_request_description;
    }

    public String getParticipants_count() {
        return participants_count;
    }

    public void setParticipants_count(String participants_count) {
        this.participants_count = participants_count;
    }

    public String getStyle_request_outfit_image() {
        return Style_request_outfit_image;
    }

    public void setStyle_request_outfit_image(String style_request_outfit_image) {
        Style_request_outfit_image = style_request_outfit_image;
    }
}
