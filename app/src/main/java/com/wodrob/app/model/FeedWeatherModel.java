package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedWeatherModel {

    String card_type;
    String card_title;
    String outfit_one_image;
    String outfit_two_image;
    String weather_image_path;
    String weather_value;
    String weather_type;
    String outfit_one_id;
    String outfit_two_id;

    public FeedWeatherModel(String outfit_one_id, String outfit_two_id,String outfit_one_image, String outfit_two_image, String weather_value, String weather_type) {
        this.card_type = card_type;
        this.card_title = card_title;
        this.outfit_one_image = outfit_one_image;
        this.outfit_two_image = outfit_two_image;
        this.weather_image_path = weather_image_path;
        this.weather_value = weather_value;
        this.weather_type = weather_type;
        this.outfit_one_id = outfit_one_id;
        this.outfit_two_id = outfit_two_id;
    }

    public String getOutfit_one_id() {
        return outfit_one_id;
    }

    public void setOutfit_one_id(String outfit_one_id) {
        this.outfit_one_id = outfit_one_id;
    }

    public String getOutfit_two_id() {
        return outfit_two_id;
    }

    public void setOutfit_two_id(String outfit_two_id) {
        this.outfit_two_id = outfit_two_id;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getCard_title() {
        return card_title;
    }

    public void setCard_title(String card_title) {
        this.card_title = card_title;
    }

    public String getOutfit_one_image() {
        return outfit_one_image;
    }

    public void setOutfit_one_image(String outfit_one_image) {
        this.outfit_one_image = outfit_one_image;
    }

    public String getOutfit_two_image() {
        return outfit_two_image;
    }

    public void setOutfit_two_image(String outfit_two_image) {
        this.outfit_two_image = outfit_two_image;
    }

    public String getWeather_image_path() {
        return weather_image_path;
    }

    public void setWeather_image_path(String weather_image_path) {
        this.weather_image_path = weather_image_path;
    }

    public String getWeather_value() {
        return weather_value;
    }

    public void setWeather_value(String weather_value) {
        this.weather_value = weather_value;
    }

    public String getWeather_type() {
        return weather_type;
    }

    public void setWeather_type(String weather_type) {
        this.weather_type = weather_type;
    }
}
