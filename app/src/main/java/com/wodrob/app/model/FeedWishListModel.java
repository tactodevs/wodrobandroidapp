package com.wodrob.app.model;

import java.util.ArrayList;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class FeedWishListModel {

    ArrayList<WishListItemModel> arrayList;

    public FeedWishListModel(ArrayList<WishListItemModel> arrayList) {
        this.arrayList = arrayList;
    }

    public ArrayList<WishListItemModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<WishListItemModel> arrayList) {
        this.arrayList = arrayList;
    }
}
