package com.wodrob.app.model;

/**
 * Created by rameesfazal on 20/2/16.
 */
public class FollowItemModel {

    String name;
    String description;
    String image_path;
    String id;

    public FollowItemModel(String id,String name, String description, String image_path) {
        this.name = name;
        this.description = description;
        this.image_path = image_path;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
