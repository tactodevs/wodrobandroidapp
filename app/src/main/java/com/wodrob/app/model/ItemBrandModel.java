package com.wodrob.app.model;

/**
 * Created by rameesfazal on 22/12/15.
 */
public class ItemBrandModel {

    String brand_id,brand_name,updated_at,image_path;

    public ItemBrandModel(String brand_id, String brand_name,String updated_at, String image_path) {
        this.brand_name = brand_name;
        this.brand_id = brand_id;
        this.updated_at = updated_at;
        this.image_path = image_path;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
