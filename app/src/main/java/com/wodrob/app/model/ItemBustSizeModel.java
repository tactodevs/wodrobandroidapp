package com.wodrob.app.model;

/**
 * Created by rameesfazal on 5/3/16.
 */
public class ItemBustSizeModel {

    String bust_size_id,band,cup,display,updated_at;

    public ItemBustSizeModel(String bust_size_id, String band, String cup, String display, String updated_at) {
        this.bust_size_id = bust_size_id;
        this.band = band;
        this.cup = cup;
        this.display = display;
        this.updated_at = updated_at;
    }

    public String getBust_size_id() {
        return bust_size_id;
    }

    public void setBust_size_id(String bust_size_id) {
        this.bust_size_id = bust_size_id;
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public String getCup() {
        return cup;
    }

    public void setCup(String cup) {
        this.cup = cup;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
