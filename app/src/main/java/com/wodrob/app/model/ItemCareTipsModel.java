package com.wodrob.app.model;

/**
 * Created by rameesfazal on 17/12/15.
 */
public class ItemCareTipsModel {

    String name,updated_at,image_path,care_tip_id;

    public ItemCareTipsModel(String care_tip_id,String name, String updated_at, String image_path) {
        this.name = name;
        this.updated_at = updated_at;
        this.image_path = image_path;
        this.care_tip_id = care_tip_id;
    }

    public String getCare_tip_id() {
        return care_tip_id;
    }

    public void setCare_tip_id(String care_tip_id) {
        this.care_tip_id = care_tip_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
