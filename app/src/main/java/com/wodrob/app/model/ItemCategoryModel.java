package com.wodrob.app.model;

/**
 * Created by rameesfazal on 16/12/15.
 */
public class ItemCategoryModel {
    String category_name;
    String category_imagge_path;
    String updated_at;
    String category_id;
    String parent_id;
    String selected_path,unselected_path;

    public ItemCategoryModel(String category_id, String category_name, String selected_path,String unselected_path, String updated_at, String parent_id) {
        this.category_name = category_name;
        this.selected_path = selected_path;
        this.unselected_path = unselected_path;
        this.updated_at = updated_at;
        this.category_id = category_id;
        this.parent_id = parent_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_imagge_path() {
        return category_imagge_path;
    }

    public void setCategory_imagge_path(String category_imagge_path) {
        this.category_imagge_path = category_imagge_path;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getSelected_path() {
        return selected_path;
    }

    public void setSelected_path(String selected_path) {
        this.selected_path = selected_path;
    }

    public String getUnselected_path() {
        return unselected_path;
    }

    public void setUnselected_path(String unselected_path) {
        this.unselected_path = unselected_path;
    }
}
