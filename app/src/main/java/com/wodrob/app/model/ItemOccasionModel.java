package com.wodrob.app.model;

/**
 * Created by rameesfazal on 17/12/15.
 */
public class ItemOccasionModel {
    String occasion_name;
    String description;
    String updated_at;
    String occassion_id;

    public ItemOccasionModel(String occassion_id,String occasion_name, String description, String updated_at) {
        this.occasion_name = occasion_name;
        this.description = description;
        this.updated_at = updated_at;
        this.occassion_id = occassion_id;
    }

    public String getOccassion_id() {
        return occassion_id;
    }

    public void setOccassion_id(String occassion_id) {
        this.occassion_id = occassion_id;
    }

    public String getOccasion_name() {
        return occasion_name;
    }

    public void setOccasion_name(String occasion_name) {
        this.occasion_name = occasion_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
