package com.wodrob.app.model;

/**
 * Created by rameesfazal on 17/12/15.
 */
public class ItemSizeModel {
    String value;
    String updated_at;
    String size_id;
    String category_id;

    public ItemSizeModel(String size_id,String value, String updated_at,String category_id) {
        this.value = value;
        this.updated_at = updated_at;
        this.size_id = size_id;
        this.category_id = category_id;
    }

    public String getSize_id() {
        return size_id;
    }

    public void setSize_id(String size_id) {
        this.size_id = size_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
