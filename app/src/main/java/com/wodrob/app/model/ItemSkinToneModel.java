package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class ItemSkinToneModel {

    String id;
    String name;
    String description;
    String filename;
    String image_width;
    String image_height;
    String updated_at;

    public ItemSkinToneModel(String id, String name, String description, String filename, String image_width, String image_height, String updated_at) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.filename = filename;
        this.image_width = image_width;
        this.image_height = image_height;
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getImage_width() {
        return image_width;
    }

    public void setImage_width(String image_width) {
        this.image_width = image_width;
    }

    public String getImage_height() {
        return image_height;
    }

    public void setImage_height(String image_height) {
        this.image_height = image_height;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
