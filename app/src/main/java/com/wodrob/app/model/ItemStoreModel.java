package com.wodrob.app.model;

/**
 * Created by rameesfazal on 23/12/15.
 */
public class ItemStoreModel {

    String store_id;
    String store_name;
    String mode_ref_type;
    String mode_ref;
    String updated_at;

    public ItemStoreModel(String store_id, String store_name, String mode_ref_type, String mode_ref, String updated_at) {
        this.updated_at = updated_at;
        this.store_id = store_id;
        this.store_name = store_name;
        this.mode_ref_type = mode_ref_type;
        this.mode_ref = mode_ref;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getMode_ref_type() {
        return mode_ref_type;
    }

    public void setMode_ref_type(String mode_ref_type) {
        this.mode_ref_type = mode_ref_type;
    }

    public String getMode_ref() {
        return mode_ref;
    }

    public void setMode_ref(String mode_ref) {
        this.mode_ref = mode_ref;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
