package com.wodrob.app.model;

/**
 * Created by rameesfazal on 17/12/15.
 */
public class ItemStyleModel {
    String style_name,style_category,style_description,updated_at,style_id;

    public ItemStyleModel(String style_id, String style_name, String style_category, String style_description, String updated_at) {
        this.style_name = style_name;
        this.style_category = style_category;
        this.style_description = style_description;
        this.updated_at = updated_at;
        this.style_id = style_id;
    }

    public String getStyle_id() {
        return style_id;
    }

    public void setStyle_id(String style_id) {
        this.style_id = style_id;
    }

    public String getStyle_name() {
        return style_name;
    }

    public void setStyle_name(String style_name) {
        this.style_name = style_name;
    }

    public String getStyle_category() {
        return style_category;
    }

    public void setStyle_category(String style_category) {
        this.style_category = style_category;
    }

    public String getStyle_description() {
        return style_description;
    }

    public void setStyle_description(String style_description) {
        this.style_description = style_description;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}


