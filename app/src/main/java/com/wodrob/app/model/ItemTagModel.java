package com.wodrob.app.model;

/**
 * Created by rameesfazal on 17/12/15.
 */
public class ItemTagModel {
    String tag_name;
    String updated_at;
    String tag_id;

    public ItemTagModel(String tag_id, String tag_name, String updated_at) {
        this.tag_name = tag_name;
        this.updated_at = updated_at;
        this.tag_id = tag_id;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
