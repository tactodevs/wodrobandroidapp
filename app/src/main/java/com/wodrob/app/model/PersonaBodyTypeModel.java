package com.wodrob.app.model;

import android.widget.LinearLayout;

/**
 * Created by rameesfazal on 26/2/16.
 */
public class PersonaBodyTypeModel {

    LinearLayout layout;
    String value;

    public PersonaBodyTypeModel(LinearLayout layout, String value) {
        this.layout = layout;
        this.value = value;
    }

    public LinearLayout getLayout() {
        return layout;
    }

    public void setLayout(LinearLayout layout) {
        this.layout = layout;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
