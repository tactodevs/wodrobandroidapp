package com.wodrob.app.model;

/**
 * Created by Ramees on 2/14/2016.
 */
public class StyleItemModel {
    String title,occassion,no_of_participants,responses,id,date,created_by;

    public StyleItemModel(String id, String title, String occassion, String responses, String no_of_participants,String date,String created_by) {
        this.title = title;
        this.occassion = occassion;
        this.responses = responses;
        this.no_of_participants = no_of_participants;
        this.id = id;
        this.date = date;
        this.created_by = created_by;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNo_of_participants() {
        return no_of_participants;
    }

    public void setNo_of_participants(String no_of_participants) {
        this.no_of_participants = no_of_participants;
    }

    public String getOccassion() {
        return occassion;
    }

    public void setOccassion(String occassion) {
        this.occassion = occassion;
    }

    public String getResponses() {
        return responses;
    }

    public void setResponses(String responses) {
        this.responses = responses;
    }
}

