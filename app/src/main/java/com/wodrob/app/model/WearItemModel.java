package com.wodrob.app.model;

/**
 * Created by rameesfazal on 18/1/16.
 */
public class WearItemModel {

    String category_name;
    String imagepath;
    String brand;
    String size;
    String status;
    String price;
    String product_id;

    public WearItemModel(String product_id,String category_name, String imagepath, String brand, String size, String status, String price) {
        this.category_name = category_name;
        this.imagepath = imagepath;
        this.brand = brand;
        this.size = size;
        this.status = status;
        this.price = price;
        this.product_id = product_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
