package com.wodrob.app.model;

/**
 * Created by rameesfazal on 19/1/16.
 */
public class WearOutfitModel {

    String name,image_path,outfit_id,created_by, occassion_type,likes;

    public WearOutfitModel(String outfit_id,String name, String image_path,String created_by,String occassion_type,String likes) {
        this.name = name;
        this.image_path = image_path;
        this.outfit_id = outfit_id;
        this.created_by = created_by;
        this.occassion_type = occassion_type;
        this.likes  =likes;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getOccassion_type() {
        return occassion_type;
    }

    public void setOccassion_type(String occassion_type) {
        this.occassion_type = occassion_type;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getOutfit_id() {
        return outfit_id;
    }

    public void setOutfit_id(String outfit_id) {
        this.outfit_id = outfit_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
