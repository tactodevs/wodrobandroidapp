package com.wodrob.app.model;

/**
 * Created by rameesfazal on 27/1/16.
 */
public class WearSimilarItemsModel {

    String product_id,product_name,image_path,price,createdby;

    public WearSimilarItemsModel(String product_id, String product_name, String image_path, String price, String createdby) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.image_path = image_path;
        this.price = price;
        this.createdby = createdby;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
