package com.wodrob.app.model;

/**
 * Created by rameesfazal on 29/2/16.
 */
public class WishListItemModel {

    String image_path,title,price,item_id;

    public WishListItemModel(String image_path, String title, String price, String item_id) {
        this.image_path = image_path;
        this.title = title;
        this.price = price;
        this.item_id = item_id;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
