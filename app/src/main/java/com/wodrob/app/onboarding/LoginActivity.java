package com.wodrob.app.onboarding;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.wodrob.app.MainActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.fragment.GetOtp;
import com.wodrob.app.fragment.Login;
import com.wodrob.app.fragment.LoginHome;
import com.wodrob.app.fragment.SignUp;

public class LoginActivity extends AppCompatActivity {

    Toolbar toolbar;
    FragmentTransaction transaction;
    LoginHome loginhome;
    Login login;
    SignUp signUp;
    GetOtp getOtp;
    public static TextView tv_title;
    String fragment_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        init();

        fragment_name = "LoginHome";
        SetFragment(fragment_name);

    }

    public void SetFragment(String fragment_type) {

        transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

        if (fragment_type.equalsIgnoreCase("Loginhome")) {

            fragment_name = "LoginHome";

            getSupportActionBar().hide();
            transaction.replace(R.id.fragment_container, this.loginhome);

        } else if (fragment_type.equalsIgnoreCase("Signup")) {

            fragment_name = "Signup";

            tv_title.setText("Sign Up");

            transaction.replace(R.id.fragment_container, this.signUp);
            getSupportActionBar().show();

        } else if (fragment_type.equalsIgnoreCase("Login")) {

            fragment_name = "Login";
            getSupportActionBar().show();
            tv_title.setText("Log In");
            transaction.replace(R.id.fragment_container, this.login);

        } else if (fragment_type.equalsIgnoreCase("GetOtp")) {

            fragment_name = "GetOtp";
            getSupportActionBar().show();
            tv_title.setText("Get Otp");

            transaction.replace(R.id.fragment_container, this.getOtp);

        }
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);
        getSupportActionBar().hide();

        tv_title = (TextView) findViewById(R.id.tv_activity_title);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_title.setTypeface(face);

        loginhome = new LoginHome();
        login = new Login();
        signUp = new SignUp();
        getOtp = new GetOtp("Login");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fragment_name.equalsIgnoreCase("Login")) {

            SetFragment("LoginHome");

        } else if (fragment_name.equalsIgnoreCase("GetOtp")) {

            SetFragment("Login");

        } else if (fragment_name.equalsIgnoreCase("SignUp")) {

            SetFragment("LoginHome");

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            Bundle databundle = data.getExtras();

            if (databundle.getString("status").equalsIgnoreCase("true")) {

                String access_token = databundle.getString("access_token");
                String token_type = databundle.getString("token_type");
                String expires_in = databundle.getString("expires_in");
                String refresh_token = databundle.getString("refresh_token");


                Wodrob.setPreference(getApplicationContext(), access_token, "access_token");
                Wodrob.setPreference(getApplicationContext(), token_type, "token_type");
                Wodrob.setPreference(getApplicationContext(), expires_in, "expires_in");
                Wodrob.setPreference(getApplicationContext(), refresh_token, "refresh_token");
                Wodrob.setbooleanpreference(getApplicationContext(), true, "has_access_token");

                if (!Wodrob.getbooleanpreference(getApplicationContext(), "IsVisited_Onboarding")) {

                    Wodrob.setbooleanpreference(getApplicationContext(), true, "IsVisited_Onboarding");
                    Intent intent = new Intent(LoginActivity.this, OnboardingActivity.class);
                    startActivity(intent);
                    finish();

                } else {

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                }

            }


        }
    }
}
