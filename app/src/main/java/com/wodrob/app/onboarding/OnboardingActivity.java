package com.wodrob.app.onboarding;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.wodrob.app.AddCanvasItem;
import com.wodrob.app.FilterActivity;
import com.wodrob.app.R;
import com.wodrob.app.adapter.OnBoardingPageAdapter;
import com.wodrob.app.adapter.WearViewPagerAdapter;
import com.wodrob.app.views.FontHelper;

public class OnboardingActivity extends AppCompatActivity {

    Toolbar toolbar;
    ViewPager vp_onboarding;
    OnBoardingPageAdapter onBoardingPageAdapter;
    TextView tv_activity_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_oboarding);
        FontHelper.applyFont(this, findViewById(R.id.root_layout), "fonts/opensansregular.ttf");

        init();

        onBoardingPageAdapter = new OnBoardingPageAdapter(getSupportFragmentManager(),5);
        vp_onboarding.setOffscreenPageLimit(5);
        vp_onboarding.setAdapter(onBoardingPageAdapter);

        vp_onboarding.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if(position == 0){

                    tv_activity_title.setText("Persona");

                }else if(position == 1){

                    tv_activity_title.setText("Privacy");

                }else if(position == 2){

                    tv_activity_title.setText("Otp");

                }else if(position == 3){

                    tv_activity_title.setText("Connect ");

                }else if(position == 4){

                    tv_activity_title.setText("Invite");
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_96dp);

        tv_activity_title = (TextView) findViewById(R.id.tv_activity_title);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/montserratregular.ttf");
        tv_activity_title.setTypeface(face);
        vp_onboarding = (ViewPager) findViewById(R.id.vp_onboarding);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_onboarding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_next:
                // Do Activity menu item stuff here
                return false;

            default:
                break;
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode ==  1)
        {

            if(data.getExtras().getString("status").equalsIgnoreCase("true")){

                vp_onboarding.setCurrentItem(3);

            }


        }
    }

    public void update_Page(String page_name){

        if(page_name.equalsIgnoreCase("persona")){

            vp_onboarding.setCurrentItem(0);

        }else if(page_name.equalsIgnoreCase("privacy")){

            vp_onboarding.setCurrentItem(1);

        }else if(page_name.equalsIgnoreCase("otp")){

            vp_onboarding.setCurrentItem(2);

        }else if(page_name.equalsIgnoreCase("connect")){

            vp_onboarding.setCurrentItem(3);

        }else if(page_name.equalsIgnoreCase("invite")){

            vp_onboarding.setCurrentItem(4);

        }
    }

}
