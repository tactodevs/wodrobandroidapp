package com.wodrob.app.onboarding;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import com.wodrob.app.DbHelper;
import com.wodrob.app.MainActivity;
import com.wodrob.app.R;
import com.wodrob.app.Wodrob;
import com.wodrob.app.WodrobApiConstant;
import com.wodrob.app.WodrobConstant;
import com.wodrob.app.gcm.QuickstartPreferences;
import com.wodrob.app.gcm.RegistrationIntentService;
import com.wodrob.app.model.ContactsModel;
import com.wodrob.app.model.ItemBodyTypeModel;
import com.wodrob.app.model.ItemBrandModel;
import com.wodrob.app.model.ItemBustSizeModel;
import com.wodrob.app.model.ItemCareTipsModel;
import com.wodrob.app.model.ItemCategoryModel;
import com.wodrob.app.model.ItemColorModel;
import com.wodrob.app.model.ItemOccasionModel;
import com.wodrob.app.model.ItemSizeModel;
import com.wodrob.app.model.ItemSkinToneModel;
import com.wodrob.app.model.ItemStoreModel;
import com.wodrob.app.model.ItemStyleModel;
import com.wodrob.app.model.ItemTagModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class WadrobSplashScreen extends AppCompatActivity {

    ProgressDialog pDialog;
    ItemCategoryModel categoryModel;
    ItemCareTipsModel careTipsModel;
    ItemColorModel colorModel;
    ItemOccasionModel occasionModel;
    ItemSizeModel sizeModel;
    ItemStyleModel styleModel;
    ItemTagModel tagModel;
    ItemBrandModel brandModel;
    ItemStoreModel storeModel;
    ItemBodyTypeModel bodyTypeModel;
    ItemBustSizeModel bustSizeModel;
    ItemSkinToneModel skinToneModel;
    ContactsModel contactsModel;
    ArrayList<ContactsModel> contactsModelArrayList;
    String contactjson="";
    DbHelper db;
    String Date;
    private static final String FIRST_LAUNCH = "first_launch";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wadrob_splash_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#FFFFFF"));
        init();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        //Assume true if the key does not yet exist
        if (prefs.getBoolean(FIRST_LAUNCH, true)) {
            //Display window
            Date = "";
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(FIRST_LAUNCH, false);
            edit.commit();
        } else {

            Date = Wodrob.getPreference(getApplicationContext(), "datetime");
        }

        if(Wodrob.getbooleanpreference(getApplicationContext(), QuickstartPreferences.SENT_TOKEN_TO_SERVER)){

            GetMasterData();

        }else{

            GcmRegistration();

        }
    }

    private void GcmRegistration() {

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                pDialog.dismiss();
              //  if(Wodrob.getbooleanpreference(getApplicationContext(),QuickstartPreferences.SENT_TOKEN_TO_SERVER)){

                    GetMasterData();

                //}

            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            pDialog.setMessage("please wait...");
            pDialog.show();

           Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }

    private void init() {

        pDialog = new ProgressDialog(this);
        db = new DbHelper(getApplicationContext());
        contactsModelArrayList = new ArrayList<>();
    }


    private void GetMasterData() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                new FetchMasterData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobApiConstant.MASTER_SYNC);

            } else {

                new FetchMasterData().execute(WodrobApiConstant.MASTER_SYNC);

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private class FetchMasterData extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";
        String current_timestamp = "";


        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Fetching Data....");
            pDialog.show();

            try {

                data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + Date;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {

            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization", "Bearer xV15QCT45Ikk6DMVDObiCTFAuzAzzyEiwGjnSG7b");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();
            } catch (Exception ex) {
                Error = ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (Error != null) {
                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "" + Error, Toast.LENGTH_LONG).show();
            } else {

                if (Content != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(Content);
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            JSONObject category_array_object = dataobject.getJSONObject("category");
                            JSONObject caretip_array_object = dataobject.getJSONObject("careTip");
                            JSONObject color_array_object = dataobject.getJSONObject("color");
                            JSONObject occassion_array_object = dataobject.getJSONObject("occasion");
                            JSONObject size_array_object = dataobject.getJSONObject("size");
                            JSONObject style_array_object = dataobject.getJSONObject("style");
                            JSONObject tags_array_object = dataobject.getJSONObject("tag");
                            JSONObject brand_array_object = dataobject.getJSONObject("brand");
                            JSONObject store_array_object = dataobject.getJSONObject("store");
                            JSONObject body_type_array_object = dataobject.getJSONObject("bodyType");
                            JSONObject bust_size_array_object = dataobject.getJSONObject("bust");
                            JSONObject skin_tone_array_object = dataobject.getJSONObject("skinTone");



//                            get array for active data

                            JSONArray category_array = category_array_object.getJSONArray("active");
                            JSONArray caretip_array = caretip_array_object.getJSONArray("active");
                            JSONArray color_array = color_array_object.getJSONArray("active");
                            JSONArray occassion_array = occassion_array_object.getJSONArray("active");
                            JSONArray size_array = size_array_object.getJSONArray("active");
                            JSONArray style_array = style_array_object.getJSONArray("active");
                            JSONArray tags_array = tags_array_object.getJSONArray("active");
                            JSONArray brand_array = brand_array_object.getJSONArray("active");
                            JSONArray store_array = store_array_object.getJSONArray("active");
                            JSONArray body_type_array = body_type_array_object.getJSONArray("active");
                            JSONArray bust_array = bust_size_array_object.getJSONArray("active");
                            JSONArray skin_tone_array = skin_tone_array_object.getJSONArray("active");




//                           get array of trash data

                            JSONArray category_trash_array = category_array_object.getJSONArray("trash");
                            JSONArray caretip_trash_array = caretip_array_object.getJSONArray("trash");
                            JSONArray color_trash_array = color_array_object.getJSONArray("trash");
                            JSONArray occassion_trash_array = occassion_array_object.getJSONArray("trash");
                            JSONArray size_trash_array = size_array_object.getJSONArray("trash");
                            JSONArray style_trash_array = style_array_object.getJSONArray("trash");
                            JSONArray tags_trash_array = tags_array_object.getJSONArray("trash");
                            JSONArray brand_trash_array = brand_array_object.getJSONArray("trash");
                            JSONArray store_trash_array = store_array_object.getJSONArray("trash");
                            JSONArray body_type_trash_array = body_type_array_object.getJSONArray("trash");
                            JSONArray bust_trash_array = bust_size_array_object.getJSONArray("trash");
                            JSONArray skin_tone_trash_array = skin_tone_array_object.getJSONArray("trash");


                            /*category insert*/

                            for (int i = 0; i < category_array.length(); i++) {
                                JSONObject category_object = category_array.getJSONObject(i);
                                categoryModel = new ItemCategoryModel(category_object.getString("_id"), category_object.getString("name"), category_object.getString("selected_image"), category_object.getString("unselected_image"), category_object.getString("updated_at"), category_object.getString("parent_id"));

                                db.insert_category(categoryModel);

                                // }
                                //Toast.makeText(getApplicationContext(),""+id,Toast.LENGTH_LONG).show();
                            }

                            /*category delete*/

                            for(int i = 0; i<category_trash_array.length();i++){

                                JSONObject category_trash_object = category_trash_array.getJSONObject(i);

                                db.DeleteCategory(category_trash_object.getString("_id"));

                            }


                            /*care tip insert*/

                            for (int i = 0; i < caretip_array.length(); i++) {
                                JSONObject care_tip_object = caretip_array.getJSONObject(i);
                                //JSONObject image_object  = care_tip_object.getJSONObject("image");
                                careTipsModel = new ItemCareTipsModel(care_tip_object.getString("_id"), care_tip_object.getString("name"), care_tip_object.getString("updated_at"), care_tip_object.getString("image"));

                                db.insert_care_tip(careTipsModel);

                            }

                            /*care tip delete*/

                            for(int i = 0; i<caretip_trash_array.length();i++){

                                JSONObject care_tip_trash_object = caretip_trash_array.getJSONObject(i);

                                db.DeleteCareTip(care_tip_trash_object.getString("_id"));

                            }

                            /*color array insert*/

                            for (int i = 0; i < color_array.length(); i++) {
                                JSONObject color_object = color_array.getJSONObject(i);
                                if (color_object.has("hexcode") && color_object.has("name")) {

                                    if (!color_object.getString("hexcode").toString().equals("")) {

                                        colorModel = new ItemColorModel(color_object.getString("_id"), color_object.getString("name"), color_object.getString("hexcode"), color_object.getString("updated_at"));

                                        db.insert_color(colorModel);

                                    }

                                }
                            }


                            /*color array delete*/


                            for(int i = 0; i<color_trash_array.length();i++){

                                JSONObject color_trash_object = color_trash_array.getJSONObject(i);

                                db.DeleteColor(color_trash_object.getString("_id"));

                            }

                            /*occassion array insert*/

                            for (int i = 0; i < occassion_array.length(); i++) {
                                JSONObject occassion_object = occassion_array.getJSONObject(i);
                                occasionModel = new ItemOccasionModel(occassion_object.getString("_id"), occassion_object.getString("name"), occassion_object.getString("description"), occassion_object.getString("updated_at"));

                                db.insert_occassion(occasionModel);

                            }

                            /*occassion delete array*/

                            for(int i = 0; i<occassion_trash_array.length();i++){

                                JSONObject occassion_trash_object = occassion_trash_array.getJSONObject(i);

                                db.DeleteOccassion(occassion_trash_object.getString("_id"));

                            }

                            /*insert size*/

                            for (int i = 0; i < size_array.length(); i++) {
                                JSONObject size_object = size_array.getJSONObject(i);
                                sizeModel = new ItemSizeModel(size_object.getString("_id"), size_object.getString("india"), size_object.getString("updated_at"), size_object.getString("category"));

                                db.insert_size(sizeModel);


                            }

                            /*delete size*/
                            for(int i = 0; i<size_trash_array.length();i++){

                                JSONObject size_trash_object = size_trash_array.getJSONObject(i);

                                db.DeleteSize(size_trash_object.getString("_id"));

                            }


//                            insert style

                            for (int i = 0; i < style_array.length(); i++) {
                                JSONObject style_object = style_array.getJSONObject(i);
                              //  JSONArray stylecategory_array = style_object.getJSONArray("categories");
                                styleModel = new ItemStyleModel(style_object.getString("_id"), style_object.getString("name"), "", style_object.getString("description"), style_object.getString("updated_at"));

                                db.insert_style(styleModel);

                            }

//                            dlete style

                            for(int i = 0; i<style_trash_array.length();i++){

                                JSONObject style_trash_object = style_trash_array.getJSONObject(i);

                                db.DeleteStyle(style_trash_object.getString("_id"));

                            }


                            /*insert tags */

                            for (int i = 0; i < tags_array.length(); i++) {
                                JSONObject tag_object = tags_array.getJSONObject(i);
                                tagModel = new ItemTagModel(tag_object.getString("_id"), tag_object.getString("name"), tag_object.getString("updated_at"));

                                db.insert_tags(tagModel);

                            }

//                            delete tags

                            for(int i = 0; i<tags_trash_array.length();i++){

                                JSONObject tags_trash_object = tags_trash_array.getJSONObject(i);

                                db.DeleteTags(tags_trash_object.getString("_id"));

                            }

//                            insert brands

                            for (int i = 0; i < brand_array.length(); i++) {
                                JSONObject brand_object = brand_array.getJSONObject(i);

                                JSONObject brand_image_object = brand_object.getJSONObject("logo");

                                String filename = brand_image_object.getString("filename");

                                    brandModel = new ItemBrandModel(brand_object.getString("_id"), brand_object.getString("name"), brand_object.getString("updated_at"), brand_image_object.getString("filename"));

                                    db.insert_brands(brandModel);

                            }

                            /*delete brands*/
                            for(int i = 0; i<brand_trash_array.length();i++){

                                JSONObject brand_trash_object = brand_trash_array.getJSONObject(i);

                                db.DeleteBrand(brand_trash_object.getString("_id"));

                            }


                            /*store insert*/

                            // store data changed...need to add more tables in sqlite

                           for (int i = 0; i < store_array.length(); i++) {

                                JSONObject store_object = store_array.getJSONObject(i);
                                storeModel = new ItemStoreModel(store_object.getString("_id"), store_object.getString("name"), "", "", store_object.getString("updated_at"));

                                db.insert_store(storeModel);

                            }

                            /*store delete*/

                            for(int i = 0; i<store_trash_array.length();i++){

                                JSONObject store_trash_object = store_trash_array.getJSONObject(i);

                                db.DeleteStore(store_trash_object.getString("_id"));

                            }


                              /*body_type insert*/
                            for (int i = 0; i < body_type_array.length(); i++) {

                                JSONObject body_type_object = body_type_array.getJSONObject(i);
                                JSONObject body_type_image_object = body_type_object.getJSONObject("image");
                                bodyTypeModel = new ItemBodyTypeModel(body_type_object.getString("_id"), body_type_object.getString("gender"), body_type_object.getString("name"), body_type_object.getString("description"),body_type_image_object.getString("filename"),body_type_image_object.getString("width"), body_type_image_object.getString("height"),body_type_object.getString("updated_at"));
                                db.insert_body_type(bodyTypeModel);

                            }

                            /*body_type delete*/

                            for(int i = 0; i<body_type_trash_array.length();i++){

                                JSONObject body_type_trash_object = body_type_trash_array.getJSONObject(i);

                                db.DeleteBodyType(body_type_trash_object.getString("_id"));

                            }

                            /*insert bust*/

                            for(int i = 0; i< bust_array.length(); i ++){

                                JSONObject bust_object = bust_array.getJSONObject(i);
                                bustSizeModel = new ItemBustSizeModel(bust_object.getString("_id"),bust_object.getString("band"),bust_object.getString("cup"),bust_object.getString("display"),bust_object.getString("updated_at"));
                                db.insert_bust_size(bustSizeModel);

                            }
                            /*delete bust*/

                            for(int i = 0; i<bust_trash_array.length();i++){

                                JSONObject bust_trash_object = bust_trash_array.getJSONObject(i);

                                db.DeleteBust(bust_trash_object.getString("_id"));

                            }


                             /*insert skin tone*/

                            for(int i = 0; i< skin_tone_array.length(); i ++){

                                JSONObject skin_tone_object = skin_tone_array.getJSONObject(i);
                                JSONObject skin_tone_image_object  = skin_tone_object.getJSONObject("image");

                                skinToneModel = new ItemSkinToneModel(skin_tone_object.getString("_id"),skin_tone_object.getString("name"),skin_tone_object.getString("description"),skin_tone_image_object.getString("filename"),skin_tone_image_object.getString("width"),skin_tone_image_object.getString("height"),skin_tone_object.getString("updated_at"));
                                db.insert_skin_tone(skinToneModel);

                            }
                            /*delete skin tone*/

                            for(int i = 0; i<skin_tone_trash_array.length();i++){

                                JSONObject skin_tone_trash_object = skin_tone_trash_array.getJSONObject(i);

                                db.DeleteSkinTone(skin_tone_trash_object.getString("_id"));

                            }

                            Wodrob.setPreference(getApplicationContext(), Wodrob.getCurrentTimeStamp(), "datetime");
                            String date = Wodrob.getPreference(getApplicationContext(), "datetime");
                            Log.e("date", date);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            pDialog.dismiss();

           /* if(!Wodrob.getbooleanpreference(getApplicationContext(),"has_access_token")){

                //login
                Intent intent = new Intent(WadrobSplashScreen.this,LoginActivity.class);
                startActivity(intent);
                finish();

            }else{

                Intent intent = new Intent(WadrobSplashScreen.this,MainActivity.class);
                startActivity(intent);
                finish();

            }*/
            GetContactInformation();



            // vp_choose.setAdapter(addItemPagerAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),"This device is not supported.",Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    public void GetContactInformation(){

        StringBuffer sb = new StringBuffer();
        sb.append("......Contact Details.....");
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        String phone = "";
        String emailContact = null;
        String emailType = null;
        String image_uri = "";
        Bitmap bitmap = null;
        String name = "",email = "",id = "";
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                id = cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts._ID));
                name = cur
                        .getString(cur
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                image_uri = cur
                        .getString(cur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                if (Integer
                        .parseInt(cur.getString(cur
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                   Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = ?", new String[] { id }, null);
                    while (pCur.moveToNext()) {

                        phone = pCur
                                .getString(pCur
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    }
                    pCur.close();

                    Cursor emailCur = cr.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID
                                    + " = ?", new String[] { id }, null);

                    while (emailCur.moveToNext()) {

                        email = emailCur
                                .getString(emailCur
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                       /* emailType = emailCur
                                .getString(emailCur
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                        sb.append("\nEmail:" + emailContact + "Email type:" + emailType);
                        System.out.println("Email " + emailContact
                                + " Email Type : " + emailType);*/

                    }

                    emailCur.close();
                }

                String names[] = name.split(" ");
                String firstname = "";
                String lastname = "";


                if(names.length>1){

                    firstname = names[0];
                    lastname = names[1];

                }else {

                    firstname = name;

                }

                if(name.contains("@")){

                    email = name;
                    name = "";
                    firstname = "";
                    lastname = "";
                }

                contactsModel = new ContactsModel(email,firstname,lastname,id,phone);
                contactsModelArrayList.add(contactsModel);
                name = "";
                email = "";
                phone = "";
                firstname = "";
                lastname = "";
            }

            CreateJson();

        }

    }

    private void CreateJson() {

        try {

            JSONArray dataarray = new JSONArray();

            JSONObject dataobjects;
            JSONArray emailarray;
            JSONArray phonearray;


            JSONArray recipients = new JSONArray();
            for (int i = 0; i< contactsModelArrayList.size(); i ++) {

                dataobjects = new JSONObject();
                dataobjects.put("first_name",contactsModelArrayList.get(i).getFirstname());
                dataobjects.put("last_name",contactsModelArrayList.get(i).getLastname());

                emailarray = new JSONArray();
                phonearray = new JSONArray();

                if(!contactsModelArrayList.get(i).getEmail().equals("")){

                    emailarray.put(contactsModelArrayList.get(i).getEmail());


                }
                dataobjects.put("email_addresses",emailarray);

                if(!contactsModelArrayList.get(i).getPhone().equals("")){

                    phonearray.put(contactsModelArrayList.get(i).getPhone());

                }
                dataobjects.put("phone_numbers",phonearray);

                dataarray.put(dataobjects);
            }


            contactjson = dataarray.toString();

            SyncContacts();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*method to sync contacts*/

    private void SyncContacts() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {


                new Sync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WodrobConstant.BASE_URL + "connection/add/phone-book");

            } else {

                new Sync().execute(WodrobConstant.BASE_URL + "connection/add/phone-book");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*background class for syncing contacts*/



    private class Sync extends AsyncTask<String, Void, Void> {

        private String Content;
        private String Error = null;
        String data = "";

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Syncing contacts...");
            pDialog.show();


            try {



                    data +="&"+ URLEncoder.encode("contacts", "UTF-8")+"="+contactjson;



                //  Log.d("data", data);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(String... urls) {


            BufferedReader reader = null;

            try {

                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
               // conn.setRequestProperty("Authorization", Wodrob.GetAccessToken(WadrobSplashScreen.this));
                conn.setRequestProperty("Authorization", "Bearer xV15QCT45Ikk6DMVDObiCTFAuzAzzyEiwGjnSG7b");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                Content = sb.toString();

            } catch (Exception ex) {

                Error = ex.getMessage();

            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //pDialog.dismiss();
            if (Error != null) {

                pDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Something is wrong ! ", Toast.LENGTH_LONG).show();

            } else {

                if (Content != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(Content);
                        // String status = jsonObject.getString("status");
                        // String message = jsonObject.getString("message");
                        if(jsonObject.has("error")){

                            JSONObject errorobject = jsonObject.getJSONObject("error");
                            String message = errorobject.getString("message");

                        }else if(jsonObject.has("status")){

                            String status = jsonObject.getString("status");
                            if(status.equals("true")){

                                Toast.makeText(getApplicationContext(),"contacts Sync successfull",Toast.LENGTH_LONG).show();

                            }else {

                                Toast.makeText(getApplicationContext(),"contacts Sync unsuccessfull",Toast.LENGTH_LONG).show();


                            }

                        }

                    }catch (Exception e){

                    }
                }else {

                    Toast.makeText(getApplicationContext(),"Try Again",Toast.LENGTH_LONG).show();

                }

            }

            pDialog.dismiss();
            if(!Wodrob.getbooleanpreference(getApplicationContext(),"has_access_token")){

                //login
                Intent intent = new Intent(WadrobSplashScreen.this,LoginActivity.class);
                startActivity(intent);
                finish();

            }else{

                Intent intent = new Intent(WadrobSplashScreen.this,MainActivity.class);
                startActivity(intent);
                finish();

            }
        }
    }


}
