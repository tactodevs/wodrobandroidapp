package com.wodrob.app.views;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ramees on 1/17/2016.
 */

public class SingleSpacingDecoration extends RecyclerView.ItemDecoration {
    private int space;
    String source;

    public SingleSpacingDecoration(int space,String source) {
        this.space = space;
        this.source = source;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if(source.equalsIgnoreCase("child")){
            //outRect.left = space;
            // outRect.right = space;
            outRect.bottom = space;
            // outRect.top = space;
        }else if(source.equalsIgnoreCase("parent")){

            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
             outRect.top = space;
        }


        // Add top margin only for the first item to avoid double space between items
        /*if(parent.getChildPosition(view) == 0)
            outRect.top = space;*/
    }
}